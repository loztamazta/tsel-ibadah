<?php

namespace Illuminate\Auth\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a notification instance.
     *
     * @param  string  $token
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed  $notifiable
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Kamu menerima surat ini karena kami menerima permintaan pasang ulang sandi dari akun kamu.')
            // ->line('You are receiving this email because we received a password reset request for your account.')
            ->action('Pasang ulang kata sandi', url(route('password.reset', $this->token, false)))
            // ->line('If you did not request a password reset, no further action is required.');
            ->line('Jika kamu tidak mengirimkan permintaan pasang ulang kata sandi, tidak ada aksi yang perlu dilakukan.');
    }
}
