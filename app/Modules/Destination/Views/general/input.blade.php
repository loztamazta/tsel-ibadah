<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
				<i class="material-icons">close</i>
			</span>
    <div class="card-content">

        <h4 class="card-title titleformgeneral">Destinasi baru</h4>
        <div class="card-content-overflow">
            <form id="FormGeneral">
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail">
                        <img class="img" id="IM" src="{{ asset('images/image_placeholder.jpg') }}" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                    <div>
					<span class="btn btn-rose btn-round btn-file">
					<span class="fileinput-new">Pilih Gambar</span>
					<span class="fileinput-exists">Ganti</span>
					<input type="file" name="Image" id="Uploads" data-old="" />
					</span>
                        <a href="#RmImage" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
                    </div>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Nama Destinasi<small>(wajib diisi)</small></label>
                    <input type="text" class="form-control" name="name" value="" required>
                    <input type="hidden" name="id">
                    <input type="hidden" value="SaveGeneral" name="save_type">
                    <input type="hidden" value="0" name="IsFlag">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Keyword Pencarian Destinasi</label>
                    <input type="text" class="form-control" name="loop_keyword" value="">
                </div>
                {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">Lokasi di Peta</label>--}}
                    {{--<input type="text" class="form-control" name="latitude">--}}
                    {{--</div>--}}
                    {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">Longitude</label>--}}
                    {{--<input type="text" class="form-control" name="longitude">--}}
                {{--</div>--}}
                {{--<div id="googleMap" style="width:100%;height:400px;"></div>--}}
                {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">Latitude</label>--}}
                    {{--<input type="text" id="latFld" name="latitude" value="">--}}

                {{--</div>--}}
                {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">Longitude</label>--}}
                    {{--<input type="text" id="lngFld" name="longitude" value="">--}}
                {{--</div>--}}
                <div class="form-group label-floating">
                    <label class="control-label">Tipe Destinasi <small>(wajib diisi)</small></label>
                    <select class="selectpicker" required data-style="select-with-transition" title="Pilih Tipe..." data-size="7" id="selectType" name="type_id">
                        {{--<option disabled>Pilih tipe</option>--}}
                        <option value="3">Pusat Perbelanjaan </option>
                        <option value="4">Rekreasi </option>
                        <option value="5">Kuliner</option>
                    </select>
                </div>
                {{--<div class="form-group label-floating">--}}
                {{--<label class="control-label">Lokasi Destinasi <small>(wajib diisi)</small></label>--}}
                {{--<select class="selectpicker" required data-style="select-with-transition" title="Pilih kota..." data-size="7" id="selectRegion" name="region_id">--}}
                {{--<option disabled>Pilih kota</option>--}}
                {{--<option value="1">Mekkah </option>--}}
                {{--<option value="2">Madinnah</option>--}}
                {{--<option value="3">Jeddah</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                <div class="form-group label-floating">
                    <label class="control-label">Alamat Destinasi</label>
                    <textarea class="form-control address" placeholder="" rows="4" name="address"></textarea>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Penjelasan Destinasi</label>
                    <textarea class="form-control remark" placeholder="" rows="4" name="remark" id="editor"></textarea>
                </div>
            </form>

        </div>

    </div>

    <div class="card-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-fill btn-rose" id="saveGeneralData">Simpan</button>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>

