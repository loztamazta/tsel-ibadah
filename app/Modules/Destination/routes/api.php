<?php

Route::group(array('module' => 'Destination', 'middleware' => ['api'], 'namespace' => 'App\Modules\Destination\Controllers'), function() {

    Route::resource('Destination', 'DestinationController');
    
});	
