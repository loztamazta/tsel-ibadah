<?php

Route::group(array('module' => 'Destination', 'middleware' => ['web'], 'namespace' => 'App\Modules\Destination\Controllers'), function() {

//    Route::resource('Destination', 'DestinationController');


    Route::get('/landmarks/index',					['uses' => 'DestinationController@landmarksIndex', 'as'=>'index-landmarks']);
    Route::get('/landmarks/getAll',					['uses' => 'DestinationController@getAllLandmarks', 'as'=>'all-landmarks']);
    Route::get('/destinations/landmarksByPlaceType',['uses' => 'DestinationController@getLandmarksByPlaceType', 'as'=>'filter-landmarks-placeType']);
    Route::get('/landmarks/search',					['uses' => 'DestinationController@SearchLandmark', 'as'=>'search-landmarks']);
    Route::get('/landmarks/input',			        ['uses' => 'DestinationController@landmarksInput', 'as'=>'landmarks-input']);
    Route::get('/landmarks/edit',                   ['uses' => 'DestinationController@editLandmarks', 'as'=>'landmarks-edit']);
    Route::get('/landmarks/view',			        ['uses' => 'DestinationController@landmarksView', 'as'=>'landmarks-view']);

    Route::post('/landmarks/post',     	            ['uses' => 'DestinationController@postLandmarks', 'as'=>'post-landmarks']);
    Route::post('/landmarks/delete',                ['uses' => 'DestinationController@deleteLandmarks', 'as'=>'delete-landmarks']);


    Route::get('/general/index',					['uses' => 'DestinationController@generalIndex', 'as'=>'index-general']);
    Route::get('/general/getAll',					['uses' => 'DestinationController@getAllGeneral', 'as'=>'all-general']);
    Route::get('/destinations/generalByPlaceTypes', ['uses' => 'DestinationController@getGeneralByType', 'as'=>'filter-general-placeType']);
    Route::get('/general/search',					['uses' => 'DestinationController@SearchGeneral', 'as'=>'search-general']);
    Route::get('/general/input',			        ['uses' => 'DestinationController@generalInput', 'as'=>'general-input']);
    Route::get('/general/edit',                     ['uses' => 'DestinationController@editGeneral', 'as'=>'general-edit']);
    Route::get('/general/view',			            ['uses' => 'DestinationController@generalView', 'as'=>'general-view']);

    Route::post('/general/post',     	            ['uses' => 'DestinationController@postGeneral', 'as'=>'post-general']);
    Route::post('/general/delete',                  ['uses' => 'DestinationController@deleteGeneral', 'as'=>'delete-general']);
});	
