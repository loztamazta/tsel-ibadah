<?php namespace App\Modules\Destination\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Input;
use DB;
use Image;
use Storage;

class DestinationController extends Controller {


	// Landmarks

	public function getLandmarksByPlaceType()
	{
		$idtype = $_GET['typeid'];
		$idreg = $_GET['idreg'];
		$a=[];

		$q="
			SELECT 
				*, A.id AS id_tempat, A.name AS nama_tempat, B.name AS type_tempat 
			FROM 
				db_ibadah.place as A
				JOIN db_ibadah.place_type AS B 
					on B.id = A.type_id
			WHERE 
				A.loop_keyword IS NULL
				AND A.type_id != 1
				AND A.type_id != 6
				AND A.IsFlag = 0
		";

		if ($idtype!='*') {
			$q=$q.' AND A.type_id = ?';
			$a[]=$idtype;
		}
		if ($idreg!='*') {
			$q=$q.' AND A.region_id = ?';
			$a[]=$idreg;
		}

		$placeTypes = DB::select($q, $a);

		return $placeTypes;
	}

	public function landmarksIndex()
	{
		$placeTypes = DB::select('SELECT *
								  FROM db_ibadah.place_type
								  WHERE id != 1
								  AND id != 2
								  AND id != 6');

		$regions = DB::select('SELECT *
							   FROM db_ibadah.place_region');

		return view("Destination::landmarks.index", compact('placeTypes','regions'));
	}

	public function getAllLandmarks()
	{
		$Landmarks = DB::select('SELECT *, A.id AS id_tempat, A.name AS nama_tempat, B.name AS type_tempat 
								  FROM db_ibadah.place as A
								  JOIN db_ibadah.place_type AS B on B.id = A.type_id
								  WHERE A.loop_keyword IS NULL
								  AND A.type_id != 1
								  AND A.type_id != 6
								  AND A.IsFlag = 0;');

		return $Landmarks;
	}

	public function SearchLandmark()
	{
		$keywordSearch = $_GET['TextId'];

		$LandoMarkos= DB::select('SELECT *, A.id AS id_tempat, A.name AS nama_tempat, B.name AS type_tempat 
									 FROM db_ibadah.place AS A
									 JOIN db_ibadah.place_type AS B ON A.type_id = B.id
									 JOIN db_ibadah.place_region AS C ON A.region_id = C.id
									 WHERE A.loop_keyword IS NULL
									 AND A.IsFlag = 0
									 AND A.type_id != 1
									 AND A.type_id != 6 
									 AND A.name LIKE ?',["%$keywordSearch%"]);

//		$Hotels = DB::select('SELECT * FROM db_ibadah.place
//							  WHERE type_id = 1 AND IsFlag = 0');

		return $LandoMarkos;
	}

	function landmarksInput()
	{
		return view("Destination::landmarks.input");
	}

	function editLandmarks()
	{
		$idLandmarks= $_GET['idLandmarks'];
		$EditLandmarks = DB::select('SELECT *, A.id AS id_tempat, A.name AS nama_tempat, B.name AS type_tempat, C.name AS daerah_tempat
								 FROM db_ibadah.place AS A
								 JOIN db_ibadah.place_type AS B ON A.type_id = B.id
								 JOIN db_ibadah.place_region AS C ON A.region_id = C.id
								 WHERE A.IsFlag = 0
								 AND A.type_id != 1
								 AND A.type_id != 6
								 AND A.id = ?',[$idLandmarks]);

		return $EditLandmarks;
	}

	function landmarksView()
	{
		return view('Destination::landmarks.view');

	}

	function postLandmarks()
	{
		//$kbihid=(Input::get('kbihid') ? Input::get('kbihid') : Auth::user()->t);

		if (Input::get('a') == 'SaveLandmarks') {
			DB::table('db_ibadah.place')->insert([
				'name' => Input::get('name'),
				'img_name' => Input::get('img'),
				'type_id' => Input::get('place_type'),
				'latitude' => Input::get('latitude'),
				'longitude' => Input::get('longitude'),
				'region_id' => Input::get('region_id'),
				'address' => Input::get('address'),
				'remark' => Input::get('remark'),
				'is_loop' => 0,
				'IsFlag' => 0,
			]);
			return json_encode(Input::all());
		} else {
			DB::table('db_ibadah.place')->where('id', Input::get('id'))->update([
				'name' => Input::get('name'),
				'img_name' => Input::get('img'),
				'type_id' => Input::get('place_type'),
				'latitude' => Input::get('latitude'),
				'longitude' => Input::get('longitude'),
				'address' => Input::get('address'),
				'remark' => Input::get('remark'),

			]);
			return json_encode(Input::all());
		};
	}

	function deleteLandmarks()
	{

		DB::table('db_ibadah.place')->where('id', Input::get('id'))->update([
			'IsFlag' => 1,
		]);
		return json_encode(Input::all());

	}


	// General

	public function generalIndex()
	{
		$placeTypes = DB::select('SELECT *
								  FROM db_ibadah.place_type
								  WHERE id != 1
								  AND id != 2
								  AND id != 6');

		return view("Destination::general.index", compact("placeTypes"));
	}

	public function getAllGeneral()
	{
		$Generals = DB::select('SELECT *, A.id AS id_tempat, A.name AS nama_tempat, B.name AS type_tempat 
									FROM db_ibadah.place AS A 
									JOIN db_ibadah.place_type AS B ON A.type_id = B.id
									WHERE loop_keyword IS NOT NULL
									AND type_id != 1
									AND type_id != 6 
									AND IsFlag = 0;');

		return $Generals;
	}

	public function getGeneralByType()
	{
		$typeid = $_GET['typeid'];

		$Generals = DB::select('SELECT *, A.id AS id_tempat, A.name AS nama_tempat, B.name AS type_tempat 
									FROM db_ibadah.place AS A 
									JOIN db_ibadah.place_type AS B ON A.type_id = B.id
									WHERE loop_keyword IS NOT NULL
									AND type_id != 1
									AND type_id != 6 
									AND IsFlag = 0
									AND A.type_id = ?',[$typeid]);

		return $Generals;
	}

	public function SearchGeneral()
	{
		$keywordSearch = $_GET['TextId'];

		$Generalo= DB::select('SELECT *, A.id AS id_tempat, A.name AS nama_tempat, B.name AS type_tempat 
									 FROM db_ibadah.place AS A
									 JOIN db_ibadah.place_type AS B ON A.type_id = B.id
									 WHERE A.loop_keyword IS NOT NULL
									 AND A.IsFlag = 0
									 AND A.type_id != 1
									 AND A.type_id != 6 
									 AND A.name LIKE ?',["%$keywordSearch%"]);

//		$Hotels = DB::select('SELECT * FROM db_ibadah.place
//							  WHERE type_id = 1 AND IsFlag = 0');

		return $Generalo;
	}

	function generalInput()
	{
		return view("Destination::general.input");
	}

	function editGeneral()
	{
		$idGeneral= $_GET['idGeneral'];
		$EditGeneral= DB::select('SELECT *, A.id AS id_tempat, A.name AS nama_tempat, B.name AS type_tempat
								 FROM db_ibadah.place AS A
								 JOIN db_ibadah.place_type AS B ON A.type_id = B.id
								 WHERE A.IsFlag = 0
								 AND A.type_id != 1
								 AND A.type_id != 6
								 AND A.id = ?',[$idGeneral]);

		return $EditGeneral;
	}

	function generalView()
	{
		return view('Destination::general.view');

	}

	function postGeneral()
	{
		//$kbihid=(Input::get('kbihid') ? Input::get('kbihid') : Auth::user()->t);

		if (Input::get('a') == 'SaveGeneral') {
			DB::table('db_ibadah.place')->insert([
				'name' => Input::get('name'),
				'img_name' => Input::get('img'),
				'type_id' => Input::get('place_type'),
				'loop_keyword' => Input::get('loop_keyword'),
				'latitude' => 0,
				'longitude' => 0,
				'region_id' => 99,
				'address' => Input::get('address'),
				'remark' => Input::get('remark'),
				'is_loop' => 1,
				'IsFlag' => 0
			]);
			return json_encode(Input::all());
		} else {
			DB::table('db_ibadah.place')->where('id', Input::get('id'))->update([
				'name' => Input::get('name'),
				'img_name' => Input::get('img'),
				'type_id' => Input::get('place_type'),
				'loop_keyword' => Input::get('loop_keyword'),
				'address' => Input::get('address'),
				'remark' => Input::get('remark'),

			]);
			return json_encode(Input::all());
		};
	}

	function deleteGeneral()
	{

		DB::table('db_ibadah.place')->where('id', Input::get('id'))->update([
			'IsFlag' => 1,
		]);
		return json_encode(Input::all());

	}
}
