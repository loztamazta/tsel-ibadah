<?php

Route::group(array('module' => 'Resell', 'middleware' => ['api'], 'namespace' => 'App\Modules\Resell\Controllers'), function() {

    Route::resource('resell', 'ResellController');
    
});	
