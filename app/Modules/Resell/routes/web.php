<?php

Route::group(array('module' => 'Resell', 'middleware' => ['web', 'auth', 'activated'], 'namespace' => 'App\Modules\Resell\Controllers'), function() {

    Route::resource('resell', 'ResellController');
 	// Route::get('/',								['uses' => 'TravelController@index', 'as'=>'travel']);
   
});	
