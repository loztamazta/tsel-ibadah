<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('images/favicon.png') }}" />
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>DTS | @yield('h-title', '')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('mdp/css/bootstrap.min.css') }}" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{ asset('mdp/css/material-dashboard.css') }}" rel="stylesheet" />
    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('mdp/css/demo.css') }}" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="{{ asset('fonts/font-awesome/font-awesome.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons" />
    {{-- custom css --}}
    <link href="{{ asset('mdp/css/custom.css') }}" rel="stylesheet" />
    @yield('css')
</head>

<body>
    <div class="loading" hidden=""></div>
    <div class="wrapper">
        @include('Dts::layouts.dts-sidebar')
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <span class="navbar-brand">@yield('title', 'Title')</span>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            @yield('nav-right')
                        </ul>
                   </div>
                </div>
            </nav>
            <div class="content">
                @yield('content')
            </div>
        </div>
        @yield('after-content')

        <div id="c-modal">            
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="{{ asset('mdp/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('mdp/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('mdp/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('mdp/js/material.min.js') }}"></script>
<script src="{{ asset('mdp/js/perfect-scrollbar.jquery.min.js') }}"></script>
<!-- Material Dashboard javascript methods -->
<script src="{{ asset('mdp/js/material-dashboard.js') }}"></script>
{{-- custom js --}}
<script src="{{ asset('js/custom.js') }}"></script>
@yield('js')
</html>