		<div class="sidebar" data-active-color="rose" data-background-color="black" data-image="{{ asset('mdp/img/sidebar-1.jpg') }}">
			<!--
				Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
				Tip 2: you can also add an image using data-image tag
				Tip 3: you can change the color of the sidebar with data-background-color="white | black"
			-->
			<div class="logo">
				<a href="{{url('/')}}" class="simple-text">
					Telkomsel Ibadah
				</a>
			</div>
			<div class="logo logo-mini">
				<a href="{{url('/')}}" class="simple-text">
					TI
				</a>
			</div>
			<div class="sidebar-wrapper">
				<div class="user">
					<div class="photo">
                        @if ((Auth::User()->profile))
                            <img src="{{ asset('storage/'.Auth::user()->profile->avatar) }}" alt="{{ Auth::user()->name }}" class="user-avatar-nav">
                        @else
							<img src="{{ asset('images/placeholder.jpg') }}" />
                        @endif
					</div>
					<div class="info">
						<a data-toggle="collapse" href="#collapseExample" class="collapsed">
							{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
							<b class="caret"></b>
						</a>
						<div class="collapse" id="collapseExample">
							<ul class="nav">
	                            <li {{ Request::is('profile/'.Auth::user()->name, 'profile/'.Auth::user()->name . '/edit') ? 'class=active' : null }}>
	                                {!! HTML::link(route('users.show',[Auth::user()->id]), trans('titles.profile')) !!}
	                            </li>
								<li>
	                                <a href="{{ route('logout') }}"
	                                    onclick="event.preventDefault();
	                                             document.getElementById('logout-form').submit();">
	                                    {!! trans('titles.logout') !!}
	                                </a>

	                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                    {{ csrf_field() }}
	                                </form>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<ul class="nav">
{{-- 
					<li>
						<a href="{{ route('travel-agen') }}">
							<i class="material-icons">dashboard</i>
							<p>Dashboard</p>
						</a>
					</li>
 --}}
 					@role('sadmin|travel|admin')
						<li>
							<a data-toggle="collapse" href="#travel">
								<i class="material-icons">flight_takeoff</i>
								<p>Travel
									<b class="caret"></b>
								</p>
							</a>
							<div class="collapse" id="travel">
								<ul class="nav">
		                			@role('sadmin')
										<li>
											<a href="{{ route('travel-agen') }}">Agen</a>
										</li>
									@endrole
									<li>
										<a href="{{ route('travel-paket') }}">Paket Perjalanan</a>
									</li>
									<li>
										<a href="{{ route('travel-pendamping') }}">Pendamping Grup</a>
									</li>
									<li>
										<a href="{{ route('travel-grup') }}">Group Keberangkatan</a>
									</li>
									@role('sadmin')
										<li>
											<a href="{{ route('travel-hotel') }}">Hotel</a>
										</li>
									@endrole
								</ul>
							</div>
						</li>
					@endrole
	                @role('sadmin')
						<li>
							<a data-toggle="collapse" href="#user">
								<i class="material-icons">account_box</i>
								<p>Pengguna
									<b class="caret"></b>
								</p>
							</a>
							<div class="collapse" id="user">
								<ul class="nav">
									<li>
										<a href="{{ route('users') }}">Manajemen Pengguna</a>
									</li>
									<li>
										<a href="{{ route('grapari') }}">Graparis</a>
									</li>
								</ul>
							</div>
						</li>
	                @endrole
                    @role('sadmin|content|admin')
	                    <li>
	                        <a data-toggle="collapse" href="#content">
	                            <i class="material-icons">perm_media</i>
	                            <p>Konten
	                                <b class="caret"></b>
	                            </p>
	                        </a>
	                        <div class="collapse" id="content">
	                            <ul class="nav">
	                                <li>
	                                    <a href="{{ route('index-article') }}">Artikel</a>
	                                </li>
	                                <li>
	                                    <a href="{{ route('index-doa') }}">Doa</a>
	                                </li>
	                                <li>
	                                    <a href="{{ route('index-glossary') }}">Istilah</a>
	                                </li>
	                                <li>
	                                    <a href="{{ route('index-guide') }}">Panduan</a>
	                                </li>
	                                <li>
	                                    <a href="{{ route('index-news') }}">Berita</a>
	                                </li>
	                                <li>
	                                    <a href="{{ route('index-tausiyah') }}">Tausiyah</a>
	                                </li>
	                                <li>
	                                    <a href="{{ route('index-tips') }}">Tips</a>
	                                </li>
	                            </ul>
	                        </div>
	                    </li>
                    @endrole
					@role('sadmin')
					<li>
						<a data-toggle="collapse" href="#destination">
							<i class="material-icons">account_box</i>
							<p>Destinasi
								<b class="caret"></b>
							</p>
						</a>
						<div class="collapse" id="destination">
							<ul class="nav">
								<li>
									<a href="{{ route('index-landmarks') }}">Unique</a>
								</li>
								<li>
									<a href="{{ route('index-general') }}">General</a>
								</li>
							</ul>
						</div>
					</li>
					@endrole
					@role('sadmin')
					<li>
						<a data-toggle="collapse" href="#product">
							<i class="material-icons">shopping_cart</i>
							<p>Produk
								<b class="caret"></b>
							</p>
						</a>
						<div class="collapse" id="product">
							<ul class="nav">
								<li>
									<a href="{{ route('products-all') }}">Daftar Produk</a>
								</li>
								<li>
									<a href="{{ route('assign-paket-home') }}">Input paket pada travel</a>
								</li>
								{{--<li>--}}
									{{--<a href="{{ route('product-reseller') }}">Reseller</a>--}}
								{{--</li>--}}
							</ul>
						</div>
					</li>
					@endrole
				</ul>
			</div>
		</div>
