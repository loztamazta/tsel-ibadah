<?php

Route::group(array('module' => 'Dts', 'middleware' => ['api'], 'namespace' => 'App\Modules\Dts\Controllers'), function() {

    Route::resource('dts', 'DtsController');
    
});	
