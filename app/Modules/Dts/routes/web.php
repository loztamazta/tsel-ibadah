<?php

Route::group(array('module' => 'Dts', 'middleware' => ['web'], 'namespace' => 'App\Modules\Dts\Controllers'), function() {

    // Route::resource('dts', 'DtsController');
 
 	Route::get('/dts/',	['uses' => 'DtsController@index', 'as'=>'index']);

});	
