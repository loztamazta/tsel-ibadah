<?php

Route::group(array('module' => 'Content', 'middleware' => ['web'], 'namespace' => 'App\Modules\Content\Controllers'), function() {

    //Route::resource('Content', 'ContentController');

    Route::Get('/news',             ['uses' => 'ContentController@NewsIndex',   'as' => 'index-news']);
    Route::Get('/allNews',          ['uses' => 'ContentController@AllNews',     'as' => 'all-news']);
    Route::Get('/inputNews',        ['uses' => 'ContentController@InputNews',   'as' => 'news-input']);
    Route::Get('/editNews',         ['uses' => 'ContentController@EditNews',    'as' => 'news-edit']);
    Route::Get('/viewNews',         ['uses' => 'ContentController@newsView',    'as' => 'view-news']);
    Route::Get('/searchNews',         ['uses' => 'ContentController@SearchNews',    'as' => 'search-news']);

    Route::Post('/postNews',        ['uses' => 'ContentController@PostNews',      'as'=>'post-news']);
    Route::Post('/publishNews',     ['uses' => 'ContentController@publishNews',   'as'=>'publish-news']);
    Route::Post('/unpublishNews',   ['uses' => 'ContentController@unpublishNews', 'as'=>'unpublish-news']);
    Route::Post('/deleteNews',       ['uses' => 'ContentController@deleteNews', 'as'=>'delete-news']);


    Route::Get('/tausiyah',          ['uses' => 'ContentController@TausiyahIndex',      'as'=>'index-tausiyah']);
    Route::Get('/allTausiyah',        ['uses' => 'ContentController@AllTausiyah',       'as'=>'all-tausiyah']);
    Route::Get('/inputTausiyah',       ['uses' => 'ContentController@InputTausiyah',    'as'=>'tausiyah-input']);
    Route::Get('/editTausiyah',        ['uses' => 'ContentController@EditTausiyah',     'as'=>'tausiyah-edit']);
    Route::Get('/viewTausiyah',        ['uses' => 'ContentController@TausiyahView',     'as'=>'view-tausiyah']);
    Route::Get('/searchTausiyah',         ['uses' => 'ContentController@SearchTausiyah',    'as' => 'search-tausiyah']);

    Route::Post('/tausiyahPost-syalala',        ['uses' => 'ContentController@PostTausiyah',        'as'=>'post-tausiyah']);
    Route::Post('/publishTausiyah',     ['uses' => 'ContentController@publishTausiyah',     'as'=>'publish-tausiyah']);
    Route::Post('/unpublishTausiyah',     ['uses' => 'ContentController@unpublishTausiyah', 'as'=>'unpublish-tausiyah']);
    Route::Post('/deleteTausiyah',       ['uses' => 'ContentController@deleteTausiyah', 'as'=>'delete-tausiyah']);

    Route::Get('/doa',              ['uses' => 'ContentController@DoaIndex',    'as'=>'index-doa']);
    Route::Get('/allDoa',          ['uses' => 'ContentController@AllDoa',     'as' => 'all-doa']);
    Route::Get('/inputDoa',        ['uses' => 'ContentController@InputDoa',   'as' => 'doa-input']);
    Route::Get('/editDoa',         ['uses' => 'ContentController@EditDoa',    'as' => 'doa-edit']);
    Route::Get('/viewDoa',         ['uses' => 'ContentController@doaView',    'as' => 'view-doa']);
    Route::Get('/searchDoa',         ['uses' => 'ContentController@SearchDoa',    'as' => 'search-doa']);

    Route::Post('/postDoa',        ['uses' => 'ContentController@PostDoa',      'as'=>'post-doa']);
    Route::Post('/publishDoa',     ['uses' => 'ContentController@publishDoa',   'as'=>'publish-doa']);
    Route::Post('/unpublishDoa',   ['uses' => 'ContentController@unpublishDoa', 'as'=>'unpublish-doa']);
    Route::Post('/deletePrayer',       ['uses' => 'ContentController@deletePrayer', 'as'=>'delete-prayer']);


    Route::Get('/guide',            ['uses' => 'ContentController@GuideIndex',          'as'=>'index-guide']);
    Route::Get('/allGuides',        ['uses' => 'ContentController@AllGuide',            'as'=>'all-guides']);
    Route::Get('/inputGuide',       ['uses' => 'ContentController@InputGuide',          'as'=>'guide-input']);
    Route::Get('/editGuide',        ['uses' => 'ContentController@EditGuide',           'as'=>'guide-edit']);
    Route::Get('/viewGuide',        ['uses' => 'ContentController@guideView',           'as'=>'view-guide']);
    Route::Get('/searchGuide',         ['uses' => 'ContentController@SearchGuide',    'as' => 'search-guide']);

    Route::Post('/postGuide',        ['uses' => 'ContentController@PostGuide', 'as'=>'post-guide']);
    Route::Post('/publishGuide',     ['uses' => 'ContentController@publishGuide', 'as'=>'publish-guide']);
    Route::Post('/unpublishGuide',     ['uses' => 'ContentController@unpublishGuide', 'as'=>'unpublish-guide']);
    Route::Post('/deleteGuide',       ['uses' => 'ContentController@deleteGuide', 'as'=>'delete-guide']);


    Route::Get('/tips',             ['uses' => 'ContentController@TipsIndex', 'as'=>'index-tips']);
    Route::Get('/allTips',          ['uses' => 'ContentController@AllTips', 'as'=>'all-tips']);
    Route::Get('/inputTips',        ['uses' => 'ContentController@InputTips', 'as'=>'tips-input']);
    Route::Get('/editTips',         ['uses' => 'ContentController@EditTips', 'as'=>'tips-edit']);
    Route::Get('/viewTips',        ['uses' => 'ContentController@viewTips',    'as'=>'view-tips']);
    Route::Get('/searchTips',         ['uses' => 'ContentController@SearchTips',    'as' => 'search-tips']);

    Route::Post('/postTips',        ['uses' => 'ContentController@PostTips', 'as'=>'post-tips']);
    Route::Post('/publishTips',         ['uses' => 'ContentController@publishTips', 'as'=>'publish-tips']);
    Route::Post('/unpublishTips',       ['uses' => 'ContentController@unpublishTips', 'as'=>'unpublish-tips']);
    Route::Post('/deleteTips',       ['uses' => 'ContentController@deleteTips', 'as'=>'delete-tips']);


    Route::Get('/article',          ['uses' => 'ContentController@ArticleIndex', 'as'=>'index-article']);
    Route::Get('/allArticles',      ['uses' => 'ContentController@AllArticles', 'as'=>'all-articles']);
    Route::Get('/inputArticle',    ['uses' => 'ContentController@InputArticle', 'as'=>'article-input']);
    Route::Get('/editArticle',     ['uses' => 'ContentController@EditArticle', 'as'=>'article-edit']);
    Route::Get('/viewArticle',      ['uses' => 'ContentController@viewArticle', 'as'=>'view-article']);
    Route::Get('/searchArticle',         ['uses' => 'ContentController@SearchArticle',    'as' => 'search-article']);

    Route::Post('/InsertArticle',   ['uses' => 'ContentController@postArticle', 'as' => 'post-article']);
    Route::Post('/publishArticle',         ['uses' => 'ContentController@publishArticle', 'as'=>'publish-article']);
    Route::Post('/unpublishArticle',       ['uses' => 'ContentController@unpublishArticle', 'as'=>'unpublish-article']);
    Route::Post('/deleteArticle',       ['uses' => 'ContentController@deleteArticle', 'as'=>'delete-article']);


    Route::Get('/glossary',         ['uses' => 'ContentController@GlossaryIndex', 'as'=>'index-glossary']);
    Route::Get('/allGlossary',      ['uses' => 'ContentController@AllGlossary', 'as'=>'all-glossary']);
    Route::Get('/inputGlossary',    ['uses' => 'ContentController@InputGlossary', 'as'=>'glossary-input']);
    Route::Get('/editGlossary',     ['uses' => 'ContentController@EditGlossary', 'as'=>'glossary-edit']);
    Route::Get('/viewGlossary',        ['uses' => 'ContentController@glossaryView',    'as'=>'view-glossary']);
    Route::Get('/searchGlossary',         ['uses' => 'ContentController@SearchGlossary',    'as' => 'search-glossary']);

    Route::Post('/postGlossary',            ['uses' => 'ContentController@PostGlossary', 'as'=>'post-glossary']);
    Route::Post('/publishGlossary',         ['uses' => 'ContentController@publishGlossary', 'as'=>'publish-glossary']);
    Route::Post('/unpublishGlossary',       ['uses' => 'ContentController@unpublishGlossary', 'as'=>'unpublish-glossary']);
    Route::Post('/deleteGlossary',       ['uses' => 'ContentController@deleteGlossary', 'as'=>'delete-glossary']);


    Route::post('/UploadImageContent',	    ['uses' => 'ContentController@uploadfile',  'as'=>'content-upload']);
    Route::post('/UploadTausiyahContent',   ['uses' => 'ContentController@uploadAudio', 'as'=>'tausiyah-upload']);

});	
