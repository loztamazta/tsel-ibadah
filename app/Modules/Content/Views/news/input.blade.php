<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
				<i class="material-icons">close</i>
    </span>
        <div class="card-content">
            <h4 class="card-title titleForm">Berita Baru</h4>
            <div class="card-content-overflow">
                <form id="FormNews">
                    <input type="hidden" name="IdNews">
                    <input type="hidden" name="save_type" value="SaveNews">
                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                            <img class="img" id="IM" src="{{ asset('images/image_placeholder.jpg') }}" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail"></div>
                        <div>
                            <span class="btn btn-rose btn-round btn-file">
							<span class="fileinput-new">Pilih Gambar</span>
							<span class="fileinput-exists">Ganti</span>
								<input type="file" name="Image" id="Uploads" data-old="" />
							</span>
                            <a href="#RmImage" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
                        </div>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Judul</label>
                        <input type="text" class="NewsTitle form-control" name="title" required>
                        <input type="hidden" name="id">
                        <input type="hidden" value="0" name="is_enabled">
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Sumber</label>
                        <input type="text" class="form-control" name="source" required>
                    </div>
                    <div class="form-group label-floating">
                        <label class="control-label">Konten Berita</label>
                        <textarea class="form-control ckeditor" placeholder="" rows="15" id="editor" name="editor"></textarea>
                    </div>
                </form>
            </div>
        </div>

    <div class="card-footer">
        <div class="pull-right">
                <button type="submit" class="btn btn-fill btn-rose" id="saveNewsData">Simpan</button>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>