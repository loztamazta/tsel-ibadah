<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
				<i class="material-icons">close</i>
    </span>
    <div class="card-content">
        <h4 class="card-title titleForm">Tausiyah Baru</h4>
        <div class="card-content-overflow">
            <form id="FormTausiyah">
                <input type="hidden" name="IdTausiyah">
                <input type="hidden" name="save_type" value="SaveTausiyah">
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail">
                        <audio controls> <source id="audio" src="" type="audio/mpeg"> </audio>
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                    <div>
                            <span class="btn btn-rose btn-round btn-file">
							<span class="fileinput-new">Pilih mp3</span>
							<span class="fileinput-exists">Ganti</span>
								<input type="file" accept="audio/*" name="Audio" id="Uploads" data-old="" />
							</span>
                        <a href="#RmImage" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
                    </div>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Judul</label>
                    <input type="text" class="form-control" name="title" required>
                    <input type="hidden" name="id">
                    <input type="hidden" value="0" name="is_enabled">
                </div>
            </form>
        </div>
    </div>

    <div class="card-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-fill btn-rose" id="saveTausiyahData">Simpan</button>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>