<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="card ">
            <span class="btn card-header card-header-icon round pull-right modal-close" data-background-color="red">
				<i class="material-icons">close</i>
            </span>
            <div class="card-content">
                <h4 class="card-title text-left">
                    <input type="text" name="title" readonly>
                    <br>
                    <input type="hidden" name="id">
                    <input type="hidden" name="is_enabled">
                </h4>
                <div class="card-description">
                    <div class="row">
                        <div class="col-md-12">
                            <textarea class="form-control ckeditor" id="editor" name="editor" rows="35" readonly></textarea>
                        </div>
                        {{--<div class="col-xs-2">--}}
                            {{--<i class="material-icons">phone</i>--}}
                        {{--</div>--}}
                    </div>
                    {{--<div class="row">--}}
                        {{--<div class="col-xs-10  text-right" id="EM">--}}

                        {{--</div>--}}
                        {{--<div class="col-xs-2">--}}
                            {{--<i class="material-icons">mail</i>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-xs-10  text-right" id="AD">--}}

                        {{--</div>--}}
                        {{--<div class="col-xs-2">--}}
                            {{--<i class="material-icons">place</i>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="card-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-fill btn-rose publish">Terbit</button>
                        </div>
                        <div class="pull-left">
                            <button type="submit" class="btn btn-fill btn-rose unpublish">Tidak Terbit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
