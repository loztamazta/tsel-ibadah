@extends('Dts::layouts.dts')

@section('title','Doa')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-info rpt input-doa">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
                Tambah Doa
            </button>
        </div>
        {{--<div class="col-md-3 pull-right">--}}
            {{--<div class="input-group">--}}
                {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">--}}
                        {{--Search...--}}
                    {{--</label>--}}
                    {{--<input id="searchDoa" name="searchDoa" type="text" class="form-control">--}}
                {{--</div>--}}
                {{--<a href="javascript:void(0)" id="doaSearch" class="input-group-addon">--}}
                    {{--<i class="material-icons">search</i>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title"></h4>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Judul</th>
                                        <th>Status Publikasi</th>
                                        <th>Tanggal Dibuat</th>
                                        <th class="disabled-sorting text-right">Tindakan</th>
                                    </tr>
                                    </thead>
                                    {{--<tfoot>--}}
                                    {{--<tr>--}}
                                    {{--<th>Name</th>--}}
                                    {{--<th>Position</th>--}}
                                    {{--<th>Office</th>--}}
                                    {{--<th>Age</th>--}}
                                    {{--<th>Start date</th>--}}
                                    {{--<th class="text-right">Actions</th>--}}
                                    {{--</tr>--}}
                                    {{--</tfoot>--}}
                                    <tbody id="ListDoa">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
    <script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.datatables.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/additional-methods.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function() {

            $("#doaSearch").on('click', function () {
                var searchid = $("#searchDoa").val();
                GetSearchDoa(searchid)
                return false;
            });
            $('#searchDoa').bind("enterKey",function(e){
                var searchid = $("#searchDoa").val();
                GetSearchDoa(searchid)

            });
            $('#searchDoa').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
                if (!this.value) {
                    GetListDoa()
                }
            });
        });

        GetListDoa()

        function GetListDoa()
        {
            console.log('Start')
            $.ajax({
                type: 'GET',
                url: '{{route('all-doa')}}',
                global:false,
                success: function (data) {
                    var $AllDoa= $('#ListDoa');
                    $AllDoa.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllDoa.append('' +
                            '<tr>'+
                            '<td>'+data[i].title+'</td>'+
                            '<td>'+data[i].status+'</td>'+
                            '<td>'+data[i].created_date+'</td>'+
                            '<td class="text-right">'+
                            '<a href="#" class="btn btn-simple btn-info btn-icon like view-doa" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                            '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-doa" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                            '<a href="#" class="btn btn-simple btn-danger btn-icon remove doa-delete" data-id="'+data[i].id+'" data-enabled="'+data[i].is_enabled+'" ><i class="material-icons">close</i></a>'+
                            '</td>'+
                            '</tr>')
                    }
                    $('#datatables').DataTable({
                        pageLength: 10,
                        bLengthChange: false,
                        responsive: true,
                        language: {
                            search: "_INPUT_",
                            searchPlaceholder: "Cari data",
                        },
                    })
                }
            });
        }

        function GetSearchDoa(searchid)
        {
            $.ajax({
                type: 'GET',
                url: '{{url('/searchDoa')}}',
                data: {TextId:searchid},
                global:false,
                success: function (data) {
                    var $AllDoa = $('#ListDoa');
                    $AllDoa.empty();
                    if(data == ''){
                        $AllDoa.append('<div class="col-md-12">' +'<h2>Data tidak ada</h2>'+'</div>');
                    }else{
                        for (var i = 0; i < data.length; i++) {
                            $AllDoa.append('' +
                                '<tr>'+
                                '<td>'+data[i].title+'</td>'+
                                '<td>'+data[i].status+'</td>'+
                                '<td>'+data[i].created_date+'</td>'+
                                '<td class="text-right">'+
                                '<a href="#" class="btn btn-simple btn-info btn-icon like view-doa" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                                '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-doa" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                                '<a href="#" class="btn btn-simple btn-danger btn-icon remove doa-delete" data-id="'+data[i].id+'" data-enabled="'+data[i].is_enabled+'" ><i class="material-icons">close</i></a>'+
                                '</td>'+
                                '</tr>')
                        }
                    }
                }
            });
        }

        $('#rp').slideReveal({
            // trigger: $(".rpt"),
            position: "right",
            push: false,
            overlay: true,
            zIndex:1300,
        });
        $('#rp').delegate('.rp-close','click',function() {
            $('#rp').slideReveal("hide")
        })

        $(document).delegate('.input-doa', 'click', function(e) {
            if($(this).attr('title') == 'Edit'){
                $('.loading').show()
                $.ajax({
                    type: 'GET',
                    url: '{{route('doa-edit')}}',
                    data:{idDoa:$(this).data('id')},
                    global:false,
                    success: function (data) {
                        $('#rp').empty().load("{{ route('doa-input') }}", function(){
                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()

//                            $('#SelectKota').on('change', function() {
//                                $('#selectRegion option[value='+data['0'].regionId+']').attr('selected','selected');
//                            });
                            var scr = 'storage/'+data['0'].img_1;
                            $('.titleForm').empty().append('Ubah Doa')
                            $("input[name='save_type']").val('EditSave')
                            $("input[name='id']").val(data['0'].id)
                            $("input[name='title']").val(data['0'].title)
                            $("input[name='is_enabled']").val('0')
                            $("select[name='doa_type']").val(data['0'].type_id).attr('selected', true).change();
                            $('#IM').attr("src", scr);
                            $('#Uploads').data('old', data['0'].img_1);
                            $('#rp').slideReveal("show")
                        })
                    }
                });

            }else{
                $('.loading').show()
                $('#rp').empty().load("{{ route('doa-input') }}", function(){
                    $('.card-content-overflow').perfectScrollbar();
                    $('.loading').hide()
                    $('#rp').slideReveal("show")
                })
            }

        })

        {{--$(document).delegate(".AddDoa","click", function (e) {--}}
            {{--e.preventDefault();--}}
            {{--var IdDoa= $(this).data('id')--}}
            {{--var iCntTxt = 1;--}}
            {{--var container = $(document.getElementById('ImageDoa'));--}}
            {{--iCntTxt = iCntTxt + 1;--}}
            {{--$(container).append('<div class="fileinput fileinput-new text-center" data-provides="fileinput">'+--}}
                {{--'                   <div class="fileinput-new thumbnail">'+--}}
                {{--'                   <img class="img" id="IM[]" src="{{ asset('images/image_placeholder.jpg') }}" alt="...">'+--}}
                {{--'               </div>'+--}}
                {{--'               <div class="fileinput-preview fileinput-exists thumbnail"></div>'+--}}
                {{--'               <div>'+--}}
                {{--'                   <span class="btn btn-rose btn-round btn-file">'+--}}
                {{--'                   <span class="fileinput-new">Select image</span>'+--}}
                {{--'                   <span class="fileinput-exists">Change</span>'+--}}
                {{--'                   <input type="file" name="Image" id="Uploads'+iCntTxt+'" class="Uploads" />'+--}}
                {{--'                   </span>'+--}}
                {{--'                   <a href="#RmImage1" class="btn btn-danger btn-round fileinput-exists1" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>'+--}}
                {{--'               </div>'+--}}
                {{--'               </div>');--}}
        {{--});--}}

        $(document).delegate('.view-doa', 'click', function(e) {
            // console.log('klik')
            $('.loading').show()
            $.ajax({
                type: 'GET',
                url: '{{route('doa-edit')}}',
                data:{idDoa:$(this).data('id')},
                global:false,
                success: function (data) {
                    $('#c-modal').empty().load("{{ route('view-doa') }}", function(){
                        $('.loading').hide()

                        var scr = 'storage/'+data['0'].img_1;
                        $("input[name='save_type']").val('EditSave')
                        $("input[name='id']").val(data['0'].id)
                        $("input[name='title']").val(data['0'].title)
                        $("input[name='is_enabled']").val(data['0'].is_enabled)
                        $("select[name='doa_type']").val(data['0'].type_id).attr('selected', true).change();
                        $('#IM').attr("src", scr);

                        if($("input[name='is_enabled']").val() == 0)
                        {
                            $('#publish').css('display','block')
                            $('#unpublish').css('display','none')
                        }
                        else
                        {
                            $('#publish').css('display','none')
                            $('#unpublish').css('display','block')
                        }

                        $('#modal').modal('show')

                        $('#modal').delegate('.modal-close','click',function() {
                            $('#modal').modal("hide")
                        })

                    })
                }
            });
        })

        $(document).delegate('.publish', 'click', function() {
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    id: $("input[name='id").val(),
                }
                $.post("{{route('publish-doa') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            });
        });

        $(document).delegate('.unpublish', 'click', function() {
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()

                data = {
                    id: $("input[name='id").val(),
                }
                $.post("{{route('unpublish-doa') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
                        //                    $('.List').DataTable().ajax.reload(null, false);
                        //                    $("#reset").click();
                        //                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');

            });
        });

        $(document).delegate('.doa-delete', 'click', function() {

            var ids = $(this).data('id')
            var published = $(this).data('enabled')

            if(published == 0)
            {
                swal({
                    title: 'Apakah Anda Yakin?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Batal'
                }).then(function () {
                    // debugger;
                    swal('Mohon Menunggu')
                    swal.showLoading()
                    data = {id: ids}
                    $.post("{{route('delete-prayer') }}", data, function (r) {
                        swal(
                            "Berhasil",
                            "",
                            "success"
                        ).then(function () {
                            location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                        })
                    }, 'json');
                });
            }
            else
            {
                swal({
                    title: 'Data masih digunakan',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonText: 'Ok',
                }).then(function () {
                    location.reload();
                })
            }
        });


        $(document).delegate('#saveDoaData', 'click', function(e) {
            e.preventDefault();
            var $validator = $('.card-content-overflow form').validate({
                rules: {
                    title: {
                        required: true,
                        contentSpecialCharacters : true,
                        minlength: 3,

                    },
                    doa_type: {
                        required: true,
                    },
//                    Image: {
//                        required: $("#Uploads").attr('data-img') == 'kosong',
//                    },

                },
                messages: {
                    title: "Judul Wajib diisi / Spesial karakter yang anda masukkan tidak diperbolehkan",
                    doa_type: "Pilih jenis doa",
                    Image: "Gambar Wajib diisi",
                },

                errorPlacement: function (error, element) {
                    $(element).parent('div').addClass('has-error');
                    $(element).parent('div').append(error);
                }
            });
            if ($("input[name='save_type").val() == 'SaveDoa') {
                $.validator.addClassRules("MyClass", {
                    required: true
                });
            }
            var $valid = $('.card-content-overflow form').valid();
            if (!$valid) {
                $validator.focusInvalid();
                if ($("input[name='save_type").val() == 'SaveDoa') {
                    if ($('#Uploads').hasClass('error')) {
                        $('#errorimagekosong').empty().append('<label id="Image-error" class="error" for="Image">Gambar Wajib diisi</label>');
                        console.log('error')
                    } else {
                        $('#errorimagekosong').empty();
//                        console.log('benar')
                    }
                }
                return false;
            }

            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()

                savedata()
            })
        });

        function savedata(){
                var oldimg = $("#Uploads").data('old');
                var img = $("#FormDoa").find('#Uploads');
                var file_data = $(img).prop("files")[0];
                var form_data = new FormData();
                form_data.append("file", file_data)
                form_data.append("nm", '')
                form_data.append("dir", 'prayer')
                form_data.append("w", 'auto')
                form_data.append("h", 'auto')

                $.ajax({
                    url: "{{route('content-upload') }}",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function(p){
                        if (!p.p1) {
                            if (oldimg!=p.p1) {
                                p.p1=oldimg;
                            }
                        }

                        var data={
                            a:$("input[name='save_type").val(),
                            id:$("input[name='id").val(),
                            title:$("input[name='title").val(),
                            is_enabled:$("input[name='is_enabled").val(),
                            type_id: $('#doa_type').find(":selected").val(),
                            img1:p.p1
                        };

                        $.post("{{route('post-doa') }}", data,function(r) {
                            swal(
                                'Berhasil',
                                '',
                                'success'
                            ).then(function() {
                                $('#rp').empty().slideReveal("hide")
                                location.reload()
                                // GetListDoa()
                            })
                        }, 'json');
                    }
                })
        }




        {{--$(document).delegate("#saveDoaData","click", function (e) {--}}
            {{--e.preventDefault();--}}
{{--//            debugger;--}}

            {{--swal({--}}
                {{--title: 'Are you sure?',--}}
                {{--type: 'question',--}}
                {{--showCancelButton: true,--}}
                {{--confirmButtonText: 'Yes, save it!'--}}
            {{--}).then(function() {--}}
                {{--swal('Importing Images...')--}}
                {{--swal.showLoading()--}}
                {{--var iu=[]--}}
                {{--var Doa = [];--}}
                {{--var images = [];--}}
                {{--var images = $('#FormDoa').find('.Uploads');--}}
                {{--var i--}}
                {{--for(i=0;i<images.length;i++){--}}
                {{--console.log(i)--}}
                {{--debugger;--}}
                {{--var oldimg = $("#FormDoa").find('#Uploads'+[i]).data('img');--}}
                    {{--var img = $("#FormDoa").find('#Uploads'+[i]);--}}
                    {{--var file_data = $(images[i]).prop("files")[0];--}}
                    {{--var form_data = new FormData();--}}
                    {{--form_data.append("file", file_data)--}}
                    {{--form_data.append("nm", '')--}}
                    {{--form_data.append("dir", 'doa')--}}
                    {{--form_data.append("w", 800)--}}
                    {{--form_data.append("h", 600)--}}
                    {{--Doa.push({'Bebas': img})--}}
                    {{--$.ajax({--}}
                        {{--url: "{{route('content-upload') }}",--}}
                        {{--cache: false,--}}
                        {{--contentType: false,--}}
                        {{--processData: false,--}}
                        {{--data: form_data,--}}
                        {{--type: 'post',--}}
                        {{--success: function(p){--}}
                            {{--if (p.p1) {--}}
                                {{--iu.push(p.p1)--}}
                            {{--}--}}
{{--//                            debugger;--}}
                            {{--if (i != images.length-1)--}}
                            {{--{--}}
                                {{--console.log(i)--}}
                                {{--var data = {--}}
                                    {{--a: $("input[name='save_type").val(),--}}
                                    {{--id: $("input[name='id").val(),--}}
                                    {{--title: $("input[name='title").val(),--}}
                                    {{--is_enabled: $("input[name='is_enabled").val(),--}}
                                    {{--doa_type: $('#doa_type').find(":selected").val(),--}}
                                    {{--img1: iu[0],--}}
                                    {{--img2: iu[1],--}}
                                    {{--img3: iu[2],--}}
                                    {{--img4: iu[3],--}}
                                    {{--img5: iu[4],--}}
                                {{--};--}}

                                {{--$.post("{{route('post-doa') }}", data, function (r) {--}}
                                    {{--swal(--}}
                                        {{--'Success',--}}
                                        {{--'',--}}
                                        {{--'success'--}}
                                    {{--).then(function () {--}}

                                        {{--$('#rp').empty().slideReveal("hide")--}}
                                        {{--GetListDoa()--}}
                                    {{--})--}}
                                {{--}, 'json');--}}
                            {{--} else {--}}
                                {{--console.log('last',i)--}}
                            {{--}--}}
                        {{--}--}}
                    {{--})--}}
                {{--}--}}
            {{--})--}}
        {{--});--}}
        {{--$(document).delegate('#saveDoaData', 'click', function(e) {--}}
            {{--e.preventDefault();--}}
            {{--swal({--}}
                {{--title: 'Are you sure?',--}}
                {{--type: 'question',--}}
                {{--showCancelButton: true,--}}
                {{--confirmButtonText: 'Yes, save it!'--}}
            {{--}).then(function() {--}}
                {{--// debugger;--}}
                {{--swal('Please wait')--}}
                {{--swal.showLoading()--}}
{{--//                debugger;--}}
                {{--var oldimg1 = $("#FormNews").find('#Uploads1').data('img1');--}}
                {{--var oldimg2 = $("#FormNews").find('#Uploads2').data('img2');--}}
                {{--var oldimg3 = $("#FormNews").find('#Uploads3').data('img3');--}}
                {{--var oldimg4 = $("#FormNews").find('#Uploads4').data('img4');--}}
                {{--var oldimg5 = $("#FormNews").find('#Uploads5').data('img5');--}}
                {{--var img1 = $("#FormNews").find('#Uploads1');--}}
                {{--var img2 = $("#FormNews").find('#Uploads2');--}}
                {{--var img3 = $("#FormNews").find('#Uploads3');--}}
                {{--var img4 = $("#FormNews").find('#Uploads4');--}}
                {{--var img5 = $("#FormNews").find('#Uploads5');--}}
                {{--var file_data1 = $(img1).prop("files")[0];--}}
                {{--var file_data2 = $(img2).prop("files")[0];--}}
                {{--var file_data3 = $(img3).prop("files")[0];--}}
                {{--var file_data4 = $(img4).prop("files")[0];--}}
                {{--var file_data5 = $(img5).prop("files")[0];--}}
                {{--var form_data1 = new FormData();--}}
                {{--form_data1.append("file", file_data1)--}}
                {{--form_data1.append("nm", '')--}}
                {{--form_data1.append("dir", 'doa')--}}
                {{--form_data1.append("w", 800)--}}
                {{--form_data1.append("h", 600)--}}
                {{--var form_data2 = new FormData();--}}
                {{--form_data2.append("file", file_data2)--}}
                {{--form_data2.append("nm", '')--}}
                {{--form_data2.append("dir", 'doa')--}}
                {{--form_data2.append("w", 800)--}}
                {{--form_data2.append("h", 600)--}}
                {{--var form_data3 = new FormData();--}}
                {{--form_data3.append("file", file_data3)--}}
                {{--form_data3.append("nm", '')--}}
                {{--form_data3.append("dir", 'doa')--}}
                {{--form_data3.append("w", 800)--}}
                {{--form_data3.append("h", 600)--}}
                {{--var form_data4 = new FormData();--}}
                {{--form_data4.append("file", file_data4)--}}
                {{--form_data4.append("nm", '')--}}
                {{--form_data4.append("dir", 'doa')--}}
                {{--form_data4.append("w", 800)--}}
                {{--form_data4.append("h", 600)--}}
                {{--var form_data5 = new FormData();--}}
                {{--form_data5.append("file", file_data5)--}}
                {{--form_data5.append("nm", '')--}}
                {{--form_data5.append("dir", 'doa')--}}
                {{--form_data5.append("w", 800)--}}
                {{--form_data5.append("h", 600)--}}
                {{--form_data = [];--}}
                {{--form_data[0] = form_data1;--}}
                {{--form_data[1] = form_data2;--}}
                {{--form_data[3] = form_data3;--}}
                {{--form_data[4] = form_data4;--}}
                {{--form_data[5] = form_data5;--}}
                {{--$.ajax({--}}
                    {{--url: "{{route('content-upload') }}",--}}
                    {{--cache: false,--}}
                    {{--contentType: false,--}}
                    {{--processData: false,--}}
                    {{--data: {form_data:form_data},--}}
                    {{--type: 'post',--}}
                    {{--success: function(p){--}}
                        {{--if (!p.p1) {--}}
                            {{--if (oldimg1!=p.p1) {--}}
                                {{--p.p0=oldimg1;--}}
                            {{--}--}}
                        {{--}--}}

                        {{--if (!p.p1) {--}}
                            {{--if (oldimg2!=p.p1) {--}}
                                {{--p.p2=oldimg2;--}}
                            {{--}--}}
                        {{--}--}}

                        {{--if (!p.p1) {--}}
                            {{--if (oldimg3!=p.p1) {--}}
                                {{--p.p3=oldimg3;--}}
                            {{--}--}}
                        {{--}--}}

                        {{--if (!p.p1) {--}}
                            {{--if (oldimg4!=p.p1) {--}}
                                {{--p.p4=oldimg4;--}}
                            {{--}--}}
                        {{--}--}}

                        {{--if (!p.p1) {--}}
                            {{--if (oldimg5!=p.p1) {--}}
                                {{--p.p5=oldimg5;--}}
                            {{--}--}}
                        {{--}--}}

                        {{--var data={--}}
                            {{--a:$("input[name='TypeSave").val(),--}}
                            {{--id:$("input[name='id").val(),--}}
                            {{--title:$("input[name='title").val(),--}}
                            {{--is_enabled:$("input[name='is_enabled").val(),--}}
                            {{--img1:p.p0,--}}
                            {{--img2:p.p2,--}}
                            {{--img3:p.p3,--}}
                            {{--img4:p.p4,--}}
                            {{--img5:p.p5--}}

                        {{--};--}}

                        {{--$.post("{{route('post-doa') }}", data,function(r) {--}}
                            {{--swal(--}}
                                {{--'Success',--}}
                                {{--'',--}}
                                {{--'success'--}}
                            {{--).then(function() {--}}
                                {{--$('#rp').empty().slideReveal("hide")--}}
                                {{--GetListNews()--}}
                            {{--})--}}
                        {{--}, 'json');--}}
                    {{--}--}}
                {{--})--}}

            {{--}, 'json').fail(function(response) {--}}
                {{--console.log(response)--}}
                {{--swal(--}}
                    {{--'Whoops.. Something is wrong!',--}}
                    {{--response.responseText,--}}
                    {{--'error'--}}
                {{--)--}}
            {{--});--}}

        {{--});--}}

    </script>

@endsection