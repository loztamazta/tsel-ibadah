<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog mt20">

        <div class="card ">
            <span class="btn card-header card-header-icon round pull-right modal-close" data-background-color="red">
				<i class="material-icons">close</i>
            </span>
            <div class="card-content">
                <h4 class="card-title text-left">
                    <input type="text" class="form-control" name="title" readonly>
                    <br>
                    <input type="hidden" name="id">
                    <input type="hidden" name="is_enabled">
                </h4>
                <div class="card-description">
                    <div class="card-image">
                        <img class="img" id="IM" src="">
                    </div>
                </div>
                <div class="card-footer">
                    <div class="pull-right">
                        <button type="submit" id="publish" class="btn btn-fill btn-rose publish">Terbit</button>
                    </div>
                    <div class="pull-left">
                        <button type="submit" id="unpublish" class="btn btn-fill btn-rose unpublish" >Tidak Terbit</button>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
