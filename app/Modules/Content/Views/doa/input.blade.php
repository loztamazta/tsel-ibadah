<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
				<i class="material-icons">close</i>
    </span>
    <div class="card-content">
        <h4 class="card-title titleForm">Doa Baru</h4>
        <div class="card-content-overflow">
            <form id="FormDoa">
                <input type="hidden" name="IdDoa">
                <input type="hidden" name="save_type" value="SaveDoa">
                <div class="form-group label-floating">
                    <label class="control-label">Judul <small>(Wajib)</small></label>
                    <input type="text" class="form-control" name="title" required>
                    <input type="hidden" name="id">
                    <input type="hidden" value="0" name="is_enabled">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Pilih Jenis Doa
                        <small>(Wajib)</small>
                    </label>
                    <select class="form-control" id="doa_type" name="doa_type" required data-style="select-with-transition" title="Single Select" data-size="7">
                        <option value="1"> Doa Sehari-hari</option>
                        <option value="2">Doa Haji & Umroh</option>
                        <option value="3">Doa Manasik - Tawaf</option>
                        <option value="4">Doa Manasik - Sai</option>
                        <option value="5">Doa Manasik - Tahalul</option>
                    </select>
                </div>
                {{--<div class="row btnadd">--}}
                    {{--<div class="col-xs-1 pull-right">--}}
                        {{--<button class="btn btn-danger btn-simple pl0 pr0 AddDoa" data-id="'+i+'" >--}}
                            {{--<i class="material-icons">add</i>--}}
                            {{--<div class="ripple-container"></div>--}}
                            {{--</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                    <div class="fileinput-new thumbnail">
                        <img class="img" id="IM" src="{{ asset('images/image_placeholder.jpg') }}" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                    <div>
                            <span class="btn btn-rose btn-round btn-file">
							<span class="fileinput-new">Pilih gambar</span>
							<span class="fileinput-exists">Ganti</span>
								<input type="file" name="Image" id="Uploads" class="MyClass " data-old="" />
							</span>
                        <a href="#RmImage" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
                        <div class="alert alert-danger" style="display:none">
                            <strong>Masukan file Doa</strong>
                        </div>
                    </div>
                    <span id="errorimagekosong"></span>
                </div>
                {{--<div id="ImageDoa">--}}

                {{--</div>--}}
            </form>
        </div>
    </div>

    <div class="card-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-fill btn-rose" id="saveDoaData">Simpan</button>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>