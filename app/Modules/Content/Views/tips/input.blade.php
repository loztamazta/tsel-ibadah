<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
				<i class="material-icons">close</i>
			</span>
    <div class="card-content">

        <h4 class="card-title titleForm">Tips Baru</h4>
        <div class="card-content-overflow">
            {{--<div class="fileinput fileinput-new text-center" data-provides="fileinput">--}}
            {{--<div class="fileinput-new thumbnail">--}}
            {{--<img src="{{ asset('images/image_placeholder.jpg') }}" alt="...">--}}
            {{--</div>--}}
            {{--<div class="fileinput-preview fileinput-exists thumbnail"></div>--}}
            {{--<div>--}}
            {{--<span class="btn btn-rose btn-round btn-file">--}}
            {{--<span class="fileinput-new">Select image</span>--}}
            {{--<span class="fileinput-exists">Change</span>--}}
            {{--<input type="file" name="_______" />--}}
            {{--</span>--}}
            {{--<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            <form id="FormGuide">
                <div class="form-group label-floating">
                    <label class="control-label">Judul</label>
                    <input type="text" class="form-control" name="title">
                    <input type="hidden" value="SaveTips" name="save_type">
                    <input type="hidden" name="id">
                    <input type="hidden" value="0" name="is_enabled">
                </div>
                {{--<div class="form-group label-floating">--}}
                {{--<label class="control-label">Sumber</label>--}}
                {{--<input type="text" class="form-control" name="source">--}}
                {{--</div>--}}
                {{--<div class="form-group label-floating">--}}
                {{--<label class="control-label">Longitude</label>--}}
                {{--<input type="text" id="lngFld" name="longitude">--}}
                {{--</div>--}}
                {{--<div class="form-group label-floating">--}}
                {{--<label class="control-label">Lokasi Hotel</label>--}}
                {{--<select class="selectpicker" data-style="select-with-transition" title="Pilih kota..." data-size="7" id="selectRegion" name="region_id">--}}
                {{--<option disabled>Pilih kota</option>--}}
                {{--<option value="2">Mekkah </option>--}}
                {{--<option value="3">Madinnah</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                <div class="form-group label-floating">
                    <label class="control-label">Konten Tips</label>
                    <textarea class="form-control ckeditor" id="editor" placeholder="" rows="35" name="editor"></textarea>
                </div>
            </form>

        </div>

    </div>

    <div class="card-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-fill btn-rose" id="saveTipsData">Simpan</button>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>