@extends('Dts::layouts.dts')

@section('title','Berita')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-info rpt input-tips">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
                 Tips
            </button>
        </div>
        {{--<div class="col-md-3 pull-right">--}}
            {{--<div class="input-group">--}}
                {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">--}}
                        {{--Search...--}}
                    {{--</label>--}}
                    {{--<input id="searchTips" name="firstname" type="text" class="form-control">--}}
                {{--</div>--}}
                {{--<a href="javascript:void(0)" id="tipsSearch" class="input-group-addon">--}}
                    {{--<i class="material-icons">search</i>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title"></h4>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Judul</th>
                                        <th>Status Publikasi</th>
                                        <th>Tanggal Dibuat</th>
                                        <th class="disabled-sorting text-right">Tindakan</th>
                                    </tr>
                                    </thead>
                                    {{--<tfoot>--}}
                                    {{--<tr>--}}
                                    {{--<th>Name</th>--}}
                                    {{--<th>Position</th>--}}
                                    {{--<th>Office</th>--}}
                                    {{--<th>Age</th>--}}
                                    {{--<th>Start date</th>--}}
                                    {{--<th class="text-right">Actions</th>--}}
                                    {{--</tr>--}}
                                    {{--</tfoot>--}}
                                    <tbody id="ListTips">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
    <script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.datatables.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
    <script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/additional-methods.js') }}"></script>
    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function() {

            $("#tipsSearch").on('click', function () {
                console.log('tai')
                var searchid = $("#searchTips").val();
                GetSearchTips(searchid)
                return false;
            });
            $('#searchTips').bind("enterKey",function(e){
                var searchid = $("#searchTips").val();
                GetSearchTips(searchid)

            });
            $('#searchTips').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
                if (!this.value) {
                    GetListTips()
                }
            });
        });

        GetListTips()

        function GetListTips()
        {
            console.log('Start')
            $.ajax({
                type: 'GET',
                url: '{{route('all-tips')}}',
                global:false,
                success: function (data) {
                    var $AllTips= $('#ListTips');
                    $AllTips.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllTips.append('' +
                            '<tr>'+
                            '<td>'+data[i].title+'</td>'+
                            '<td>'+data[i].status+'</td>'+
                            '<td>'+data[i].created_date+'</td>'+
                            '<td class="text-right">'+
                            '<a href="#" class="btn btn-simple btn-info btn-icon like view-tips" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                            '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-tips" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                            '<a href="#" class="btn btn-simple btn-danger btn-icon remove tips-delete" data-id="'+data[i].id+'" data-enabled="'+data[i].is_enabled+'" ><i class="material-icons">close</i></a>'+
                            '</td>'+
                            '</tr>')
                    }
                    $('#datatables').DataTable({
                        pageLength: 10,
                        bLengthChange: false,
                        responsive: true,
                        language: {
                            search: "_INPUT_",
                            searchPlaceholder: "Cari data",
                        },
                    })
                }
            });
        }

        function GetSearchTips(searchid)
        {
            $.ajax({
                type: 'GET',
                url: '{{url('/searchTips')}}',
                data: {TextId:searchid},
                global:false,
                success: function (data) {
                    var $AllTips = $('#ListTips');
                    $AllTips.empty();
                    if(data == ''){
                        $AllTips.append('<div class="col-md-12">' +'<h2>Data tidak ada</h2>'+'</div>');
                    }else{
                        for (var i = 0; i < data.length; i++) {
                            $AllTips.append('' +
                                '<tr>'+
                                '<td>'+data[i].title+'</td>'+
                                '<td>'+data[i].status+'</td>'+
                                '<td>'+data[i].created_date+'</td>'+
                                '<td class="text-right">'+
                                '<a href="#" class="btn btn-simple btn-info btn-icon like view-tips" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                                '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-tips" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                                '<a href="#" class="btn btn-simple btn-danger btn-icon remove tips-delete" data-id="'+data[i].id+'" data-enabled="'+data[i].is_enabled+'" ><i class="material-icons">close</i></a>'+
                                '</td>'+
                                '</tr>')
                        }
                    }
                }
            });
        }

        $('#rp').slideReveal({
            // trigger: $(".rpt"),
            position: "right",
            push: false,
            overlay: true,
            zIndex:1300,
        });
        $('#rp').delegate('.rp-close','click',function() {
            $('#rp').slideReveal("hide")
        })

        $(document).delegate('.input-tips', 'click', function(e) {
            if($(this).attr('title') == 'Edit'){
                $('.loading').show()
                $.ajax({
                    type: 'GET',
                    url: '{{route('tips-edit')}}',
                    data:{idTips:$(this).data('id')},
                    global:false,
                    success: function (data) {
                        $('#rp').empty().load("{{ route('tips-input') }}", function(){
                            CKEDITOR.replace('editor');
                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()

//                            $('#SelectKota').on('change', function() {
//                                $('#selectRegion option[value='+data['0'].regionId+']').attr('selected','selected');
//                            });
                            var scr = 'storage/'+data['0'].Img;
                            $('.titleForm').empty().append('Ubah Tips')
                            $("input[name='save_type']").val('EditSave')
                            $("input[name='id']").val(data['0'].id)
                            $("input[name='title']").val(data['0'].title)
                            $("input[name='is_enabled']").val('0')
                            $("textarea.ckeditor").val(data['0'].remark);
                            $('#rp').slideReveal("show")
                        })
                    }
                });

            }else{
                $('.loading').show()
                $('#rp').empty().load("{{ route('tips-input') }}", function(){
                    CKEDITOR.replace('editor');
                    $('.card-content-overflow').perfectScrollbar();
                    $('.loading').hide()
                    $('#rp').slideReveal("show")
                })
            }

        })
        $(document).delegate('.view-tips', 'click', function(e) {
            // console.log('klik')
            $('.loading').show()
            $.ajax({
                type: 'GET',
                url: '{{route('tips-edit')}}',
                data:{idTips:$(this).data('id')},
                global:false,
                success: function (data) {
                    $('#c-modal').empty().load("{{ route('view-tips') }}", function(){
                        CKEDITOR.replace('editor');
                        $('.loading').hide()
//                        var scr = 'storage/'+data['0'].Img;
                        $("input[name='id']").val(data['0'].id)
                        $("input[name='title']").val(data['0'].title)
                        $("input[name='is_enabled']").val(data['0'].is_enabled)
                        $("textarea.ckeditor").val(data['0'].remark);

                        if($("input[name='is_enabled']").val() == 0)
                        {
                            $('.publish').css('display','block')
                            $('.unpublish').css('display','none')
                        }
                        else
                        {
                            $('.publish').css('display','none')
                            $('.unpublish').css('display','block')
                        }

                        $('#modal').modal('show')

                        $('#modal').delegate('.modal-close','click',function() {
                            $('#modal').modal("hide")
                        })

                    })
                }
            });
        })

        $(document).delegate('.publish', 'click', function() {
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    id: $("input[name='id").val(),
                }
                $.post("{{route('publish-tips') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            });
        });

        $(document).delegate('.unpublish', 'click', function() {
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    id: $("input[name='id").val(),
                }
                $.post("{{route('unpublish-tips') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            });
        });

        $(document).delegate('.tips-delete', 'click', function() {

            var ids = $(this).data('id')
            var published = $(this).data('enabled')

            if(published == 0)
            {
                swal({
                    title: 'Apakah Anda Yakin?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Batal'
                }).then(function () {
                    // debugger;
                    swal('Mohon Menunggu')
                    swal.showLoading()
                    data = {id: ids}
                    $.post("{{route('delete-tips') }}", data, function (r) {
                        swal(
                            "Berhasil",
                            "",
                            "success"
                        ).then(function () {
                            location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                        })
                    }, 'json');
                })
            }
            else
            {
                swal({
                    title: 'Data masih digunakan',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonText: 'Ok',
                }).then(function () {
                    location.reload();
                })
            }
        });

        $(document).delegate('#saveTipsData', 'click', function(e) {
            e.preventDefault();
            var $validator = $('.card-content-overflow form').validate({
                rules: {
                    title: {
                        required: true,
                        contentSpecialCharacters: true
                    },

                },
                messages: {
                    title: "Wajib diisi / Spesial Karakter Tidak Diperbolehkan",

                },
                errorPlacement: function(error, element) {
                    $(element).parent('div').addClass('has-error');
                    $(element).parent('div').append(error);

                }

            });
            var $valid = $('.card-content-overflow form').valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            swal({
                title: 'Apakah Anda Yakin ?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()

                savedata()
            })
        });

        function savedata(){

                data = {
                    a: $("input[name='save_type").val(),
                    id: $("input[name='id").val(),
                    title: $("input[name='title").val(),
                    is_enabled: $("input[name='is_enabled").val(),
                    remark: CKEDITOR.instances['editor'].getData(),
                }
                $.post("{{route('post-tips') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            }



    </script>

@endsection