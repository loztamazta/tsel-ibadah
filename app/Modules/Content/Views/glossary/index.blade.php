@extends('Dts::layouts.dts')

@section('title','Daftar Istilah')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-info rpt input-glossary">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
                Tambah Istilah
            </button>
        </div>
        {{--<div class="col-md-3 pull-right">--}}
            {{--<div class="input-group">--}}
                {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">--}}
                        {{--Search...--}}
                    {{--</label>--}}
                    {{--<input id="searchGlossary" name="firstname" type="text" class="form-control">--}}
                {{--</div>--}}
                {{--<a href="javascript:void(0)" id="glossarySearch" class="input-group-addon">--}}
                    {{--<i class="material-icons">search</i>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title"></h4>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Judul</th>
                                        <th>Status Publikasi</th>
                                        <th>Tanggal Dibuat</th>
                                        <th class="disabled-sorting text-right">Tindakan</th>
                                    </tr>
                                    </thead>
                                    {{--<tfoot>--}}
                                    {{--<tr>--}}
                                    {{--<th>Name</th>--}}
                                    {{--<th>Position</th>--}}
                                    {{--<th>Office</th>--}}
                                    {{--<th>Age</th>--}}
                                    {{--<th>Start date</th>--}}
                                    {{--<th class="text-right">Actions</th>--}}
                                    {{--</tr>--}}
                                    {{--</tfoot>--}}
                                    <tbody id="ListGlossaries">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
    <script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.datatables.js') }}"></script>
    <script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/additional-methods.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function() {

            $("#glossarySearch").on('click', function () {
                var searchid = $("#searchGlossary").val();
                GetSearchGlossary(searchid)
                return false;
            });
            $('#searchGlossary').bind("enterKey",function(e){
                var searchid = $("#searchGlossary").val();
                GetSearchGlossary(searchid)

            });
            $('#searchGlossary').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
                if (!this.value) {
                    GetListGlossaries()
                }
            });
        });


        GetListGlossaries()

        function GetListGlossaries()
        {
            console.log('Start')
            $.ajax({
                type: 'GET',
                url: '{{route('all-glossary')}}',
                global:false,
                success: function (data) {
                    var $AllGlossaries= $('#ListGlossaries');
                    $AllGlossaries.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllGlossaries.append('' +
                            '<tr>'+
                            '<td>'+data[i].title+'</td>'+
                            '<td>'+data[i].status+'</td>'+
                            '<td>'+data[i].created_date+'</td>'+
                            '<td class="text-right">'+
                            '<a href="#" class="btn btn-simple btn-info btn-icon like view-glossary" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                            '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-glossary" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                            '<a href="#" class="btn btn-simple btn-danger btn-icon remove glossary-delete" data-id="'+data[i].id+'" data-enabled="'+data[i].is_enabled+'" ><i class="material-icons">close</i></a>'+
                            '</td>'+
                            '</tr>')
                    }
                    $('#datatables').DataTable({
                        pageLength: 10,
                        bLengthChange: false,
                        responsive: true,
                        language: {
                            search: "_INPUT_",
                            searchPlaceholder: "Cari data",
                        },
                    })
                }
            });
        }

        function GetSearchGlossary(searchid)
        {
            $.ajax({
                type: 'GET',
                url: '{{url('/searchGlossary')}}',
                data: {keywordGlo:searchid},
                global:false,
                success: function (data) {
                    var $AllGlossaries = $('#ListGlossaries');
                    $AllGlossaries.empty();
                    if(data == ''){
                        $AllGlossaries.append('<div class="col-md-12">' +'<h2>Data tidak ada</h2>'+'</div>');
                    }else{
                        for (var i = 0; i < data.length; i++) {
                            $AllGlossaries.append('' +
                                '<tr>'+
                                '<td>'+data[i].title+'</td>'+
                                '<td>'+data[i].status+'</td>'+
                                '<td>'+data[i].created_date+'</td>'+
                                '<td class="text-right">'+
                                '<a href="#" class="btn btn-simple btn-info btn-icon like view-glossary" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                                '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-glossary" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                                '<a href="#" class="btn btn-simple btn-danger btn-icon remove glossary-delete" data-id="'+data[i].id+'" data-enabled="'+data[i].is_enabled+'" ><i class="material-icons">close</i></a>'+
                                '</td>'+
                                '</tr>')
                        }
                    }
                }
            });
        }

        $('#rp').slideReveal({
            // trigger: $(".rpt"),
            position: "right",
            push: false,
            overlay: true,
            zIndex:1300,
        });
        $('#rp').delegate('.rp-close','click',function() {
            $('#rp').slideReveal("hide")
        })

        $(document).delegate('.input-glossary', 'click', function(e) {
            if($(this).attr('title') == 'Edit'){
                $('.loading').show()
                $.ajax({
                    type: 'GET',
                    url: '{{route('glossary-edit')}}',
                    data:{idGlossary:$(this).data('id')},
                    global:false,
                    success: function (data) {
                        $('#rp').empty().load("{{ route('glossary-input') }}", function(){
                            CKEDITOR.replace('editor');
                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()

//                            $('#SelectKota').on('change', function() {
//                                $('#selectRegion option[value='+data['0'].regionId+']').attr('selected','selected');
//                            });
//                            var scr = 'storage/'+data['0'].Img;
                            $('.titleForm').empty().append('Ubah Istilah')
                            $("input[name='save_type']").val('EditSave')
                            $("input[name='id']").val(data['0'].id)
                            $("input[name='title']").val(data['0'].title)
                            $("input[name='is_enabled']").val(data['0'].is_enabled)
                            $("textarea.ckeditor").val(data['0'].remark);
                            $('#rp').slideReveal("show")
                        })
                    }
                });

            }else{
                $('.loading').show()
                $('#rp').empty().load("{{ route('glossary-input') }}", function(){
                    CKEDITOR.replace('editor');
                    $('.card-content-overflow').perfectScrollbar();
                    $('.loading').hide()
                    $('#rp').slideReveal("show")
                })
            }

        })
        $(document).delegate('.view-glossary', 'click', function(e) {
            // console.log('klik')
            $('.loading').show()
            $.ajax({
                type: 'GET',
                url: '{{route('glossary-edit')}}',
                data:{idGlossary:$(this).data('id')},
                global:false,
                success: function (data) {
                    $('#c-modal').empty().load("{{ route('view-glossary') }}", function(){
                        CKEDITOR.replace('editor');
                        $('.loading').hide()
                        var scr = 'storage/'+data['0'].Img;
                        $("input[name='id']").val(data['0'].id)
                        $("input[name='title']").val(data['0'].title)
                        $("input[name='is_enabled']").val(data['0'].is_enabled)
                        $("textarea.ckeditor").val(data['0'].remark);

                        if($("input[name='is_enabled']").val() == 0)
                        {
                            $('.publish').css('display','block')
                            $('.unpublish').css('display','none')
                        }
                        else
                        {
                            $('.publish').css('display','none')
                            $('.unpublish').css('display','block')
                        }

                        $('#modal').modal('show')

                        $('#modal').delegate('.modal-close','click',function() {
                            $('#modal').modal("hide")
                        })

                    })
                }
            });
        })

        $(document).delegate('.publish', 'click', function() {
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    id: $("input[name='id").val(),
                }
                $.post("{{route('publish-glossary') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
                        //                    $('.List').DataTable().ajax.reload(null, false);
                        //                    $("#reset").click();
                        //                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            });
        });

        $(document).delegate('.unpublish', 'click', function() {
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    id: $("input[name='id").val(),
                }
                $.post("{{route('unpublish-glossary') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
                        //                    $('.List').DataTable().ajax.reload(null, false);
                        //                    $("#reset").click();
                        //                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            });
        });

        $(document).delegate('.glossary-delete', 'click', function() {

            var ids = $(this).data('id')
            var published = $(this).data('enabled')

            if(published == 0 )
            {
                swal({
                    title: 'Apakah anda yakin?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Batal'
                }).then(function () {
                    // debugger;
                    swal('Mohon Menunggu')
                    swal.showLoading()
                    data = {
                        id: ids,
                    }
                    $.post("{{route('delete-glossary') }}", data, function (r) {
                        swal(
                            "Berhasil",
                            "",
                            "success"
                        ).then(function () {
                            location.reload();
                            //                    $('.List').DataTable().ajax.reload(null, false);
                            //                    $("#reset").click();
                            //                    $('#FormNews').find("input,textarea,select").val('').end();
                        })
                    }, 'json');
                });
            }
            else
            {
                swal({
                    title: 'Data masih digunakan',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonText: 'Ok',
                }).then(function () {
                    location.reload();
                })
            }

        });

        $(document).delegate('#saveGlossaryData', 'click', function(e) {
            e.preventDefault();
            var $validator = $('.card-content-overflow form').validate({
                rules: {
                    title: {
                        required: true,
                        contentSpecialCharacters: true
                    },

                },
                messages: {
                    title: "Wajib diisi / Spesial Karakter Tidak Diperbolehkan",

                },
                errorPlacement: function(error, element) {
                    $(element).parent('div').addClass('has-error');
                    $(element).parent('div').append(error);

                }

            });
            var $valid = $('.card-content-overflow form').valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            swal({
                title: 'Apakah Anda Yakin ?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()

                savedata()
            })
        });

        function savedata(){
                data = {
                    a: $("input[name='save_type").val(),
                    id: $("input[name='id").val(),
                    title: $("input[name='title").val(),
                    is_enabled: $("input[name='is_enabled").val(),
                    remark: CKEDITOR.instances['editor'].getData(),
                }
                $.post("{{route('post-glossary') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            };

        //Coding Nouzar :


        {{--$(".edit-hotel").click(function(){--}}
        {{--$('.loading').show()--}}
        {{--$.ajax({--}}
        {{--type: 'GET',--}}
        {{--url: '{{url('/travel/hotel/edit')}}',--}}
        {{--data:{idHotel:$(this).data('id')},--}}
        {{--global:false,--}}
        {{--success: function (data) {--}}
        {{--$('#rp').empty().load("{{ route('travel-hotel-edit') }}", function(){--}}
        {{--$('.card-content-overflow').perfectScrollbar();--}}
        {{--$('.loading').hide()--}}
        {{--$(document).delegate('#update-hotel', 'click', function() {--}}

        {{--data={--}}
        {{--id:$("input[name='hotel_id").val(),--}}
        {{--name:$("input[name='hotel_name").val(),--}}
        {{--type_id:$("input[name='place_type").val(),--}}
        {{--latitude:$("input[name='latitude").val(),--}}
        {{--longitude:$("input[name='longitude").val(),--}}
        {{--region_id:$('#regionId').find(":selected").val(),--}}
        {{--address:$("input[name='hotel_address").val(),--}}
        {{--}--}}
        {{--$.post("{{route('post-hotel') }}", data, function(r) {--}}
        {{--swal({--}}
        {{--title: "Success",--}}
        {{--text: "Success",--}}
        {{--showConfirmButton: true,--}}
        {{--type: "success",--}}
        {{--}, function(){--}}
        {{--$('.List').DataTable().ajax.reload(null, false);--}}
        {{--$("#reset").click();--}}
        {{--$('#FormNews').find("input,textarea,select").val('').end();--}}
        {{--})--}}
        {{--}, 'json');--}}
        {{--});--}}
        {{--$('#rp').slideReveal("show")--}}
        {{--})--}}
        {{--}--}}
        {{--});--}}


        {{--$('#rp').empty().load("{{ route('travel-hotel-edit') }}", function(){--}}
        {{--$('.card-content-overflow').perfectScrollbar();--}}
        {{--$('.loading').hide()--}}
        {{--$(document).delegate('#update-hotel', 'click', function() {--}}

        {{--data={--}}
        {{--id:$("input[name='hotel_id").val(),--}}
        {{--name:$("input[name='hotel_name").val(),--}}
        {{--type_id:$("input[name='place_type").val(),--}}
        {{--latitude:$("input[name='latitude").val(),--}}
        {{--longitude:$("input[name='longitude").val(),--}}
        {{--region_id:$('#regionId').find(":selected").val(),--}}
        {{--address:$("input[name='hotel_address").val(),--}}
        {{--}--}}
        {{--$.post("{{route('post-hotel') }}", data, function(r) {--}}
        {{--swal({--}}
        {{--title: "Success",--}}
        {{--text: "Success",--}}
        {{--showConfirmButton: true,--}}
        {{--type: "success",--}}
        {{--}, function(){--}}
        {{--$('.List').DataTable().ajax.reload(null, false);--}}
        {{--$("#reset").click();--}}
        {{--$('#FormNews').find("input,textarea,select").val('').end();--}}
        {{--})--}}
        {{--}, 'json');--}}
        {{--});--}}
        {{--$('#rp').slideReveal("show")--}}
        {{--})--}}
        //        })

        {{--$(document).delegate('.delete-hotel', 'click', function(e) {--}}
        {{--// console.log('klik')--}}
        {{--e.preventDefault();--}}
        {{--var IdTravel = {{$hotel->id}}--}}
        {{--swal({--}}
        {{--title: 'Are you sure delete this Data?',--}}
        {{--type: 'warning',--}}
        {{--showCancelButton: true,--}}
        {{--confirmButtonColor: "#DD6B55",--}}
        {{--confirmButtonText: 'Yes, Delete  it!'--}}
        {{--}).then(function() {--}}
        {{--// debugger;--}}
        {{--swal('Please wait')--}}
        {{--swal.showLoading()--}}
        {{--var data={--}}
        {{--id:IdTravel,--}}
        {{--};--}}
        {{--$.post("{{route('delete-hotel') }}", data,function(r) {--}}

        {{--swal(--}}
        {{--'Deleted!',--}}
        {{--'Data Travel has been deleted.',--}}
        {{--'success'--}}
        {{--).then(function() {--}}
        {{--//                        location.reload();--}}
        {{--})--}}
        {{--}, 'json');--}}

        {{--})--}}
        {{--})--}}

        {{--$(".view-hotel").click(function(){--}}
        {{--// console.log('klik')--}}
        {{--$('.loading').show()			--}}
        {{--$('#c-modal').empty().load("{{ route('travel-hotel-view') }}", function(){--}}
        {{--$('.loading').hide()--}}
        {{--$('#modal').modal('show')--}}
        {{--})--}}
        {{--})--}}



        // $(".card-travel").click(function(){
        // 	console.log('klik')
        // }).find(".card-actions").click(function(e) {
        // 	return false;
        // });


    </script>

@endsection