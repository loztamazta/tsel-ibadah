<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
				<i class="material-icons">close</i>
			</span>
    <div class="card-content">

        <h4 class="card-title titleForm">Article Baru</h4>
        <div class="card-content-overflow">
            <form id="FormArticle">
                    <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                        <div class="fileinput-new thumbnail">
                            <img id="IMS" src="{{ asset('images/image_placeholder.jpg') }}" alt="...">
                        </div>
                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                    <div>
                        <span class="btn btn-rose btn-round btn-file">
                        <span class="fileinput-new">Pilih Gambar</span>
                        <span class="fileinput-exists">Ganti</span>
                        <input type="file" name="Image" id="Uploads" data-old="" />
                        </span>
                        <a href="#RmImage" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
                    </div>
                </div>

                <div class="form-group label-floating">
                    <label class="control-label">Judul <small>(wajib diisi)</small></label>
                    <input type="text" class="form-control" name="title" required>
                    <input type="hidden" name="TypeSave" value="SaveArticle">
                    <input type="hidden" name="id">
                    <input type="hidden" value="0" name="is_enabled">
                </div>
                {{--<div class="form-group label-floating">--}}
                {{--<label class="control-label">Sumber</label>--}}
                {{--<input type="text" class="form-control" name="source">--}}
                {{--</div>--}}
                {{--<div class="form-group label-floating">--}}
                {{--<label class="control-label">Longitude</label>--}}
                {{--<input type="text" id="lngFld" name="longitude">--}}
                {{--</div>--}}
                {{--<div class="form-group label-floating">--}}
                {{--<label class="control-label">Lokasi Hotel</label>--}}
                {{--<select class="selectpicker" data-style="select-with-transition" title="Pilih kota..." data-size="7" id="selectRegion" name="region_id">--}}
                {{--<option disabled>Pilih kota</option>--}}
                {{--<option value="2">Mekkah </option>--}}
                {{--<option value="3">Madinnah</option>--}}
                {{--</select>--}}
                {{--</div>--}}
                <div class="form-group label-floating">
                    <label class="control-label">Konten Article <small>(wajib diisi)</small></label>
                    <div class="alert alert-danger control-label" style="display:none">
                        <strong>Isi Artikel tidak boleh kosong</strong>
                    </div>
                    <textarea class="form-control ckeditor" id="editor" placeholder="" rows="35" name="remark"></textarea>
                </div>
            </form>

        </div>

    </div>

    <div class="card-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-fill btn-rose" id="saveArticleData">Simpan</button>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>