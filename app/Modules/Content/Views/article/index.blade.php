@extends('Dts::layouts.dts')

@section('title','Artikel')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-info rpt input-article">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
                Tambah Artikel
            </button>
        </div>
        {{--<div class="col-md-3 pull-right">--}}
            {{--<div class="input-group">--}}
                {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">--}}
                        {{--Search...--}}
                    {{--</label>--}}
                    {{--<input id="searchArticle" name="firstname" type="text" class="form-control">--}}
                {{--</div>--}}
                {{--<a href="javascript:void(0)" id="articleSearch" class="input-group-addon">--}}
                    {{--<i class="material-icons">search</i>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title"></h4>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Judul</th>
                                        <th>Status Publikasi</th>
                                        <th>Tanggal Dibuat</th>
                                        <th class="disabled-sorting text-right">Tindakan</th>
                                    </tr>
                                    </thead>
                                    {{--<tfoot>--}}
                                    {{--<tr>--}}
                                    {{--<th>Name</th>--}}
                                    {{--<th>Position</th>--}}
                                    {{--<th>Office</th>--}}
                                    {{--<th>Age</th>--}}
                                    {{--<th>Start date</th>--}}
                                    {{--<th class="text-right">Actions</th>--}}
                                    {{--</tr>--}}
                                    {{--</tfoot>--}}
                                    <tbody id="ListArticles">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
    <script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.datatables.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/additional-methods.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function() {

            $("#articleSearch").on('click', function () {
                var searchid = $("#searchArticle").val();
                GetSearchArticle(searchid)
                return false;
            });
            $('#searchArticle').bind("enterKey",function(e){
                var searchid = $("#searchArticle").val();
                GetSearchArticle(searchid)

            });
            $('#searchArticle').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
                if (!this.value) {
                    GetListArticles()
                }
            });
        });


        GetListArticles()

        function GetListArticles()
        {
            console.log('Start')
            $.ajax({
                type: 'GET',
                url: '{{route('all-articles')}}',
                global:false,
                success: function (data) {
                    var $AllArticles= $('#ListArticles');
                    $AllArticles.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllArticles.append('' +
                            '<tr>'+
                            '<td>'+data[i].title+'</td>'+
                            '<td>'+data[i].status+'</td>'+
                            '<td>'+data[i].created_date+'</td>'+
                            '<td class="text-right">'+
                            '<a href="#" class="btn btn-simple btn-info btn-icon like view-article" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                            '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-article" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                            '<a href="#" class="btn btn-simple btn-danger btn-icon remove article-delete" data-id="'+data[i].id+'" data-enabled="'+data[i].is_enabled+'" ><i class="material-icons">close</i></a>'+
                            '</td>'+
                            '</tr>')
                    }
                    $('#datatables').DataTable({
                        pageLength: 10,
                        bLengthChange: false,
                        responsive: true,
                        language: {
                            search: "_INPUT_",
                            searchPlaceholder: "Cari data",
                        },
                    })
                }
            });
        }

        function GetSearchArticle(searchid)
        {
            $.ajax({
                type: 'GET',
                url: '{{url('/searchArticle')}}',
                data: {TextId:searchid},
                global:false,
                success: function (data) {
                    var $AllArticles = $('#ListArticles');
                    $AllArticles.empty();
                    if(data == ''){
                        $AllArticles.append('<div class="col-md-12">' +'<h2>Data tidak ada</h2>'+'</div>');
                    }else{
                        for (var i = 0; i < data.length; i++) {
                            $AllArticles.append('' +
                                '<tr>'+
                                '<td>'+data[i].title+'</td>'+
                                '<td>'+data[i].status+'</td>'+
                                '<td>'+data[i].created_date+'</td>'+
                                '<td class="text-right">'+
                                '<a href="#" class="btn btn-simple btn-info btn-icon like view-article" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                                '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-article" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                                '<a href="#" class="btn btn-simple btn-danger btn-icon remove article-delete" data-id="'+data[i].id+'" data-enabled="'+data[i].is_enabled+'" ><i class="material-icons">close</i></a>'+
                                '</td>'+
                                '</tr>')
                        }
                    }

                }
            });
        }

        $('#rp').slideReveal({
            // trigger: $(".rpt"),
            position: "right",
            push: false,
            overlay: true,
            zIndex:1300,
        });
        $('.ckeditor').each(function(){
            CKEDITOR.replace( 'editor');
        });
        $('#rp').delegate('.rp-close','click',function() {
            $('#rp').slideReveal("hide")
        })


        $(document).delegate('.input-article', 'click', function(e) {
            if($(this).attr('title') == 'Edit'){
                $('.loading').show()
                $.ajax({
                    type: 'GET',
                    url: '{{route('article-edit')}}',
                    data:{idArticle:$(this).data('id')},
                    global:false,
                    success: function (data) {
                        $('#rp').empty().load("{{ route('article-input') }}", function(){
                            CKEDITOR.replace('editor');
                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()

//                            $('#SelectKota').on('change', function() {
//                                $('#selectRegion option[value='+data['0'].regionId+']').attr('selected','selected');
//                            });
                            var scr = 'storage/'+data['0'].img_name;
                            $('.titleForm').empty().append('Ubah Artikel ')
                            $("input[name='TypeSave']").val('EditSave')
                            $("input[name='id']").val(data['0'].id)
                            $("input[name='title']").val(data['0'].title)
                            $("input[name='is_enabled']").val('0')
                            $("textarea.ckeditor").val(data['0'].remark);
                            $('#IMS').attr("src", scr);
                            $('#Uploads').data('old', data['0'].img_name )
                            $('#rp').slideReveal("show")
                        })
                    }
                });

            }else{
                $('.loading').show()
                $('#rp').empty().load("{{ route('article-input') }}", function(){
                    CKEDITOR.replace('editor');
                    $('.card-content-overflow').perfectScrollbar();
                    $('.loading').hide()
                    $('#rp').slideReveal("show")
                })
            }
        })
        $(document).delegate('.view-article', 'click', function(e) {
            // console.log('klik')
            $('.loading').show()
            $.ajax({
                type: 'GET',
                url: '{{route('article-edit')}}',
                data:{idArticle:$(this).data('id')},
                global:false,
                success: function (data) {
                    $('#c-modal').empty().load("{{ route('view-article') }}", function(){
                        CKEDITOR.replace('editor2');
                        $('.loading').hide()
                        var scr = 'storage/'+data['0'].img_name;
                        $("input[name='id']").val(data['0'].id)
                        $("input[name='title']").val(data['0'].title)
                        $("input[name='is_enabled']").val(data['0'].is_enabled)
                        $('#created_date').empty().append(data['0'].created_date)
                        $("textarea.ckeditor").val(data['0'].remark);
                        $('#IM').attr("src", scr);

                        if($("input[name='is_enabled']").val() == 0)
                        {
                            $('.publish').css('display','block')
                            $('.unpublish').css('display','none')
                        }
                        else
                        {
                            $('.publish').css('display','none')
                            $('.unpublish').css('display','block')
                        }

                        $('#modal').modal('show')

                        $('#modal').delegate('.modal-close','click',function() {
                            $('#modal').modal("hide")
                        })

                    })
                }
            });
        })
        $(document).delegate('.publish', 'click', function() {
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    id: $("input[name='id").val(),
                }
                $.post("{{route('publish-article') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            });
        });

        $(document).delegate('.unpublish', 'click', function() {
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    id: $("input[name='id").val(),
                }
                $.post("{{route('unpublish-article') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            });
        });

        $(document).delegate('.article-delete', 'click', function() {

            var ids = $(this).data('id')
            var published = $(this).data('enabled')

            if(published == 0)
            {
                swal({
                    title: 'Apakah Anda Yakin?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Batal'
                }).then(function () {
                    // debugger;
                    swal('Silahkan tunggu')
                    swal.showLoading()
                    data = {id: ids}
                    $.post("{{route('delete-article') }}", data, function (r) {
                        swal(
                            "Berhasil",
                            "",
                            "success"
                        ).then(function () {
                            location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                        })
                    }, 'json');
                });
            }
            else
            {
                swal({
                    title: 'Data masih digunakan',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonText: 'Ok',
                }).then(function () {
                    location.reload();
                })
            }
        });

        $(document).delegate('#saveArticleData', 'click', function(e) {
            e.preventDefault();
            var $validator = $('.card-content-overflow form').validate({
                rules: {
                    title: {
                        required: true,
                        contentSpecialCharacters: true,
                        minlength: 3,
                    },
                    remark: {
                        required: true,
                        minlength: 3,
                    },

                },

                errorPlacement: function (error, element) {
                    $(element).parent('div').addClass('has-error');
                }
            });
            var messageLength = CKEDITOR.instances['editor'].getData().replace(/<[^>]*>/gi, '').length;
            var $valid = $('.card-content-overflow form').valid();
            if (!$valid || !messageLength) {
                $validator.focusInvalid();
                $('.alert-danger').show();
                setTimeout(function () {
                    $(".alert-danger").slideUp(500);
                }, 2000);
                return false;
            }


            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()

                var oldimg = $("#Uploads").data('old');
                var img = $("#FormArticle").find('#Uploads');
                var file_data = $(img).prop("files")[0];
                var form_data = new FormData();
                form_data.append("file", file_data)
                form_data.append("nm", '')
                form_data.append("dir", 'article')
                form_data.append("w", 800)
                form_data.append("h", 600)

                $.ajax({
                    url: "{{route('content-upload') }}",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (p) {
                        if (!p.p1) {
                            if (oldimg != p.p1) {
                                p.p1 = oldimg;
                            }
                        }

                        var data = {
                            a: $("input[name='TypeSave").val(),
                            id: $("input[name='id").val(),
                            title: $("input[name='title").val(),
                            is_enabled: $("input[name='is_enabled").val(),
                            remark: CKEDITOR.instances['editor'].getData(),
                            img: p.p1
                        };

                        $.post("{{route('post-article') }}", data, function (r) {
                            swal(
                                'Berhasil',
                                '',
                                'success'
                            ).then(function () {
                                $('#rp').empty().slideReveal("hide")
                                location.reload()
                                // GetListArticles()
                            })
                        }, 'json').fail(function (response) {
                            console.log(response)
                            swal(
                                'Terjadi Kesalahan!',
                                //response.responseText,
                                'Mohon Dicek Kembali',
                                'error'
                            )
                        })
                    }
                })
            })
        })

    </script>

@endsection