<?php namespace App\Modules\Content\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Input;
use DB;
use Image;
use Storage;
use Excel;

class ContentController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    function uploadAudio()
    {

        $file = Input::File('file');

        // if not exist, return empty
        if (!$file) {
            $p="";

            return $p;
        }

        // random filename
        $fn=$file->hashName();
        $path = Input::get('dir')."/".$fn;

        Storage::put(Input::get('dir'), $file);

        $p=$path;

        return $p;
    }

    function uploadfile()
    {

        $file = Input::file('file');
        // check if image exist
        // if not exist, return empty
        if (!$file) {
            $p=[
                "p1"=> ""
            ];
            return $p;
        }

        // random filename
        $fn=$file->hashName();
        $path = Input::get('dir')."/".$fn;
        $path2 = Input::get('dir')."/thumb/".$fn;
        $image = Image::make($file);

        // resize image
        if (Input::get('w')=="auto" && Input::get('h')=="auto") {
            // if w&h auto, dont set size
        } else if (Input::get('w')!="auto" && Input::get('h')!="auto"){
            // if w&h not auto, set size
            $image->fit(Input::get('w'), Input::get('h'));
        } else if (Input::get('h')=="auto"){
            // if h auto, set w
            $image->widen(Input::get('w'), function ($constraint) {
                $constraint->upsize();
            });
        } else if (Input::get('w')=="auto"){
            // if w auto, set h
            $image->heighten(Input::get('h'), function ($constraint) {
                $constraint->upsize();
            });
        }
        Storage::put($path, (string) $image->encode());

        if (Input::get('tmb')) {
            if (Input::get('w2')=="auto" && Input::get('h2')=="auto") {
                // if w&h auto, dont set size
            } else if (Input::get('w2')!="auto" && Input::get('h2')!="auto"){
                // if w&h not auto, set size
                $image->fit(Input::get('w2'), Input::get('h2'));
            } else if (Input::get('h2')=="auto"){
                // if h auto, set w
                $image->widen(Input::get('w2'), function ($constraint) {
                    $constraint->upsize();
                });
            } else if (Input::get('w2')=="auto"){
                // if h auto, set w
                $image->heighten(Input::get('h2'), function ($constraint) {
                    $constraint->upsize();
                });
            }
            Storage::put($path2, (string) $image->encode());
        }

        $p=[
            "p1"=> $path,
            "p2"=> $path2,
        ];

        return $p;
    }


	// News
    public function NewsIndex()
    {
        return view('Content::news.index');
    }

    public function AllNews()
    {
        $News = DB::select('SELECT * , IF(db_ibadah.content_news.is_enabled = 1, \'Ya\',\'Tidak\') as status
                              FROM db_ibadah.content_news
                              WHERE IsFlag = 0');

        return $News;
    }

    function SearchNews()
    {
        $keywordNews= $_GET['TextId'];
        $GetFilteredNews= DB::select('SELECT *, IF(db_ibadah.content_news.is_enabled = 1, \'Ya\',\'Tidak\') as status 
                                      FROM db_ibadah.content_news
									  WHERE IsFlag = 0 AND title LIKE "%'.$keywordNews.'%"');
        return $GetFilteredNews;
    }

    public function EditNews()
    {
        $idNews= $_GET['idNews'];
        $EditNews= DB::select('SELECT * FROM db_ibadah.content_news
                                  WHERE id = ?',[$idNews]);

        return $EditNews;
    }

    public function InputNews()
    {
        return view('Content::news.input');
    }

    public function newsView()
    {
        return view('Content::news.view');
    }

    function publishNews()
    {

        DB::table('db_ibadah.content_news')->where('id', Input::get('id'))->update([
            'is_enabled' => 1,
        ]);
        return json_encode(Input::all());

    }

    function unpublishNews()
    {

        DB::table('db_ibadah.content_news')->where('id', Input::get('id'))->update([
            'is_enabled' => 0,
        ]);
        return json_encode(Input::all());

    }

    function deleteNews()
    {

        DB::table('db_ibadah.content_news')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
        ]);
        return json_encode(Input::all());

    }

    public function PostNews()
    {
        if(Input::get('a') == 'SaveNews'){
            DB::table('db_ibadah.content_news')->insert([
                'title' => Input::get('title'),
                'source' => Input::get('source'),
                'remark' => Input::get('remark'),
                'created_date' => Carbon::now(),
                'is_enabled' => Input::get('is_enabled'),
                'img_name' => Input::get('img'),

            ]);
            return json_encode(Input::all());
        }else{
            DB::table('db_ibadah.content_news')->where('id', Input::get('id'))->update([
                'title' => Input::get('title'),
                'source' => Input::get('source'),
                'remark' => Input::get('remark'),
                'created_date' => Carbon::now(),
                'is_enabled' => 0,
                'img_name' => Input::get('img'),

            ]);
            return json_encode(Input::all());
        }
    }

    //Tausiyah

    public function TausiyahIndex()
    {
        return view('Content::tausiyah.index');
    }

    public function AllTausiyah()
    {
        $Tausiyah= DB::select('SELECT *, IF(db_ibadah.content_tausiyah.is_enabled = 1, \'Terbit\',\'Tidak\') as status
                                FROM db_ibadah.content_tausiyah
                                WHERE IsFlag = 0');

        return $Tausiyah;
    }

    function SearchTausiyah()
    {
        $keywordTausiyah= $_GET['TextId'];
        $GetFilteredTausiyah= DB::select('SELECT *, IF(db_ibadah.content_tausiyah.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status 
                                      FROM db_ibadah.content_tausiyah
									  WHERE IsFlag = 0 AND title LIKE "%'.$keywordTausiyah.'%"');
        return $GetFilteredTausiyah;
    }

    public function EditTausiyah()
    {
        $idTausiyah= $_GET['idTausiyah'];
        $EditTausiyah= DB::select('SELECT * FROM db_ibadah.content_tausiyah
                                  WHERE id = ?',[$idTausiyah]);

        return $EditTausiyah;
    }

    public function InputTausiyah()
    {
        return view('Content::tausiyah.input');
    }

    public function tausiyahView()
    {
        return view('Content::tausiyah.view');
    }

    function publishTausiyah()
    {

        DB::table('db_ibadah.content_tausiyah')->where('id', Input::get('id'))->update([
            'is_enabled' => 1,
        ]);
        return json_encode(Input::all());

    }

    function unpublishTausiyah()
    {

        DB::table('db_ibadah.content_tausiyah')->where('id', Input::get('id'))->update([
            'is_enabled' => 0,
        ]);
        return json_encode(Input::all());

    }

    function deleteTausiyah()
    {

        DB::table('db_ibadah.content_tausiyah')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
        ]);
        return json_encode(Input::all());

    }

    public function PostTausiyah()
    {
        if(Input::get('a') == 'SaveTausiyah'){
            DB::table('db_ibadah.content_tausiyah')->insert([
                'title' => Input::get('title'),
                'is_enabled' => Input::get('is_enabled'),
                'audio_name' => Input::get('file'),
                'created_date' => Carbon::now(),

            ]);
            return json_encode(Input::all());
        }else{
            DB::table('db_ibadah.content_tausiyah')->where('id', Input::get('id'))->update([
                'title' => Input::get('title'),
                'is_enabled' => 0,
                'audio_name' => Input::get('file'),
                'created_date' => Carbon::now(),

            ]);
            return json_encode(Input::all());
        }
    }

    //Doa

    public function DoaIndex()
    {
        return view('Content::doa.index');
    }

    public function AllDoa()
    {
        $Doa= DB::select('SELECT *, IF(db_ibadah.content_prayer.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status 
                            FROM db_ibadah.content_prayer
                            WHERE IsFlag = 0');

        return $Doa;
    }

    function SearchDoa()
    {
        $keywordDoa = $_GET['TextId'];
        $GetFilteredDoa= DB::select('SELECT *, IF(db_ibadah.content_prayer.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status 
                                      FROM db_ibadah.content_prayer
									  WHERE IsFlag = 0 AND title LIKE "%'.$keywordDoa.'%"');
        return $GetFilteredDoa;
    }

    public function EditDoa()
    {
        $idDoa= $_GET['idDoa'];
        $EditDoa= DB::select('SELECT * FROM db_ibadah.content_prayer
                                  WHERE id = ?',[$idDoa]);

        return $EditDoa
            ;
    }

    public function InputDoa()
    {
        return view('Content::doa.input');
    }

    public function doaView()
    {
        return view('Content::doa.view');
    }

    function publishDoa()
    {

        DB::table('db_ibadah.content_prayer')->where('id', Input::get('id'))->update([
            'is_enabled' => 1,
        ]);
        return json_encode(Input::all());

    }

    function unpublishDoa()
    {

        DB::table('db_ibadah.content_prayer')->where('id', Input::get('id'))->update([
            'is_enabled' => 0,
        ]);
        return json_encode(Input::all());

    }

    public function PostDoa()
    {
        if(Input::get('a') == 'SaveDoa'){
            DB::table('db_ibadah.content_prayer')->insert([
                'title' => Input::get('title'),
                'type_id' => Input::get('type_id'),
                'img_1' => Input::get('img1'),
                'created_date' => Carbon::now(),
                'is_enabled' => Input::get('is_enabled'),
            ]);
            return json_encode(Input::all());
        }else{
            DB::table('db_ibadah.content_prayer')->where('id', Input::get('id'))->update([
                'title' => Input::get('title'),
                'type_id' => Input::get('type_id'),
                'img_1' => Input::get('img1'),
                'created_date' => Carbon::now(),
                'is_enabled' => 0,
            ]);
            return json_encode(Input::all());
        }
    }

    function deletePrayer()
    {

        DB::table('db_ibadah.content_prayer')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
        ]);
        return json_encode(Input::all());

    }

    //Guide

    public function GuideIndex()
    {
        return view('Content::guide.index');
    }

    public function AllGuide()
    {
        $Guides= DB::select('SELECT *, IF(db_ibadah.content_guide.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status
                              FROM db_ibadah.content_guide
                              WHERE IsFlag = 0');

        return $Guides;
    }

    function SearchGuide()
    {
        $keywordGuide = $_GET['TextId'];
        $GetFilteredGuide = DB::select('SELECT *, IF(db_ibadah.content_guide.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status 
                                      FROM db_ibadah.content_guide
									  WHERE IsFlag = 0 AND title LIKE "%'.$keywordGuide.'%"');
        return $GetFilteredGuide;
    }

    public function EditGuide()
    {
        $idGuide = $_GET['idGuide'];
        $EditGuide= DB::select('SELECT * FROM db_ibadah.content_guide
                                WHERE id = ?',[$idGuide]);

        return $EditGuide;
    }

    public function InputGuide()
    {
        return view('Content::guide.input');
    }

    public function PostGuide()
    {
        if(Input::get('a') == 'SaveGuide'){
            DB::table('db_ibadah.content_guide')->insert([
                'title' => Input::get('title'),
                'remark' => Input::get('remark'),
                'created_date' => Carbon::now(),
                'is_enabled' => Input::get('is_enabled'),

            ]);
            return json_encode(Input::all());
        }else{
            DB::table('db_ibadah.content_guide')->where('id', Input::get('id'))->update([
                'title' => Input::get('title'),
                'remark' => Input::get('remark'),
                'created_date' => Carbon::now(),
                'is_enabled' => 0,

            ]);
            return json_encode(Input::all());
        }
    }

    function guideView()
    {
        return view('Content::guide.view');
    }

    function publishGuide()
    {

        DB::table('db_ibadah.content_guide')->where('id', Input::get('id'))->update([
            'is_enabled' => 1,
        ]);
        return json_encode(Input::all());

    }

    function unpublishGuide()
    {

        DB::table('db_ibadah.content_guide')->where('id', Input::get('id'))->update([
            'is_enabled' => 0,
        ]);
        return json_encode(Input::all());

    }

    function deleteGuide()
    {

        DB::table('db_ibadah.content_guide')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
        ]);
        return json_encode(Input::all());

    }

    //Tips

    public function TipsIndex()
    {
        return view('Content::tips.index');
    }

    public function AllTips()
    {
        $Guides= DB::select('SELECT *, IF(db_ibadah.content_tips.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status 
                              FROM db_ibadah.content_tips
                              WHERE IsFlag = 0');

        return $Guides;
    }

    function SearchTips()
    {
        $keywordTips= $_GET['TextId'];
        $GetFilteredTips= DB::select('SELECT *, IF(db_ibadah.content_tips.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status 
                                      FROM db_ibadah.content_tips
									  WHERE IsFlag = 0 AND title LIKE "%'.$keywordTips.'%"');
        return $GetFilteredTips;
    }

    public function EditTips()
    {
        $idTips= $_GET['idTips'];
        $EditTips= DB::select('SELECT * FROM db_ibadah.content_tips
                                WHERE id = ?',[$idTips]);

        return $EditTips;
    }

    public function InputTips()
    {
        return view('Content::tips.input');
    }

    public function viewTips()
    {
        return view('Content::tips.view');
    }

    function publishTips()
    {

        DB::table('db_ibadah.content_tips')->where('id', Input::get('id'))->update([
            'is_enabled' => 1,
        ]);
        return json_encode(Input::all());

    }

    function unpublishTips()
    {

        DB::table('db_ibadah.content_tips')->where('id', Input::get('id'))->update([
            'is_enabled' => 0,
        ]);
        return json_encode(Input::all());

    }

    function deleteTips()
    {

        DB::table('db_ibadah.content_tips')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
        ]);
        return json_encode(Input::all());

    }

    public function PostTips()
    {
        if(Input::get('a') == 'SaveTips'){
            DB::table('db_ibadah.content_tips')->insert([
                'title' => Input::get('title'),
                'remark' => Input::get('remark'),
                'created_date' => Carbon::now(),
                'is_enabled' => Input::get('is_enabled'),


            ]);
            return json_encode(Input::all());
        }else{
            DB::table('db_ibadah.content_tips')->where('id', Input::get('id'))->update([
                'title' => Input::get('title'),
                'remark' => Input::get('remark'),
                'created_date' => Carbon::now(),
                'is_enabled' => 0,

            ]);
            return json_encode(Input::all());
        }
    }

    //Article

    public function ArticleIndex()
    {
        return view('Content::article.index');
    }

    public function AllArticles()
    {
        $Articles= DB::select('SELECT *, IF(db_ibadah.content_article.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status
                                FROM db_ibadah.content_article 
                                WHERE IsFlag = 0');

        return $Articles;
    }

    function SearchArticle()
    {
        $keywordArticle= $_GET['TextId'];
        $GetFilteredArticle= DB::select('SELECT *, IF(db_ibadah.content_article.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status 
                                      FROM db_ibadah.content_article
									  WHERE IsFlag = 0 AND title LIKE "%'.$keywordArticle.'%"');
        return $GetFilteredArticle;
    }

    public function EditArticle()
    {
        $idArticle= $_GET['idArticle'];
        $EditArticle= DB::select('SELECT * FROM db_ibadah.content_article
                                  WHERE id = ?',[$idArticle]);

        return $EditArticle;
    }

    public function viewArticle()
    {
        return view('Content::article.view');
    }

    function publishArticle()
    {

        DB::table('db_ibadah.content_article')->where('id', Input::get('id'))->update([
            'is_enabled' => 1,
        ]);
        return json_encode(Input::all());

    }

    function unpublishArticle()
    {

        DB::table('db_ibadah.content_article')->where('id', Input::get('id'))->update([
            'is_enabled' => 0,
        ]);
        return json_encode(Input::all());

    }

    function deleteArticle()
    {

        DB::table('db_ibadah.content_article')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
        ]);
        return json_encode(Input::all());

    }

    public function InputArticle()
    {
        return view('Content::article.input');
    }

    public function postArticle()
    {
        if(Input::get('a') == 'SaveArticle'){
            DB::table('db_ibadah.content_article')->insert([
                'title' => Input::get('title'),
                'remark' => Input::get('remark'),
                'is_enabled' => Input::get('is_enabled'),
                'img_name' => Input::get('img'),
                'created_date' => Carbon::now(),
            ]);
            return json_encode(Input::all());
        }else{
            DB::table('db_ibadah.content_article')->where('id', Input::get('id'))->update([
                'title' => Input::get('title'),
                'remark' => Input::get('remark'),
                'is_enabled' => 0,
                'img_name' => Input::get('img'),
                'created_date' => Carbon::now(),
            ]);
            return json_encode(Input::all());
        }
    }


    //Glossary

    public function GlossaryIndex()
    {
        return view('Content::glossary.index');
    }

    public function AllGlossary()
    {
        $Glossaries= DB::select('SELECT *, IF(db_ibadah.content_glossary.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status 
                                  FROM db_ibadah.content_glossary
                                  WHERE IsFlag = 0');

        return $Glossaries;
    }

    function SearchGlossary()
    {
        $keywordGlossary = $_GET['keywordGlo'];
        $GetFilteredGlossary= DB::select('SELECT *, IF(db_ibadah.content_glossary.is_enabled = 1, \'Terbit\',\'Tidak Terbit\') as status 
                                      FROM db_ibadah.content_glossary
									  WHERE IsFlag = 0 AND title LIKE "%'.$keywordGlossary.'%"');
        return $GetFilteredGlossary;
    }

    public function EditGlossary()
    {
        $idGlossary= $_GET['idGlossary'];
        $EditGlossary= DB::select('SELECT * FROM db_ibadah.content_glossary
                                WHERE id = ?',[$idGlossary]);

        return $EditGlossary;
    }

    public function InputGlossary()
    {
        return view('Content::glossary.input');
    }

    public function PostGlossary()
    {
        if(Input::get('a') == 'SaveGlossary'){
            DB::table('db_ibadah.content_glossary')->insert([
                'title' => Input::get('title'),
                'remark' => Input::get('remark'),
                'created_date' => Carbon::now(),
                'is_enabled' => Input::get('is_enabled'),

            ]);
            return json_encode(Input::all());
        }else{
            DB::table('db_ibadah.content_glossary')->where('id', Input::get('id'))->update([
                'title' => Input::get('title'),
                'remark' => Input::get('remark'),
                'created_date' => Carbon::now(),
                'is_enabled' => Input::get('is_enabled'),

            ]);
            return json_encode(Input::all());
        }
    }

    function glossaryView()
    {
        return view('Content::glossary.view');
    }

    function publishGlossary()
    {

        DB::table('db_ibadah.content_glossary')->where('id', Input::get('id'))->update([
            'is_enabled' => 1,
        ]);
        return json_encode(Input::all());

    }

    function unpublishGlossary()
    {

        DB::table('db_ibadah.content_glossary')->where('id', Input::get('id'))->update([
            'is_enabled' => 0,
        ]);
        return json_encode(Input::all());

    }

    function deleteGlossary()
    {

        DB::table('db_ibadah.content_glossary')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
        ]);
        return json_encode(Input::all());

    }


}
