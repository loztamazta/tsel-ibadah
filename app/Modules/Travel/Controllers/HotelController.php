<?php namespace App\Modules\Travel\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Input;
use DB;
use Image;
use Storage;
use Excel;

class HotelController extends Controller {
	/**
		hotel
	**/
	public function hotel()
	{
        $travels=DB::select('SELECT idkbih i, kbihname n FROM db_ibadah.mt_kbih where IsFlag=0');

		return view("Travel::hotel.index",compact('travels'));
	}

	public function AllHotels()
    {

        $Hotels= DB::select('SELECT A.id as hotel_id, A.name as hotel_name, C.name as kota, latitude, longitude, address, IsFlag, ChangeBy, A.img_name AS img_name
                             FROM db_ibadah.place AS A
                             JOIN db_ibadah.place_type AS B ON A.type_id = B.id
                             JOIN db_ibadah.place_region AS C ON A.region_id = C.id
                             WHERE A.type_id = 1 AND IsFlag = 0');

//		$Hotels = DB::select('SELECT * FROM db_ibadah.place
//							  WHERE type_id = 1 AND IsFlag = 0');

		return $Hotels;
	}

    public function SearchHotel()
    {
        $keywordHotel= $_GET['TextId'];

        $Hotels= DB::select('SELECT A.id as hotel_id, A.name as hotel_name, C.name as kota, latitude, longitude, address, IsFlag, ChangeBy, A.img_name AS img_name
                             FROM db_ibadah.place AS A
                             JOIN db_ibadah.place_type AS B ON A.type_id = B.id
                             JOIN db_ibadah.place_region AS C ON A.region_id = C.id
                             WHERE A.type_id = 1 AND IsFlag = 0 AND A.name LIKE ?',["%$keywordHotel%"]);

//		$Hotels = DB::select('SELECT * FROM db_ibadah.place
//							  WHERE type_id = 1 AND IsFlag = 0');

        return $Hotels;
    }


	function hotelInput()
	{
		return view("Travel::hotel.input");
	}

    function postHotel()
    {
   		$kbihid=(Input::get('kbihid') ? Input::get('kbihid') : Auth::user()->t);

        if (Input::get('a') == 'SaveHotel') {
            DB::table('db_ibadah.place')->insert([
                'name' => Input::get('name'),
                'img_name' => Input::get('img'),
                'type_id' => Input::get('type_id'),
                'latitude' => Input::get('latitude'),
                'longitude' => Input::get('longitude'),
                'region_id' => Input::get('region_id'),
                'address' => Input::get('address'),
                'IsFlag' => Input::get('IsFlag'),
                'KbihId' => $kbihid,
            ]);
            return json_encode(Input::all());
        } else {
            DB::table('db_ibadah.place')->where('id', Input::get('IdHotel'))->update([
                'name' => Input::get('name'),
                'img_name' => Input::get('img'),
                'type_id' => Input::get('type_id'),
                'latitude' => Input::get('latitude'),
                'longitude' => Input::get('longitude'),
                'region_id' => Input::get('region_id'),
                'address' => Input::get('address'),

            ]);
            return json_encode(Input::all());
        };
    }

	function editHotel()
	{
        $idHotel = $_GET['idHotel'];
        $EditHotels = DB::select('SELECT A.id as hotel_id, A.name as hotel_name, A.region_id, C.name as kota, latitude, longitude, address, IsFlag, ChangeBy, A.img_name
	                             FROM db_ibadah.place AS A
	                             JOIN db_ibadah.place_type AS B ON A.type_id = B.id
	                             JOIN db_ibadah.place_region AS C ON A.region_id = C.id
	                             WHERE A.IsFlag = 0 AND A.id = ?',[$idHotel]);

        return $EditHotels;
	}

	function hotelView()
	{
        return view('Travel::hotel.view');

	}
    
	function deleteHotel()
	{

  //       $CheckData = DB::select('SELECT *, DATE_ADD(DateofDepa, INTERVAL IntervalDate DAY) AS DatePaket FROM db_ibadah.mt_pack_kbih AS A
  //                                   JOIN db_ibadah.itinerary_package AS B ON A.IdPackege = B.id
  //                               WHERE HotelMekkahId = ? OR HotelMadinahId = ? OR HotelJeddahId = ?',[Input::get('id'),Input::get('id'),Input::get('id')]);
		// if(count($CheckData)>0){
		//     if($CheckData[0]->DatePaket <= Carbon::now()->toDateString()){
  //               DB::table('db_ibadah.place')->where('id', Input::get('id'))->update([
  //                   'IsFlag' => 1,
  //                   'ChangeBy' => Auth::user()->id,
  //               ]);
  //               $Respond = 2;
  //           }else{
  //               $Respond = 1;
  //           }
  //       }
  //       else{
  //           DB::table('db_ibadah.place')->where('id', Input::get('id'))->update([
  //               'IsFlag' => 1,
  //               'ChangeBy' => Auth::user()->id,
  //           ]);
  //           $Respond = 2;
  //       }

        DB::table('db_ibadah.place')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
            'ChangeBy' => Auth::user()->id,
        ]);
        $Respond = 2;

		return json_encode($Respond);

	}

}
