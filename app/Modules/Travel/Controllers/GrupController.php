<?php 
namespace App\Modules\Travel\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Input;
use DB;
use Image;
use Storage;
use Excel;
// use App\Models\User;
// use App\Notifications\SendJamaahEmail;
use Mail;

class GrupController extends Controller {
	/**
		grup
	**/
	function grup()
	{
        $travels=DB::select('SELECT idkbih i, kbihname n FROM db_ibadah.mt_kbih where IsFlag=0');

		return view("Travel::grup.grup",compact('travels'));
	}
	function grupInput()
	{
		return view("Travel::grup._input");
	}
	function grupEdit()
	{
		return view("Travel::grup._edit");
	}
	function groupgetedit()
	{
	    $PackId = $_GET['IdPack'];
        $a = DB::select('SELECT A.*, B.IntervalDate, C.MtwFirstName, C.MtwLastName, B.name, D.name AS HM, E.name AS HME, F.name AS HMF 
                                FROM db_ibadah.mt_pack_kbih AS A 
                                    JOIN db_ibadah.itinerary_package AS B ON A.IdPackege = B.id
                                    JOIN db_ibadah.mt_mtw AS C ON A.StaffId = C.Idmtw
                                    LEFT JOIN db_ibadah.place AS D ON A.HotelMekkahId = D.id
                                    LEFT JOIN db_ibadah.place AS E ON A.HotelMadinahId = E.id
                                    LEFT JOIN db_ibadah.place AS F ON A.HotelJeddahId = F.id
                                WHERE IdPack = ?',[$PackId]);
        return $a;
	}
	function grupJamaahInput($idpack,$id)
	{
		$j=DB::select('SELECT * from db_ibadah.mt_jamaah where iduserapps=?', [$id]);
		if ($j) {
			$j=$j[0];
		}
		return view("Travel::grup._jamaahinput",compact('idpack','j'));
	}
	function grupJamaahView($idpack,$id)
	{
		$j=DB::select('SELECT * from db_ibadah.mt_jamaah where iduserapps=?', [$id]);
		if ($j) {
			$j=$j[0];
		}
		return view("Travel::grup.view_jamaah",compact('idpack','j'));
	}
	function detailjamaah()
	{
		return view("Travel::grup.view");
	}
	function grupDetail($id)
	{
	    $a = DB::select('SELECT A.*, B.IntervalDate, C.MtwFirstName, C.MtwLastName, B.name, D.name AS HM, E.name AS HME, F.name AS HMF 
                                FROM db_ibadah.mt_pack_kbih AS A 
                                    JOIN db_ibadah.itinerary_package AS B ON A.IdPackege = B.id
                                    JOIN db_ibadah.mt_mtw AS C ON A.StaffId = C.Idmtw
                                    LEFT JOIN db_ibadah.place AS D ON A.HotelMekkahId = D.id
                                    LEFT JOIN db_ibadah.place AS E ON A.HotelMadinahId = E.id
                                    LEFT JOIN db_ibadah.place AS F ON A.HotelJeddahId = F.id
                                WHERE IdPack = ?',[$id]);
		return view("Travel::grup.detail",compact('a'));
	}
	function grupData()
	{
   		$kbihid=(Input::get('kbihid') ? Input::get('kbihid') : Auth::user()->t);

		switch (Input::get('a')) {
			case 'save':
				$now=Carbon::now();
				$r=DB::insert("
					INSERT INTO db_ibadah.mt_pack_kbih
						(KbihId,IdPackege,PackName,HotelMekkahId,HotelMadinahId,HotelJeddahId,TotalPeople,DateofDepa,MaskapaiB,MaskapaiP,ChangeBy,ChangeDate,StaffId) 
					VALUES
						(?,?,?,?,?,?,?,?,?,?,?,?,?)
				",[$kbihid,Input::get('PilihPaket'),Input::get('PackName'),Input::get('PilihHMekkah'),Input::get('PilihHMadinah'),Input::get('PilihHJeddah'),0,Carbon::createFromFormat('m/d/Y',Input::get('DateofDepa'))->format('Y/m/d'),Input::get('MaskapaiB'),Input::get('MaskapaiP'),Auth::user()->name,$now,Input::get('PilihStaff')]);
				if ($r) {
					$r=DB::select("
						SELECT idpack FROM db_ibadah.mt_pack_kbih where PackName=? and ChangeBy=? and ChangeDate=? limit 1
					",[Input::get('PackName'),Auth::user()->name,$now])[0];
				}

				return json_encode($r);
				break;
			case 'edit':
                $now=Carbon::now();
				$r=DB::update("
					UPDATE db_ibadah.mt_pack_kbih
					SET
						IdPackege = ?,
						PackName = ?,
						HotelMekkahId =?,
						HotelMadinahId =?,
						HotelJeddahId =?,
						TotalPeople =?,
						DateofDepa =?,
						MaskapaiB =?,
						MaskapaiP =?,
						ChangeBy =?,
						ChangeDate =?,
						StaffId =?
					WHERE 
						IdPack = ?
				",[Input::get('PilihPakets'),Input::get('PackNames'),Input::get('PilihHMekkahs'),Input::get('PilihHMadinahs'),Input::get('PilihHJeddahs'),0,Input::get('DateofDepas'),Input::get('MaskapaiBs'),Input::get('MaskapaiPs'),Auth::user()->name,$now,Input::get('PilihStaffs'),Input::get('idpack')]);

				return $r;
				break;

			case 'getjamaah':
				$r=DB::select("
					SELECT 
						FirstName,LastName,ThirdName,Gender,MobileNo,Address,DateOfBirth,PassportNo,VisaNo,IdUser 
					FROM 
						db_ibadah.dt_user_apps as a
						join db_ibadah.mt_jamaah b
							on a.iduser=b.iduserapps
					where
						IdKbihGroup=?
				",[Input::get('b')]);

				return json_encode($r);
				break;

			case 'savejamaah':
				DB::beginTransaction();

				// set default password
				$p=substr(Input::get('firstname'), 0, 5).substr(Input::get('mobileno'), -5);
				$now=Carbon::now();
				// create user mobile
				$r=DB::insert("
					INSERT INTO db_ibadah.dt_user_apps
						(Username,Password,UserType,IdKbihGroup,ChangeBy,PasswordReal,ChangeDate)
					VALUES
						(?,?,?,?,?,?,?)
				",[Input::get('mobileno'),md5($p),1,Input::get('idpack'),Auth::user()->name,$p,$now]);
				// ambil id yg di-create
				$j=DB::select("
					SELECT iduser FROM db_ibadah.dt_user_apps where Username=? and ChangeBy=? and ChangeDate=? order by iduser desc limit 1
				",[Input::get('mobileno'),Auth::user()->name,$now])[0];
				// insert jamaah
				$r=DB::insert("
					INSERT INTO db_ibadah.mt_jamaah
						(iduserapps,title,email,mobileno,firstname,lastname,thirdname,address,gender,dateofbirth,placeofbirth,phoneno,idnumber,passportno,visano,kloterno,Img,ImgPassport,ImgVisa,changeby)
					VALUES
						(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
				",[$j->iduser,Input::get('title'),Input::get('email'),Input::get('mobileno'),Input::get('firstname'),Input::get('lastname'),Input::get('thirdname'),Input::get('address'),Input::get('title'),Input::get('dateofbirth'),Input::get('placeofbirth'),Input::get('phoneno'),Input::get('idnumber'),Input::get('passportno'),Input::get('visano'),Input::get('kloterno'),Input::get('Img'),Input::get('ImgPassport'),Input::get('ImgVisa'),Auth::user()->name]);
				// update total jamaah
				$r=DB::update("
					UPDATE 
						db_ibadah.mt_pack_kbih
					SET
						TotalPeople = (SELECT count(*) FROM db_ibadah.dt_user_apps where IdKbihGroup=?)
					WHERE 
						IdPack=?
				",[Input::get('idpack'),Input::get('idpack')]);

				$this->sendemail(Input::get('email'),Input::get('mobileno'),$p,Input::get('firstname'));

				DB::commit();

				return json_encode($r);
				break;

			case 'editjamaah':
				DB::beginTransaction();

				$r=DB::update("
					UPDATE db_ibadah.mt_jamaah
					SET
						Title = ?,
						Email = ?,
						MobileNo = ?,
						FirstName = ?,
						LastName = ?,
						ThirdName = ?,
						Address = ?,
						Gender = ?,
						DateOfBirth = ?,
						PlaceOfBirth = ?,
						PhoneNo = ?,
						IdNumber = ?,
						PassportNo = ?,
						VisaNo = ?,
						KloterNo = ?,
						Img = ?,
						ImgPassport = ?,
						ImgVisa = ?,
						ChangeBy = ?,
						ChangeDate = ?
					WHERE 
						iduserapps = ?
				",[Input::get('title'),Input::get('email'),Input::get('mobileno'),Input::get('firstname'),Input::get('lastname'),Input::get('thirdname'),Input::get('address'),Input::get('title'),Input::get('dateofbirth'),Input::get('placeofbirth'),Input::get('phoneno'),Input::get('idnumber'),Input::get('passportno'),Input::get('visano'),Input::get('kloterno'),Input::get('Img'),Input::get('ImgPassport'),Input::get('ImgVisa'),Auth::user()->name,Carbon::now(),Input::get('idjamaah')]);

				$r=DB::update("
					UPDATE db_ibadah.dt_user_apps
					SET
						Username = ?,
						ChangeBy = ?,
						ChangeDate = ?
					WHERE 
						IdUser = ?
				", [Input::get('mobileno'),Auth::user()->name,Carbon::now(),Input::get('idjamaah')]);

				DB::commit();

				return $r;
				break;
			case 'deletejamaah':
				$r=DB::delete('DELETE from db_ibadah.dt_user_apps where IdUser=?',[Input::get('id')]);
				$r=DB::delete('DELETE from db_ibadah.mt_jamaah where iduserapps=?',[Input::get('id')]);
				return $r;
				break;
			
			case 'loadS':
		        $GetStaff = DB::select('
					SELECT 
					    a.*,
					    coalesce(stat,0,1) stat
					FROM
					    db_ibadah.mt_mtw a
					    left join(
							SELECT 
								StaffId,
								(case when 
									? <= DATE_ADD(DateofDepa, INTERVAL IntervalDate DAY) 
									and DateofDepa <= DATE_ADD(?, INTERVAL (select IntervalDate from db_ibadah.itinerary_package where id = ?)  DAY)
					                then 1 else 0
								end) stat
							FROM
								db_ibadah.mt_pack_kbih b
								left JOIN db_ibadah.itinerary_package c 
									ON b.IdPackege = c.id
							where
								if((now() BETWEEN DateofDepa and DATE_ADD(DateofDepa, INTERVAL IntervalDate DAY)),1,0)=1    
					    ) b
							on a.Idmtw = b.StaffId
		        	WHERE 
		        		TravelId=?
		        		and a.del=0
		        	having
		        		stat=0
		        ',[Input::get('dt'),Input::get('dt'),Input::get('pkt'),$kbihid]);

		        return $GetStaff;
				break;

			case 'delGrup':
				$q=DB::update("
					UPDATE db_ibadah.mt_pack_kbih
					SET
						ChangeBy = ?,
						del = 1
					WHERE IdPack = ?
					",[Auth::user()->name,Input::get('id')]);

					return $q;
				break;
			default:
				return "invalid action";
				break;
		}
	}

	function grupExcel()
	{
		if(Input::hasFile('file')){
			$path = Input::file('file')->getRealPath();
			$data = Excel::load($path, function($reader) {})->get();

			DB::beginTransaction();

			if(!empty($data) && $data->count()){
				foreach ($data as $key => $value) {

					if ($value->mobileno) {
						// set default password
						$p=substr($value->firstname, 0, 5).substr($value->mobileno, -5);
						$now=Carbon::now();
						// create user mobile
						$r=DB::insert("
							INSERT INTO db_ibadah.dt_user_apps
								(Username,Password,UserType,IdKbihGroup,ChangeBy,PasswordReal,ChangeDate)
							VALUES
								(?,?,?,?,?,?,?)
						",[$value->mobileno,md5($p),1,Input::get('id'),Auth::user()->name,$p,$now]);
						// ambil id yg di-create
						$r=DB::select("
							SELECT iduser FROM db_ibadah.dt_user_apps where Username=? and ChangeBy=? and ChangeDate=? order by iduser desc limit 1
						",[$value->mobileno,Auth::user()->name,$now])[0];
						// insert jamaah
						$r=DB::insert("
							INSERT INTO db_ibadah.mt_jamaah
								(iduserapps,title,email,mobileno,firstname,lastname,thirdname,address,gender,dateofbirth,placeofbirth,phoneno,idnumber,passportno,visano,kloterno,changeby)
							VALUES
								(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
						",[$r->iduser,$value->title,$value->email,$value->mobileno,$value->firstname,$value->lastname,$value->thirdname,$value->address,$value->gender,$value->dateofbirth,$value->placeofbirth,$value->phoneno,$value->idnumber,$value->passportno,$value->visano,$value->kloterno,Auth::user()->name]);
						// update total jamaah
						$r=DB::update("
							UPDATE 
								db_ibadah.mt_pack_kbih
							SET
								TotalPeople = (SELECT count(*) FROM db_ibadah.dt_user_apps where IdKbihGroup=?)
							WHERE 
								IdPack=?
						",[Input::get('id'),Input::get('id')]);

						$this->sendemail($value->email,$value->mobileno,$p,$value->firstname);
					}
				}
				// if(!empty($insert)){
				// 	return json_encode($insert);
				// 	DB::table('items')->insert($insert);
				// 	dd('Insert Record successfully.');
				// }
			}

			DB::commit();

		}else{
			return 'no file found';
		}
	}

    function Get010()
    {
   		$kbihid=Auth::user()->t;
    	if (isset($_GET['kbihid'])) {
    		$kbihid=$_GET['kbihid'];
    	}

        $GetPlaceMe = DB::select('SELECT * FROM db_ibadah.place WHERE region_id = 1 AND type_id = 1 and IsFlag=0');
        $GetPlaceMa = DB::select('SELECT * FROM db_ibadah.place WHERE region_id = 2 AND type_id = 1 and IsFlag=0');
        $GetPlaceJd = DB::select('SELECT * FROM db_ibadah.place WHERE region_id = 3 AND type_id = 1 and IsFlag=0');
        $GetPaket = DB::select('SELECT * FROM db_ibadah.itinerary_package WHERE KbihId=? and IsFlag=0',[$kbihid]);
        $GetStaff = DB::select('SELECT * FROM db_ibadah.mt_mtw WHERE TravelId=?',[$kbihid]);

        return  response()->json(array('HotelMe' => $GetPlaceMe,'HotelMa' => $GetPlaceMa, 'HotelJd' => $GetPlaceJd, 'Paket' => $GetPaket,'Staff' => $GetStaff));
    }

    function Get011()
    {
        $IdPaket = $_GET['PaketId'];
        $GetTime = DB::select('SELECT * FROM db_ibadah.itinerary_detail WHERE package_id = ?',[$IdPaket]);

        return  response()->json(array('TimeKegiatan' => $GetTime));
    }

    function Get012()
    {
   		$kbihid=Auth::user()->t;
    	if (isset($_GET['kbihid'])) {
    		$kbihid=$_GET['kbihid'];
    	}

        // if (Auth::user()->hasRole(['travel'])) {
        //     $Where = 'WHERE A.KbihId = '.Auth::user()->t;
        // }else if(Auth::user()->hasRole(['sadmin'])){
        //     $Where ='';
        // }
        $GetKeber = DB::select('SELECT A.*, B.IntervalDate, C.*
                                FROM db_ibadah.mt_pack_kbih AS A 
                                    JOIN db_ibadah.itinerary_package AS B ON A.IdPackege = B.id
                                    JOIN db_ibadah.mt_mtw AS C ON A.StaffId = C.Idmtw
                                where A.KbihId=? and C.del=0 and A.del=0',[$kbihid]);

        return  $GetKeber;
    }


    function Get013()
    {
        $RombonganText = $_GET['TextId'];

        if (Auth::user()->hasRole(['travel'])) {
            $Where = 'WHERE A.KbihId = '.Auth::user()->t.' AND A.PackName LIKE "%'.$RombonganText.'%"';
        }else if(Auth::user()->hasRole(['sadmin'])){
            $Where ='WHERE A.PackName LIKE "%'.$RombonganText.'%"';
        }
        $GetKeber = DB::select('SELECT A.*, B.IntervalDate, C.MtwFirstName, C.MtwLastName,c.MtwImg
                                FROM db_ibadah.mt_pack_kbih AS A 
                                    JOIN db_ibadah.itinerary_package AS B ON A.IdPackege = B.id
                                    JOIN db_ibadah.mt_mtw AS C ON A.StaffId = C.Idmtw '.$Where);

        return  $GetKeber;
    }

    public function sendemail($e,$u,$p,$n1)
    {
        $param=[
        	'u'=>$u,
        	'p'=>$p,
        	'n1'=>$n1
        ];

        Mail::send('Travel::grup.email-jamaah', $param, function ($message) use ($e)
        {
        	$message->subject("Informasi Akun Telkomsel Ibadah");
            $message->from(config('mail.username'), 'Telkomsel Ibadah');
            $message->to($e);
        });

        // return response()->json(['message' => 'Request completed']);

    }

}
