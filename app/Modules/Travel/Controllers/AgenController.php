<?php namespace App\Modules\Travel\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Input;
use DB;
use Image;
use Storage;
use Excel;

class AgenController extends Controller {

	public function travelDetail()
	{

		return view("Travel::travel.detail");
	}

	public function travelEdit()
	{
		return view("Travel::travel.edit");
	}

	/**
		agen
	**/
	public function agen()
	{
		if (Auth::user()->hasRole(['admin','sadmin'])) {
			return view("Travel::agen.index");
		}
		if (Auth::user()->hasRole(['travel'])) {
			return view("Travel::agen.dashboard",compact('a'));
		}
	}

	function agenInput()
	{
		return view("Travel::agen.input");
	}

	function agenView()
	{
		return view("Travel::agen.view");
	}

	function agenDashboard($a)
	{

		return view("Travel::agen.dashboard",compact('a'));
	}

	function travelschedule()
    {
        $GetAllSchedule = DB::select('SELECT PackName as title, DateofDepa as start FROM db_ibadah.mt_pack_kbih WHERE KbihId = ?',[Auth::user()->t]);
        $GetAllCount = DB::select('SELECT 
                                        COUNT(FirstName) AS Counts
                                    FROM 
                                        db_ibadah.dt_user_apps as a
                                        join db_ibadah.mt_jamaah b
                                            on a.iduser=b.iduserapps
                                    where
                                        IdKbihGroup = ?',[Auth::user()->t]);
        $GetAllPendamping = DB::select('SELECT COUNT(*) AS Counts FROM db_ibadah.mt_mtw WHERE TravelId = ?',[Auth::user()->t]);
        return  response()->json(array('GetAllCount' => $GetAllCount,'GetKegiatan'=>$GetAllSchedule,'GetPendamping'=>$GetAllPendamping));
    }

    /**
    Get List All Travels
     **/
    function Get004()
    {
        $GetAllTravels = DB::select('SELECT A.*, B.name AS ProName, C.name AS  KabName, D.name AS CityName FROM db_ibadah.mt_kbih AS A
										LEFT JOIN db_ibadah.provinces AS B ON A.ProvinceId = B.id
										LEFT JOIN db_ibadah.regencies AS C ON A.KabId = C.id
										LEFT JOIN db_ibadah.districts AS D ON A.CityId = D.id
									  WHERE A.IsFlag = 0');
        return $GetAllTravels;
    }

    /**
    Get Detail Travels
     **/
    function Get005()
    {
        $IdTravel = $_GET['IdTravel'];
        $GetAllTravels = DB::select('SELECT A.*, B.name AS ProName, C.name AS  KabName, D.name AS CityName FROM db_ibadah.mt_kbih AS A
										LEFT JOIN db_ibadah.provinces AS B ON A.ProvinceId = B.id
										LEFT JOIN db_ibadah.regencies AS C ON A.KabId = C.id
										LEFT JOIN db_ibadah.districts AS D ON A.CityId = D.id
									  WHERE IsFlag = 0 AND IdKbih = ?',[$IdTravel]);
        return $GetAllTravels;
    }

    /**
    Edit Detail Travels
     **/
    function Get006()
    {
        $IdTravel = $_GET['IdTravel'];
        $EditTravels = DB::select('SELECT A.*, B.name AS ProName, C.name AS  KabName, D.name AS CityName FROM db_ibadah.mt_kbih AS A
										LEFT JOIN db_ibadah.provinces AS B ON A.ProvinceId = B.id
										LEFT JOIN db_ibadah.regencies AS C ON A.KabId = C.id
										LEFT JOIN db_ibadah.districts AS D ON A.CityId = D.id
									  WHERE IsFlag = 0 AND IdKbih = ?',[$IdTravel]);
        return $EditTravels;
    }

    /**
    Search Travels
     **/
    function Get007()
    {
        $IdTravel = $_GET['TextId'];
        $GetAllTravels = DB::select('SELECT A.*, B.name AS ProName, C.name AS  KabName, D.name AS CityName FROM db_ibadah.mt_kbih AS A
										LEFT JOIN db_ibadah.provinces AS B ON A.ProvinceId = B.id
										LEFT JOIN db_ibadah.regencies AS C ON A.KabId = C.id
										LEFT JOIN db_ibadah.districts AS D ON A.CityId = D.id
									  WHERE A.IsFlag = 0 AND A.KbihName LIKE "%'.$IdTravel.'%"');
        return $GetAllTravels;
    }

    /**
    Post Data Travel
     **/
    function Post001()
    {
        if(Input::get('a') == 'SaveTravel'){
            DB::table('db_ibadah.mt_kbih')->insert([
                'KbihName' => Input::get('nam'),
                'Owner' => Input::get('own'),
                'PilotNumber' => Input::get('nopil'),
                'ProvinceId' => Input::get('province'),
                'KabId' => Input::get('kabupaten'),
                'CityId' => Input::get('kota'),
                'Address' => Input::get('address'),
                'PhoneNum' => Input::get('notelp'),
                'Fax' => Input::get('nofax'),
                'email' => Input::get('email'),
                'Siup' => Input::get('siup'),
                'Img' => Input::get('img'),
                'Website' => Input::get('website'),
                'RegDate' => Carbon::now(),
                'ChangeBy' => Auth::user()->id,

            ]);
            return json_encode(Input::all());
        }else{
            DB::table('db_ibadah.mt_kbih')->where('IdKbih', Input::get('id'))->update([
                'KbihName' => Input::get('nam'),
                'Owner' => Input::get('own'),
                'PilotNumber' => Input::get('nopil'),
                'ProvinceId' => Input::get('province'),
                'KabId' => Input::get('kabupaten'),
                'CityId' => Input::get('kota'),
                'Address' => Input::get('address'),
                'PhoneNum' => Input::get('notelp'),
                'Fax' => Input::get('nofax'),
                'email' => Input::get('email'),
                'Siup' => Input::get('siup'),
                'Img' => Input::get('img'),
                'Website' => Input::get('website'),
                'RegDate' => Carbon::now(),
                'ChangeBy' => Auth::user()->id,

            ]);
            return json_encode(Input::all());
        }

    }
    /**
    Post Delete Travel
     **/
    function Post002()
    {
        DB::table('db_ibadah.mt_kbih')->where('IdKbih', Input::get('id'))->update([
            'IsFlag' => 1,
            'ChangeBy' => Auth::user()->id,
        ]);
        return json_encode(Input::all());
    }

}
