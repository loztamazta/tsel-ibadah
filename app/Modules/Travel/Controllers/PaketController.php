<?php namespace App\Modules\Travel\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Input;
use DB;
use Image;
use Storage;
use Excel;

class PaketController extends Controller {
	/**
		paket
	**/
	function paket()
	{
        $travels=DB::select('SELECT idkbih i, kbihname n FROM db_ibadah.mt_kbih where IsFlag=0');

        return view("Travel::paket.paket",compact('travels'));
	}
	function paketInput()
	{
		return view("Travel::paket.input");
	}
	function paketView()
	{
		return view("Travel::paket.view");
	}

    function Get007()
    {
        $kbihid=Auth::user()->t;
        if (isset($_GET['kbihid'])) {
            $kbihid=$_GET['kbihid'];
        }

        $GetAllPaketTravels = DB::select('
            SELECT 
                A.*
            FROM
                db_ibadah.itinerary_package AS A
            WHERE
                A.IsFlag = 0
                and KbihId=?
        ',[$kbihid]);
        return $GetAllPaketTravels;
    }

    function Get0011()
    {
        $kbihid=Auth::user()->t;
        if (isset($_GET['kbihid'])) {
            $kbihid=$_GET['kbihid'];
        }

        $PaketText = $_GET['TextId'];

        $GetAllPaketTravels = DB::select('
            SELECT 
                A.*
            FROM
                db_ibadah.itinerary_package AS A
            WHERE
                A.IsFlag = 0
                and KbihId=? AND A.name LIKE ?
        ',[$kbihid, "%$PaketText%"]);
        return $GetAllPaketTravels;
    }

    function Get008()
    {
        $IdPaketTravel = $_GET['IdPaket'];

        $GetAllTravels = DB::select('SELECT * FROM db_ibadah.itinerary_package WHERE id = ?',[$IdPaketTravel]);

        $GetDetailItener = DB::select('SELECT * FROM  db_ibadah.itinerary_detail 
										WHERE package_id = ? GROUP BY dayinter',[$IdPaketTravel]);

        return  response()->json(array('DetailPaketTravel' => $GetAllTravels,'DetailItener' => $GetDetailItener));
    }

    function Get009()
    {
        $IdDay = $_GET['IdDay'];
        $IdPaket = $_GET['IdPaket'];

        if($IdDay == 0){
            $IdDays = 1;
        }else{
            $IdDays = $IdDay;
        }
        $GetDayByDay = DB::select('SELECT * FROM db_ibadah.itinerary_detail WHERE dayinter = ? AND package_id = ? ORDER BY itinerary_time ASC',[$IdDays, $IdPaket]);

        return  $GetDayByDay;
    }

    function Get010()
    {
        $IdPaketTravel = $_GET['IdPaket'];

        $GetAllTravels = DB::select('SELECT * FROM db_ibadah.itinerary_package WHERE id = ?',[$IdPaketTravel]);

        $GetDetailItener = DB::select('SELECT * FROM  db_ibadah.itinerary_detail 
										WHERE package_id = ?',[$IdPaketTravel]);

        return  response()->json(array('DetailPaketTravel' => $GetAllTravels,'DetailItener' => $GetDetailItener));
    }

    function Post003()
    {
        $kbihid=Auth::user()->t;
        if (isset($_POST['kbihid'])) {
            $kbihid=$_POST['kbihid'];
        }

        if(Input::get('a') == 'SaveTravel'){
            DB::table('db_ibadah.itinerary_package')->insert([
                'name' => Input::get('PaketName'),
                'KbihId' => $kbihid,
                'Descrip' => Input::get('Desk'),
                'IntervalDate' => Input::get('Duration')

            ]);
            $InsertOne = DB::select('SELECT id FROM db_ibadah.itinerary_package ORDER BY id DESC LIMIT 1 ')[0];
            $Times = Input::get('Itinerary');
            //        dd($InsertOne);
            foreach ($Times as $DataTime ){
                //            dd((string)$DataTime['Times']);
                DB::table('db_ibadah.itinerary_detail')->insert([
                    'package_id' => $InsertOne->id,
                    'dayinter' => $DataTime['Days'],
                    'itinerary_time' => $DataTime['Times'],
                    'activity_name' => $DataTime['KegName']
                ]);
            }
            return json_encode($Times);
        }else{
            DB::table('db_ibadah.itinerary_package')->where('id', Input::get('IdPaket'))->update([
                'name' => Input::get('PaketName'),
                'KbihId' => $kbihid,
                'Descrip' => Input::get('Desk'),
                'IntervalDate' => Input::get('Duration')
            ]);

            DB::table('db_ibadah.itinerary_detail')->where('package_id',Input::get('IdPaket'))->delete();

//            $InsertOne = DB::select('SELECT id FROM db_ibadah.itinerary_package ORDER BY id DESC LIMIT 1 ')[0];
            $Times = Input::get('Itinerary');
            //        dd($InsertOne);
            foreach ($Times as $DataTime ){
                //            dd((string)$DataTime['Times']);
                DB::table('db_ibadah.itinerary_detail')->insert([
                    'package_id' => Input::get('IdPaket'),
                    'dayinter' => $DataTime['Days'],
                    'itinerary_time' => $DataTime['Times'],
                    'activity_name' => $DataTime['KegName']
                ]);
            }
            return json_encode($Times);
        }

    }

    /**
    Post Delete Paket
     **/
    function Post004()
    {
        DB::table('db_ibadah.itinerary_package')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
//            'ChangeBy' => Auth::user()->id,
        ]);
        return json_encode(Input::all());
    }

}
