<?php namespace App\Modules\Travel\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Input;
use DB;
use Image;
use Storage;
use Excel;

class TravelController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (Auth::user()->hasRole(['admin','sadmin','travel'])) {
			return redirect()->route('travel-agen');
		} else if (Auth::user()->hasRole(['content'])) {
			return redirect()->route('index-news');
		} else{
			return redirect()->route('travel-agen');
		}

	}

	/* uploadfile
		parameters:
			file	-> the file (file)
			-- nm		-> file name (string)
			dir		-> directory (string)
			w		-> width ("auto" or int)
			h		-> height ("auto" or int)
			tmb		-> thumbnail (null or not)
			w2		-> thumbnail width ("auto" or int)
			h2		-> thumbnail height ("auto" or int)
		return:
			*object*
			p[
				p1
				p2
			]
	*/
	function uploadfile()
	{

		$file = Input::file('file');
		// check if image exist
		// if not exist, return empty
		if (!$file) {
			$p=[
				"p1"=> ""
			];
			return $p;
		}

		// random filename
		$fn=$file->hashName();
		$path = Input::get('dir')."/".$fn;
		$path2 = Input::get('dir')."/thumb/".$fn;
		$image = Image::make($file);

		// resize image
		if (Input::get('w')=="auto" && Input::get('h')=="auto") {
			// if w&h auto, dont set size
		} else if (Input::get('w')!="auto" && Input::get('h')!="auto"){
			// if w&h not auto, set size
			$image->fit(Input::get('w'), Input::get('h'));
		} else if (Input::get('h')=="auto"){
			// if h auto, set w
			$image->widen(Input::get('w'), function ($constraint) {
				$constraint->upsize();
			});
		} else if (Input::get('w')=="auto"){
			// if w auto, set h
			$image->heighten(Input::get('h'), function ($constraint) {
				$constraint->upsize();
			});
		}
		Storage::put($path, (string) $image->encode());

		if (Input::get('tmb')) {
			if (Input::get('w2')=="auto" && Input::get('h2')=="auto") {
				// if w&h auto, dont set size
			} else if (Input::get('w2')!="auto" && Input::get('h2')!="auto"){
				// if w&h not auto, set size
				$image->fit(Input::get('w2'), Input::get('h2'));
			} else if (Input::get('h2')=="auto"){
				// if h auto, set w
				$image->widen(Input::get('w2'), function ($constraint) {
					$constraint->upsize();
				});
			} else if (Input::get('w2')=="auto"){
				// if h auto, set w
				$image->heighten(Input::get('h2'), function ($constraint) {
					$constraint->upsize();
				});
			}
			Storage::put($path2, (string) $image->encode());
		}

		$p=[
			"p1"=> $path,
			"p2"=> $path2,
		];

		return $p;
	}
	function uploadfileTwo()
	{

		$file1 = Input::file('file1');
		$file2 = Input::file('file2');
		// check if image exist
		// if not exist, return empty
		if (!$file1 && !$file2) {
			$p=[
				"p1"=> "",
                "p3"=>""
			];
			return $p;
		}

		if($file1){
			$fn=$file1->hashName();
			$path = "paspor/".$fn;
			$path2 = Input::get('dir')."/thumb/".$fn;
			$image = Image::make($file1);

				// resize image
			if (Input::get('w')=="auto" && Input::get('h')=="auto") {
				// if w&h auto, dont set size
			} else if (Input::get('w')!="auto" && Input::get('h')!="auto"){
				// if w&h not auto, set size
				$image->fit(Input::get('w'), Input::get('h'));
			} else if (Input::get('h')=="auto"){
				// if h auto, set w
				$image->widen(Input::get('w'), function ($constraint) {
					$constraint->upsize();
				});
			} else if (Input::get('w')=="auto"){
				// if w auto, set h
				$image->heighten(Input::get('h'), function ($constraint) {
					$constraint->upsize();
				});
			}
			Storage::put($path, (string) $image->encode());

			if (Input::get('tmb')) {
				if (Input::get('w2')=="auto" && Input::get('h2')=="auto") {
					// if w&h auto, dont set size
				} else if (Input::get('w2')!="auto" && Input::get('h2')!="auto"){
					// if w&h not auto, set size
					$image->fit(Input::get('w2'), Input::get('h2'));
				} else if (Input::get('h2')=="auto"){
					// if h auto, set w
					$image->widen(Input::get('w2'), function ($constraint) {
						$constraint->upsize();
					});
				} else if (Input::get('w2')=="auto"){
					// if h auto, set w
					$image->heighten(Input::get('h2'), function ($constraint) {
						$constraint->upsize();
					});
				}
				Storage::put($path2, (string) $image->encode());
			}
		}
		if($file2){
			$fn2=$file2->hashName();
			$path2 = "visa/".$fn2;
			$path22 = Input::get('dir')."/thumb/".$fn2;
			$image2 = Image::make($file2);

			// resize image2
			if (Input::get('w')=="auto" && Input::get('h')=="auto") {
				// if w&h auto, dont set size
			} else if (Input::get('w')!="auto" && Input::get('h')!="auto"){
				// if w&h not auto, set size
				$image2->fit(Input::get('w'), Input::get('h'));
			} else if (Input::get('h')=="auto"){
				// if h auto, set w
				$image2->widen(Input::get('w'), function ($constraint) {
					$constraint->upsize();
				});
			} else if (Input::get('w')=="auto"){
				// if w auto, set h
				$image2->heighten(Input::get('h'), function ($constraint) {
					$constraint->upsize();
				});
			}
			Storage::put($path2, (string) $image2->encode());

			if (Input::get('tmb')) {
				if (Input::get('w2')=="auto" && Input::get('h2')=="auto") {
					// if w&h auto, dont set size
				} else if (Input::get('w2')!="auto" && Input::get('h2')!="auto"){
					// if w&h not auto, set size
					$image2->fit(Input::get('w2'), Input::get('h2'));
				} else if (Input::get('h2')=="auto"){
					// if h auto, set w
					$image2->widen(Input::get('w2'), function ($constraint) {
						$constraint->upsize();
					});
				} else if (Input::get('w2')=="auto"){
					// if h auto, set w
					$image2->heighten(Input::get('h2'), function ($constraint) {
						$constraint->upsize();
					});
				}
				Storage::put($path22, (string) $image2->encode());
			}
		}

		// // random filename
		// $fn=$file1->hashName();
		// $fn2=$file2->hashName();
		// $path = Input::get('dir')."/".$fn;
		// $path2 = Input::get('dir')."/thumb/".$fn;
		// $image = Image::make($file1);

		// $path2 = Input::get('dir')."/".$fn2;
		// $path22 = Input::get('dir')."/thumb/".$fn2;
		// $image2 = Image::make($file2);

		// // resize image
		// if (Input::get('w')=="auto" && Input::get('h')=="auto") {
		// 	// if w&h auto, dont set size
		// } else if (Input::get('w')!="auto" && Input::get('h')!="auto"){
		// 	// if w&h not auto, set size
		// 	$image->fit(Input::get('w'), Input::get('h'));
		// } else if (Input::get('h')=="auto"){
		// 	// if h auto, set w
		// 	$image->widen(Input::get('w'), function ($constraint) {
		// 		$constraint->upsize();
		// 	});
		// } else if (Input::get('w')=="auto"){
		// 	// if w auto, set h
		// 	$image->heighten(Input::get('h'), function ($constraint) {
		// 		$constraint->upsize();
		// 	});
		// }
		// Storage::put($path, (string) $image->encode());

		// if (Input::get('tmb')) {
		// 	if (Input::get('w2')=="auto" && Input::get('h2')=="auto") {
		// 		// if w&h auto, dont set size
		// 	} else if (Input::get('w2')!="auto" && Input::get('h2')!="auto"){
		// 		// if w&h not auto, set size
		// 		$image->fit(Input::get('w2'), Input::get('h2'));
		// 	} else if (Input::get('h2')=="auto"){
		// 		// if h auto, set w
		// 		$image->widen(Input::get('w2'), function ($constraint) {
		// 			$constraint->upsize();
		// 		});
		// 	} else if (Input::get('w2')=="auto"){
		// 		// if h auto, set w
		// 		$image->heighten(Input::get('h2'), function ($constraint) {
		// 			$constraint->upsize();
		// 		});
		// 	}
		// 	Storage::put($path2, (string) $image->encode());
		// }
  //   // resize image2
		// if (Input::get('w')=="auto" && Input::get('h')=="auto") {
		// 	// if w&h auto, dont set size
		// } else if (Input::get('w')!="auto" && Input::get('h')!="auto"){
		// 	// if w&h not auto, set size
		// 	$image2->fit(Input::get('w'), Input::get('h'));
		// } else if (Input::get('h')=="auto"){
		// 	// if h auto, set w
		// 	$image2->widen(Input::get('w'), function ($constraint) {
		// 		$constraint->upsize();
		// 	});
		// } else if (Input::get('w')=="auto"){
		// 	// if w auto, set h
		// 	$image2->heighten(Input::get('h'), function ($constraint) {
		// 		$constraint->upsize();
		// 	});
		// }
		// Storage::put($path2, (string) $image2->encode());

		// if (Input::get('tmb')) {
		// 	if (Input::get('w2')=="auto" && Input::get('h2')=="auto") {
		// 		// if w&h auto, dont set size
		// 	} else if (Input::get('w2')!="auto" && Input::get('h2')!="auto"){
		// 		// if w&h not auto, set size
		// 		$image2->fit(Input::get('w2'), Input::get('h2'));
		// 	} else if (Input::get('h2')=="auto"){
		// 		// if h auto, set w
		// 		$image2->widen(Input::get('w2'), function ($constraint) {
		// 			$constraint->upsize();
		// 		});
		// 	} else if (Input::get('w2')=="auto"){
		// 		// if h auto, set w
		// 		$image2->heighten(Input::get('h2'), function ($constraint) {
		// 			$constraint->upsize();
		// 		});
		// 	}
		// 	Storage::put($path22, (string) $image2->encode());
		// }

		$p=[
			"p1"=> isset($path)?$path:'',
			"p2"=> isset($path2)?$path2:'',
			"p3"=> isset($path2)?$path2:'',
			"p4"=> isset($path22)?$path22:'',
		];

		return $p;
	}
	/**
	Get Data Province
	 **/
	function Get001()
	{
		$GetDataProvince = DB::select('SELECT * FROM db_ibadah.provinces');
		return $GetDataProvince;
	}
	/**
		Get Data Kabupaten
	 **/
	function Get002()
	{
		$IdProvince = $_GET['IdProvince'];
		$GetDataKabupaten = DB::select('SELECT * FROM db_ibadah.regencies WHERE province_id = ?', [$IdProvince]);
		return $GetDataKabupaten;
	}

	/**
		Get Data Kota
	 **/
	function Get003()
	{
		$IdKabupaten = $_GET['IdKabupaten'];
		$GetDataKota = DB::select('SELECT * FROM db_ibadah.districts WHERE regency_id = ?', [$IdKabupaten]);
		return $GetDataKota;
	}

	function updateprofile(){
		$r=DB::update('
			UPDATE profiles 
			set 
				avatar = ?, 
				updated_at = ?
			WHERE
				user_id = ?
		',[Input::get('p1'),Carbon::now(),Input::get('idp')]);
		return $r;
	}
}
