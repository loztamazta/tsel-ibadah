<?php namespace App\Modules\Travel\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Input;
use DB;
use Image;
use Storage;
use Excel;
use Mail;

class PendampingController extends Controller {
	/**
		pendamping
	**/
	function pendamping()
	{
        $travels=DB::select('SELECT idkbih i, kbihname n FROM db_ibadah.mt_kbih where IsFlag=0');

		return view("Travel::pendamping.pendamping",compact('travels'));
	}
	function pendampingInput()
	{
		$p=DB::select('SELECT * FROM db_ibadah.mt_mtw where idmtw=?', [Input::get('idmtw')]);
		if ($p) {
			$p=$p[0];
		}

		return view("Travel::pendamping.pendamping-input",compact('p'));
	}
	function pendampingProfile($b)
	{
   		$kbihid=(Input::get('kbihid') ? Input::get('kbihid') : Auth::user()->t);

		$r=DB::select("
			SELECT 
			    a.*,
			    coalesce(stat,0,1) stat
			FROM
			    db_ibadah.mt_mtw a
			    left join(
					SELECT 
						StaffId,
						if((now() BETWEEN DateofDepa and DATE_ADD(DateofDepa, INTERVAL IntervalDate DAY)),1,0) stat
					FROM
						db_ibadah.mt_pack_kbih b
						left JOIN db_ibadah.itinerary_package c 
							ON b.IdPackege = c.id
					where
						if((now() BETWEEN DateofDepa and DATE_ADD(DateofDepa, INTERVAL IntervalDate DAY)),1,0)=1    
			    ) b
					on a.Idmtw = b.StaffId
			where 
				travelid=? and idmtw=? limit 1
		",[$kbihid, $b])[0];
		$H=DB::select("
			SELECT 
			    PackName,
			    DateofDepa,
			    DATE_ADD(DateofDepa, INTERVAL IntervalDate DAY) sampai,
			    IdPack,
                (case
					when (now() BETWEEN DateofDepa and DATE_ADD(DateofDepa, INTERVAL IntervalDate DAY)) then 1 -- on progress
                    when (DATE_ADD(DateofDepa, INTERVAL IntervalDate DAY) < now()) then 2 -- past
                    when (DateofDepa > now()) then 0 -- present
				end) stat
			FROM
			    db_ibadah.mt_mtw a
			        LEFT JOIN
			    db_ibadah.mt_pack_kbih b ON a.Idmtw = b.StaffId
			        JOIN
			    db_ibadah.itinerary_package c ON b.IdPackege = c.id
			WHERE
			    a.Idmtw = ?
			order by
				DateofDepa desc
		",[$b]);
		return view("Travel::pendamping.pendamping-profile",compact('r','H'));
	}
	function pendampingData()
	{
   		$kbihid=(Input::get('kbihid') ? Input::get('kbihid') : Auth::user()->t);

		switch (Input::get('a')) {
			case 'getall':
				$r=DB::select("
					SELECT 
					    Idmtw, MtwFirstName, MtwLastName,TravelId,
					    coalesce(stat,0,1) stat
					FROM
					    db_ibadah.mt_mtw a
					    left join(
							SELECT 
								StaffId,
								if((now() BETWEEN DateofDepa and DATE_ADD(DateofDepa, INTERVAL IntervalDate DAY)),1,0) stat
							FROM
								db_ibadah.mt_pack_kbih b
								left JOIN db_ibadah.itinerary_package c 
									ON b.IdPackege = c.id
							where
								if((now() BETWEEN DateofDepa and DATE_ADD(DateofDepa, INTERVAL IntervalDate DAY)),1,0)=1    
					    ) b
							on a.Idmtw = b.StaffId
					WHERE
					    del = 0 AND travelid = ?
				",[$kbihid]);
				return json_encode($r);
				break;
			case 'savenew':
				$e=DB::transaction(function () use($kbihid) {
					$now=Carbon::now();
					DB::insert("
						INSERT INTO db_ibadah.mt_mtw
							(MtwFirstName,MtwLastName,MtwPhone,Mtwaddress,MtwImg,MtwEmail,TravelId,ChangeBy,NoPaspor)
						VALUES
							(?,?,?,?,?,?,?,?,?)
					",[
						Input::get('MtwFirstName'),
						Input::get('MtwLastName'),
						Input::get('MtwPhone'),
						Input::get('Mtwaddress'),
						Input::get('MtwImg'),
						Input::get('MtwEmail'),
						$kbihid,
						Auth::user()->name,
						Input::get('NoPaspor')
					]);

					$p=substr(Input::get('MtwFirstName'), 0, 5).substr(Input::get('MtwPhone'), -5);
					DB::insert("
						INSERT INTO db_ibadah.dt_user_apps
							(Username,Password,UserType,IdKbihGroup,ChangeBy,PasswordReal,ChangeDate)
						VALUES
							(?,?,?,?,?,?,?)
					",[
						Input::get('MtwPhone'),
						md5($p),
						2,
						$kbihid,
						Auth::user()->name,
						$p,
						$now
					]);

					$r=DB::select("
						SELECT iduser FROM db_ibadah.dt_user_apps where Username=? and ChangeBy=? and ChangeDate=? order by iduser desc limit 1
					",[Input::get('MtwPhone'),Auth::user()->name,$now])[0];

					DB::insert("
						INSERT INTO db_ibadah.mt_kbih_staff
						(IdUserApps,Email,MobileNo,FirstName,LastName,Address,PassportNo,Img,ChangeBy)
						VALUES
						(?,?,?,?,?,?,?,?,?)

					", [
						$r->iduser,
						Input::get('MtwEmail'),
						Input::get('MtwPhone'),
						Input::get('MtwFirstName'),
						Input::get('MtwLastName'),
						Input::get('Mtwaddress'),
						Input::get('NoPaspor'),
						Input::get('MtwImg'),
						Auth::user()->name
					]);

					$t1=DB::select("
						SELECT KbihName FROM db_ibadah.mt_kbih where IdKbih=? limit 1
					",[$kbihid])[0];

					$this->sendemail(Input::get('MtwEmail'),Input::get('MtwPhone'),$p,Input::get('MtwFirstName'),$t1->KbihName);

				});				

				return json_encode($e);
				break;

			case 'editpendamping':
				$r=DB::update("
					UPDATE db_ibadah.mt_mtw
					SET
						MtwFirstName = ?,
						MtwLastName = ?,
						MtwPhone = ?,
						Mtwaddress = ?,
						MtwImg = ?,
						MtwEmail = ?,
						NoPaspor = ?,
						ChangeBy = ?
					WHERE 
						Idmtw = ?
				", [
					Input::get('MtwFirstName'),
					Input::get('MtwLastName'),
					Input::get('MtwPhone'),
					Input::get('Mtwaddress'),
					Input::get('MtwImg'),
					Input::get('MtwEmail'),
					Input::get('NoPaspor'),
					Auth::user()->name,
					Input::get('Idmtw'),
				]);

				return json_encode($r);
				break;

			case 'delete':
				$r=DB::update("
					UPDATE db_ibadah.mt_mtw
					SET
						ChangeBy=?,
						del=1,
						deldate=?
					WHERE 
						Idmtw = ?
				", [
					Auth::user()->name,
					Carbon::now(),
					Input::get('idmtw')
				]);

				return json_encode($r);
				break;
			
			default:
				return "invalid action";
				break;
		}
	}

    public function sendemail($e,$u,$p,$n1,$t1)
    {
        $param=[
        	'u'=>$u,
        	'p'=>$p,
        	'n1'=>$n1,
        	't1'=>$t1
        ];

        Mail::send('Travel::pendamping.email', $param, function ($message) use ($e)
        {
        	$message->subject("Informasi Akun Telkomsel Ibadah");
            $message->from(config('mail.username'), 'Telkomsel Ibadah');
            $message->to($e);
        });

        // return response()->json(['message' => 'Request completed']);
    }


}
