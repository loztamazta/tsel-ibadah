<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog mt20">

		<div class="card ">
			<div class="card-image">
				<div id="googleMap" style="width:100%;height:400px;"></div>
			</div>
			<div class="card-content">
				<h4 class="card-title text-left">

					<br>
					<input type="hidden" name="IdHotel">
				</h4>
				<div class="card-description">
					<div class="row">
						<div class="col-xs-1">
							<i class="material-icons">hotel</i>
						</div>
						<div class="col-xs-11  text-right" id="source" name="source">
							<input type="text" class="form-control" name="hotel_name" readonly>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-1">
							<i class="material-icons">location_city</i>
						</div>
						<div class="col-xs-11  text-right" id="source" name="source">
							<input type="text" class="form-control" name="region_name" readonly>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-1">
							<i class="material-icons">location_on</i>
						</div>
						<div class="col-md-11">
							<textarea class="form-control address ckeditor" id="editor" name="editor" rows="3" readonly></textarea>
						</div>
					</div>
					{{--<div class="card-footer">--}}
					{{--<div class="pull-right">--}}
					{{--<button type="submit" class="btn btn-fill btn-rose publish" Data">Publish</button>--}}
					{{--</div>--}}
					{{--<div class="pull-left">--}}
					{{--<button type="submit" class="btn btn-fill btn-rose unpublish" >Unpublish</button>--}}
					{{--</div>--}}
					{{--</div>--}}
				</div>
			</div>
		</div>


	</div>
</div>
