@extends('Dts::layouts.dts')

@section('title','Hotel')

@section('content')
	<div class="row">
		<div class="col-md-6">

			<button class="btn btn-info rpt input-hotel">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
				Tambah Hotel
			</button>
		</div>
        {{-- 
        @role('sadmin')
            <div class="col-md-3">
                <div class="input-group">
                    <div class="form-group label-floating">
                        <label class="control-label">
                            Travel Agent
                        </label>
                        <select class="selectpicker" data-style="select-with-transition" name="t" id="trv">
                            <option value="0" disabled> Pilih agen perjalanan</option>
                            @foreach($travels as $travel)
                                <option value="{{ $travel->i }}">{{ $travel->n }}</option>
                            @endforeach
                        </select>
                    </div>
                    <span href="javascript:void(0)" class="input-group-addon">
                    </span>
                </div>
            </div>
        @endrole
        --}}
         <div class="col-md-3 pull-right">
            <div class="input-group">
                <div class="form-group label-floating">
                    <label class="control-label">
                        Search...
                    </label>
                    <input id="searchHotel" name="searchHotel" type="text" class="form-control">
                </div>
                <a href="javascript:void(0)" id="hotelSearch" class="input-group-addon">
                    <i class="material-icons">search</i>
                </a>
            </div>
        </div>
	</div>

	<div class="row" id="ListHotel"></div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
	<script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
	<script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
	<script src="{{ asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('vendor/additional-methods.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKZrw7eKJ7OHhQ_bnpEBeCUV8MTdwV3LI"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function() {

            $("#hotelSearch").on('click', function () {
                var searchid = $("#searchHotel").val();
                GetSearchHotel(searchid)
                return false;
            });
            $('#searchHotel').bind("enterKey",function(e){
                var searchid = $("#searchHotel").val();
                GetSearchHotel(searchid)

            });
            $('#searchHotel').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
                if (!this.value) {
                    GetListHotel()
                }
            });
        });

        GetListHotel()
        
        $('#trv').change(function() {
            GetListHotel();
        })

        String.prototype.trunc = String.prototype.trunc ||
            function(n){
                return (this.length > n) ? this.substr(0, n-1) + '&hellip;' : this;
            };

        function GetListHotel()
        {
            kbihid=$('#trv').val()
            // console.log('Start')
            $.ajax({
                type: 'GET',
                url: '{{route('all-hotel')}}',
                data:{kbihid:kbihid},
                global:false,
                success: function (data) {
                    var $AllHotels= $('#ListHotel');
                    $AllHotels.empty();
                    for (var i = 0; i < data.length; i++) {
                        console.log(data[i]);
                        $AllHotels.append('' +
                            '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">' +
                            '<div class="card card-product card-travel pointer">' +
                            '    <div class="card-image" data-header-animation="true">' +
                            '        <img class="img" src="'+(data[i].img_name  ? '{{ asset('storage/') }}'+'/'+data[i].img_name : '{{ asset('images/default-hotel.jpg') }}')+'">'+
                            '    </div>' +
                            '    <div class="card-content">' +
                            '        <div class="card-actions">' +
                            '            <button type="button" class="btn btn-danger btn-simple fix-broken-card">' +
                            '                <i class="material-icons">build</i> Fix Header!' +
                            '            </button>' +
                            '            <button type="button" class="btn btn-default btn-simple view-hotel" data-id="'+data[i].hotel_id+'" rel="tooltip" data-placement="bottom" title="Quick view">' +
                            '                <i class="material-icons">art_track</i>' +
                            '            </button>' +
                            '            <button type="button" class="btn btn-success btn-simple input-hotel" data-id="'+data[i].hotel_id+'" rel="tooltip" data-placement="bottom" title="Edit">' +
                            '                <i class="material-icons">edit</i>' +
                            '            </button>' +
                            '            <button type="button" class="btn btn-danger btn-simple delete-hotel" data-id="'+data[i].hotel_id+'" rel="tooltip" data-placement="bottom" title="Remove">' +
                            '                <i class="material-icons">close</i>' +
                            '            </button>' +
                            '    </div>' +
                            '        <h4 class="card-title text-left">' +
                            '            <a href="#TravelName">'+data[i].hotel_name.trunc(30)+'</a>' +
                            '        </h4>'+
                            '</div>'+
                            '        <div class="card-description text-left">' +
                            '                <div class="col-xs-2">' +
                            '                    <i class="material-icons">place</i>' +
                            '                </div>' +
                            '                <div class="col-xs-10">' +
                                                data[i].kota+
                            '                </div>' +
                            '        </div>' +

                            '</div>')
                    }
                }
            });
        }

        function GetSearchHotel(searchid)
        {
            kbihid=$('#trv').val()
//             console.log('Start search')
//            debugger
            $.ajax({
                type: 'GET',
                url: '{{route('search-hotel')}}',
                data:{kbihid:kbihid,
                      TextId:searchid},
                global:false,
                success: function (data) {
                    var $AllHotels= $('#ListHotel');
                    $AllHotels.empty();
                    for (var i = 0; i < data.length; i++) {
                        console.log(data[i]);
                        $AllHotels.append('' +
                            '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">' +
                            '<div class="card card-product card-travel pointer">' +
                            '    <div class="card-image" data-header-animation="true">' +
                            '        <img class="img" src="'+(data[i].img_name  ? '{{ asset('storage/') }}'+'/'+data[i].img_name : '{{ asset('images/default-hotel.jpg') }}')+'">'+
                            '    </div>' +
                            '    <div class="card-content">' +
                            '        <div class="card-actions">' +
                            '            <button type="button" class="btn btn-danger btn-simple fix-broken-card">' +
                            '                <i class="material-icons">build</i> Fix Header!' +
                            '            </button>' +
                            '            <button type="button" class="btn btn-default btn-simple view-hotel" data-id="'+data[i].hotel_id+'" rel="tooltip" data-placement="bottom" title="Quick view">' +
                            '                <i class="material-icons">art_track</i>' +
                            '            </button>' +
                            '            <button type="button" class="btn btn-success btn-simple input-hotel" data-id="'+data[i].hotel_id+'" rel="tooltip" data-placement="bottom" title="Edit">' +
                            '                <i class="material-icons">edit</i>' +
                            '            </button>' +
                            '            <button type="button" class="btn btn-danger btn-simple delete-hotel" data-id="'+data[i].hotel_id+'" rel="tooltip" data-placement="bottom" title="Remove">' +
                            '                <i class="material-icons">close</i>' +
                            '            </button>' +
                            '    </div>' +
                            '        <h4 class="card-title text-left">' +
                            '            <a href="#TravelName">'+data[i].hotel_name.trunc(30)+'</a>' +
                            '        </h4>'+
                            '</div>'+
                            '</div>')
                    }
                }
            });
        }


		$('#rp').slideReveal({
			// trigger: $(".rpt"),
			position: "right",
			push: false,
			overlay: true,
			zIndex:1300,
		});
		$('#rp').delegate('.rp-close','click',function() {
			$('#rp').slideReveal("hide")
		})

        $(document).delegate('.input-hotel', 'click', function(e) {
            kbihid=$('#trv').val()
            if($(this).attr('title') == 'Edit'){
                $('.loading').show()
                $.ajax({
                    type: 'GET',
                    url: '{{route('travel-hotel-edit')}}',
                    data:{kbihid:kbihid,
                          idHotel:$(this).data('id')},
                    global:false,
                    success: function (data) {
                        $('#rp').empty().load("{{ route('travel-hotel-input') }}", function(){

                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()

                            $('#SelectKota').on('change', function() {
                                $('#selectRegion option[value='+data['0'].regionId+']').attr('selected','selected');
                            });
                            var scr = '{{asset('storage')}}/'+(data['0'].img_name ? data['0'].img_name:'');
//                            var scrs = data['0'].img_name;
                            $('.titleformhotel').empty().append('Ubah Hotel '+data['0'].hotel_name)

                            $("input[name='save_type']").val('EditSave')
                            $("input[name='IdHotel']").val(data['0'].hotel_id)
                            $("input[name='hotel_name']").val(data['0'].hotel_name)
                            $("input[name='place_type']").val('1')
                            $("input[name='latitude']").val(data['0'].latitude)
                            $("input[name='longitude']").val(data['0'].longitude)
                            $("select[name='region_id']").val(data['0'].region_id).attr('selected', true).change();
                            $("textarea.address").val(data['0'].address);
                            $('#IM').attr("src", scr);
                            $('#Uploads').data('old', (data['0'].img_name ? data['0'].img_name:'') )
                            $('#rp').slideReveal("show")
                            var marker;

                            function myMap() {
                                var mapProp= {
                                    center:new google.maps.LatLng(24.68773000,46.72185000),
                                    zoom:5,
                                    scrollwheel: false
                                };
                                var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

                                google.maps.event.addListener(map, 'click', function (event) {
                                    placeMarker(event.latLng);
                                    document.getElementById("latFld").value = event.latLng.lat();
                                    document.getElementById("lngFld").value = event.latLng.lng();
                                })
                                var lat = document.getElementById('latFld').value;
                                var lng = document.getElementById('lngFld').value;
                                var Location = new google.maps.LatLng(lat, lng);

                                placeMarker(Location)

                                function placeMarker(location) {



                                    if (marker == undefined){
                                        marker = new google.maps.Marker({
                                            position: location,
                                            map: map,
                                            animation: google.maps.Animation.DROP,
                                        });
                                    }
                                    else{
                                        marker.setPosition(location);
                                    }
                                    map.setCenter(location);

                                }
                            }
                            myMap()
                        })
                    }
                });

            }else{
                $('.loading').show()
                $('#rp').empty().load("{{ route('travel-hotel-input') }}", function(){

                    $('.titleformhotel').empty().append('Hotel Baru')

                    $('.card-content-overflow').perfectScrollbar();
                    $('.loading').hide()
                    $('#rp').slideReveal("show")
                    var marker;

                    function myMap() {
                        var mapProp= {
                            center:new google.maps.LatLng(24.68773000,46.72185000),
                            zoom:5,
                            scrollwheel: false
                        };
                        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

                        google.maps.event.addListener(map, 'click', function (event) {
                            placeMarker(event.latLng);
                            document.getElementById("latFld").value = event.latLng.lat();
                            document.getElementById("lngFld").value = event.latLng.lng();
                        })

                        function placeMarker(location) {



                            if (marker == undefined){
                                marker = new google.maps.Marker({
                                    position: location,
                                    map: map,
                                    animation: google.maps.Animation.DROP,
                                });
                            }
                            else{
                                marker.setPosition(location);
                            }
                            map.setCenter(location);

                        }
                    }
                    myMap()
                })
            }

        })
        $(document).delegate('.view-hotel', 'click', function(e) {
            // console.log('klik')
            $('.loading').show()
            $.ajax({
                type: 'GET',
                url: '{{route('travel-hotel-edit')}}',
                data:{idHotel:$(this).data('id')},
                global:false,
                success: function (data) {
                    $('#c-modal').empty().load("{{ route('travel-hotel-view') }}", function(){
                        $('.loading').hide()
                        var scr = 'storage/'+data['0'].Img;
                        $("input[name='TypeSave']").val('EditSave')
                        $("input[name='IdHotel']").val(data['0'].hotel_id)
                        $("input[name='hotel_name']").val(data['0'].hotel_name)
                        $("input[name='place_type']").val('1')
                        $("input[name='latitude']").val(data['0'].latitude)
                        $("input[name='longitude']").val(data['0'].longitude)
                        $("input[name='region_name']").val(data['0'].kota);
                        $("textarea.address").val(data['0'].address);
                        $('#IM').attr("src", scr);
                        $('#modal').modal('show')
                        var marker;
                        var map;
                        function myMap() {
                            var lat = data['0'].latitude;
                            var lng = data['0'].longitude;
                            var Location = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                            var mapProp= {
                                center:Location,
                                zoom:5,
                                scrollwheel: false
                            };
                            map = new google.maps.Map(document.getElementById("googleMap"),mapProp);


                            console.log(Location);
                            placeMarker(Location)

                            function placeMarker(location) {



                                if (marker == undefined){
                                    marker = new google.maps.Marker({
                                        position: location,
                                        map: map,
                                        animation: google.maps.Animation.DROP,
                                    });
                                }
                                else{
                                    marker.setPosition(location);
                                }
                                map.setCenter(location);

                            }
                        }
                        myMap()
                        $('#modal').on('shown.bs.modal', function () {
                            google.maps.event.trigger(map, "resize");
                            var lat = data['0'].latitude;
                            var lng = data['0'].longitude;
                            var Location = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                            map.setCenter(Location);
                        })
                    })
                }
            });
        })
        $(document).delegate('.delete-hotel', 'click', function(e) {
            // console.log('klik')
            e.preventDefault();
            var IdHotel = $(this).data('id')
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Iya!',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon menunggu')
                swal.showLoading()
                var data={
                    id:IdHotel,
                };
                $.post("{{route('delete-hotel') }}", data,function(r) {
                    console.log(r)
                    if(r == 1){
                        swal(
                            'Maaf!',
                            'Data Hotel sedang digunakan.',
                            'error'
                        ).then(function() {
//                            location.reload();
                        })
                    }else{
                        swal(
                            'Berhasil!',
                            //'Data Hotel telah dihapus.',
                            '',
                            'success'
                        ).then(function() {
                            location.reload();
                        })
                    }

                }, 'json');

            })
        })

        $(document).delegate('#saveHotelData', 'click', function(e) {
            e.preventDefault();
            var $validator = $('.card-content-overflow form').validate({
                rules: {
                    hotel_name: {
                        required: true,
                        contentSpecialCharacters : true,
                        minlength: 3
                    },
                    latitude: {
                        required: true,
                        number: true,
                    },
                    longitude: {
                        required: true,
                        number: true,
                    },

                },
                messages: {
                    hotel_name: 
                    {
                       required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   
                    },
                    latitude: "Hanya angka dan tanda -",
                    longitude: "Hanya angka dan tanda -",

                },
                errorPlacement: function(error, element) {
                    $(element).parent('div').addClass('has-error');
                }
            });
            var $valid = $('.card-content-overflow form').valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            swal({
                title: 'Apakah Anda Yakin ?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()

                var oldimg = $("#Uploads").data('old');
                var img = $("#FormHotel").find('#Uploads');
                var file_data = $(img).prop("files")[0];
                var form_data = new FormData();
                form_data.append("file", file_data)
                form_data.append("nm", '')
                form_data.append("dir", 'hotels')
                form_data.append("w", 800)
                form_data.append("h", 600)

                $.ajax({
                    url: "{{route('content-upload') }}",
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    type: 'post',
                    success: function (p) {
                        if (!p.p1) {
                            if (oldimg != p.p1) {
                                p.p1 = oldimg;
                            }
                        }

                        data = {
                            a: $("input[name='save_type").val(),
                            IdHotel: $("input[name='IdHotel").val(),
                            name: $("input[name='hotel_name").val(),
                            type_id: $("input[name='place_type").val(),
                            latitude: $("input[name='latitude").val(),
                            longitude: $("input[name='longitude").val(),
                            region_id: $('#selectRegion').find(":selected").val(),
                            address: $("textarea.address").val(),
                            IsFlag: $("input[name='IsFlag").val(),
                            img: p.p1,
                            kbihid:kbihid
                        };

                        $.post("{{route('post-hotel') }}", data, function (r) {
                            swal(
                                "Berhasil",
                                "",
                                "success"
                            ).then(function () {
                                $('#rp').empty().slideReveal("hide")
                                GetListHotel()
                            })
                        }, 'json');
                    }
                })
        }, 'json').fail(function(response) {
            console.log(response)
            swal(
                'Terjadi Kesalahan!',
                //response.responseText,
                'Mohon Dicek Kembali',
                'error'
            )
        });

        });

	</script>

@endsection