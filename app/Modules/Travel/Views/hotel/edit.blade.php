<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
				<i class="material-icons">close</i>
			</span>
    <div class="card-content">

        <h4 class="card-title">Edit Hotel</h4>
        <div class="card-content-overflow">

            {{--<div class="fileinput fileinput-new text-center" data-provides="fileinput">--}}
            {{--<div class="fileinput-new thumbnail">--}}
            {{--<img src="{{ asset('images/image_placeholder.jpg') }}" alt="...">--}}
            {{--</div>--}}
            {{--<div class="fileinput-preview fileinput-exists thumbnail"></div>--}}
            {{--<div>--}}
            {{--<span class="btn btn-rose btn-round btn-file">--}}
            {{--<span class="fileinput-new">Select image</span>--}}
            {{--<span class="fileinput-exists">Change</span>--}}
            {{--<input type="file" name="_______" />--}}
            {{--</span>--}}
            {{--<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            <form id="FormHotel">
                <div class="form-group label-floating">
                    <label class="control-label">Nama Hotel</label>
                    <input type="text" class="form-control" name="hotel_name">
                    <input type="hidden" value="1" name="place_type">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Lokasi di Peta</label>
                    {{--<input type="text" class="form-control" name="latitude">--}}
                {{--</div>--}}
                {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">Longitude</label>--}}
                    {{--<input type="text" class="form-control" name="longitude">--}}
                </div>
                <div id="googleMap" style="width:100%;height:400px;"></div>
                <div class="form-group label-floating">
                    <label class="control-label">Latitude</label>
                    <input type="text" id="latFld" name="latitude">

                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Longitude</label>
                    <input type="text" id="lngFld" name="longitude">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Lokasi Hotel</label>
                    <select class="selectpicker" data-style="select-with-transition" title="Pilih kota..." data-size="7" id="regionId" name="region_id">
                        <option disabled>Pilih kota</option>
                        <option value="2">Mekkah </option>
                        <option value="3">Madinnah</option>
                    </select>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Alamat Hotel</label>
                    <textarea class="form-control" placeholder="" rows="4" name="address"></textarea>
                </div>
            </form>

        </div>

    </div>

    <div class="card-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-fill btn-rose update-hotel" id="saveHotelData">Save</button>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>

//Google Maps

<script>

        var marker;

    function myMap() {
        var mapProp= {
            center:new google.maps.LatLng(51.508742,-0.120850),
            zoom:5,
        };
        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

        google.maps.event.addListener(map, 'click', function (event) {
            placeMarker(event.latLng);
            document.getElementById("latFld").value = event.latLng.lat();
            document.getElementById("lngFld").value = event.latLng.lng();
        })

        function placeMarker(location) {



            if (marker == undefined){
                marker = new google.maps.Marker({
                    position: location,
                    map: map,
                    animation: google.maps.Animation.DROP,
                });
            }
            else{
                marker.setPosition(location);
            }
            map.setCenter(location);

        }
    }
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKZrw7eKJ7OHhQ_bnpEBeCUV8MTdwV3LI&callback=myMap"></script>
