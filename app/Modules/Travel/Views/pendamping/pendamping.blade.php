@extends('Dts::layouts.dts')

@section('h-title','Pendamping Grup')
@section('title','Pendamping Grup')

@section('content')
    <div class="row">
        <div class="col-md-5">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="ccol-md-12 col-lg-12">
							<button class="btn btn-info rpt input-pendamping">
								<span class="btn-label">
									<i class="material-icons">add</i>
								</span>
								Tambah Pendamping
							</button>							
						</div>
				        @role('sadmin')
				            <div class="col-md-12 col-lg-12 pull-right">
				                <div class="input-group">
				                    <div class="form-group label-floating">
				                        <label class="control-label">
				                            Agen Perjalanan
				                        </label>
				                        <select class="selectpicker" data-style="select-with-transition" name="t" id="trv">
				                            <option value="0" disabled> Pilih agen perjalanan</option>
				                            @foreach($travels as $travel)
				                                <option value="{{ $travel->i }}">{{ $travel->n }}</option>
				                            @endforeach
				                        </select>
				                    </div>
				                    <span href="javascript:void(0)" class="input-group-addon">
				                    </span>
				                </div>
				            </div>
				        @endrole						
					</div>
				</div>
				<div class="card-content">
					<table id="list" class="table table-striped table-no-bordered table-hover compact" width="100%" >
						<thead>
							<tr class="">
								<th class="col-md-7">Nama</th>
								<th class="col-md-2">Status</th>
								<th class="col-md-3 text-right">Aksi</th>
							</tr>
						</thead>
		            </table>
				</div>
			</div>

        </div>
		<div class="col-md-7" id="profile">
		</div>
    </div>



@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
@endsection

@section('js')
	<script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
	<script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
	<script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
	<script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('vendor/additional-methods.js') }}"></script>
	<script>
        var kbihid=''
        kbihid=($('#trv').val()?$('#trv').val():0)
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
        $('.selectpicker').selectpicker();

        $('#trv').change(function() {
        	kbihid=$('#trv').val()
			$("#list").DataTable().ajax.reload(null, true)
			$('#profile').fadeOut(100)
       })

	</script>

	{{-- right panel --}}
	<script>
		$('#rp').slideReveal({
			// trigger: $(".rpt"),
			position: "right",
			push: false,
			overlay: true,
			zIndex:1300,
		});
		$('#rp').delegate('.rp-close','click',function() {
			$('#rp').slideReveal("hide")
		})

		$(".input-pendamping").click(function(){
			$('.loading').show()			
			$('#rp').empty().load("{{ route('travel-pendamping-input') }}", {idmtw:'0'}, function(){
				$('.card-content-overflow').perfectScrollbar();
				$('.loading').hide()
				$('#rp').slideReveal("show")
			})
		})

		$(".card-travel").click(function(){
			// console.log('klik')
		}).find(".card-actions").click(function(e) {
			return false;
		});
	</script>

	{{-- datatable --}}
	<script src="{{ asset('mdp/js/jquery.datatables.js') }}"></script>
	<script>
		$("#list").DataTable({
			ajax: {
				url: "{{ route('travel-pendamping-data') }}",
				dataSrc: '',
				type: "POST",
				data:function (d) {
					d.a="getall"
					d.kbihid=kbihid
				}
			},
			columns: [
				{
					render: function (data, type, full, meta) {
						return full.MtwFirstName+" "+full.MtwLastName;
					}
				},//1
				{
					data: "stat",
					render: function (data, type, full, meta) {

						return data==0?
							'<span class="label label-success">Tersedia</span>':
							'<span class="label label-danger">Ditugaskan</span>';
					}
				},//2
				{
					// orderable:false,
					class : "text-center",
					render: function (data, type, full, meta) {
						a=''
						+'	<div class="list-action text-right">'
						+'		<button class="btn btn-simple btn-info btn-icon list-view" data-idmtw="'+full.Idmtw+'" title="Lihat detil">'
						+'			<i class="material-icons">art_track</i>'
						+'			<div class="ripple-container"></div>'
						+'		</button>'
						+'		<button class="btn btn-simple btn-warning btn-icon list-edit" data-idmtw="'+full.Idmtw+'" title="Ubah">'
						+'			<i class="material-icons">edit</i>'
						+'			<div class="ripple-container"></div>'
						+'		</button>'
						+'		<button class="btn btn-simple btn-danger btn-icon list-delete" data-idmtw="'+full.Idmtw+'" title="Hapus">'
						+'			<i class="material-icons">close</i>'
						+'			<div class="ripple-container"></div>'
						+'		</button>'
						+'	</div>'
						return a
					}
				},//3
			],
			columnDefs: [
				{targets: 2, orderable: false }
			],
			createdRow:  function ( row, data, index ) {
				$(row).data("idmtw",data.Idmtw)
			},
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Cari data",
                emptyTable:"Tidak ada data",
			    info:"Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
			    infoEmpty:"Menampilkan 0 sampai 0 dari 0 data",
			    lengthMenu:"Menampilkan _MENU_ data",
				paginate: {
					previous: "Sebelum",
					next: "Berikut"
				}
            },
            dom: "<'row'<'col-sm-12'<'pull-left''toolbar'><'pull-right'f><'clearfix'>>>tr<'row'<'col-sm-12'<'pull-left'i><'pull-right'p><'clearfix'>>>"
		});
		$("div.toolbar").html(''
					+'<button class="btn btn-info rpt input-pendamping">'
					+'	<span class="btn-label">'
					+'		<i class="material-icons">add</i>'
					+'	</span>'
					+'	Tambah Pendamping'
					+'</button>'
		)

		// 
		// Action
		// 

		// view
	    $('#list tbody').on( 'click', '.list-view', function (e) {
            e.preventDefault();
	    	idmtw=$(this).data("idmtw")
			$('#profile').fadeOut(100, function() {
				var url='{{ route('travel-pendamping-profile', ":idmtw") }}'
				url = url.replace(':idmtw', idmtw)
				url=url+"?kbihid="+kbihid
				$('#profile').load(url,  function(){
					$('#profile').fadeIn(300)
				})
			})
	    })

		// edit
	    $('#list tbody').on( 'click', '.list-edit', function (e) {
            e.preventDefault();
	    	idmtw=$(this).data("idmtw")
			$('.loading').show()			
			$('#rp').empty().load("{{ route('travel-pendamping-input') }}", {idmtw:idmtw}, function(){
				$('.card-content-overflow').perfectScrollbar();
				$('.loading').hide()
				$('#rp').slideReveal("show")
			})
	    })

		// delete
	    $('#list tbody').on( 'click', '.list-delete', function (e) {
            e.preventDefault();
	    	idmtw=$(this).data("idmtw")
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ya!',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon menunggu..')
                swal.showLoading()
                var data={
                	a:'delete',
                    idmtw:idmtw,
                };
                $.post("{{route('travel-pendamping-data') }}", data,function(r) {
                    swal(
                        'Berhasil!',
                        //'Data Pendamping telah dihapus.',
						'',
                        'success'
                    ).then(function() {
						$('#list').DataTable().ajax.reload(null, false);
						$('#profile').fadeOut(100)
                    })
                }, 'json').fail(function(response) {
                    console.log(response)
                    swal(
                        'Terjadi Kesalahan!',
                        //response.responseText,
						'Mohon Dicek Kembali',
                        'error'
                    )
                });
            })
	    })
	</script>

@endsection