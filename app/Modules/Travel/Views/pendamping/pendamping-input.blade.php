<div class="card">
	<div class="card-header card-header-icon" data-background-color="blue">
		<i class="material-icons">mode_edit</i>
	</div>
	<span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
		<i class="material-icons">close</i>
	</span>
	<div class="card-content">
		<h4 class="card-title">{{ $p ? 'Ubah pendamping : '.$p->MtwFirstName.' '.$p->MtwLastName : 'Pendamping baru' }}</h4>
		<div class="card-content-overflow">
			<form id="fpendamping">
				<input type="hidden" name="Idmtw" id="Idmtw" value="{{ $p?$p->Idmtw:0 }}">
				<div class="fileinput fileinput-new text-center" data-provides="fileinput">
					<div class="fileinput-new thumbnail img-circle">
						<img src="{{ $p ? asset('storage/'.$p->MtwImg) : asset('images/image_placeholder.jpg') }}" alt="...">
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
					<div>
						<span class="btn btn-rose btn-round btn-file">
						<span class="fileinput-new">Pilih gambar</span>
						<span class="fileinput-exists">Ganti</span>
							<input type="file" name="img" id="img" data-old="{{ $p ? $p->MtwImg : '' }}" />
						</span>
						<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
					</div>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nama Depan</label>
					<input type="text" class="form-control" name="MtwFirstName" value="{{ $p?$p->MtwFirstName:'' }}">
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nama Belakang</label>
					<input type="text" class="form-control" name="MtwLastName" value="{{ $p?$p->MtwLastName:'' }}">
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Email</label>
					<input type="text" class="form-control" name="MtwEmail" value="{{ $p?$p->MtwEmail:'' }}">
				</div>
				<div class="form-group label-floating">
					<label class="control-label">No Telfon Seluler</label>
					<input type="text" class="form-control" name="MtwPhone" value="{{ $p?$p->MtwPhone:'' }}">
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nomor Paspor</label>
					<input type="text" class="form-control" name="NoPaspor" value="{{ $p?$p->NoPaspor:'' }}">
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Alamat</label>
					<input type="text" class="form-control" name="Mtwaddress" value="{{ $p?$p->Mtwaddress:'' }}">
				</div>
			</form>
		</div>
	</div>
	<div class="card-footer">
		<div class="pull-right">
			<button id="sfpendamping" type="submit" class="btn btn-fill btn-rose">Simpan</button>
		</div>				
	</div>
</div>

<script>
	$('#sfpendamping').click(function(e) {
		e.preventDefault()
        var $validator = $('.card-content-overflow form').validate({
            rules: {
                MtwFirstName: {
                    required: true,
                    contentSpecialCharacters: true
                },
                MtwLastName: {
                    required: true,
                    contentSpecialCharacters: true
                },
                MtwPhone: {
                    required: true,
                    phonetsel: true
                },
                MtwEmail: {
                    required: true,
                    email: true
                },
                NoPaspor: {
                    required: true,
                    alphanumeric: true,
                    contentSpecialCharacters : true,
                },

            },
            messages: {
            	MtwFirstName:
                {
                    required: "Data Wajib diisi",
                    contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"
                }, 
                MtwEmail: 
                       
                    {
                        required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   
                    },
            	MtwLastName: 
                       
                    {
                       required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   
                    },
            	MtwPhone: 
                    {
                       required: "Data Wajib diisi",
                       phonetsel : "Harus Nomor Telkomsel " 
                    },
            	NoPaspor: 
                    {
                       required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter "   
                    },

            },
            errorPlacement: function(error, element) {
                $(element).parent('div').addClass('has-error');
                $(element).parent('div').append(error);

            }

        });
        var $valid = $('.card-content-overflow form').valid();
        if(!$valid) {
            $validator.focusInvalid();
            return false;
        }
        swal({
            title: 'Apakah Anda Yakin?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Iya!',
			cancelButtonText: 'Batal',
        }).then(function() {
            swal('Mohon menunggu')
            swal.showLoading()

            old1=$("#img").data('old')
            var form_data = new FormData();
			form_data.append("file", $("#img").prop("files")[0])
            form_data.append("dir", 'travel/'+kbihid)
            form_data.append("w", 800)
            form_data.append("h", 800)

            $.ajax({
                url: "{{route('cms-upload') }}",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(p){
					if (!p.p1) {
						if (old1!=p.p1) {
							p.p1=old1;
						}
					}

					data=($('#Idmtw').val()=='0'?'a=savenew':'a=editpendamping')
					data+='&MtwImg='+p.p1+'&kbihid='+kbihid+'&'
					data+=$("form#fpendamping").serialize();
	                    console.log(data)

                    $.post("{{route('travel-pendamping-data') }}", data,function(r) {
                        swal(
                            'Berhasil',
                            '',
                            'success'
                        ).then(function() {
                            $('#rp').empty().slideReveal("hide")
							$('#list').DataTable().ajax.reload(null, false);
							$('#profile').fadeOut(100)
                        })
	                }, 'json').fail(function(response) {
	                    console.log(response)
	                    swal(
	                        'Terjadi Kesalahan!',
	                        //response.responseText,
							'Mohon Dicek Kembali',
	                        'error'
	                    )
	                }); 
                }
            })
        })
	})


</script>