<div class="card card-profile">
	<div class="card-avatar pull-right">
		<a href="#">
			<img class="img" src="{{ asset('storage/') }}/{{ $r->MtwImg }}" />
		</a>
	</div>
	<div class="card-content text-left">
		<h4 class="card-title">{{ $r->MtwFirstName }} {{ $r->MtwLastName }} <span class="label label-{{ $r->stat==1?'danger':'success' }}"> {{ $r->stat==1?'Ditugaskan':'Tersedia' }}</span></h4> 
		<div class="card-description mt20">
			<div class="row">
				<div class="col-xs-2 col-md-1 text-right">
					<i class="material-icons">phone</i>
				</div>
				<div class="col-xs-10 col-md-11">
					<p>{{ $r->MtwPhone }}</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-2 col-md-1 text-right">
					<i class="material-icons">place</i>
				</div>
				<div class="col-xs-10 col-md-11">
					<p>{{ $r->Mtwaddress }}</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-2 col-md-1 text-right">
					<i class="material-icons">mail</i>
				</div>
				<div class="col-xs-10 col-md-11">
					<p>{{ $r->MtwEmail }}</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-2 col-md-1 text-right">
					<i class="material-icons">book</i>
				</div>
				<div class="col-xs-10 col-md-11">
					<p>{{ $r->NoPaspor }}</p>
				</div>
			</div>
			
		</div>
	</div>
</div>

<ul class="timeline timeline-simple">
	@foreach ($H as $h)
		<li class="timeline-inverted">
			<div class="timeline-badge {{ $h->stat==2?'success':($h->stat==1?'danger':'warning') }}">
				<i class="material-icons">{{ $h->stat==2?'flight_land':($h->stat==1?'flight':'flight_takeoff') }}</i>
			</div>
			<div class="timeline-panel">
				<div class="timeline-heading">
					<span class="label label-{{ $h->stat==2?'success':($h->stat==1?'danger':'warning') }}">{{ $h->DateofDepa }} - {{ $h->sampai }}</span>
				</div>
				<div class="timeline-body">
					<a href="{{ route('travel-grup-detail',[$h->IdPack]) }}">{{ $h->PackName }}</a>
				</div>
			</div>
		</li>
	@endforeach

</ul>

<script>

</script>