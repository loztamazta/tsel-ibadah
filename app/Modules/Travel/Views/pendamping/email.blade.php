<html>
<head></head>
<body>
	<p>Halo {{ $n1 }},</p>
	<p>Selamat anda tergabung di dalam aplikasi Telkomsel Ibadah sebagai pendamping dari {{ $t1 }}. Berikut adalah informasi untuk mengakses aplikasi Telkomsel Ibadah dari ponsel Android anda:</p>
	<ul>
		<li>Nama Pengguna: {{ $u }}</li>
		<li>Kata Sandi : {{ $p }}</li>
	</ul>
	<p>Selamat menikmati fitur-fitur yang tersedia.</p>
	<p>Terima kasih.</p>
</body>
</html>