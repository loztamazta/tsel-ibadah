<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog mt20">

			<div class="card ">
				<div class="card-image">
					<img class="img" id="IM" src="">
				</div>
				<div class="card-content">
					<h4 class="card-title text-left">
						<a href="#TravelName" id="TN"></a>
						<br>
						<small id="OW"></small>
					</h4>
					<div class="card-description">
						<div class="row">
							<div class="col-xs-10  text-right" id="PN">

							</div>
							<div class="col-xs-2">
								<i class="material-icons">phone</i>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-10  text-right" id="EM">

							</div>
							<div class="col-xs-2">
								<i class="material-icons">mail</i>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-10  text-right" id="AD">

							</div>
							<div class="col-xs-2">
								<i class="material-icons">place</i>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-10  text-right" id="RD">

							</div>
							<div class="col-xs-2">
								<i class="material-icons">date_range</i>
							</div>
						</div>
					</div>
				</div>
			</div>


	</div>
</div>
