@extends('Dts::layouts.dts')

@section('h-title','Agen Perjalanan')

@section('title','Agen Perjalanan')

@section('content')
	<div class="row">
		<div class="col-md-6">

			<button class="btn btn-info rpt input-travel">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
				Tambah Travel
			</button>
		</div>
		<div class="col-md-3 pull-right">
			<div class="input-group">
				<div class="form-group label-floating">
					<label class="control-label">
						Cari Agen Perjalanan ...
					</label>
					<input id="AgenSearch" name="AgenSearch" type="text" class="form-control">
				</div>
				<a href="javascript:void(0)" id="SearchTravelAgen" class="input-group-addon">
			        <i class="material-icons">search</i>
			    </a>
			</div>
		</div>
	</div>

	<div class="row" id="ListTravelAgent"></div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
	<script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
	<script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('vendor/additional-methods.js') }}"></script>
	<script type="application/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function() {
            $("#SearchTravelAgen").on('click', function () {
                var searchid = $("#AgenSearch").val();
                GetSearchData(searchid)
                return false;
            });
            $('#AgenSearch').bind("enterKey",function(e){
                var searchid = $("#AgenSearch").val();
                GetSearchData(searchid)

            });
            $('#AgenSearch').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
                if (!this.value) {
                    GetListTravelAgent()
                }
            });
        });

		GetListTravelAgent()

        String.prototype.trunc = String.prototype.trunc ||
            function(n){
                return (this.length > n) ? this.substr(0, n-1) + '&hellip;' : this;
            };

        function GetListTravelAgent()
		{
            $.ajax({
                type: 'GET',
                url: '{{url('/GetAllTravelsAgent')}}',
                global:false,
                success: function (data) {
                    var $AllTravels = $('#ListTravelAgent');
                    $AllTravels.empty();

                    for (var i = 0; i < data.length; i++) {
                        $AllTravels.append('' +
                            '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">' +
                                '<div class="card card-product card-travel pointer">' +
                                '    <div class="card-image" data-header-animation="true">' +
                                '        <img class="img" src="'+(data[i].Img  ? '{{ asset('storage/') }}'+'/'+data[i].Img : '{{ asset('images/default-hotel.jpg') }}')+'">'+
                                '    </div>' +
                                '    <div class="card-content">' +
                                '        <div class="card-actions">' +
                                '            <button type="button" class="btn btn-danger btn-simple fix-broken-card">' +
                                '                <i class="material-icons">build</i> Fix Header!' +
                                '            </button>' +
                                '            <button type="button" class="btn btn-default btn-simple view-travel" data-id="'+data[i].IdKbih+'" rel="tooltip" data-placement="bottom" title="Detil">' +
                                '                <i class="material-icons">art_track</i>' +
                                '            </button>' +
                                '            <button type="button" class="btn btn-success btn-simple input-travel" data-id="'+data[i].IdKbih+'" rel="tooltip" data-placement="bottom" title="Ubah">' +
                                '                <i class="material-icons">edit</i>' +
                                '            </button>' +
                                '            <button type="button" class="btn btn-danger btn-simple delete-travel" data-id="'+data[i].IdKbih+'" rel="tooltip" data-placement="bottom" title="Hapus">' +
                                '                <i class="material-icons">close</i>' +
                                '            </button>' +
                                '        </div>' +
                                '        <h4 class="card-title text-left">' +
                                '            <a href="#TravelName">'+data[i].KbihName.trunc(30)+'</a>' +
                                '            <small><br>'+data[i].Owner+'</small>' +
                                '        </h4>' +
                                '        <div class="card-description">' +
                                '            <div class="row">' +
                                '                <div class="col-xs-10  text-right">' +
                                                    data[i].PhoneNum+
                                '                </div>' +
                                '                <div class="col-xs-2">' +
                                '                    <i class="material-icons">phone</i>' +
                                '                </div>' +
                                '            </div>' +
                                '            <div class="row">' +
                                '                <div class="col-xs-10  text-right">' +
                                                    data[i].Email+
                                '                </div>' +
                                '                <div class="col-xs-2">' +
                                '                    <i class="material-icons">mail</i>' +
                                '                </div>' +
                                '            </div>' +
                                '            <div class="row">' +
                                '                <div class="col-xs-10  text-right">' +
                                                    data[i].CityName+' '+data[i].KabName+
                                '                </div>' +
                                '                <div class="col-xs-2">' +
                                '                    <i class="material-icons">place</i>' +
                                '                </div>' +
                                '            </div>' +
                                '            <div class="row">' +
                                '                <div class="col-xs-10  text-right">' +
                                                  data[i].RegDate+
                                '                </div>' +
                                '                <div class="col-xs-2">' +
                                '                    <i class="material-icons">date_range</i>' +
                                '                </div>' +
                                '            </div>' +
                                '            <div class="row">' +
                                '                <div class="col-xs-10  text-right">' +
                                                  data[i].Website+
                                '                </div>' +
                                '                <div class="col-xs-2">' +
                                '                    <i class="material-icons">domain</i>' +
                                '                </div>' +
                                '            </div>' +
                                '        </div>' +
                                '   </div>' +
                                '</div>' +
                            '</div>')
                    }
                }
            });
        }
        function GetSearchData(searchid)
        {
            $.ajax({
                type: 'GET',
                url: '{{url('/GetTravelSearch')}}',
                data: {TextId:searchid},
                global:false,
                success: function (data) {
                    var $AllTravels = $('#ListTravelAgent');
                    $AllTravels.empty();
                    if(data == ''){
                        $AllTravels.append('<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">' +'<h2>Travel tidak ada</h2>'+'</div>');
                    }else{
                        for (var i = 0; i < data.length; i++) {
                            $AllTravels.append('' +
                                '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">' +
                                    '<div class="card card-product card-travel pointer">' +
                                    '    <div class="card-image" data-header-animation="true">' +
                                    '        <img class="img" src="'+(data[i].Img  ? '{{ asset('storage/') }}'+'/'+data[i].Img : '{{ asset('images/default-hotel.jpg') }}')+'">'+
                                    '    </div>' +
                                    '    <div class="card-content">' +
                                    '        <div class="card-actions">' +
                                    '            <button type="button" class="btn btn-danger btn-simple fix-broken-card">' +
                                    '                <i class="material-icons">build</i> Fix Header!' +
                                    '            </button>' +
                                    '            <button type="button" class="btn btn-default btn-simple view-travel" data-id="'+data[i].IdKbih+'" rel="tooltip" data-placement="bottom" title="Detil">' +
                                    '                <i class="material-icons">art_track</i>' +
                                    '            </button>' +
                                    '            <button type="button" class="btn btn-success btn-simple input-travel" data-id="'+data[i].IdKbih+'" rel="tooltip" data-placement="bottom" title="Ubah">' +
                                    '                <i class="material-icons">edit</i>' +
                                    '            </button>' +
                                    '            <button type="button" class="btn btn-danger btn-simple delete-travel" data-id="'+data[i].IdKbih+'" rel="tooltip" data-placement="bottom" title="Hapus">' +
                                    '                <i class="material-icons">close</i>' +
                                    '            </button>' +
                                    '        </div>' +
                                    '        <h4 class="card-title text-left">' +
                                    '            <a href="#TravelName">'+data[i].KbihName.trunc(30)+'</a>' +
                                    '            <small><br>'+data[i].Owner+'</small>' +
                                    '        </h4>' +
                                    '        <div class="card-description">' +
                                    '            <div class="row">' +
                                    '                <div class="col-xs-10  text-right">' +
                                                        data[i].PhoneNum+
                                    '                </div>' +
                                    '                <div class="col-xs-2">' +
                                    '                    <i class="material-icons">phone</i>' +
                                    '                </div>' +
                                    '            </div>' +
                                    '            <div class="row">' +
                                    '                <div class="col-xs-10  text-right">' +
                                                        data[i].Email+
                                    '                </div>' +
                                    '                <div class="col-xs-2">' +
                                    '                    <i class="material-icons">mail</i>' +
                                    '                </div>' +
                                    '            </div>' +
                                    '            <div class="row">' +
                                    '                <div class="col-xs-10  text-right">' +
                                                        data[i].CityName+' '+data[i].KabName+
                                    '                </div>' +
                                    '                <div class="col-xs-2">' +
                                    '                    <i class="material-icons">place</i>' +
                                    '                </div>' +
                                    '            </div>' +
                                    '            <div class="row">' +
                                    '                <div class="col-xs-10  text-right">' +
                                                      data[i].RegDate+
                                    '                </div>' +
                                    '                <div class="col-xs-2">' +
                                    '                    <i class="material-icons">date_range</i>' +
                                    '                </div>' +
                                    '            </div>' +
                                    '            <div class="row">' +
                                    '                <div class="col-xs-10  text-right">' +
                                                      data[i].Website+
                                    '                </div>' +
                                    '                <div class="col-xs-2">' +
                                    '                    <i class="material-icons">domain</i>' +
                                    '                </div>' +
                                    '            </div>' +
                                    '        </div>' +
                                    '   </div>' +
                                    '</div>' +
                                '</div>')
                        }
                    }
                }
            });
        }

		$('#rp').slideReveal({
			// trigger: $(".rpt"),
			position: "right",
			push: false,
			overlay: true,
			zIndex:1300,
		});
		$('#rp').delegate('.rp-close','click',function() {
			$('#rp').slideReveal("hide")
		})

        $(document).delegate('.input-travel', 'click', function(e) {
            if($(this).attr('title') == 'Ubah'){
                $('.loading').show()
                $.ajax({
                    type: 'GET',
                    url: '{{url('/EditTravelAgent')}}',
                    data:{IdTravel:$(this).data('id')},
                    global:false,
                    success: function (data) {
                        $('#rp').empty().load("{{ route('travel-agen-input') }}", function(){
                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()

                            LoadProvince();

                            $('#SelectProvince').on('change', function() {
                                $('#SelectProvince option[value='+data['0'].ProvinceId+']').attr('selected','selected');
                                var SelectProvince = $(this).find(":selected").val();
                                LoadKabupaten(SelectProvince);
                            });

                            $('#SelectKabupaten').on('change', function() {
                                $('#SelectKabupaten option[value='+data['0'].KabId+']').attr('selected','selected');
                                var SelectKabupaten = $(this).find(":selected").val();
                                LoadKota(SelectKabupaten);
                            });

                            $('#SelectKota').on('change', function() {
                                $('#SelectKota option[value='+data['0'].CityId+']').attr('selected','selected');
                            });
                            var scr = '{{ asset('storage/') }}'+'/'+data['0'].Img;
                            $('.titleForm').empty().append('Ubah Agen '+data['0'].KbihName)
                            $("input[name='TypeSave']").val('EditSave')
                            $("input[name='IdTravel']").val(data['0'].IdKbih)
                            $("input[name='TravelName']").val(data['0'].KbihName)
                            $("input[name='Owner']").val(data['0'].Owner)
                            $("input[name='NoPilot']").val(data['0'].PilotNumber)
                            $("input[name='Address']").val(data['0'].Address)
                            $("input[name='NoTelp']").val(data['0'].PhoneNum)
                            $("input[name='NoFax']").val(data['0'].Fax)
                            $("input[name='Email']").val(data['0'].Email)
                            $("input[name='Siup']").val(data['0'].Siup)
                            $("input[name='Website']").val(data['0'].Website)
                            $('#Uploads').attr('data-img',data['0'].Img);
                            $('#IMS').attr("src", scr);

                            $('#rp').slideReveal("show")

                        })
                    }
                });

            }else{
                $('.loading').show()
                $('#rp').empty().load("{{ route('travel-agen-input') }}", function(){
                    $('.card-content-overflow').perfectScrollbar();
                    $('.loading').hide()
                    LoadProvince();

                    $('#SelectProvince').on('change', function() {
                        var SelectProvince = $(this).find(":selected").val();
                        LoadKabupaten(SelectProvince);
                    });

                    $('#SelectKabupaten').on('change', function() {
                        var SelectKabupaten = $(this).find(":selected").val();
                        LoadKota(SelectKabupaten);
                    });
                    $('#rp').slideReveal("show")
                })
            }

		})
        $(document).delegate('.view-travel', 'click', function(e) {
			// console.log('klik')
			$('.loading').show()
            $.ajax({
                type: 'GET',
                url: '{{url('/GetDetailTravel')}}',
                data:{IdTravel:$(this).data('id')},
                global:false,
                success: function (data) {
                    $('#c-modal').empty().load("{{ route('travel-agen-view') }}", function(){
                        $('.loading').hide()
                        var scr = '{{ asset('storage/') }}'+'/'+data['0'].Img;
                        $('#TN').empty().append(data['0'].KbihName)
                        $('#OW').empty().append(data['0'].Owner)
                        $('#PN').empty().append(data['0'].PhoneNum)
                        $('#EM').empty().append(data['0'].Email)
                        $('#AD').empty().append(data['0'].CityName+' '+data['0'].KabName)
                        $('#RD').empty().append(data['0'].RegDate)
                        $('#WB').empty().append(data['0'].Website)
                        $('#IM').attr("src", scr);
                        console.log(scr)
                        $('#modal').modal('show')
                    })
                }
            });
		})
        $(document).delegate('.delete-travel', 'click', function(e) {
			// console.log('klik')
            e.preventDefault();
            var IdTravel = $(this).data('id')
            swal({
                title: 'Apakah anda yakin?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Please wait')
                swal.showLoading()
                var data={
                    id:IdTravel,
                };
                $.post("{{route('delete-travel') }}", data,function(r) {

                    swal(
                        'Berhasil!',
                        'Data Travel sudah terhapus.',
                        'success'
                    ).then(function() {
                        GetListTravelAgent()
                    })
                }, 'json');

            })
		})

		// $(".card-travel").click(function(){
		// 	console.log('klik')
		// }).find(".card-actions").click(function(e) {
		// 	return false;
		// });
        function LoadProvince(){
			$.ajax({
				type: 'GET',
				url: '{{url('/ScanProvince')}}',
				global:false,
				success: function (data) {
					var $ScanProvince = $('#SelectProvince');
                    $ScanProvince.empty();
					for (var i = 0; i < data.length; i++) {
                        $ScanProvince.append('<option id="' + data[i].id + '" value="' + data[i].id + '">' + data[i].name + '</option>');
					}
					$('#SelectProvince option:first-child').attr("selected", "selected");
                    $ScanProvince.change();

				}
			});
        }

        function LoadKabupaten(ProvinceId)
		{
            $.ajax({
                type: 'GET',
                url: '{{url('/ScanKabupaten')}}',
				data:{IdProvince:ProvinceId},
                global:false,
                success: function (data) {
                    var $ScanKabupaten = $('#SelectKabupaten');
                    $ScanKabupaten.empty();
                    for (var i = 0; i < data.length; i++) {
                        $ScanKabupaten.append('<option id="' + data[i].id + '" value="' + data[i].id + '">' + data[i].name + '</option>');
                    }
                    $('#SelectKabupaten option:first-child').attr("selected", "selected");
                    $ScanKabupaten.change();

                }
            });
		}

        function LoadKota(KabupatenId)
		{
            $.ajax({
                type: 'GET',
                url: '{{url('/ScanKota')}}',
				data:{IdKabupaten:KabupatenId},
                global:false,
                success: function (data) {
                    var $ScanKota = $('#SelectKota');
                    $ScanKota.empty();
                    for (var i = 0; i < data.length; i++) {
                        $ScanKota.append('<option id="' + data[i].id + '" value="' + data[i].id + '">' + data[i].name + '</option>');
                    }
                    $('#SelectKota option:first-child').attr("selected", "selected");
                    $ScanKota.change();

                }
            });
		}
        $(document).delegate('#SaveTravelData', 'click', function(e) {
            e.preventDefault();
//            var ImageUpload = $('#Uploads').attr('data-img')
            var $validator = $('.card-content-overflow form').validate({
                rules: {
                    TravelName: {
                        required: true,
                        contentSpecialCharacters: true,
                    },
//                    Image: {
//                        required: $("#Uploads").attr('data-img') == 'kosong',
//                    },
                    Owner: {
                        required: true,
                        contentSpecialCharacters: true,
                    },
                    NoPilot: {
                        required: true,
                        phonetsel: true,
                    },
                    Address: {
                        required: true,
                    },
                    NoTelp: {
                        required: true,
                        telpindo: true,
                    },
                    NoFax: {
                        required: false,
                        telpindo: true,
                    },
                    Email: {
                        required: true,
                        email2: true
                    },
                    Siup: {
                        required: true,
                        alphanumeric: true
                    },


                },
                messages: {
                    TravelName: 
                    {
                       required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   
                    },
                    Owner: 
                    {
                        required: "Data Wajib diisi",
                        contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   

                    },
                    NoPilot: {
                        required: "Data Wajib diisi",
                        phonetsel: "Harus Nomor Telkomsel ",
                    },
                    Address: "Karakter dan Angka tanpa Simbol/Spesial Karakter",
                    NoTelp: "Harus Nomor Telepon Indonesia",
                    NoFax: "Harus Nomor Telepon Indonesia",
                    Siup: "Data Wajib diisi",
                    Email: "Wajib diisi dengan Email yang benar",
                    Image: "Image Wajib diisi",

                },
                errorPlacement: function(error, element) {
                    $(element).parent('div').addClass('has-error');
                    $(element).parent('div').append(error);

                }

            });
            if($("input[name='TypeSave").val() == 'SaveTravel') {
                $.validator.addClassRules("MyClass", {
                    required: true
                });
            }
            var $valid = $('.card-content-overflow form').valid();
            if(!$valid) {
                $validator.focusInvalid();
                if($("input[name='TypeSave").val() == 'SaveTravel'){
                    if($('#Uploads').hasClass('error')){
                        $('#errorimagekosong').empty().append('<label id="Image-error" class="error" for="Image">Gambar Wajib diisi</label>');
//                        console.log('error')
                    }else{
                        $('#errorimagekosong').empty();
//                        console.log('benar')
                    }
                }
                return false;
            }
            swal({
                title: 'Apakah Anda Yakin ?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon Tunggu')
                swal.showLoading()

                savedata()
            })
        });

        function savedata(){
            var oldimg = $("#FormTravel").find('#Uploads').data('img');
            var img = $("#FormTravel").find('#Uploads');
            var file_data = $(img).prop("files")[0];
            var form_data = new FormData();
            form_data.append("file", file_data)
            form_data.append("nm", '')
            form_data.append("dir", 'travels')
            form_data.append("w", 800)
            form_data.append("h", 600)

            $.ajax({
                url: "{{route('cms-upload') }}",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(p){
                    if (!p.p1) {
                        if (oldimg!=p.p1) {
                            p.p1=oldimg;
                        }
                    }

                    var data={
                        a:$("input[name='TypeSave").val(),
                        id:$("input[name='IdTravel").val(),
                        nam:$("input[name='TravelName").val(),
                        own:$("input[name='Owner").val(),
                        nopil:$("input[name='NoPilot").val(),
                        province:$('#SelectProvince').find(":selected").val(),
                        kabupaten:$('#SelectKabupaten').find(":selected").val(),
                        kota:$('#SelectKota').find(":selected").val(),
                        address:$("input[name='Address").val(),
                        notelp:$("input[name='NoTelp").val(),
                        nofax:$("input[name='NoFax").val(),
                        email:$("input[name='Email").val(),
                        siup:$("input[name='Siup").val(),
                        website:$("input[name='Website").val(),
                        img:p.p1
                    };

                    $.post("{{route('cms-inserttravel') }}", data,function(r) {
                        swal(
                            'Berhasil',
                            '',
                            'success'
                        ).then(function() {
                            $('#rp').empty().slideReveal("hide")
                            GetListTravelAgent()
                        })
                    }, 'json');
                }
            })            
        }

	</script>
@endsection