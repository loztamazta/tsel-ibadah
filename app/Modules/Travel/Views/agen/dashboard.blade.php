@extends('Dts::layouts.dts')

@section('h-title','Travel Agent Dashboard')
@section('title','Travel Agent Dashboard')

@section('content')
    <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="orange">
                    <i class="material-icons">supervisor_account</i>
                </div>
                <div class="card-content">
                    <p class="category">Total Jamaah</p>
                    <h3 class="card-title TotalJamaah"></h3>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="rose">
                    <i class="material-icons">flight_takeoff</i>
                </div>
                <div class="card-content">
                    <p class="category">Keberangkatan</p>
                    <h3 class="card-title">0</h3>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="green">
                    <i class="material-icons">flight_land</i>
                </div>
                <div class="card-content">
                    <p class="category">Rombongan</p>
                    <h3 class="card-title">0</h3>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats">
                <div class="card-header" data-background-color="blue">
                    <i class="material-icons">person</i>
                </div>
                <div class="card-content">
                    <p class="category">Pendamping</p>
                    <h3 class="card-title TotalPendamping"></h3>
                </div>
                <div class="card-footer">

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div id="ChartJamaah" style="height: 340px;"></div>
        </div>
        <div class="col-md-4">
            <div id="ChartPaket" style="height: 300px;"></div>
        </div>
        <div class="col-md-4">
            <div id="PieJamaah" style="height: 300px;"></div>
        </div>
    </div>
@endsection


@section('css')
@endsection

@section('js')
    <script src="{{asset('mdp/js/moment.min.js')}}"></script>
    <script src="{{ asset('mdp/js/fullcalendar.min.js') }}"></script>
    <script src="{{asset('vendor/echarts/echarts.min.js')}}"></script>
    <script type="application/javascript">
        GetSchedule();
        function GetSchedule(){
            $.ajax({
                url: '{{ route('schedule-travel') }}',
                type: 'GET',
                global: false,
                success: function (data) {
                    var EventTravel = [];
                    for (i = 0; i < data.GetKegiatan.length; i ++ ) {
                        EventTravel.push({
                            title:data.GetKegiatan[i].title,
                            start:data.GetKegiatan[i].start,
                            className:'Warning'
                        })
                    }
                    $('.TotalJamaah').empty().append(data.GetAllCount['0'].Counts);
                    $('.TotalPendamping').empty().append(data.GetPendamping['0'].Counts);
                    console.log(data)
                    $('#fullCalendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay'
                        },
                        events: EventTravel,
                        eventRender: function(event, element) {
                            if (event.className == 'booked') {
                                element.css({
                                    'background-color': '#333333',
                                    'border-color': '#333333'
                                });
                            }
                        }
                    });
                }
            });
        }
    </script>
    <script type="text/javascript">
        var SingleChart = option = {
            title: {
                text: 'Summary Jamaah',
                subtext: 'Jumlah Jamaah Perbulan'
            },
            tooltip : {
                trigger: 'axis',
                axisPointer : {
                    type : 'shadow'
                }
            },
            legend: {
                x : 'right',
                y : 'top',
                data:['Jamaah']
            },
            grid: {
                left: '1%',
                right: '1%',
                bottom: '20%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'category',
//                    data : ['January','February','March','April','May','June','July','August','September','October','November','December']
                    data : ['January','February','March','April','May']
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'Jamaah',
                    type:'bar',
                    data:[0, 0, 0, 0, 0]
                }
            ]
        };
        var MultiChart = option = {
            title: {
                text: 'Summary Pembelian Paket Jamaah',
                subtext: 'Jumlah Pembelian Paket Perbulan'
            },
            tooltip : {
                trigger: 'axis',
                axisPointer : {
                    type : 'shadow'
                }
            },
            legend: {
                x : 'right',
                y : 'bottom',
                data:['Internet Umroh 9 Hr','Internet Umroh 14 Hr','3 IN 1 Umroh 9 Hr','3 IN 1 Umroh 14']
            },
            grid: {
                left: '1%',
                right: '1%',
                bottom: '15%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'category',
                    data : ['January','February','March','April','May']
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:'Internet Umroh 9 Hr',
                    type:'bar',
                    stack: 'paket',
                    data:[0, 0, 0, 0, 0]
                },
                {
                    name:'Internet Umroh 14 Hr',
                    type:'bar',
                    stack: 'paket',
                    data:[0, 0, 0, 0, 0]
                },
                {
                    name:'3 IN 1 Umroh 9 Hr',
                    type:'bar',
                    stack: 'paket',
                    data:[0, 0, 0, 0, 0]
                },
                {
                    name:'3 IN 1 Umroh 14',
                    type:'bar',
                    stack: 'paket',
                    data:[0, 0, 0, 0, 0]
                }
            ]
        };
        var PieChart = option = {
            title : {
            },
            tooltip : {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            legend: {
                x : 'left',
                y : 'bottom',
                data:['Jamaah Pria','Jamaah Wanita']
            },
            toolbox: {
                show : true,
                feature : {
                    restore : {show: true},
                    saveAsImage : {show: true}
                }
            },
            calculable : true,
            series : [
                {

                    type:'pie',
                    itemStyle : {
                        normal : {
                            label : {
                                show : true,
                                formatter : "{d} %"
                            },
                            labelLine : {
                                show : true
                            }
                        }
                    },
                    data:[
                        {value:0, name:'Jamaah Wanita'},
                        {value:0,  name:'Jamaah Pria'}
                    ]
                }
            ]
        };
        var ChartsSummary = echarts.init(document.getElementById('ChartJamaah'));
        ChartsSummary.setOption(SingleChart);

        var ChartsPaket = echarts.init(document.getElementById('ChartPaket'));
        ChartsPaket.setOption(MultiChart);

        var ChartsJamaah = echarts.init(document.getElementById('PieJamaah'));
        ChartsJamaah.setOption(PieChart);

        setTimeout(function (){
            window.onresize = function () {
                ChartsSummary.resize();
                ChartsPaket.resize();
                ChartsJamaah.resize();
            }
        },200);
    </script>
@endsection