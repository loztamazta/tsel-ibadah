<div class="card">
	<div class="card-header card-header-icon" data-background-color="blue">
		<i class="material-icons">mode_edit</i>
	</div>
	<span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
		<i class="material-icons">close</i>
	</span>
	<div class="card-content">
		<h4 class="card-title titleForm">Travel baru</h4>
		<div class="card-content-overflow">
			<form id="FormTravel">
				<input type="hidden" name="IdTravel">
				<input type="hidden" name="TypeSave" value="SaveTravel">
				<div class="fileinput fileinput-new text-center" data-provides="fileinput">
					<div class="fileinput-new thumbnail">
						<img id="IMS" src="{{ asset('images/image_placeholder.jpg') }}" alt="...">
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail"></div>
					<div>
						<span class="btn btn-rose btn-round btn-file">
						<span class="fileinput-new">Pilih Gambar</span>
						<span class="fileinput-exists">Ganti Gambar</span>
							<input type="file" name="Image" id="Uploads" class="MyClass " data-image="kosong"/>
						</span>
						<a href="#RmImage" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
					</div>
					<span id="errorimagekosong"></span>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nama Travel</label>
					<input type="text" class="form-control" name="TravelName">
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Pemilik</label>
					<input type="text" class="form-control" name="Owner" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">No Pilot</label>
					<input type="text" class="form-control" name="NoPilot" required>
					<span class="error_label"></span>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Provinsi</label>
					<select class="form-control" id="SelectProvince" name="Provinsi" required>
					</select>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Kabupaten</label>
					<select class="form-control" id="SelectKabupaten" name="Kabupaten" required>
					</select>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Kota</label>
					<select class="form-control" id="SelectKota" name="Kota" required>
					</select>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Alamat</label>
					<input type="text" class="form-control" name="Address" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">No Telp</label>
					<input type="text" class="form-control" name="NoTelp" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">No Fax</label>
					<input type="text" class="form-control" name="NoFax" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Email</label>
					<input type="email" class="form-control" name="Email">
				</div>
				<div class="form-group label-floating">
					<label class="control-label">SIUP</label>
					<input type="text" class="form-control" name="Siup" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Situs Web</label>
					<input type="text" class="form-control" name="Website" >
				</div>
			</form>
		</div>
	</div>
	<div class="card-footer">
		<div class="pull-right">
			<button type="submit" class="btn btn-fill btn-rose" id="SaveTravelData">Simpan</button>
		</div>
	</div>
</div>

<script>
	
</script>