<div class="card">
	<div class="card-header card-header-icon" data-background-color="blue">
		<i class="material-icons">mode_edit</i>
	</div>
	<span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
		<i class="material-icons">close</i>
	</span>
	<div class="card-content">
		<h4 class="card-title">{{ $j ? 'Ganti jamaah : '.$j->FirstName.' '.$j->LastName.' '.$j->ThirdName : 'Tambah Jamaah' }}</h4>
		<div class="card-content-overflow">
			<form id="fjamaah">
				<input type="hidden" name="idjamaah" id="idjamaah" value="{{ $j?$j->IdUserApps:0 }}">
				<input type="hidden" name="idpack" value="{{ $idpack }}">
				<div class="fileinput fileinput-new text-center" data-provides="fileinput">
					<div class="fileinput-new thumbnail">
						<img id="IMS" src="{{ $j ? asset('storage/'.$j->Img) : asset('images/image_placeholder.jpg') }}" alt="...">
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail"></div>
					<div>
						<span class="btn btn-rose btn-round btn-file">
						<span class="fileinput-new">Pilih Gambar</span>
						<span class="fileinput-exists">Ganti</span>
							<input type="file" name="______" id="img" data-old="{{ $j ? $j->Img : '' }}" />
						</span>
						<a href="#RmImage" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
					</div>
				</div>

				<div class="form-group label-floating">
					<label class="control-label">Gelar</label>
					<select class="selectpicker" name="title" data-style="select-with-transition">
						<option value="LK" {{ $j ? ($j->Title == 'LK' ? 'selected':'') : '' }}>Tn. </option>
						<option value="PR" {{ $j ? ($j->Title == 'PR' ? 'selected':'') : '' }}>Ny.</option>
					</select>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group label-floating">
							<label class="control-label">Nama Depan</label>
							<input type="text" class="form-control" name="firstname" value="{{ $j?$j->FirstName:'' }}" required>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group label-floating">
							<label class="control-label">Nama Tengah</label>
							<input type="text" class="form-control" name="lastname" value="{{ $j?$j->LastName:'' }}" required>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group label-floating">
							<label class="control-label">Nama Belakang</label>
							<input type="text" class="form-control" name="thirdname" value="{{ $j?$j->ThirdName:'' }}" required>
						</div>
					</div>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Email</label>
					<input type="email" class="form-control" name="email" value="{{ $j?$j->Email:'' }}" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nomor HP</label>
					<input type="number" class="form-control" name="mobileno" value="{{ $j?$j->MobileNo:'' }}" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Alamat</label>
					<input type="text" class="form-control" name="address" value="{{ $j?$j->Address:'' }}" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Tanggal Lahir</label>
					<input name="dateofbirth" value="{{ $j?$j->DateOfBirth:'' }}" type="text" class="form-control datetimepicker" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nomor Telepon Rumah</label>
					<input type="number" class="form-control" name="phoneno" value="{{ $j?$j->PhoneNo:'' }}" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nomer ID</label>
					<input type="number" class="form-control" name="idnumber" value="{{ $j?$j->IdNumber:'' }}" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nomor Passport</label>
					<input type="text" class="form-control" name="passportno" value="{{ $j?$j->PassportNo:'' }}" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nomor Visa</label>
					<input type="text" class="form-control" name="visano" value="{{ $j?$j->VisaNo:'' }}" required>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nomor Kloter</label>
					<input type="number" class="form-control" name="kloterno" value="{{ $j?$j->KloterNo:'' }}" required>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="fileinput fileinput-new text-center" data-provides="fileinput">
							<div class="fileinput-new thumbnail">
								<img id="IMS" src="{{ $j ? asset('storage/'.$j->ImgPassport) : asset('images/image_placeholder.jpg') }}" alt="...">
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail"></div>
							<div>
								<span class="btn btn-rose btn-round btn-file">
								<span class="fileinput-new">Gambar Passport</span>
								<span class="fileinput-exists">Ganti</span>
									<input type="file" name="imgpassport" id="imgpassport" class="MyClassPs " data-old="{{ $j ? $j->ImgPassport : '' }}" />
								</span>
								<a href="#RmImage" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
							</div>
                            <span id="errorimagekosongPs"></span>
						</div>
					</div>
					<div class="col-md-6">
						<div class="fileinput fileinput-new text-center" data-provides="fileinput">
							<div class="fileinput-new thumbnail">
								<img id="IMS" src="{{ $j ? asset('storage/'.$j->ImgVisa) : asset('images/image_placeholder.jpg') }}" alt="...">
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail"></div>
							<div>
								<span class="btn btn-rose btn-round btn-file">
								<span class="fileinput-new">Gambar Visa</span>
								<span class="fileinput-exists">Ganti</span>
									<input type="file" name="imgvisa" id="imgvisa" class="MyClassVs " data-old="{{ $j ? $j->ImgVisa : '' }}"/>
								</span>
								<a href="#RmImage" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
							</div>
                            <span id="errorimagekosongVs"></span>
						</div>					
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="card-footer">
		<div class="pull-right">
			<button type="submit" class="btn btn-fill btn-rose" id="sfjamaah">Simpan</button>
		</div>
	</div>
</div>
<script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
<script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('vendor/additional-methods.js') }}"></script>
<script>

	$('.selectpicker').selectpicker();
    $('.datetimepicker').datetimepicker({
        format: 'YYYY/MM/DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove',
            inline: true
        }
    });

	idkbih={{ Auth::user()->t }}
	$('#sfjamaah').click(function(e) {
		e.preventDefault()
        var $validator = $('#fjamaah').validate({
            rules: {
                firstname: {
                    required: true,
                    contentSpecialCharacters:true
                },

                lastname: {
                    required: true,
                    contentSpecialCharacters:true
                },
                thirdname: {
                    required: true,
                    contentSpecialCharacters:true
                },
                email: {
                    required: true,
                    email2: true,
                },
                mobileno: {
                    required: true,
                    phonetsel: true,
                },
                phoneno: {
                    required: true,
                    telpindo: true,
                },
                passportno: {
                    required: true,
                    contentSpecialCharacters:true,
                },
                visano: {
                    required: true,
                    contentSpecialCharacters:true,
                },
                idnumber: {
                    required: true,
                    contentSpecialCharacters:true,
                },
                // imgpassport: {
                //     required: true,
                // },
                // imgvisa: {
                //     required: true,
                // },
            },
            messages: {
                firstname: 
                    {
                       required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   
                    },

                lastname: 
                    {
                       required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   
                    },

                thirdname: 
                    {
                       required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   
                    },

                    
                mobileno: 
                    {
                       required: "Data Wajib diisi",
                       phonetsel : "Harus Nomor Telkomsel "   
                    },


                visano: 
                    {
                       required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   
                    },

                idnumber: 
                    {
                       required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   
                    },

                passportno: 
                    {
                       required: "Data Wajib diisi",
                       contentSpecialCharacters : "Tidak boleh mengandung Simbol/Spesial Karakter"   
                    },
                email: "Wajib diisi dengan Format Email yang benar",
                phoneno: "Harus Nomor Telepon Indonesia",
                imgpassport:"Wajib Diisi",
                imgvisa:"Wajib Diisi",

            },
            errorPlacement: function(error, element) {
            	console.log(error, element)
            	// if (element[0]==$('input#imgpassport.error')[0]||element[0]==$('input#imgvisa.error')[0]) {
            	// 	console.log('gambar')
	            //     $(element).parents('.fileinput').addClass('has-error');
	            //     $(element).parents('.fileinput').append(error);
            	// } else {
	                $(element).parent('div').addClass('has-error');
	                $(element).parent('div').append(error);
	            // }

            }

        });
        if($('#idjamaah').val() == '0') {
            $.validator.addClassRules("MyClassPs", {
                required: true
            });

            $.validator.addClassRules("MyClassVs", {
                required: true
            });
        }   
        var $valid = $('#fjamaah').valid();
        if(!$valid) {
            $validator.focusInvalid();
            if($("#idjamaah").val() == '0'){
                    if($('#imgvisa').hasClass('error')){
                        $('#errorimagekosongVs').empty().append('<label id="Image-error" class="error" for="Image">Gambar Wajib diisi</label>');
//                        console.log('error')
                    }else{
                        $('#errorimagekosongVs').empty();
//                        console.log('benar')
                    }
                    if($('#imgpassport').hasClass('error')){
                        $('#errorimagekosongPs').empty().append('<label id="Image-error" class="error" for="Image">Gambar Wajib diisi</label>');
//                        console.log('error')
                    }else{
                        $('#errorimagekosongPs').empty();
//                        console.log('benar')
                    }
                }
            return false;
        }
//        var $validator = $('#fjamaah').validate({
//            rules: {
//                firstname: {
//                    required: true,
//                },
//
//            },
//
//            errorPlacement: function(error, element) {
//                $(element).parent('div').addClass('has-error');
//            }
//        });
//        var $valid = $('#fjamaah').valid();
//        if(!$valid) {
//            $validator.focusInvalid();
//            return false;
//        }

        swal({
            title: 'Apakah Anda Yakin?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then(function() {
            swal('Mohon Menunggu')
            swal.showLoading()

            old1=$("#img").data('old')
            var form_data = new FormData();
			form_data.append("file", $("#img").prop("files")[0])
            form_data.append("dir", 'jamaah')
            form_data.append("w", 600)
            form_data.append("h", 800)

            old2=$("#imgpassport").data('old')
            old3=$("#imgvisa").data('old')
            var form_data2 = new FormData();
			form_data2.append("file1", $("#imgpassport").prop("files")[0])
			form_data2.append("file2", $("#imgvisa").prop("files")[0])
            form_data2.append("dir", 'jamaah')
            form_data2.append("w",'auto')
            form_data2.append("h",'auto')

            $.ajax({
                url: "{{route('cms-upload') }}",
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function(p){
					if (!p.p1) {
						if (old1!=p.p1) {
							p.p1=old1;
						}
					}
                    $.ajax({
                        url: "{{route('cms-uploadtwo') }}",
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data2,
                        type: 'post',
                        success: function (ps) {
                            if (!ps.p1) {
								if (old2!=ps.p1) {
									ps.p1=old2;
								}
                            }
                            if (!ps.p3) {
								if (old3!=ps.p3) {
									ps.p3=old3;
								}
                            }
//                          console.log(ps);
							
							if ($('#idjamaah').val() == '0') {
	                            data='a=savejamaah'
							} else {
	                            data='a=editjamaah'
							}
                            data+='&Img='+p.p1+'&ImgPassport='+ps.p1+'&'+'ImgVisa='+ps.p3+'&'
                            data+=$("form#fjamaah").serialize();

                            $.post("{{route('travel-grup-data') }}", data,function(r) {
                                swal(
                                    'Berhasil',
                                    '',
                                    'success'
                                ).then(function() {
                                    $('#rp').empty().slideReveal("hide")
                                    $('#list').DataTable().ajax.reload(null, false);
                                })
                            }, 'json').fail(function(response) {
                                console.log(response)
                                swal(
                                    'Terjadi Kesalahan!',
                                    //response.responseText,
									'Mohon Dicek Kembali',
                                    'error'
                                )
                            });
                        }
                    });

                },
                fail: function (response) {
                    swal(
                        'Whoops.. Ada yang salah!',
                        response.responseText,
                        'error'
                    )
                }
            })

        })

	})

</script>