@extends('Dts::layouts.dts')


@section('title','Detail Rombongan')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-pills nav-pills-info">
				<li class="active">
					<a href="#tab1" data-toggle="tab">Rombongan</a>
				</li>
				<li>
					<a href="#tab2" data-toggle="tab">Jamaah</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="card-content-overflow tab-content">
		{{-- 1 --}}
		<div class="tab-pane active form-horizontal" id="tab1">
			<div class="row">
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<a href="{{ url()->previous() }}" class="btn btn-sm btn-info">
								<i class="fa fa-chevron-left fa-fw" aria-hidden="true"></i>&nbsp;
								<span class="hidden-xs hidden-sm"> Kembali</span>
							</a>
							<h4 class="card-title">Detail Keberangkatan Rombongan
								<small>{{$a[0]->PackName}}</small>
							</h4>

						</div>
						<div class="card-content">
							<form class="form-horizontal">
								<div class="form-group label-floating">
									<label class="control-label">Nama Rombongan</label>
									<input name="PackName" type="text" class="form-control" readonly value="{{$a[0]->PackName}}">
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Tanggal Keberangkatan</label>
									<input name="DateofDepa" type="text" class="form-control"  id="DateofDepa" readonly value="{{ Carbon\Carbon::createFromFormat('Y-m-d', $a[0]->DateofDepa)->format('d/m/Y') }}"/>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Paket Perjalanan</label>
									<input name="PilihPaket" type="text" id="PilihPaket" data-val="{{$a[0]->IntervalDate}}" data-id="{{$a[0]->IdPackege}}" class="form-control"  readonly value="{{$a[0]->name}}"/>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group label-floating">
											<label class="control-label">Hotel Mekkah</label>
											<input name="PilihHMekkah" type="text" class="form-control"  readonly value="{{$a[0]->HM}}"/>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group label-floating">
											<label class="control-label">Hotel Madinah</label>
											<input name="PilihHMadinah" type="text" class="form-control"  readonly value="{{$a[0]->HME}}"/>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group label-floating">
											<label class="control-label">Hotel Jeddah</label>
											<input name="PilihHJeddah" type="text" class="form-control"  readonly value="{{$a[0]->HMF}}"/>
										</div>
									</div>
								</div>
								<div class="form-group label-floating">
									<div class="form-group label-floating">
										<label class="control-label">Pendamping</label>
										<input name="PilihStaff" type="text" class="form-control"  readonly value="{{$a[0]->MtwFirstName}} {{$a[0]->MtwLastName}}"/>
									</div>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Maskapai Pemberangkatan</label>
									<input name="MaskapaiB" type="text" class="form-control"  readonly value="{{$a[0]->MaskapaiB}}"/>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Maskapai Kepulangan</label>
									<input name="MaskapaiP" type="text" class="form-control"  readonly value="{{$a[0]->MaskapaiP}}"/>
								</div>
								{{--<div class="form-group label-floating">--}}
									{{--<select class="selectpicker" data-style="select-with-transition" title="Single Select" data-size="7">--}}
										{{--<option disabled selected>Paket Roaming</option>--}}
										{{--<option value="2">Foobar</option>--}}
										{{--<option value="3">Is great</option>--}}
									{{--</select>--}}
								{{--</div>--}}
								<span class="btn btn-sm btn-warning" id="chg-dt" data-id="{{$a[0]->IdPack}}">
									<i class="fa fa-fw fa-pencil"></i> &nbsp;
									<span class="txt">Ubah</span>
								</span>
								<span class="btn btn-sm btn-info" id="save-dt" style="display: none;" data-idpack="{{ $a[0]->IdPack }}">
									<i class="fa fa-fw fa-check"></i> &nbsp;
									<span class=""> Simpan</span>
								</span>

							</form>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							<h4 class="card-title">Detail Kegiatan Rombongan
								<small>{{$a[0]->PackName}}  </small>
							</h4>
						</div>
						<div class="card-content" id="DetailItener">

						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-pane form-horizontal" id="tab2">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header card-header-icon" data-background-color="rose">
							<i class="material-icons">people</i>
						</div>
						<span class="card-header card-header-icon round btn btn-info btn-xs pull-right input-jamaah" data-background-color="blue">
							<i class="material-icons">add</i>
						</span>                    
						<div class="card-content">
						<h4 class="card-title">Daftar Jamaah</h4>
							<table id="list" class="table table-striped table-no-bordered table-hover compact" width="100%" >
								<thead>
									<tr class="">
										<th class="text-center"></th>
										<th class="col-md-2">Nama</th>
										<th class="col-md-1">Kelamin</th>
										<th class="col-md-2">Tlp</th>
										<th class="col-md-3">Alamat</th>
										<th class="col-md-2">Lahir</th>
										<th class="col-md-1">Passport</th>
										<th class="col-md-1">Visa</th>
										<th class="text-right">Action</th>
									</tr>
								</thead>
							</table>								
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>
@endsection

@section('after-content')
	<div id='rp' class="slidePanel slidePanel-right">
	</div>
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
@endsection

@section('js')
	<script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
	<script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
	<script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
	<script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
	<!-- DateTimePicker Plugin -->
	<script src="{{ asset('mdp/js/jquery.bootstrap-wizard.js') }}"></script>
	<script src="{{asset('mdp/js/moment.min.js')}}"></script>
	<script src="{{asset('mdp/js/bootstrap-datetimepicker.js') }}"></script>
	<script src="{{ asset('mdp/js/wizard.js') }}"></script>

	<script>
		$('.datepicker').datetimepicker();
	</script>
	<!-- Select Plugin -->
	<script src="{{asset('mdp/js/jquery.select-bootstrap.js') }}"></script>

	<script>
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});		
	</script>
	{{-- right panel --}}
	<script>
        var kbihid={{$a[0]->KbihId}}
		$('#rp').slideReveal({
			// trigger: $(".rpt"),
			position: "right",
			push: false,
			overlay: true,
			zIndex:1300,
		});
		$('#rp').delegate('.rp-close','click',function() {
			$('#rp').slideReveal("hide")
		})
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY',
            minDate:moment(),
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove',
                inline: true
            }
        });

		// edit mode
		$('#chg-dt').click(function(e){
			e.preventDefault()
            $('.loading').show()
			console.log($(this).data('id'))
            $.ajax({
                type: 'GET',
                url: '{{route('travel-group-getedit')}}',
                data:{IdPack:$(this).data('id')},
                global:false,
                success: function (data) {
                    console.log(data)
                    $('#rp').empty().load("{{ route('travel-grup-edit') }}", function(){
                        $('.card-content-overflow').perfectScrollbar();
                        $('.loading').hide()
                        demo.initMaterialWizard();
                        LoadDataPHS()
						$("input[name='PackId']").val(data[0].IdPack)
						$("input[name='PackNames']").val(data[0].PackName)
						$("input[name='DateofDepas']").val(data[0].DateofDepa)
						$("input[name='MaskapaiPs']").val(data[0].MaskapaiP)
						$("input[name='MaskapaiBs']").val(data[0].MaskapaiB)

                        $(document).delegate(".nextRombongan","click", function (e) {

                            Date.prototype.addDays = function(days) {
                                var dat = new Date(this.valueOf())
                                dat.setDate(dat.getDate() + days);
                                return dat;
                            }

                            function getDates(startDate, stopDate) {
                                var dateArray = new Array();
                                var currentDate = startDate;
                                while (currentDate <= stopDate) {
                                    dateArray.push(currentDate)
                                    currentDate = currentDate.addDays(1);
                                }
                                return dateArray;
                            }

                            var dateArray = getDates(new Date($('#DateofDepas').val()), (new Date($('#DateofDepas').val())).addDays($('#PilihPakets').find(":selected").data('val')));
                            $.ajax({
                                type: 'GET',
                                url: '{{url('/GetKegiatanTime')}}',
                                data:{PaketId:$('#PilihPakets').find(":selected").val()},
                                global: false,
                                success: function (data) {
                                    $("#accordionItenerEdit").empty();
                                    // console.log(data)
                                    for (i = 1; i < dateArray.length; i ++ ) {
                                        // console.log(getFormattedDate(dateArray[i]));
                                        $("#accordionItenerEdit").append('' +
                                            '<div class="panel panel-default">'+
                                            '	<div class="panel-heading" role="tab" id="kegiatan'+i+'">'+
                                            '		<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+i+'" aria-expanded="true" aria-controls="collapse'+i+'">'+
                                            '			<h4 class="panel-title">'+
                                            getFormattedDate(dateArray[i])+
                                            '				<i class="material-icons">keyboard_arrow_down</i>'+
                                            '			</h4>'+
                                            '		</a>'+
                                            '	</div>'+
                                            '	<div id="collapse'+i+'" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading'+i+'">'+
                                            '		<div class="panel-body BersihLahEdit" id="DayItenerTimeEdit'+i+'">'+


                                            '		</div>'+
                                            '	</div>'+
                                            '</div>');
                                    }
                                    $('.BersihLahEdit').empty();
                                    for (var i = 0; i < data.TimeKegiatan.length; i++) {

                                        var b = $('#DayItenerTimeEdit'+data.TimeKegiatan[i].dayinter).append(
                                            '			<div class="row ListKegiatan'+i+'">'+
                                            '				<div class="col-xs-4">'+
                                            '					<div class="form-group label-floating">'+
                                            '						<label class="control-label">Mulai</label>'+
                                            '						<input type="text" class="form-control timepicker" value="'+data.TimeKegiatan[i].itinerary_time+'" readonly/>'+
                                            '					</div>'+
                                            '				</div>'+
                                            '				<div class="col-xs-8">'+
                                            '					<div class="form-group label-floating">'+
                                            '						<label class="control-label">Kegiatan</label>'+
                                            '						<input type="text" class="form-control KegName" value="'+data.TimeKegiatan[i].activity_name+'" readonly/>'+
                                            '					</div>'+
                                            '				</div>'+
                                            '			</div>'
                                        )
                                        // console.log(b)
                                    }
                                }
                            });


                        });

                        $('.datetimepicker').datetimepicker({
                            format: 'YYYY-MM-DD',
                            icons: {
                                time: "fa fa-clock-o",
                                date: "fa fa-calendar",
                                up: "fa fa-chevron-up",
                                down: "fa fa-chevron-down",
                                previous: 'fa fa-chevron-left',
                                next: 'fa fa-chevron-right',
                                today: 'fa fa-screenshot',
                                clear: 'fa fa-trash',
                                close: 'fa fa-remove',
                                inline: true
                            }
                        });
                        $('#rp').slideReveal("show")
                    });

                }
            });

//            $(this).find('.fa').toggleClass('fa-pencil').toggleClass('fa-times')
//			if($(this).find('.fa').find('.txt').text()=='Batal'){
//				$(this).find('.txt').text('Ubah')
//			}
//			else{
//				$(this).find('.txt').text('Batal')
//			}
//			$('#save-dt').slideToggle('fast')
//			$('form.form-horizontal').find('input').prop("readonly", (_, val) => !val)
//			$('input[name="PackName"]').focus()
		})
        function LoadDataPHS(){
            $.ajax({
                type: 'GET',
                url: '{{url('/GetDataPHS')}}',
                data:{kbihid:kbihid},
                global:false,
                success: function (data) {
                    var $ScanPaket = $('#PilihPakets');

                    $ScanPaket.empty();
                    for (var i = 0; i < data.Paket.length; i++) {
                        $ScanPaket.append('<option id="' + data.Paket[i].id + '" value="' + data.Paket[i].id + '" data-val="' + data.Paket[i].IntervalDate + '">' + data.Paket[i].name + '</option>');
                    }
                    console.log(data.Paket)
                    $('#PilihPakets  option[value={{$a[0]->IdPackege}}]').attr("selected", "selected");

                    var $ScanMk = $('#PilihHMekkahs');
                    $ScanMk.empty();
                    for (var i = 0; i < data.HotelMe.length; i++) {
                        $ScanMk.append('<option id="' + data.HotelMe[i].id + '" value="' + data.HotelMe[i].id + '">' + data.HotelMe[i].name + '</option>');
                    }
                    $('#PilihHMekkahs option[value={{$a[0]->HotelMekkahId}}]').attr('selected','selected');

                    var $ScanMd = $('#PilihHMadinahs');
                    $ScanMd.empty();
                    for (var i = 0; i < data.HotelMa.length; i++) {
                        $ScanMd.append('<option id="' + data.HotelMa[i].id + '" value="' + data.HotelMa[i].id + '">' + data.HotelMa[i].name + '</option>');
                    }
                    $('#PilihHMadinahs option[value={{$a[0]->HotelMadinahId}}]').attr('selected','selected');
                    var $ScanJd = $('#PilihHJeddahs');
                    // $ScanJd.empty();
                    for (var i = 0; i < data.HotelJd.length; i++) {
                        $ScanJd.append('<option id="' + data.HotelJd[i].id + '" value="' + data.HotelJd[i].id + '">' + data.HotelJd[i].name + '</option>');
                    }

                    PilihHJeddahs=('{{$a[0]->HotelJeddahId}}'?'{{$a[0]->HotelJeddahId}}':0)
                    $('#PilihHJeddahs option[value='+PilihHJeddahs+']').attr('selected','selected');

                    // var $ScanStaff = $('#PilihStaffs');
                    // $ScanStaff.empty();
                    // for (var i = 0; i < data.Staff.length; i++) {
                    //     $ScanStaff.append('<option id="' + data.Staff[i].Idmtw + '" value="' + data.Staff[i].Idmtw + '">' + data.Staff[i].MtwFirstName + '</option>');
                    // }
                    // $('#PilihStaff option[value={{$a[0]->StaffId}}]').attr('selected','selected');
                    loads({{$a[0]->StaffId}})
                }
            });
        }
        $('#rp').delegate('#PilihPakets','change',function() {
            loads()
        })
        $('#rp').delegate('#DateofDepa','dp.change',function() {
            loads()
        })
        function loads(staf){
        	staf=(staf?staf:0)
            data={
                a:'loadS',
                kbihid:kbihid,
                dt:moment($('#DateofDepa').val(),'MM/DD/YYYY').format('YYYY-MM-DD'),
                pkt:$('#PilihPaket').val()
            }
            $.post("{{ route('travel-grup-data') }}", data, function(R) {
                var $ScanStaff = $('#PilihStaffs');
                $ScanStaff.empty();
                for (var i = 0; i < R.length; i++) {
                    $ScanStaff.append('<option id="' + R[i].Idmtw + '" value="' + R[i].Idmtw + '">' + R[i].MtwFirstName+' '+R[i].MtwLastName+ '</option>');
                }
                $('#PilihStaffs option[value='+staf+']').attr('selected','selected');
                // $('#PilihStaff option:first-child').attr("selected", "selected");
            }, 'json')
        }

		// save
		$('#save-dt').click(function(e){
			// d=$('form.form-horizontal').serialize()
			console.log($(this).data('idpack'))
			e.preventDefault()

	        swal({
	            title: 'Apakah Anda Yakin?',
	            type: 'question',
	            showCancelButton: true,
	            confirmButtonText: 'Ya!',
	            cancelButtonText: 'Batal',
	        }).then(function() {
				swal('Mohon menunggu')
				swal.showLoading()

				p='a=edit&idpack='+$(this).data('idpack')+'&'
				p+=$("form").serialize();
				$.post("{{ route('travel-grup-data') }}", p,function(r) {
					console.log(r)
					swal(
						'Berhasil',
						'',
						'success'
					).then(function() {
						$('#chg-dt').click()
					})
				}, 'json').fail(function(response) {
					swal(
						'Terjadi Kesalahan!',
						//response.responseText,
						'Mohon Dicek Kembali',
						'error'
					)
				})
	        })
		})

		$("#DetailItener").empty();
		Date.prototype.addDays = function(days) {
			var dat = new Date(this.valueOf())
			dat.setDate(dat.getDate() + days);
			return dat;
		}

		function getDates(startDate, stopDate) {
			var dateArray = new Array();
			var currentDate = startDate;
			while (currentDate <= stopDate) {
				dateArray.push(currentDate)
				currentDate = currentDate.addDays(1);
			}
			return dateArray;
		}
		DetailItenerss()
		var dateArray = getDates(new Date(moment($('#DateofDepa').val(),'DD/MM/YYYY').format('YYYY-MM-DD')), (new Date(moment($('#DateofDepa').val(),'DD/MM/YYYY').format('YYYY-MM-DD'))).addDays($('#PilihPaket').data('val')-1 ));
		function DetailItenerss() {
			$.ajax({
				type: 'GET',
				url: '{{url('/GetKegiatanTime')}}',
				data:{PaketId:$('#PilihPaket').data('id')},
				global: false,
				success: function (data) {
					// console.log(data)
					// console.log('dateArray',dateArray)
					for (i = 0; i < dateArray.length; i ++ ) {
						// console.log('getFormattedDate',getFormattedDate(dateArray[i]));
						$("#DetailItener").append('' +
										'<div class="panel panel-default">'+
										'	<div class="panel-heading" role="tab" id="kegiatan'+i+'">'+
										'		<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+i+'" aria-expanded="true" aria-controls="collapse'+i+'">'+
										'			<h4 class="panel-title">'+
														getFormattedDate(dateArray[i])+
										'				<i class="material-icons">keyboard_arrow_down</i>'+
										'			</h4>'+
										'		</a>'+
										'	</div>'+
										'	<div id="collapse'+i+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'+i+'">'+
										'		<div class="panel-body" id="DayItenerTime'+i+'">'+


										'		</div>'+
										'	</div>'+
										'</div>');
					}
//                            $('.ListKegiatan').empty()
					for (var i = 0; i < data.TimeKegiatan.length; i++) {

						var b = $('#DayItenerTime'+data.TimeKegiatan[i].dayinter).append(
							'			<div class="row ListKegiatan'+i+'">'+
							'				<div class="col-xs-4">'+
							'					<div class="form-group label-floating">'+
							'						<label class="control-label">Mulai</label>'+
							'						<input type="text" class="form-control timepicker" value="'+data.TimeKegiatan[i].itinerary_time+'" readonly/>'+
							'					</div>'+
							'				</div>'+
							'				<div class="col-xs-8">'+
							'					<div class="form-group label-floating">'+
							'						<label class="control-label">Kegiatan</label>'+
							'						<input type="text" class="form-control KegName" value="'+data.TimeKegiatan[i].activity_name+'" readonly/>'+
							'					</div>'+
							'				</div>'+
							'			</div>'
						)
						// console.log(b)
					}
				}
			});
		}

		// input jamaah
		$(".input-jamaah").click(function(){
			$('.loading').show()
			$('#rp').empty().load("{{ route('travel-grup-jamaahinput',[$a[0]->IdPack,0]) }}", function(){
				$('.card-content-overflow').perfectScrollbar();
				$('.loading').hide()
				$('#rp').slideReveal("show")
			})
		})

		$(".card-travel").click(function(){
			console.log('klik')
		}).find(".card-actions").click(function(e) {
			return false;
		});
		function getFormattedDate(today)
		{
			var week = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
			var months = [ "January", "February", "March", "April", "May", "June",
				"July", "August", "September", "October", "November", "December" ];
			var day  = week[today.getDay()];
			var dd   = today.getDate();
			var mm   = months[today.getMonth()]; //January is 0!
			var yyyy = today.getFullYear();
			var hour = today.getHours();
			var minu = today.getMinutes();

			if(dd<10)  { dd='0'+dd }
			if(mm<10)  { mm='0'+mm }
			if(minu<10){ minu='0'+minu }

			return day+' - '+dd+' '+mm+' '+yyyy;
		}
	</script>

	{{-- datatable --}}
	<script src="{{ asset('mdp/js/jquery.datatables.js') }}"></script>
	<script>
		$("#list").DataTable({
			ajax: {
				url: "{{ route('travel-grup-data') }}",
				dataSrc: '',
				type: "POST",
				data: {
					a:'getjamaah',
					b:'{{ $a[0]->IdPack }}',
				},
			},
			columns: [
				{
					render: function (data, type, full, meta) {
						a=''
						+'	<div class="text-center">'
						+'		<button class="btn btn-info btn-round btn-fab btn-fab-mini list-dtl" data-id="'+full.IdUser+'">'
						+'			<i class="material-icons">zoom_in</i>'
						+'			<div class="ripple-container"></div>'
						+'		</button>'
						+'	</div>'
						return a
					}										
				},
				{
					render: function (data, type, full, meta) {
						return full.FirstName+" "+full.LastName+" "+full.ThirdName;
					}
				},//0
				{
					data:"Gender"
				},//0
				{
					data:"MobileNo"
				},//0
				{
					data:"Address"
				},//0
				{
					data:"DateOfBirth"
				},//0
				{
					data:"PassportNo"
				},//0
				{
					data:"VisaNo"
				},//0
				{
					render: function (data, type, full, meta) {
						a=''
						+'	<div class="list-action text-right">'
						+'		<button class="btn btn-success btn-simple list-edit" data-id="'+full.IdUser+'">'
						+'			<i class="material-icons">mode_edit</i>'
						+'			<div class="ripple-container"></div>'
						+'		</button>'
						+'		<button class="btn btn-danger btn-simple list-delete" data-id="'+full.IdUser+'">'
						+'			<i class="material-icons">close</i>'
						+'			<div class="ripple-container"></div>'
						+'		</button>'
						+'	</div>'
						return a
					}					
				},
			],
			createdRow:  function ( row, data, index ) {
				$(row).data("id",data.IdUser);
			},
			columnDefs: [
				{targets: 0, orderable: false },
				{targets: 8, orderable: false }
			],
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Cari data",
                emptyTable:"Tidak ada data",
			    info:"Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
			    infoEmpty:"Menampilkan 0 sampai 0 dari 0 data",
			    lengthMenu:"Menampilkan _MENU_ data",
				paginate: {
					previous: "Sebelum",
					next: "Berikut"
				}
            },

		});
		// $("div.toolbar").html(''
		// 			+'<button class="btn btn-info rpt input-pendamping">'
		// 			+'	<span class="btn-label">'
		// 			+'		<i class="material-icons">add</i>'
		// 			+'	</span>'
		// 			+'	Tambah Pendamping'
		// 			+'</button>'
		// )

		// row click
		// $('#list tbody').on( 'click', 'tr', function () {
		//    	id=$(this).data("id")
		// 	console.log(id)
  //           $('.loading').show()

  //           $('#c-modal').empty().load("{{ route('travel-agen-view') }}", function(){
  //               $('.loading').hide()

  //               $('#modal').modal('show')
  //           })
		// })

		// row edit
		$('#list tbody').on( 'click', '.list-edit', function (e) {
			e.preventDefault()
			$('.loading').show()
			$('#rp').empty().load("{{ route('travel-grup-jamaahinput',[$a[0]->IdPack,'']) }}/"+$(this).data('id'), function(){
				$('.card-content-overflow').perfectScrollbar();
				$('.loading').hide()
				$('#rp').slideReveal("show")
			})
		})

		$('#list tbody').on( 'click', '.list-dtl', function (e) {
			e.preventDefault()
			$('.loading').show()
			{{--$('#rp').empty().load("{{ route('travel-grup-jamaahview',[$a[0]->IdPack,'']) }}/"+$(this).data('id'), function(){--}}
				{{--$('.card-content-overflow').perfectScrollbar();--}}
				{{--$('.loading').hide()--}}
				{{--$('#rp').slideReveal("show")--}}
			{{--})--}}
            $('.loading').show()
            $('#c-modal').empty().load("{{ route('travel-grup-jamaah-view',[$a[0]->IdPack,'']) }}/"+$(this).data('id'), function(){
                $('.loading').hide()
                $('#modaljamaah').modal('show')
            })
		})

		// row delete
		$('#list tbody').on( 'click', '.list-delete', function (e) {
			e.preventDefault()
			id=$(this).data('id')
			swal({
				title: 'Apakah Anda Yakin?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: 'Ya!',
				cancelButtonText: 'Batal'
			}).then(function() {
				swal('Mohon Menunggu')
				swal.showLoading()

				var data={
					a:'deletejamaah',
					id:id,
				};
				$.post("{{route('travel-grup-data') }}", data,function(r) {
					swal(
						'Berhasil',
						'',
						'success'
					).then(function() {
						$('#list').DataTable().ajax.reload(null, false);
					})
				}, 'json').fail(function(response) {
					console.log(response)
					swal(
						'Terjadi Kesalahan!',
						//response.responseText,
						'Mohon Dicek Kembali',
						'error'
					)
				});
			})
		})

	</script>

@endsection