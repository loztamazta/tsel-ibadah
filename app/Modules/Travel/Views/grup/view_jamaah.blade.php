<div class="modal fade" id="modaljamaah" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg margin-top-lg-4">
        <div class="card card-profile">
            <div class="card-avatar">
                <a href="#jamaah">
                    <img class="img" src="
                                        @if(empty($j->Img))
                                            {{asset('mdp/img/faces/marc.jpg')}}
                                        @else
                                        {{ asset('storage/') }}/{{ $j->Img }}
                                        @endif">
                </a>
            </div>
            <div class="card-content  table-responsive">
                <br>
                <br>
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th colspan="4"><h4>{{$j->FirstName }} {{$j->LastName }} {{$j->ThirdName }}</h4></th>
                        </tr>
                        <tr>
                            <th>Tanggal Lahir</th>
                            <td>{{$j->DateOfBirth }}</td>
                            <th>Jenis Kelamin</th>
                            <td>{{ $j->Title == 'LK' ? 'Laki-Laki':'Perempuan' }}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{$j->Email }}</td>
                            <th>Nomor ID</th>
                            <td>{{$j->IdNumber }}</td>
                        </tr>
                        <tr>
                            <th>Nomor HP</th>
                            <td>{{$j->MobileNo }}</td>
                            <th>Nomor Telepon</th>
                            <td>{{$j->PhoneNo }}</td>
                        </tr>
                        <tr>
                            <th>Nomor Passport</th>
                            <td>{{$j->PassportNo }}</td>
                            <th>Nomor Visa</th>
                            <td>{{$j->VisaNo }}</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <a href="#passport">
                                    <img class="imgpassvisa img-rounded" src="
                                    @if(empty($j->ImgPassport))
                                    {{asset('mdp/img/placeholder.jpg')}}
                                    @else
                                    {{ asset('storage/') }}/{{ $j->ImgPassport }}
                                    @endif">
                                </a>
                            </td>
                            <td colspan="2">
                                <a href="#visa">
                                    <img class="imgpassvisa img-rounded" src="
                                    @if(empty($j->ImgVisa))
                                    {{asset('mdp/img/placeholder.jpg')}}
                                    @else
                                    {{ asset('storage/') }}/{{ $j->ImgVisa }}
                                    @endif">
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <div class="stats pull-right">
                    <button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
</div>