@extends('Dts::layouts.dts')

@section('h-title','Grup Keberangkatan')
@section('title','Grup Keberangkatan')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-info rpt input-group-keber">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
                Tambah Rombongan
            </button>
        </div>
        @role('sadmin')
        <div class="col-md-3">
            <div class="input-group">
                <div class="form-group label-floating">
                    <label class="control-label">
                        Agen Perjalanan
                    </label>
                    <select class="selectpicker" data-style="select-with-transition" name="t" id="trv">
                        <option value="0" disabled> Pilih agen perjalanan</option>
                        @foreach($travels as $travel)
                            <option value="{{ $travel->i }}">{{ $travel->n }}</option>
                        @endforeach
                    </select>
                </div>
                <span href="javascript:void(0)" class="input-group-addon">
                    </span>
            </div>
        </div>
        @endrole
        <div class="col-md-3 pull-right">
            <div class="input-group">
                <div class="form-group label-floating">
                    <label class="control-label">
                        Cari Grup Keberangkatan ...
                    </label>
                    <input id="GroupRombongan" name="GroupRombongan" type="text" class="form-control">
                </div>
                <a href="javascript:void(0)" id="SearchGroupRombongan" class="input-group-addon">
                    <i class="material-icons">search</i>
                </a>
            </div>
        </div>
    </div>
    <div class="row" id="ListGroupKeber">

    </div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
    <link href="{{ asset('vendor/datepicker/css/datepicker.css') }}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
    <script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('mdp/js/moment.min.js') }}"></script>
    <script src="{{ asset('mdp/js/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.bootstrap-wizard.js') }}"></script>
    <script src="{{ asset('mdp/js/wizard.js') }}"></script>
    <script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
    <script src="{{ asset('vendor/additional-methods.js') }}"></script>
    <script src="{{ asset('vendor/datepicker/js/bootstrap-datepicker.js') }}"></script>


    <script>
        var kbihid=0
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#rp').slideReveal({
            // trigger: $(".rpt"),
            position: "right",
            push: false,
            overlay: true,
            zIndex:1300,
        });
        $('#rp').delegate('.rp-close','click',function() {
            $('#rp').slideReveal("hide")
        })

        $(function() {
            $('.selectpicker').selectpicker();

            $("#SearchGroupRombongan").on('click', function () {
                var searchid = $("#GroupRombongan").val();
                GetSearchRombonganData(searchid)
                return false;
            });
            $('#GroupRombongan').bind("enterKey",function(e){
                var searchid = $("#GroupRombongan").val();
                GetSearchRombonganData(searchid)

            });
            $('#GroupRombongan').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
                if (!this.value) {
                    GetAllListGroupKeber()
                }
            });
        });

        $('#trv').change(function() {
            GetAllListGroupKeber();
        })

        GetAllListGroupKeber();
        function GetAllListGroupKeber()
        {
            kbihid=$('#trv').val()
            $.ajax({
                type: 'GET',
                url: '{{url('/GetLisKeberangkatan')}}',
                data:{kbihid:kbihid},
                global: false,
                success: function (data) {
                    $('#ListGroupKeber').empty();
                    for (i = 0; i < data.length; i ++ ) {
                        // console.log(data[i].IntervalDate)
                        $('#ListGroupKeber').append('' +
                            '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12"">' +
                            '	<div class="card pointer">' +
                            '		<a href="{{ URL('/travel/grup/detail' )}}/'+data[i].IdPack+'"> ' +
                            '			<div class="card-header card-header-tabs text-center" data-background-color="blue" data-header-animation="true">' +
                            '			<h4 class="card-title">'+data[i].PackName+'</h4>' +
                            '			<span class="">'+moment(data[i].DateofDepa, "YYYY/MM/DD").format('ll')+' - '+moment(moment(data[i].DateofDepa).add(parseInt(data[i].IntervalDate), 'day'), "YYYY/MM/DD").format('ll')+'</span>' +
                            '		</div> </a>' +
                            '		<div class="card-content text-center">' +
                            '			<div class="card-actions">' +
                            '               <a href="{{ URL('/travel/grup/detail' )}}/'+data[i].IdPack+'"> ' +
                            '                  <button type="button" class="btn btn-success btn-simple" rel="tooltip" data-placement="bottom" title="Edit">' +
                            '                      <i class="material-icons">edit_mode</i>' +
                            '                  </button>' +
                            '               </a>' +
                            '				<button type="button" class="btn btn-danger btn-simple" rel="tooltip" data-placement="bottom" title="Delete" data-id="'+data[i].IdPack+'">' +
                            '					<i class="material-icons">close</i>' +
                            '				</button>' +
                            '			</div>' +
                            '			<div class="card-description text-left">' +
                            '				<div class="row">' +
                            '					<div class="col-md-4 pull-right">' +
                            '						<div class="photo photo-round photo-50">' +
                            '                       <img src="'+(data[i].MtwImg? '{{ asset('storage') }}'+'/'+data[i].MtwImg:'{{ asset('images/image_placeholder.jpg') }}')+'" />' +
                            '						</div>' +
                            '					</div>' +
                            '					<div class="col-md-8">' +
                            '						<div class="row">' +
                            '							<div class="col-xs-2">' +
                            '								<span class="material-icons">face</span>' +
                            '							</div>' +
                            '							<div class="col-md-10">' +
                            '								<p> '+data[i].MtwFirstName+'  '+data[i].MtwLastName+' </p>' +
                            '							</div>' +
                            '							<div class="col-xs-2">' +
                            '								<span class="material-icons">group</span>' +
                            '							</div>' +
                            '							<div class="col-xs-10 text-left">' +
                            '								<p>'+data[i].TotalPeople+'</p>' +
                            '							</div>' +
                            '						</div>' +
                            '					</div>' +
                            '				</div>' +
                            '			</div>' +
                            '		</div>' +
                            '	</div>' +
                            '</div>');
                    }
                }
            });
        }

        function GetSearchRombonganData(searchid)
        {

            $.ajax({
                type: 'GET',
                url: '{{url('/GetRobonganSearch')}}',
                data: {TextId:searchid},
                global: false,
                success: function (data) {
                    $('#ListGroupKeber').empty();
                    if(data == ''){
                        $('#ListGroupKeber').append('<div class="col-md-3">' +'<h2>Rombongan tidak ada</h2>'+'</div>');
                    }else{
                        for (i = 0; i < data.length; i ++ ) {
                            // console.log(data[i].IntervalDate)
                            $('#ListGroupKeber').append('' +
                                '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12"">' +
                                '	<div class="card pointer">' +
                                '		<a href="{{ URL('/travel/grup/detail' )}}/'+data[i].IdPack+'"> ' +
                                '			<div class="card-header card-header-tabs text-center" data-background-color="blue" data-header-animation="true">' +
                                '			<h4 class="card-title">'+data[i].PackName+'</h4>' +
                                '			<span class="">'+moment(data[i].DateofDepa, "YYYY/MM/DD").format('ll')+' - '+moment(moment(data[i].DateofDepa).add(parseInt(data[i].IntervalDate), 'day'), "YYYY/MM/DD").format('ll')+'</span>' +
                                '		</div> </a>' +
                                '		<div class="card-content text-center">' +
                                '			<div class="card-actions">' +
                                '               <a href="{{ URL('/travel/grup/detail' )}}/'+data[i].IdPack+'"> ' +
                                '                  <button type="button" class="btn btn-success btn-simple" rel="tooltip" data-placement="bottom" title="Edit">' +
                                '                      <i class="material-icons">edit_mode</i>' +
                                '                  </button>' +
                                '               </a>' +
                                '				<button type="button" class="btn btn-danger btn-simple" rel="tooltip" data-placement="bottom" title="Delete" data-id="'+data[i].IdPack+'">' +
                                '					<i class="material-icons">close</i>' +
                                '				</button>' +
                                '			</div>' +
                                '			<div class="card-description text-left">' +
                                '				<div class="row">' +
                                '					<div class="col-md-4 pull-right">' +
                                '						<div class="photo photo-round photo-50">' +
                                '                       <img src="'+(data[i].MtwImg? '{{ asset('storage') }}'+'/'+data[i].MtwImg:'{{ asset('images/image_placeholder.jpg') }}')+'" />' +
                                '						</div>' +
                                '					</div>' +
                                '					<div class="col-md-8">' +
                                '						<div class="row">' +
                                '							<div class="col-xs-2">' +
                                '								<span class="material-icons">face</span>' +
                                '							</div>' +
                                '							<div class="col-md-10">' +
                                '								<p> '+data[i].MtwFirstName+'  '+data[i].MtwLastName+' </p>' +
                                '							</div>' +
                                '							<div class="col-xs-2">' +
                                '								<span class="material-icons">group</span>' +
                                '							</div>' +
                                '							<div class="col-xs-10 text-left">' +
                                '								<p>'+data[i].TotalPeople+'</p>' +
                                '							</div>' +
                                '						</div>' +
                                '					</div>' +
                                '				</div>' +
                                '			</div>' +
                                '		</div>' +
                                '	</div>' +
                                '</div>');
                        }
                    }
                }
            });
        }

        $(".input-group-keber").click(function(){
            $('.loading').show()
            $('#rp').empty().load("{{ route('travel-grup-input') }}", function(){
                $('.card-content-overflow').perfectScrollbar();
                $('.loading').hide()
                demo.initMaterialWizard();
                LoadDataPHS();
                $(document).delegate(".nextRombongan","click", function (e) {

                    Date.prototype.addDays = function(days) {
                        var dat = new Date(this.valueOf())
                        dat.setDate(dat.getDate() + days);
                        return dat;
                    }

                    function getDates(startDate, stopDate) {
                        var dateArray = new Array();
                        var currentDate = startDate;
                        while (currentDate <= stopDate) {
                            dateArray.push(currentDate)
                            currentDate = currentDate.addDays(1);
                        }
                        return dateArray;
                    }

                    var dateArray = getDates(new Date($('#DateofDepa').val()), (new Date($('#DateofDepa').val())).addDays($('#PilihPaket').find(":selected").data('val')));
                    $.ajax({
                        type: 'GET',
                        url: '{{url('/GetKegiatanTime')}}',
                        data:{PaketId:$('#PilihPaket').find(":selected").val()},
                        global: false,
                        success: function (data) {
                            $("#accordionItener").empty();
                            // console.log(data)
                            for (i = 1; i < dateArray.length; i ++ ) {
                                // console.log(getFormattedDate(dateArray[i]));
                                $("#accordionItener").append('' +
                                    '<div class="panel panel-default">'+
                                    '	<div class="panel-heading" role="tab" id="kegiatan'+i+'">'+
                                    '		<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+i+'" aria-expanded="true" aria-controls="collapse'+i+'">'+
                                    '			<h4 class="panel-title">'+
                                    getFormattedDate(dateArray[i])+
                                    '				<i class="material-icons">keyboard_arrow_down</i>'+
                                    '			</h4>'+
                                    '		</a>'+
                                    '	</div>'+
                                    '	<div id="collapse'+i+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'+i+'">'+
                                    '		<div class="panel-body BersihLah" id="DayItenerTime'+i+'">'+


                                    '		</div>'+
                                    '	</div>'+
                                    '</div>');
                            }
                            $('.BersihLah').empty();
                            for (var i = 0; i < data.TimeKegiatan.length; i++) {

                                var b = $('#DayItenerTime'+data.TimeKegiatan[i].dayinter).append(
                                    '			<div class="row ListKegiatan'+i+'">'+
                                    '				<div class="col-xs-4">'+
                                    '					<div class="form-group label-floating">'+
                                    '						<label class="control-label">Mulai</label>'+
                                    '						<input type="text" class="form-control timepicker" value="'+data.TimeKegiatan[i].itinerary_time+'" readonly/>'+
                                    '					</div>'+
                                    '				</div>'+
                                    '				<div class="col-xs-8">'+
                                    '					<div class="form-group label-floating">'+
                                    '						<label class="control-label">Kegiatan</label>'+
                                    '						<input type="text" class="form-control KegName" value="'+data.TimeKegiatan[i].activity_name+'" readonly/>'+
                                    '					</div>'+
                                    '				</div>'+
                                    '			</div>'
                                )
                                // console.log(b)
                            }
                        }
                    });


                });
                var date = new Date();
                var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
                var end = new Date(date.getFullYear(), date.getMonth(), date.getDate());

                $('.datetimepicker').datepicker({
                    format: 'mm/dd/yyyy',
                    startDate: '+0d',
                    autoclose: 'true',
                    todayHighlight: true
                });

                $('.datetimepicker').datepicker('setDate', today);

                loads()

                $('#rp').slideReveal("show")
            })
        })

        function LoadDataPHS(){
            $.ajax({
                type: 'GET',
                url: '{{url('/GetDataPHS')}}',
                data:{kbihid:kbihid},
                global:false,
                success: function (data) {
                    var $ScanPaket = $('#PilihPaket');
                    $ScanPaket.empty();
                    for (var i = 0; i < data.Paket.length; i++) {
                        $ScanPaket.append('<option id="' + data.Paket[i].id + '" value="' + data.Paket[i].id + '" data-val="' + data.Paket[i].IntervalDate + '">' + data.Paket[i].name + '</option>');
                    }
                    $('#PilihPaket option:first-child').attr("selected", "selected");

                    var $ScanMk = $('#PilihHMekkah');
                    $ScanMk.empty();
                    for (var i = 0; i < data.HotelMe.length; i++) {
                        $ScanMk.append('<option id="' + data.HotelMe[i].id + '" value="' + data.HotelMe[i].id + '">' + data.HotelMe[i].name + '</option>');
                    }
                    $('#PilihHMekkah option:first-child').attr("selected", "selected");

                    var $ScanMd = $('#PilihHMadinah');
                    $ScanMd.empty();
                    for (var i = 0; i < data.HotelMa.length; i++) {
                        $ScanMd.append('<option id="' + data.HotelMa[i].id + '" value="' + data.HotelMa[i].id + '">' + data.HotelMa[i].name + '</option>');
                    }

                    var $ScanJd = $('#PilihHJeddah');
                    // $ScanJd.empty();
                    for (var i = 0; i < data.HotelJd.length; i++) {
                        $ScanJd.append('<option id="' + data.HotelJd[i].id + '" value="' + data.HotelJd[i].id + '">' + data.HotelJd[i].name + '</option>');
                    }

//                     var $ScanStaff = $('#PilihStaff');
//                     $ScanStaff.empty();
//                     for (var i = 0; i < data.Staff.length; i++) {
//                         $ScanStaff.append('<option id="' + data.Staff[i].Idmtw + '" value="' + data.Staff[i].Idmtw + '">' + data.Staff[i].MtwFirstName+' '+data.Staff[i].MtwLastName+ '</option>');
//                     }
//                     $('#PilihStaff option:first-child').attr("selected", "selected");
                }
            });
        }

        $('#rp').delegate('#PilihPaket','change',function() {
            loads()
        })
        $('#rp').delegate('#DateofDepa','dp.change',function() {
            loads()
        })

        // delete
        $('#ListGroupKeber').delegate('.btn.btn-danger.btn-simple','click',function() {
            data={
                a:'delGrup',
                id:$(this).data('id'),
            }

            swal({
                title: 'Apakah Anda Yakin?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                $.post("{{ route('travel-grup-data') }}", data, function(R) {
                    swal(
                        'Berhasil!',
                        //'Data grup keberangkatan sudah terhapus.',
                        '',
                        'success'
                    ).then(function() {
                        GetAllListGroupKeber()
                    })
                }, 'json');
            })
        })

        function loads(){
//             console.log(moment($('#DateofDepa').val(),'MM/DD/YYYY').format('YYYY-MM-DD')!='Invalid date'?moment($('#DateofDepa').val(),'MM/DD/YYYY').format('YYYY-MM-DD'):moment().format('YYYY-MM-DD'))
            console.log('INDEHOY');
            data={
                a:'loadS',
                kbihid:kbihid,
                dt:moment($('#DateofDepa').val(),'MM/DD/YYYY').format('YYYY-MM-DD')!='Invalid date'?moment($('#DateofDepa').val(),'MM/DD/YYYY').format('YYYY-MM-DD'):moment().format('YYYY-MM-DD'),
                pkt:$('#PilihPaket').val()
            }
            $.post("{{ route('travel-grup-data') }}", data, function(R) {
                var $ScanStaff = $('#PilihStaff');
                $ScanStaff.empty();
                for (var i = 0; i < R.length; i++) {
                    $ScanStaff.append('<option id="' + R[i].Idmtw + '" value="' + R[i].Idmtw + '">' + R[i].MtwFirstName+' '+R[i].MtwLastName+ '</option>');
                }
                $('#PilihStaff option:first-child').attr("selected", "selected");
            }, 'json')
        }
        function getFormattedDate(today)
        {
            var week = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            var months = [ "January", "February", "March", "April", "May", "June",
                "July", "August", "September", "October", "November", "December" ];
            var day  = week[today.getDay()];
            var dd   = today.getDate();
            var mm   = months[today.getMonth()]; //January is 0!
            var yyyy = today.getFullYear();
            var hour = today.getHours();
            var minu = today.getMinutes();

            if(dd<10)  { dd='0'+dd }
            if(mm<10)  { mm='0'+mm }
            if(minu<10){ minu='0'+minu }

            return day+' - '+dd+' '+mm+' '+yyyy;
        }


        //		$(".card-travel").click(function(){
        //			console.log('klik')
        //		}).find(".card-actions").click(function(e) {
        //			return false;
        //		});
    </script>

    {{-- datatable --}}
    <script src="{{ asset('mdp/js/jquery.datatables.js') }}"></script>

@endsection