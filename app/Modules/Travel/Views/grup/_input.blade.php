<div class="card">
	<div class="card-header card-header-icon" data-background-color="blue">
		<i class="material-icons">mode_edit</i>
	</div>
	<span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
		<i class="material-icons">close</i>
	</span>
	<div class="card-content">
		<h4 class="card-title">Rombongan Keberangkatan</h4>
		<div class="wizard-container  card-content-overflow">
			<div class="wizard-card" data-color="rose" id="wizardProfile">
				<form action="#" method="">
					<div class="wizard-navigation">
						<ul>
							<li>
								<a href="#DetailRombongan" data-toggle="tab">Detil Rombongan</a>
							</li>
							<li>
								<a href="#Itenerary" id="IteneraryTab" class="nextRombongan" data-toggle="tab">Rencana Perjalanan</a>
							</li>
							<li>
								<a href="#Jammah" id="JammahTab" class="nextRombongan" data-toggle="tab">Jamaah</a>
							</li>
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane" id="DetailRombongan">
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">class</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Nama Rombongan
												<small>(wajib diisi)</small>
											</label>
											<input name="PackName" type="text" class="form-control">
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">date_range</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Tanggal Keberangkatan
												<small>(wajib diisi)</small>
											</label>
											<input name="DateofDepa" id="DateofDepa" type="text" class="form-control datetimepicker">
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">assignment</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Pilih Paket
												<small>(wajib diisi)</small>
											</label>
											<select class="form-control" id="PilihPaket" name="PilihPaket" data-style="select-with-transition" title="Single Select" data-size="7" readonly>
												<option disabled selected>Pilih Paket</option>
											</select>
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">local_hotel</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Pilih Hotel Mekkah
												<small>(wajib diisi)</small>
											</label>
											<select class="form-control" id="PilihHMekkah" name="PilihHMekkah" data-style="select-with-transition" title="Single Select" data-size="7">
												<option disabled selected>Pilih Hotel Mekkah</option>
											</select>
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">local_hotel</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Pilih Hotel Madinah
												<small>(wajib diisi)</small>
											</label>
											<select class="form-control" id="PilihHMadinah" name="PilihHMadinah" data-style="select-with-transition" title="Single Select" data-size="7">
												<option disabled selected>Pilih Hotel Madinah</option>
											</select>
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">local_hotel</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Pilih Hotel Jeddah
											</label>
											<select class="form-control" id="PilihHJeddah" name="PilihHJeddah" data-style="select-with-transition" title="Single Select" data-size="7">
												<option disabled selected>Pilih Hotel Jeddah</option>
												<option value="0">None</option>
											</select>
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">flight</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Maskapai Keberangkatan
												<small>(wajib diisi)</small>
											</label>
											<input name="MaskapaiB" type="text" class="form-control">
										</div>
									</div>
									{{--<div class="input-group">--}}
										{{--<span class="input-group-addon">--}}
											{{--<i class="material-icons">flight</i>--}}
										{{--</span>--}}
										{{--<div class="form-group label-floating">--}}
											{{--<label class="control-label">Maskapai Transit</label>--}}
											{{--<input name="MaskapaiT" type="text" class="form-control">--}}
										{{--</div>--}}
									{{--</div>--}}
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">flight</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Maskapai Kepulangan
												<small>(wajib diisi)</small>
											</label>
											<input name="MaskapaiP" type="text" class="form-control">
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">account_circle</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Pilih Staff
												<small>(wajib diisi)</small>
											</label>
											<select class="form-control" id="PilihStaff" name="PilihStaff" data-style="select-with-transition" title="Single Select" data-size="7">
												<option disabled selected>Pilih Staff</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="Itenerary">
							<div class="panel-group" id="accordionItener" role="tablist" aria-multiselectable="true">

							</div>
						</div>
						<div class="tab-pane" id="Jammah">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
								<a href="{{ asset('Template Excel jamaah.xlsx') }}" class="btn btn-info">
									<span class="btn-label">
										<i class="material-icons">file_download</i>
									</span> Unduh contoh excel
									<div class="ripple-container"></div>
								</a>

								<input type="file" class="form-control" name="import_file" id="excel" />
							</div>
						</div>
					</div>
				</form>
				<input type='button' class='btn btn-next nextRombongan' id="np" style="display: none;" />
				<input type='button' class='btn btn-finish' id="FinishRombongan" style="display: none;" />
				<input type='button' class='btn btn-previous' id="pp" style="display: none;" />
			</div>
		</div>
	</div>
	<div class="card-footer">
		<div class="pull-right">
			<input type='button' class='btn btn-next btn-fill btn-rose btn-wd nextPaket' name='next' id="nextPaket" value='Lanjut' />
			<input type='button' class='btn btn-finish btn-fill btn-rose btn-wd' name='finish' id="FinishPaket" value='Simpan' />
		</div>
		<div class="pull-left">
			<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' id="prevPaket" value='Kembali' />
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<script>
	$('#nextPaket').click(function(){
		$('.nextRombongan').click()
	})
	$('#FinishPaket').click(function(){
		$('#FinishRombongan').click()
	})
	$('#prevPaket').click(function(){
		$('#pp').click()
	})

	$('#FinishRombongan').click(function (e) {
		e.preventDefault()

        swal('Mohon Menunggu')
        swal.showLoading()

		p='a=save&kbihid='+(kbihid?kbihid:0)+'&'
		p+=$("form").serialize();
		$.post("{{ route('travel-grup-data') }}", p,function(r) {
			console.log(r)
			if(r){
                swal('Mengimpor dokumen...')
                swal.showLoading()

				var form_data = new FormData();
				form_data.append("file", $("#excel").prop("files")[0])
				form_data.append("id", r.idpack)

				$.ajax({
					url: "{{route('travel-grup-excel') }}",
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'post',
					success: function(p){
                        swal(
                            'Berhasil',
                            '',
                            'success'
                        ).then(function() {
                            $('#rp').empty().slideReveal("hide")
                            GetAllListGroupKeber()
                        })
					}
				})
			}
		}, 'json').fail(function(response) {
			swal(
				'Terjadi kesalahan!',
				//response.responseText,
				'Mohon Dicek Kembali',
				'error'
			)
		}); 
		// $('#rp').slideReveal("hide")
	})
</script>