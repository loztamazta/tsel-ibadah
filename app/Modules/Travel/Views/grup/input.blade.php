@extends('Dts::layouts.dts')


@section('title','Rombongan Baru')

@section('content')
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-pills nav-pills-info">
				<li class="active">
					<a href="#tab1" data-toggle="tab">Rombongan</a>
				</li>
				<li>
					<a href="#tab2" data-toggle="tab">Kegiatan</a>
				</li>
				<li>
					<a href="#tab3" data-toggle="tab">Jamaah</a>
				</li>
			</ul>		
		</div>
	</div>

	<div class="card-content-overflow tab-content">
		{{-- 1 --}}
		<div class="tab-pane active form-horizontal" id="tab1">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-content">
							<form class="form-horizontal">
								<div class="form-group label-floating">
									<label class="control-label">Nama Rombongan</label>
									<input type="text" class="form-control">
								</div>
									<div class="form-group label-floating">
										<label class="control-label">Tanggal Keberangkatan</label>
										<input type="text" class="form-control datepicker"/>
									</div>
								{{--<div class="form-group label-floating">--}}
									{{--<label class="control-label">Paket Perjalanan</label>--}}
									{{--<input type="text" class="form-control">--}}
								{{--</div>--}}
                                <div class="form-group label-floating">
                                    <select class="selectpicker" data-style="select-with-transition" title="Single Select" data-size="7">
                                        <option disabled selected>Choose city</option>
                                        <option value="2">Foobar</option>
                                        <option value="3">Is great</option>
                                    </select>
                                </div>
								<div class="row">
									<div class="form-group label-floating col-md-6">
                                        <select class="selectpicker" data-style="select-with-transition" title="Single Select" data-size="7">
                                            <option disabled selected>Hotel Mekkah</option>
                                            <option value="2">Foobar</option>
                                            <option value="3">Is great</option>
                                        </select>
									</div>
									<div class="form-group label-floating col-md-6">
                                        <select class="selectpicker" data-style="select-with-transition" title="Single Select" data-size="7">
                                            <option disabled selected>Hotel Madinah</option>
                                            <option value="2">Foobar</option>
                                            <option value="3">Is great</option>
                                        </select>
									</div>
								</div>
								<div class="form-group label-floating">
                                    <select class="selectpicker" data-style="select-with-transition" title="Single Select" data-size="7">
                                        <option disabled selected>Pendamping Grup</option>
                                        <option value="2">Foobar</option>
                                        <option value="3">Is great</option>
                                    </select>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Maskapai Pemberangkatan</label>
									<input type="text" class="form-control">
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Maskapai Kepulangan</label>
									<input type="text" class="form-control">
								</div>
								<div class="form-group label-floating">
                                    <select class="selectpicker" data-style="select-with-transition" title="Single Select" data-size="7">
                                        <option disabled selected>Paket Roaming</option>
                                        <option value="2">Foobar</option>
                                        <option value="3">Is great</option>
                                    </select>
								</div>
							</form>
						</div>
					</div>

				</div>
			</div>

		</div>
		<div class="tab-pane" id="tab2">

		</div>
		<div class="tab-pane" id="tab3">
		    <div class="row">
		        <div class="col-md-3">
                    <div class="card card-product card-travel pointer">
                        <div class="card-image" data-header-animation="true">
                            <img class="img" src="{{ asset('mdp/img/card-1.jpg') }}">
                        </div>
                        <div class="card-content">
                            <div class="card-actions">
                                <button type="button" class="btn btn-danger btn-simple fix-broken-card">
                                    <i class="material-icons">build</i> Fix Header!
                                </button>
                                <button type="button" class="btn btn-default btn-simple view-travel" rel="tooltip" data-placement="bottom" title="Quick view">
                                    <i class="material-icons">art_track</i>
                                </button>
                                <button type="button" class="btn btn-success btn-simple input-travel" rel="tooltip" data-placement="bottom" title="Edit">
                                    <i class="material-icons">edit</i>
                                </button>
                                <button type="button" class="btn btn-danger btn-simple" rel="tooltip" data-placement="bottom" title="Remove">
                                    <i class="material-icons">close</i>
                                </button>
                            </div>
                            <h4 class="card-title text-left">
                                <a href="#pablo">Paspor</a>
                                <small><br>Khoirul Umam</small>
                            </h4>
                            <div class="card-description">
                                <div class="row">
                                    <div class="col-xs-4  text-right">
                                        #ID
                                    </div>
                                    <div class="col-xs-8">
                                        123456789
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4  text-right">
                                        Paspor
                                    </div>
                                    <div class="col-xs-8">
                                        103254598
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4  text-right">
                                        Visa
                                    </div>
                                    <div class="col-xs-8">
                                        125152
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="col-md-12 btn btn-info rpt">
                        <span class="btn-label">
                        <i class="material-icons">add</i>
                        </span>
                        Download Format Formulir Jamaah
                    </button>
                    <button class="col-md-12 btn btn-info rpt">
                        <span class="btn-label">
                        <i class="material-icons">add</i>
                        </span>
                        Upload Formulir Penambahan Jamaah
                    </button>
		        </div>
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							{{--<button class="btn btn-info rpt input-pendamping">--}}
								{{--<span class="btn-label">--}}
									{{--<i class="material-icons">add</i>--}}
								{{--</span>--}}
								{{--Tambah Jamaah--}}
							{{--</button>--}}
							<i class="material-icons">mode_edit</i>
							<h4 class="title"> Informasi Jamaah </h4>
						</div>
						<div class="card-content">

							<table id="list" class="table table-striped table-no-bordered table-hover compact">
								<thead>
								<tr class="">
									<th class="col-md-11">Nama Depan : </th>
								</tr>
								<tr class="">
									<th class="col-md-11">Nama Tengah : </th>
								</tr>
								<tr class="">
									<th class="col-md-11">Nama Akhir : </th>
								</tr>
								<tr class="">
									<th class="col-md-11">Jenis Kelamin : </th>
								</tr>
								<tr class="">
									<th class="col-md-11">Tempat Tanggal Lahir : </th>
								</tr>
								<tr class="">
									<th class="col-md-11">Address Lengkap : </th>
								</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
                <div class="col-md-3">
                    <div class="card card-product card-travel pointer">
                        <div class="card-image" data-header-animation="true">
                            <img class="img" src="{{ asset('mdp/img/card-1.jpg') }}">
                        </div>
                        <div class="card-content">
                            <div class="card-actions">
                                <button type="button" class="btn btn-danger btn-simple fix-broken-card">
                                    <i class="material-icons">build</i> Fix Header!
                                </button>
                                <button type="button" class="btn btn-default btn-simple view-travel" rel="tooltip" data-placement="bottom" title="Quick view">
                                    <i class="material-icons">art_track</i>
                                </button>
                                <button type="button" class="btn btn-success btn-simple input-travel" rel="tooltip" data-placement="bottom" title="Edit">
                                    <i class="material-icons">edit</i>
                                </button>
                                <button type="button" class="btn btn-danger btn-simple" rel="tooltip" data-placement="bottom" title="Remove">
                                    <i class="material-icons">close</i>
                                </button>
                            </div>
                            <h4 class="card-title text-left">
                                <a href="#pablo">Travel DTS</a>
                                <small><br>Khoirul Umam</small>
                            </h4>
                            <div class="card-description">
                                <div class="row">
                                    <div class="col-xs-10  text-right">
                                        81296697853
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="material-icons">phone</i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10  text-right">
                                        tdts@mail.com
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="material-icons">mail</i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10  text-right">
                                        Tebet Jakarta Selatan
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="material-icons">place</i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10  text-right">
                                        2017-04-12
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="material-icons">date_range</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-product card-travel pointer">
                        <div class="card-image" data-header-animation="true">
                            <img class="img" src="{{ asset('mdp/img/card-1.jpg') }}">
                        </div>
                        <div class="card-content">
                            <div class="card-actions">
                                <button type="button" class="btn btn-danger btn-simple fix-broken-card">
                                    <i class="material-icons">build</i> Fix Header!
                                </button>
                                <button type="button" class="btn btn-default btn-simple view-travel" rel="tooltip" data-placement="bottom" title="Quick view">
                                    <i class="material-icons">art_track</i>
                                </button>
                                <button type="button" class="btn btn-success btn-simple input-travel" rel="tooltip" data-placement="bottom" title="Edit">
                                    <i class="material-icons">edit</i>
                                </button>
                                <button type="button" class="btn btn-danger btn-simple" rel="tooltip" data-placement="bottom" title="Remove">
                                    <i class="material-icons">close</i>
                                </button>
                            </div>
                            <h4 class="card-title text-left">
                                <a href="#pablo">Travel DTS</a>
                                <small><br>Khoirul Umam</small>
                            </h4>
                            <div class="card-description">
                                <div class="row">
                                    <div class="col-xs-10  text-right">
                                        81296697853
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="material-icons">phone</i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10  text-right">
                                        tdts@mail.com
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="material-icons">mail</i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10  text-right">
                                        Tebet Jakarta Selatan
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="material-icons">place</i>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-10  text-right">
                                        2017-04-12
                                    </div>
                                    <div class="col-xs-2">
                                        <i class="material-icons">date_range</i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
		    </div>
		</div>
	</div>






@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
@endsection

@section('js')

	<script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
	<script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
	<!-- DateTimePicker Plugin -->
	<script src="{{asset('mdp/js/moment.min.js')}}"></script>
	<script src="{{asset('mdp/js/bootstrap-datetimepicker.js') }}"></script>
	<script>
		$('.datepicker').datetimepicker();
	</script>
    <!-- Select Plugin -->
    <script src="{{asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
	{{-- right panel --}}
	<script>
		$('#rp').slideReveal({
			// trigger: $(".rpt"),
			position: "right",
			push: false,
			overlay: true,
			zIndex:1300,
		});
		$('#rp').delegate('.rp-close','click',function() {
			$('#rp').slideReveal("hide")
		})

		$(".input-pendamping").click(function(){
			$('.loading').show()			
			$('#rp').empty().load("{{ route('travel-pendamping-input') }}", function(){
				$('.card-content-overflow').perfectScrollbar();
				$('.loading').hide()
				$('#rp').slideReveal("show")
			})
		})

		$(".card-travel").click(function(){
			console.log('klik')
		}).find(".card-actions").click(function(e) {
			return false;
		});
	</script>

	{{-- datatable --}}
	<script src="{{ asset('mdp/js/jquery.datatables.js') }}"></script>
	<script>
		// $("#list").DataTable({
		// 	ajax: {
		// 		url: "{{ route('travel-pendamping-data') }}",
		// 		dataSrc: '',
		// 		// type: "POST",
		// 		data:{
		// 			a:"getall"
		// 		}
		// 	},
		// 	columns: [
		// 		{
		// 			class : "text-center",
		// 			render: function (data, type, full, meta) {
		// 				a=''
		// 					+'<a href="#profile" class="btn btn-simple btn-info btn-icon like"><i class="material-icons">zoom_in</i><div class="ripple-container"></div></a>'
		// 				return a;
		// 			}
		// 		},//0
		// 		{
		// 			render: function (data, type, full, meta) {
		// 				return full.MtwFirstName+" "+full.MtwLastName;
		// 			}
		// 		},//1
		// 	],
		// 	columnDefs: [
		// 		// {targets: '_all', orderable: false }
		// 	],
  //           responsive: true,
  //           language: {
  //               search: "_INPUT_",
  //               searchPlaceholder: "Search records",
  //           },
  //           dom: "<'row'<'col-sm-12'<'pull-left''toolbar'><'pull-right'f><'clearfix'>>>tr<'row'<'col-sm-12'<'pull-left'i><'pull-right'p><'clearfix'>>>"
		// });
		// $("div.toolbar").html(''
		// 			+'<button class="btn btn-info rpt input-pendamping">'
		// 			+'	<span class="btn-label">'
		// 			+'		<i class="material-icons">add</i>'
		// 			+'	</span>'
		// 			+'	Tambah Pendamping'
		// 			+'</button>'
		// )

		// // row click
	 //    $('#list tbody').on( 'click', 'tr', function () {
	 //    	idmtw=$(this).data("idmtw")
		// 	console.log(idmtw)

		// 	$('#profile').fadeOut(100, function() {
		// 		$('#profile').load("{{ route('travel-pendamping-profile') }}", function(){
		// 			$('#profile').fadeIn(300)
		// 		})
		// 	})


	 //    })
		
	</script>

@endsection