<div class="card">
	<div class="card-header card-header-icon" data-background-color="blue">
		<i class="material-icons">mode_edit</i>
	</div>
	<span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
		<i class="material-icons">close</i>
	</span>
	<div class="card-content">
		<h4 class="card-title">Edit Rombongan Keberangkatan</h4>
		<div class="wizard-container  card-content-overflow">
			<div class="wizard-card" data-color="rose" id="wizardProfile">
				<form action="#" method="">
					<div class="wizard-navigation">
						<ul>
							<li>
								<a href="#DetailRombongan" data-toggle="tab">Detil Rombongan</a>
							</li>
							<li>
								<a href="#IteneraryEdit" id="IteneraryTab" class="nextRombongan" data-toggle="tab">Rencana Perjalanan</a>
							</li>
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane" id="DetailRombongan">
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">class</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Nama Rombongan
												<small>(wajib diisi)</small>
											</label>
											<input name="PackId" id="PackId" type="hidden" class="form-control">
											<input name="PackNames" type="text" class="form-control">
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">date_range</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Tanggal Keberangkatan
												<small>(wajib diisi)</small>
											</label>
											<input name="DateofDepas" id="DateofDepas" type="text" class="form-control datetimepicker" value="08/10/2017">
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">assignment</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Pilih Paket
												<small>(wajib diisi)</small>
											</label>
											<select class="form-control" id="PilihPakets" name="PilihPakets" data-style="select-with-transition" title="Single Select" data-size="7">
												<option disabled selected>Pilih Paket</option>
											</select>
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">local_hotel</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Pilih Hotel Mekkah
												<small>(wajib diisi)</small>
											</label>
											<select class="form-control" id="PilihHMekkahs" name="PilihHMekkahs" data-style="select-with-transition" title="Single Select" data-size="7">
												<option disabled selected>Pilih Hotel Mekkah</option>
											</select>
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">local_hotel</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Pilih Hotel Madinah
												<small>(wajib diisi)</small>
											</label>
											<select class="form-control" id="PilihHMadinahs" name="PilihHMadinahs" data-style="select-with-transition" title="Single Select" data-size="7">
												<option disabled selected>Pilih Hotel Madinah</option>
											</select>
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">local_hotel</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Pilih Hotel Jeddah
											</label>
											<select class="form-control" id="PilihHJeddahs" name="PilihHJeddahs" data-style="select-with-transition" title="Single Select" data-size="7">
												<option disabled selected>Pilih Hotel Jeddah</option>
												<option value="0">None</option>
											</select>
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">flight</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Maskapai Keberangkatan
												<small>(wajib diisi)</small>
											</label>
											<input name="MaskapaiBs" type="text" class="form-control">
										</div>
									</div>
									{{--<div class="input-group">--}}
										{{--<span class="input-group-addon">--}}
											{{--<i class="material-icons">flight</i>--}}
										{{--</span>--}}
										{{--<div class="form-group label-floating">--}}
											{{--<label class="control-label">Maskapai Transit</label>--}}
											{{--<input name="MaskapaiT" type="text" class="form-control">--}}
										{{--</div>--}}
									{{--</div>--}}
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">flight</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Maskapai Kepulangan
												<small>(wajib diisi)</small>
											</label>
											<input name="MaskapaiPs" type="text" class="form-control">
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">account_circle</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Pilih Staff
												<small>(wajib diisi)</small>
											</label>
											<select class="form-control" id="PilihStaffs" name="PilihStaffs" data-style="select-with-transition" title="Single Select" data-size="7">
												<option disabled selected>Pilih Staff</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="IteneraryEdit">
							<div class="panel-group" id="accordionItenerEdit" role="tablist" aria-multiselectable="true">

							</div>
						</div>
					</div>
				</form>
				<input type='button' class='btn btn-next nextRombongan' id="np" style="display: none;" />
				<input type='button' class='btn btn-finish' id="FinishRombongan" style="display: none;" />
				<input type='button' class='btn btn-previous' id="pp" style="display: none;" />
			</div>
		</div>
	</div>
	<div class="card-footer">
		<div class="pull-right">
			<input type='button' class='btn btn-next btn-fill btn-rose btn-wd nextPaket' name='next' id="nextPaket" value='Lanjut' />
			<input type='button' class='btn btn-finish btn-fill btn-rose btn-wd' name='finish' id="FinishPaket" value='Simpan' />
		</div>
		<div class="pull-left">
			<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' id="prevPaket" value='Kembali' />
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<script>
	$('#nextPaket').click(function(){
		$('.nextRombongan').click()
	})
	$('#FinishPaket').click(function(){
		$('#FinishRombongan').click()
	})
	$('#prevPaket').click(function(){
		$('#pp').click()
	})

	$('#FinishRombongan').click(function (e) {
		e.preventDefault()
        swal({
            title: 'Apakah Anda Yakin?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Ya!',
            cancelButtonText: 'Batal',
        }).then(function() {
            swal('Mohon menunggu')
            swal.showLoading()

            p='a=edit&idpack='+$('#PackId').val()+'&'
            p+=$("form").serialize();
            $.post("{{ route('travel-grup-data') }}", p,function(r) {
                console.log(r)
                swal(
                    'Berhasil',
                    '',
                    'success'
                ).then(function() {
                    $('#rp').slideReveal("hide")
                    location.reload();
                })
            }, 'json').fail(function(response) {
                swal(
                    'Terjadi Kesalahan!',
                    //response.responseText,
					'Mohon Dicek Kembali',
                    'error'
                )
            })
        })
//		 $('#rp').slideReveal("hide")
	})
</script>