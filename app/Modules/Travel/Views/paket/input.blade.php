<div class="card">
	<div class="card-header card-header-icon" data-background-color="blue">
		<i class="material-icons">mode_edit</i>
	</div>
	<span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
		<i class="material-icons">close</i>
	</span>
	<div class="card-content">
		<h4 class="card-title titleForm">Paket Baru</h4>
		<div class="wizard-container card-content-overflow">
			<div class="wizard-card" data-color="rose" id="wizardProfile">
				<form action="#" method="" id="meetingForm" class="taskForm">
					<div class="wizard-navigation">
						<ul>
							<li>
								<a href="#DetailPaket" data-toggle="tab">Detail Paket</a>
							</li>
							<li>
								<a href="#Itenerary" id="IteneraryTab" class="nextPaket" data-toggle="tab">Rencana Perjalanan</a>
							</li>
						</ul>
					</div>
					<div class="tab-content">
						<div class="tab-pane" id="DetailPaket">
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">class</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Nama Paket
												<small>(wajib diisi)</small>
											</label>
											<input type="hidden" name="IdPaket">
											<input type="hidden" name="TypeSave" value="SaveTravel">
											<input name="PaketName" type="text" class="form-control">
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">date_range</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Lama Hari
												<small>(wajib diisi)</small>
											</label>
											<input name="Duration" type="text"  min="3"  class="form-control">
										</div>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons">description</i>
										</span>
										<div class="form-group label-floating">
											<label class="control-label">Deskripsi
												<small>(wajib diisi)</small>
											</label>
											<input name="Desk" type="text" class="form-control">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="Itenerary">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

							</div>
						</div>
					</div>
				</form>
				<input type='button' class='btn btn-next' id="np" style="display: none" />
				<input type='button' class='btn btn-finish' id="fp" style="display: none" />
				<input type='button' class='btn btn-previous' id="pp" style="display: none" />

			</div>
		</div>
	</div>
	<div class="card-footer">
		<div class="pull-right">
			<input type='button' class='btn btn-next btn-fill btn-rose btn-wd nextPaket' name='next' id="nextPaket" value='Lanjut' />
			<input type='button' class='btn btn-finish btn-fill btn-rose btn-wd' name='finish' id="FinishPaket" value='Simpan' />
		</div>
		<div class="pull-left">
			<input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous' id="prevPaket" value='Kembali' />
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<script>
	$('#nextPaket').click(function(){
		$('#np').click()
	})
	$('#FinishPaket').click(function(){
		$('#fp').click()
	})
	$('#prevPaket').click(function(){
		$('#pp').click()
	})
</script>