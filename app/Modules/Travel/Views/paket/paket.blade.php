@extends('Dts::layouts.dts')

@section('h-title','Paket Perjalanan')

@section('title','Paket Perjalanan')

@section('content')
	<div class="row">
		<div class="col-md-6">

			<button class="btn btn-info rpt input-paket">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
				Tambah Paket
			</button>
		</div>
        @role('sadmin')
            <div class="col-md-3">
                <div class="input-group">
                    <div class="form-group label-floating">
                        <label class="control-label">
                            Agen Perjalanan
                        </label>
                        <select class="selectpicker" data-style="select-with-transition" name="t" id="trv">
                            <option value="0" disabled> Pilih agen perjalanan</option>
                            @foreach($travels as $travel)
                                <option value="{{ $travel->i }}">{{ $travel->n }}</option>
                            @endforeach
                        </select>
                    </div>
                    <span href="javascript:void(0)" class="input-group-addon">
                    </span>
                </div>
            </div>
        @endrole
		<div class="col-md-3 pull-right">
			<div class="input-group">
				<div class="form-group label-floating">
					<label class="control-label">
						Cari Paket Perjalanan ...
					</label>
					<input id="PaketPerjalanan" name="PaketPerjalanan" type="text" class="form-control">
				</div>
				<a href="javascript:void(0)" id="SearchPaket" class="input-group-addon">
					<i class="material-icons">search</i>
				</a>
			</div>
		</div>
	</div>

	<div class="row" id="ListAllPaketPerjalanan">

	</div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<style>
		.connectedSortable {
			border: 1px solid #eee;
			/*width: 142px;*/
			min-height: 20px;
			list-style-type: none;
			margin: 0;
			/*padding: 5px 0 0 0;*/
			/*float: left;*/
			margin-right: 10px;
		}
		.connectedSortable li  {
			margin: 0 5px 5px 5px;
			padding: 5px;
			font-size: 1.2em;
			width: 120px;
		}
	</style>
@endsection

@section('js')
	<script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
	<script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
	<script src="{{ asset('mdp/js/moment.min.js') }}"></script>
	<script src="{{ asset('mdp/js/bootstrap-datetimepicker.js') }}"></script>
	<script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('vendor/additional-methods.js') }}"></script>
	<script src="{{ asset('mdp/js/jquery.bootstrap-wizard.js') }}"></script>
	<script src="{{ asset('mdp/js/wizard.js') }}"></script>
	<script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
	<script>
        var kbihid=''
       	$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function() {
            $('.selectpicker').selectpicker();
        	$('#PaketPerjalanan').keyup(function(event) {
        		/* Act on the event */
        		// console.log($(this).val())
        	});

            $("#SearchPaket").on('click', function () {
                var searchid = $("#PaketPerjalanan").val();
                GetSearchPaketTravel(searchid)
                return false;
            });
            $('#PaketPerjalanan').bind("enterKey",function(e){
                var searchid = $("#PaketPerjalanan").val();
                GetSearchPaketTravel(searchid)

            });
            $('#PaketPerjalanan').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
                if (!this.value) {
                    GetListPaketTravel()
                }
            });
        });
        GetListPaketTravel()

        $('#trv').change(function() {
            GetListPaketTravel();
        })

        String.prototype.trunc = String.prototype.trunc ||
            function(n){
                return (this.length > n) ? this.substr(0, n-1) + '&hellip;' : this;
            };

        function GetListPaketTravel()
		{
            kbihid=$('#trv').val()
            $.ajax({
                type: 'GET',
                url: '{{url('/GetAllPaketTravel')}}',
                data:{kbihid:kbihid},
                global:false,
                success: function (data) {
                    var $AllPaketTravels = $('#ListAllPaketPerjalanan');
                    $AllPaketTravels.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllPaketTravels.append('' +
                            '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">' +
							'	<div class="card pointer">' +
							'		<div class="card-header card-header-tabs" data-background-color="blue" data-header-half-animation="true">' +
								'			<h4 class="card-title text-center">'+data[i].name.trunc(30)+'</h4>' +
							'		</div>' +
							'		<div class="card-content text-center">' +
							'			<div class="card-actions">' +
							'				<button type="button" class="btn btn-default btn-simple view-paket" data-id="'+data[i].id+'" rel="tooltip" data-placement="bottom" title="Quick view">' +
							'                	<i class="material-icons">art_track</i>' +
							'            	</button>' +
							'				<button type="button" class="btn btn-success btn-simple edit-paket" data-id="'+data[i].id+'"  rel="tooltip" data-placement="bottom" title="Edit">' +
							'					<i class="material-icons">edit</i>' +
							'				</button>' +
							'				<button type="button" class="btn btn-danger btn-simple delete-paket" data-id="'+data[i].id+'" rel="tooltip" data-placement="bottom" title="Delete">' +
							'					<i class="material-icons">close</i>' +
							'				</button>' +
							'			</div>' +
							'			<h6>'+(data[i].IntervalDate)+' Hari '+(data[i].IntervalDate-1)+' Malam</h6>' +
							'			<div class="card-description">' +
                            			(data[i].Descrip)+
							'			</div>	' +
							'		</div>' +
							'	</div>	' +
							'</div>')
                    }
                }
            });
        }
        function GetSearchPaketTravel(searchid)
		{
            $.ajax({
                type: 'GET',
                url: '{{url('/GetSearchPaketTravel')}}',
                data: {TextId:searchid,kbihid:kbihid},
                global:false,
                success: function (data) {
                    var $AllPaketTravels = $('#ListAllPaketPerjalanan');
                    $AllPaketTravels.empty();
                    if(data == ''){
                        $AllPaketTravels.append('<div class="col-md-12">' +'<h2>Paket Perjalanan tidak ada</h2>'+'</div>');
                    }else{
						for (var i = 0; i < data.length; i++) {
							$AllPaketTravels.append('' +
								'<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">' +
								'	<div class="card pointer">' +
								'		<div class="card-header card-header-tabs" data-background-color="blue" data-header-half-animation="true">' +
									'			<h4 class="card-title text-center">'+data[i].name.trunc(30)+'</h4>' +
								'		</div>' +
								'		<div class="card-content text-center">' +
								'			<div class="card-actions">' +
								'				<button type="button" class="btn btn-default btn-simple view-paket" data-id="'+data[i].id+'" rel="tooltip" data-placement="bottom" title="Quick view">' +
								'                	<i class="material-icons">art_track</i>' +
								'            	</button>' +
								'				<button type="button" class="btn btn-success btn-simple edit-paket" data-id="'+data[i].id+'"  rel="tooltip" data-placement="bottom" title="Edit">' +
								'					<i class="material-icons">edit</i>' +
								'				</button>' +
								'				<button type="button" class="btn btn-danger btn-simple delete-paket" data-id="'+data[i].id+'" rel="tooltip" data-placement="bottom" title="Delete">' +
								'					<i class="material-icons">close</i>' +
								'				</button>' +
								'			</div>' +
								'			<h6>'+(data[i].IntervalDate)+' Hari '+(data[i].IntervalDate-1)+' Malam</h6>' +
								'			<div class="card-description">' +
											(data[i].Descrip)+
								'			</div>	' +
								'		</div>' +
								'	</div>	' +
								'</div>')
						}
					}
                }
            });
        }
		$('#rp').slideReveal({
			// trigger: $(".rpt"),
			position: "right",
			push: false,
			overlay: true,
			zIndex:1300,
		});
		$('#rp').delegate('.rp-close','click',function() {
			$('#rp').slideReveal("hide")
		})

        $(document).delegate(".btnadd .AddItener","click", function (e) {
            e.preventDefault();
            var IdDay = $(this).data('id')
            var iCntTxt = 1;
            var container = $(document.getElementById('sortable'+IdDay));
            iCntTxt = iCntTxt + 1;
            $(container).append(
                '			<div class="row" data-id="'+iCntTxt+'">'+
                '				<div class="col-xs-4">'+
                '					<div class="form-group label-floating">'+
                '						<label class="control-label">Mulai <span class="error-msg"></span></label>'+
                '						<input type="hidden" class="Day" name="IdKeg[]" value="'+IdDay+'"/>'+
                '						<input type="time" class="form-control timepicker timepicker'+IdDay+'-'+iCntTxt+'" name="DateStart[]" required="required"/>'+
                '					</div>'+
                '				</div>'+
                '				<div class="col-xs-7">'+
                '					<div class="form-group label-floating">'+
                '						<label class="control-label">Kegiatan <span class="error-msg"></span></label>'+
                '						<input type="text" class="form-control KegName" name="KegName[]" required="required"/>'+
                '					</div>'+
                '				</div>'+
                '				<div class="col-xs-1">'+
                '					<button class="btn btn-danger btn-simple pl0 pr0 DelItener" data-id="'+IdDay+'" id="DelItener'+IdDay+'">'+
                '						<i class="material-icons">close</i>'+
                '						<div class="ripple-container"></div>'+
                '					</button>'+
                '				</div>'+
                '			</div>');

            $('.timepicker'+IdDay+'-'+iCntTxt).datetimepicker({
                useCurrent: true,
                format: 'HH:mm',
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-chevron-up",
                    down: "fa fa-chevron-down",
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-screenshot',
                    clear: 'fa fa-trash',
                    close: 'fa fa-remove',
                    inline: true
                }
            });
            $( "#sortable"+IdDay).sortable({
                connectWith: ".connectedSortable"
            }).disableSelection();
        });
		$(".input-paket").click(function(){
			$('.loading').show()
			$('#rp').empty().load("{{ route('travel-paket-input') }}", function(){
				$('.card-content-overflow').perfectScrollbar();
				$('.loading').hide()
                demo.initMaterialWizard();
					$(document).delegate(".nextPaket","click", function (e) {
                        var iCnt = $("input[name='Duration").val();
                        $("#accordion").empty();
                        for(var i = 1; i <= iCnt; i++) {
                            $("#accordion").append('' +
										'<div class="panel panel-default">'+
										'	<div class="panel-heading" role="tab" id="kegiatan'+i+'">'+
										'		<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+i+'" aria-expanded="true" aria-controls="collapse'+i+'">'+
										'			<h4 class="panel-title">'+
										'				Hari ke-'+i+
										'				<i class="material-icons">keyboard_arrow_down</i>'+
										'			</h4>'+
										'		</a>'+
										'	</div>'+
										'	<div id="collapse'+i+'" class="panel-collapse collapse '+(i == 1 ? 'in' : '')+'" role="tabpanel" aria-labelledby="heading'+i+'">'+
										'		<div class="panel-body" id="DayItener'+i+'">'+
										'			<div class="row btnadd">'+
										'				<div class="col-xs-1 pull-right">'+
										'					<button class="btn btn-danger btn-simple pl0 pr0 AddItener" data-id="'+i+'" >'+
										'						<i class="material-icons">add</i>'+
										'						<div class="ripple-container"></div>'+
										'					</button>'+
										'				</div>'+
										'			</div>'+
										'			<ul id="sortable'+i+'" class="connectedSortable">'+
										'				<div class="row">'+
										'					<div class="col-xs-4">'+
										'						<div class="form-group label-floating">'+
										'							<label class="control-label">Mulai <span class="error-msg"></span></label>'+
										'							<input type="hidden" class="Day" name="IdKeg[]" value="'+i+'"/>'+
										'							<input type="time" class="form-control timepicker" name="DateStart[]" required="required"/>'+
										'						</div>'+
										'					</div>'+
										'					<div class="col-xs-7">'+
										'						<div class="form-group label-floating">'+
										'							<label class="control-label">Kegiatan <span class="error-msg"></span></label>'+
										'							<input type="text" class="form-control KegName" name="KegName[]" required="required"/>'+
										'						</div>'+
										'					</div>'+
										'					<div class="col-xs-1">'+
										'						<button class="btn btn-danger btn-simple pl0 pr0 DelItener" data-id="'+i+'" id="DelItener'+i+'">'+
										'							<i class="material-icons">close</i>'+
										'							<div class="ripple-container"></div>'+
										'						</button>'+
										'					</div>'+
										'				</div>'+
										'			</ul>'+
										'		</div>'+
										'	</div>'+
										'</div>');
                        }
                        $('.timepicker').datetimepicker({
                            format: 'HH:mm',
                            icons: {
                                time: "fa fa-clock-o",
                                date: "fa fa-calendar",
                                up: "fa fa-chevron-up",
                                down: "fa fa-chevron-down",
                                previous: 'fa fa-chevron-left',
                                next: 'fa fa-chevron-right',
                                today: 'fa fa-screenshot',
                                clear: 'fa fa-trash',
                                close: 'fa fa-remove',
                                inline: true
                            }
                        });

					});

					$(document).delegate(".DelItener","click", function (e) {
						e.preventDefault();
                        if ($(this).parents('ul').find('div.row').length != 1) {
                           $(this).parent('div').parent('div').remove()
                        }
					});

					$(document).delegate("#FinishPaket","click", function (e) {
						e.preventDefault();
                        var count=0;
                        $(".connectedSortable input").each(function() {
                            count = count+($.trim($(this).val())=="" ? 1 : 0);
                            if ($(this).val() == "") {
//                                $(".panel-collapse.in").removeClass("in");
                                $(this).closest(".panel-collapse").addClass("in").css("height","auto");
                                $(this).parent('div').addClass('has-error');
//                                $(this).parent('div').append('<span class="error-msg has-error"> Wajib diisi</span>');
                                $(this).parent('div').find('.error-msg').empty().append('<span class="error-msg has-error"> Wajib diisi</span>');
                                return true;
                            }
                        });
                        if(count<1){
                            var TimeIte = [];
                                var txtboxes = document.getElementsByClassName('timepicker');
                                var txtKegiatan = document.getElementsByClassName('KegName');
                                var txtDay = document.getElementsByClassName('Day');
                                var boxlengt = txtboxes.length;
                                for(var i=0;i<boxlengt;i++){
                                    TimeIte.push({'Days': txtDay[i].value,'Times': txtboxes[i].value, 'KegName': txtKegiatan[i].value})
                                }
                                // console.log(TimeIte)
                                swal({
                                    title: 'Apakah Anda Yakin ?',
                                    type: 'question',
                                    showCancelButton: true,
                                    confirmButtonText: 'Ya!',
                                    cancelButtonText: 'Batal'
                                }).then(function() {
                                    var data={
                                        a:$("input[name='TypeSave']").val(),
                                        PaketName:$("input[name='PaketName").val(),
                                        Duration:$("input[name='Duration").val(),
                                        Desk:$("input[name='Desk").val(),
                                        Itinerary:TimeIte,
                                        kbihid:kbihid
                                    };
                                    $.post("{{route('save-pakettravel') }}", data,function(r) {
                                        swal(
                                            'Berhasil',
                                            '',
                                            'success'
                                        ).then(function() {
                                            $('#rp').empty().slideReveal("hide")
                                            GetListPaketTravel()
                                        })
                                    }, 'json');
                                });
						}

					});


				$('#rp').slideReveal("show")
			})
		})



        $(document).delegate('.view-paket', 'click', function(e) {
            $('.loading').show()
			var Paketid = $(this).data('id');
            $.ajax({
                type: 'GET',
                url: '{{url('/GetDetailPaketTravel')}}',
                data:{IdPaket:Paketid},
                global:false,
                success: function (data) {
                    $('#c-modal').empty().load("{{ route('travel-paket-view') }}", function(){
                        $('.loading').hide()
                        demo.initMaterialWizard();
                        $('.PaketName').empty().append(data.DetailPaketTravel['0'].name)
                        $('.LongDate').empty().append((data.DetailPaketTravel['0'].IntervalDate)+' Hari '+(data.DetailPaketTravel['0'].IntervalDate-1)+' Malam')
                        $('.DescripPaket').empty().append(data.DetailPaketTravel['0'].Descrip)
                        $('#ListDays').empty();
                        for (var i = 0; i < data.DetailItener.length; i++) {
                            $('#ListDays').append('' +
								'<button type="button" class="btn btn-round btn-info btn-sm DayByDayClick" data-day="'+data.DetailItener[i].dayinter+'" data-id="'+data.DetailItener[i].package_id+'">Hari '+data.DetailItener[i].dayinter+'</button>')
                        }
                        DayByDay(0,Paketid);
                        $(document).delegate('.DayByDayClick', 'click', function(e) {
                        	e.preventDefault()
							var DaybyDays = $(this).data('day');
                            DayByDay(DaybyDays,$(this).data('id'))
                        });
                        $('#modalpaket').modal('show')
                    })
                }
            });
        })

        $(document).delegate('.edit-paket', 'click', function(e) {
           $('.loading').show()
            $.ajax({
                type: 'GET',
                url: '{{url('/GetEditPaketTravel')}}',
                data: {IdPaket: $(this).data('id')},
                global: false,
                success: function (data) {
                    $('#rp').empty().load("{{ route('travel-paket-input') }}", function() {
                        $('.card-content-overflow').perfectScrollbar();
                        $('.loading').hide()
                        demo.initMaterialWizard();
                        $('.titleForm').empty().append('Ubah Paket '+data.DetailPaketTravel["0"].name)
                        $("input[name='PaketName']").val(data.DetailPaketTravel["0"].name)
                        $("input[name='TypeSave']").val('editPaket')
                        $("input[name='IdPaket']").val(data.DetailPaketTravel["0"].id)
                        $("input[name='Duration']").val(data.DetailPaketTravel["0"].IntervalDate)
                        $("input[name='Desk']").val(data.DetailPaketTravel["0"].Descrip)
                        
						$(document).delegate("#nextPaket","click", function (e) {
							var iCnt = $("input[name='Duration").val();
							$("#accordion").empty();
							for(var i = 1; i <= iCnt; i++) {
								$("#accordion").append('' +
													'<div class="panel panel-default">'+
													'	<div class="panel-heading" role="tab" id="kegiatan'+i+'">'+
													'		<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+i+'" aria-expanded="true" aria-controls="collapse'+i+'">'+
													'			<h4 class="panel-title">'+
																	'Hari Ke-'+i+
													'				<i class="material-icons">keyboard_arrow_down</i>'+
													'			</h4>'+
													'		</a>'+
													'	</div>'+
													'	<div id="collapse'+i+'" class="panel-collapse collapse '+(i == 1 ? 'in' : '')+'" role="tabpanel" aria-labelledby="heading'+i+'">'+
													'		<div class="panel-body" id="DayItenerTime'+i+'">'+
													'			<div class="row btnadd">'+
													'				<div class="col-xs-1 pull-right">'+
													'					<button class="btn btn-danger btn-simple pl0 pr0 AddItener" data-id="'+i+'" >'+
													'						<i class="material-icons">add</i>'+
													'						<div class="ripple-container"></div>'+
													'					</button>'+
													'				</div>'+
													'			</div>'+
													'		</div>'+
                                    				'		<ul id="sortable'+i+'" class="connectedSortable">'+
                                                    '               <div class="row">'+
                                                    '                   <div class="col-xs-4">'+
                                                    '                       <div class="form-group label-floating">'+
                                                    '                           <label class="control-label">Mulai <span class="error-msg"></span></label>'+
                                                    '                           <input type="hidden" class="Day" name="IdKeg[]" value="'+i+'"/>'+
                                                    '                           <input type="time" class="form-control timepicker" name="DateStart[]" required="required"/>'+
                                                    '                       </div>'+
                                                    '                   </div>'+
                                                    '                   <div class="col-xs-7">'+
                                                    '                       <div class="form-group label-floating">'+
                                                    '                           <label class="control-label">Kegiatan <span class="error-msg"></span></label>'+
                                                    '                           <input type="text" class="form-control KegName" name="KegName[]" required="required"/>'+
                                                    '                       </div>'+
                                                    '                   </div>'+
                                                    '                   <div class="col-xs-1">'+
                                                    '                       <button class="btn btn-danger btn-simple pl0 pr0 DelItener" data-id="'+i+'" id="DelItener'+i+'">'+
                                                    '                           <i class="material-icons">close</i>'+
                                                    '                           <div class="ripple-container"></div>'+
                                                    '                       </button>'+
                                                    '                   </div>'+
                                                    '               </div>'+
                                    				'		</ul>'+
													'	</div>'+
													'</div>');
							}

                            for (var i = 0; i < data.DetailItener.length; i++) {
                                $('#sortable'+data.DetailItener[i].dayinter).empty()
                            }
                            for (var i = 0; i < data.DetailItener.length; i++) {

									var b = $('#sortable'+data.DetailItener[i].dayinter).append(
										'			<div class="row ListKegiatan'+i+'">'+
										'				<div class="col-xs-4">'+
										'					<div class="form-group label-floating">'+
										'						<label class="control-label">Mulai <span class="error-msg"></span></label>'+
                                        '						<input type="hidden" class="Day" name="IdKeg[]" value="'+data.DetailItener[i].dayinter+'"/>'+
										'						<input type="text" class="form-control timepicker TimePickers" name="DateStart[]" required="required" value="'+data.DetailItener[i].itinerary_time+'"/>'+
										'					</div>'+
										'				</div>'+
										'				<div class="col-xs-7">'+
										'					<div class="form-group label-floating">'+
										'						<label class="control-label">Kegiatan <span class="error-msg"></span></label>'+
										'						<input type="text" class="form-control KegName" name="KegName[]" required="required" value="'+data.DetailItener[i].activity_name+'"/>'+
										'					</div>'+
										'				</div>'+
										'				<div class="col-xs-1">'+
										'					<button class="btn btn-danger btn-simple pl0 pr0 DelItener" data-id="'+data.DetailItener[i].dayinter+'" id="DelItener'+data.DetailItener[i].dayinter+'">'+
										'						<i class="material-icons">close</i>'+
										'						<div class="ripple-container"></div>'+
										'					</button>'+
										'				</div>'+
										'			</div>'
									)
									// console.log(b)
							}
                            $(".connectedSortable").sortable({
                                connectWith: ".connectedSortable"
                            }).disableSelection();
							$('.timepicker').datetimepicker({
								format: 'HH:mm',
								icons: {
									time: "fa fa-clock-o",
									date: "fa fa-calendar",
									up: "fa fa-chevron-up",
									down: "fa fa-chevron-down",
									previous: 'fa fa-chevron-left',
									next: 'fa fa-chevron-right',
									today: 'fa fa-screenshot',
									clear: 'fa fa-trash',
									close: 'fa fa-remove',
									inline: true
								}
							});
						});

                        $(document).delegate("#IteneraryTab","click", function (e) {
							var iCnt = $("input[name='Duration").val();
							$("#accordion").empty();
                            for(var i = 1; i <= iCnt; i++) {
                                $("#accordion").append('' +
                                                    '<div class="panel panel-default">'+
                                                    '   <div class="panel-heading" role="tab" id="kegiatan'+i+'">'+
                                                    '       <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse'+i+'" aria-expanded="true" aria-controls="collapse'+i+'">'+
                                                    '           <h4 class="panel-title">'+
                                                                    'Hari Ke-'+i+
                                                    '               <i class="material-icons">keyboard_arrow_down</i>'+
                                                    '           </h4>'+
                                                    '       </a>'+
                                                    '   </div>'+
                                                    '   <div id="collapse'+i+'" class="panel-collapse collapse '+(i == 1 ? 'in' : '')+'" role="tabpanel" aria-labelledby="heading'+i+'">'+
                                                    '       <div class="panel-body" id="DayItenerTime'+i+'">'+
                                                    '           <div class="row btnadd">'+
                                                    '               <div class="col-xs-1 pull-right">'+
                                                    '                   <button class="btn btn-danger btn-simple pl0 pr0 AddItener" data-id="'+i+'" >'+
                                                    '                       <i class="material-icons">add</i>'+
                                                    '                       <div class="ripple-container"></div>'+
                                                    '                   </button>'+
                                                    '               </div>'+
                                                    '           </div>'+
                                                    '       </div>'+
                                                    '       <ul id="sortable'+i+'" class="connectedSortable">'+
                                                    '               <div class="row">'+
                                                    '                   <div class="col-xs-4">'+
                                                    '                       <div class="form-group label-floating">'+
                                                    '                           <label class="control-label">Mulai <span class="error-msg"></span></label>'+
                                                    '                           <input type="hidden" class="Day" name="IdKeg[]" value="'+i+'"/>'+
                                                    '                           <input type="time" class="form-control timepicker" name="DateStart[]" required="required"/>'+
                                                    '                       </div>'+
                                                    '                   </div>'+
                                                    '                   <div class="col-xs-7">'+
                                                    '                       <div class="form-group label-floating">'+
                                                    '                           <label class="control-label">Kegiatan <span class="error-msg"></span></label>'+
                                                    '                           <input type="text" class="form-control KegName" name="KegName[]" required="required"/>'+
                                                    '                       </div>'+
                                                    '                   </div>'+
                                                    '                   <div class="col-xs-1">'+
                                                    '                       <button class="btn btn-danger btn-simple pl0 pr0 DelItener" data-id="'+i+'" id="DelItener'+i+'">'+
                                                    '                           <i class="material-icons">close</i>'+
                                                    '                           <div class="ripple-container"></div>'+
                                                    '                       </button>'+
                                                    '                   </div>'+
                                                    '               </div>'+
                                                    '       </ul>'+
                                                    '   </div>'+
                                                    '</div>');
                            }

                            for (var i = 0; i < data.DetailItener.length; i++) {
                                $('#sortable'+data.DetailItener[i].dayinter).empty()
                            }
                            for (var i = 0; i < data.DetailItener.length; i++) {

                                    var b = $('#sortable'+data.DetailItener[i].dayinter).append(
                                        '           <div class="row ListKegiatan'+i+'">'+
                                        '               <div class="col-xs-4">'+
                                        '                   <div class="form-group label-floating">'+
                                        '                       <label class="control-label">Mulai <span class="error-msg"></span></label>'+
                                        '                       <input type="hidden" class="Day" name="IdKeg[]" value="'+data.DetailItener[i].dayinter+'"/>'+
                                        '                       <input type="text" class="form-control timepicker TimePickers" name="DateStart[]" required="required" value="'+data.DetailItener[i].itinerary_time+'"/>'+
                                        '                   </div>'+
                                        '               </div>'+
                                        '               <div class="col-xs-7">'+
                                        '                   <div class="form-group label-floating">'+
                                        '                       <label class="control-label">Kegiatan <span class="error-msg"></span></label>'+
                                        '                       <input type="text" class="form-control KegName" name="KegName[]" required="required" value="'+data.DetailItener[i].activity_name+'"/>'+
                                        '                   </div>'+
                                        '               </div>'+
                                        '               <div class="col-xs-1">'+
                                        '                   <button class="btn btn-danger btn-simple pl0 pr0 DelItener" data-id="'+data.DetailItener[i].dayinter+'" id="DelItener'+data.DetailItener[i].dayinter+'">'+
                                        '                       <i class="material-icons">close</i>'+
                                        '                       <div class="ripple-container"></div>'+
                                        '                   </button>'+
                                        '               </div>'+
                                        '           </div>'
                                    )
                                    // console.log(b)
                            }
                            $(".connectedSortable").sortable({
                                connectWith: ".connectedSortable"
                            }).disableSelection();
							$('.timepicker').datetimepicker({
								format: 'HH:mm',
								icons: {
									time: "fa fa-clock-o",
									date: "fa fa-calendar",
									up: "fa fa-chevron-up",
									down: "fa fa-chevron-down",
									previous: 'fa fa-chevron-left',
									next: 'fa fa-chevron-right',
									today: 'fa fa-screenshot',
									clear: 'fa fa-trash',
									close: 'fa fa-remove',
									inline: true
								}
							});
						});


                        $(document).delegate(".DelItener","click", function (e) {
                            e.preventDefault();
                            // console.log($(this).parents('ul').find('div.row').length)
                            if ($(this).parents('ul').find('div.row').length != 1) {
                               $(this).parent('div').parent('div').remove()
                            }
                        });
                        $(document).delegate("#FinishPaket","click", function (e) {
						e.preventDefault();
							var count = 0
                            $(".connectedSortable input").each(function() {
                                count = count+($.trim($(this).val())=="" ? 1 : 0);
                                if ($(this).val() == "") {
//                                $(".panel-collapse.in").removeClass("in");
                                    $(this).closest(".panel-collapse").addClass("in").css("height","auto");
                                    $(this).parent('div').addClass('has-error');
                                    return true;
                                }
                            });
                            if(count<1){
                                var TimeIte = [];
                                var txtboxes = document.getElementsByClassName('timepicker');
                                var txtKegiatan = document.getElementsByClassName('KegName');
                                var txtDay = document.getElementsByClassName('Day');
                                var boxlengt = txtboxes.length;
                                for(var i=0;i<boxlengt;i++){
                                    TimeIte.push({'Days': txtDay[i].value,'Times': txtboxes[i].value, 'KegName': txtKegiatan[i].value})
                                }
                                swal({
                                    title: 'Apakah Anda Yakin ?',
                                    type: 'question',
                                    showCancelButton: true,
                                    confirmButtonText: 'Ya',
                                    cancelButtonText: 'Batal'
                                }).then(function() {
                                    var data={
                                        a:$("input[name='TypeSave']").val(),
                                        IdPaket:$("input[name='IdPaket']").val(),
                                        PaketName:$("input[name='PaketName").val(),
                                        Duration:$("input[name='Duration").val(),
                                        Desk:$("input[name='Desk").val(),
                                        Itinerary:TimeIte,
                                        kbihid:kbihid
                                    };
                                    $.post("{{route('save-pakettravel') }}", data,function(r) {
                                        swal(
                                            'Berhasil',
                                            '',
                                            'success'
                                        ).then(function() {
                                            $('#rp').empty().slideReveal("hide")
                                            GetListPaketTravel()
                                        })
                                    }, 'json');
                                });
                            }

					});
                        $('#rp').slideReveal("show")
                    });

                }
            });
        });

        $(document).delegate('.delete-paket', 'click', function(e) {
            e.preventDefault();
            var IdPaket = $(this).data('id')
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                var data={
                    id:IdPaket,
                };
                $.post("{{route('delete-paket') }}", data,function(r) {

                    swal(
                        'Berhasil!',
                        //'Data Paket Travel Telah dihapus.',
                        '',
                        'success'
                    ).then(function() {
                        GetListPaketTravel()
                    })
                }, 'json');

            })
        });

		function DayByDay(IdDay, IdPakets)
		{
            $.ajax({
                type: 'GET',
                url: '{{url('/GetDetailDayByDay')}}',
                data:{IdDay:IdDay, IdPaket:IdPakets},
                global:false,
                success: function (data) {
                    $('.ListKeg tbody').empty();
                    for (var i = 0; i < data.length; i++) {
                        $('.ListKeg tbody').append(
                            '<tr>'+
								'<td>'+data[i].itinerary_time+'</td>'+
								'<td>'+data[i].activity_name+'</td>'+
							'</tr>'
						);
                    }
                }
            });
		}
	</script>
@endsection