<div class="modal fade" id="modalpaket" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg mt20">
		<div class="card">
			<div class="card-header card-header-tabs text-center" data-background-color="blue">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
					<i class="material-icons">clear</i>
				</button>
				<h4 class="card-title PaketName"></h4>
				<span class="LongDate"></span>
				<br>
				<span class="DescripPaket"></span>
			</div>
			<div class="card-content text-center">
				<div class="btn-group" id="ListDays">

				</div>
				<div class="card-description text-left ListItener">
					<div>
						<h4 class="DayKe"></h4>
						<div class="table-responsive">
							<table class="table table-striped ListKeg">
								<thead>
									<tr>
										<td>Jam</td>
										<td>Kegiatan</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th>00.00 - 01.00</th>
										<th>Lorem ipsum dolor sit amet</th>
									</tr>
									<tr>
										<td>00.00 - 01.00</td>
										<td>Lorem ipsum dolor sit amet</td>
									</tr>
									<tr>
										<td>00.00 - 01.00</td>
										<td>Lorem ipsum dolor sit amet</td>
									</tr>
								</tbody>
							</table>				
						</div>                  
					</div>
				</div>
			</div>
			<div class="card-footer">
				<div class="stats pull-right">
					<button type="button" class="btn btn-danger btn-simple" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>          
	</div>
</div>
