@extends4123('Dts::layouts.dts')

@section('title','Travel Profile')

@section('content')

    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="card">
                <div class="card-header card-header-icon edit-travel" data-background-color="blue">
                    <i class="material-icons">mode_edit</i>Ubah
                </div>
                {{--<span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">--}}
                            {{--<i class="material-icons">close</i>--}}
                {{--</span>--}}
                <div class="card-content">
                    {{--<h4 class="card-title">Travel baru</h4>--}}
                    <div class="card-content-overflow">

                        <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                            <div class="thumbnail-custom">
                                <img src="{{ asset('mdp/img/card-1.jpg') }}" alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"></div>
                            {{--<div>--}}
                                        {{--<span class="btn btn-rose btn-round btn-file">--}}
                                        {{--<span class="fileinput-new">Select image</span>--}}
                                        {{--<span class="fileinput-exists">Change</span>--}}
                                            {{--<input type="file" name="_______" />--}}
                                        {{--</span>--}}
                                {{--<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>--}}
                            {{--</div>--}}
                        </div>

                        <div class="form-group label-floating">
                            <label class="control-label">Nama Travel</label>
                            <input type="text" class="form-control" name="_______">
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label">Owner</label>
                            <input type="text" class="form-control" name="_______">
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label">Pilot Number</label>
                            <input type="text" class="form-control" name="_______">
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label">Address</label>
                            <input type="text" class="form-control" name="_______">
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label">City</label>
                            <input type="text" class="form-control" name="_______">
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label">Province</label>
                            <input type="text" class="form-control" name="_______">
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label">Phone Number</label>
                            <input type="text" class="form-control" name="_______">
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label">Fax</label>
                            <input type="text" class="form-control" name="_______">
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label">Email</label>
                            <input type="text" class="form-control" name="_______">
                        </div>
                        <div class="form-group label-floating">
                            <label class="control-label">SIUP</label>
                            <input type="text" class="form-control" name="_______">
                        </div>
                    </div>
                </div>


                {{--<div class="card-footer">--}}
                    {{--<div class="pull-right">--}}
                        {{--<button type="submit" class="btn btn-fill btn-rose">Save</button>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>

    {{-- right panel --}}
    <script>
        $('#rp').slideReveal({
            // trigger: $(".rpt"),
            position: "right",
            push: false,
            overlay: true,
            zIndex:1300,
        });
        $('#rp').delegate('.rp-close','click',function() {
            $('#rp').slideReveal("hide")
        })

        $(".edit-travel").click(function(){
            $('.loading').show()
            $('#rp').empty().load("{{ route('travel-edit') }}", function(){
                $('.card-content-overflow').perfectScrollbar();
                $('.loading').hide()
                $('#rp').slideReveal("show")
            })
        })

        $(".card-travel").click(function(){
            console.log('klik')
        }).find(".card-actions").click(function(e) {
            return false;
        });
    </script>

    {{-- datatable --}}
    <script src="{{ asset('mdp/js/jquery.datatables.js') }}"></script>
    <script>
        $("#list").DataTable({
            ajax: {
                url: "{{ route('travel-pendamping-data') }}",
                dataSrc: '',
                // type: "POST",
                data:{
                    a:"getall"
                }
            },
            columns: [
                {
                    class : "text-center",
                    render: function (data, type, full, meta) {
                        a=''
                            +'<a href="#profile" class="btn btn-simple btn-info btn-icon like"><i class="-aterial-icons">zoom_in</i><div class="ripple-container"></div></a>'
                        return a;
                    }
                },//0
                {
                    render: function (data, type, full, meta) {
                        return full.MtwFirstName+" "+full.MtwLastName;
                    }
                },//1
                {
                    data: "Idmtw",
                    render: function (data, type, full, meta) {
                        return '<span class="label label-success">Available</span>';
                    }
                },//2
            ],
            columnDefs: [
                // {targets: '_all', orderable: false }
            ],
            createdRow:  function ( row, data, index ) {
                $(row).data("idmtw",data.Idmtw).addClass("pointer");
            },
            responsive: true,
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            },
            dom: "<'row'<'col-sm-12'<'pull-left''toolbar'><'pull-right'f><'clearfix'>>>tr<'row'<'col-sm-12'<'pull-left'i><'pull-right'p><'clearfix'>>>"
        });
        $("div.toolbar").html(''
            +'<button class="btn btn-info rpt input-pendamping">'
            +'	<span class="btn-label">'
            +'		<i class="material-icons">add</i>'
            +'	</span>'
            +'	Tambah Pendamping'
            +'</button>'
        )

        // row click
        $('#list tbody').on( 'click', 'tr', function () {
            idmtw=$(this).data("idmtw")
            console.log(idmtw)

            $('#profile').fadeOut(100, function() {
                $('#profile').load("{{ route('travel-pendamping-profile') }}", function(){
                    $('#profile').fadeIn(300)
                })
            })


        })

    </script>

@endsection