<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
				<i class="material-icons">close</i>
			</span>
    <div class="card-content">
        <h4 class="card-title">Ubah Informasi</h4>
        <div class="card-content-overflow">

            <div class="fileinput fileinput-new text-center" data-provides="fileinput">
                <div class="fileinput-new thumbnail img-circle">
                    <img src="{{ asset('mdp/img/placeholder.jpg') }}" alt="...">
                </div>
                <div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
                <div>
							<span class="btn btn-rose btn-round btn-file">
							<span class="fileinput-new">Select photo</span>
							<span class="fileinput-exists">Change</span>
								<input type="file" name="_______" />
							</span>
                    <a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>
                </div>
            </div>

            <div class="form-group label-floating">
                <label class="control-label">Nama Travel</label>
                <input type="text" class="form-control" name="_______">
            </div>
            <div class="form-group label-floating">
                <label class="control-label">Owner</label>
                <input type="text" class="form-control" name="_______">
            </div>
            <div class="form-group label-floating">
                <label class="control-label">Pilot Number</label>
                <input type="text" class="form-control" name="_______">
            </div>
            <div class="form-group label-floating">
                <label class="control-label">Address</label>
                <input type="text" class="form-control" name="_______">
            </div>
            <div class="form-group label-floating">
                <label class="control-label">City</label>
                <input type="text" class="form-control" name="_______">
            </div>
            <div class="form-group label-floating">
                <label class="control-label">Province</label>
                <input type="text" class="form-control" name="_______">
            </div>
            <div class="form-group label-floating">
                <label class="control-label">Phone Number</label>
                <input type="text" class="form-control" name="_______">
            </div>
            <div class="form-group label-floating">
                <label class="control-label">Fax</label>
                <input type="text" class="form-control" name="_______">
            </div>
            <div class="form-group label-floating">
                <label class="control-label">Email</label>
                <input type="text" class="form-control" name="_______">
            </div>
            <div class="form-group label-floating">
                <label class="control-label">SIUP</label>
                <input type="text" class="form-control" name="_______">
            </div>
        </div>
    </div>


    <div class="card-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-fill btn-rose">Simpan</button>
        </div>
    </div>
</div>
