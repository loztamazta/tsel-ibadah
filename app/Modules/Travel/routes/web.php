<?php

Route::group(array('module' => 'Travel', 'middleware' => ['web', 'auth', 'activated'], 'namespace' => 'App\Modules\Travel\Controllers'), function() {

    // Route::resource('travel', 'TravelController');
	Route::get('/',								['uses' => 'TravelController@index', 'as'=>'travel']);

	Route::get('/travel/agen',					['uses' => 'AgenController@agen', 'as'=>'travel-agen']);
	Route::get('/travel/agen/input',			['uses' => 'AgenController@agenInput', 'as'=>'travel-agen-input']);
	Route::get('/travel/agen/view',				['uses' => 'AgenController@agenView', 'as'=>'travel-agen-view']);
	Route::get('/travel/detail',                ['uses' => 'AgenController@travelDetail', 'as'=>'travel-detail']);
	Route::get('/travel/edit',                  ['uses' => 'AgenController@travelEdit', 'as'=>'travel-edit']);
    Route::get('/travel/agen/schedule',			['uses' => 'AgenController@travelschedule', 'as'=>'schedule-travel']);
	Route::get('/travel/agen/{a}',				['uses' => 'AgenController@agenDashboard', 'as'=>'travel-agen-dashboard', 'middleware' => ['role:sadmin']]);

	Route::get('/travel/pendamping',			['uses' => 'PendampingController@pendamping', 'as'=>'travel-pendamping']);
	Route::post('/travel/pendamping/input',		['uses' => 'PendampingController@pendampingInput', 'as'=>'travel-pendamping-input']);
	Route::get('/travel/pendamping/profile/{b}',['uses' => 'PendampingController@pendampingProfile', 'as'=>'travel-pendamping-profile']);
	Route::post('/travel/pendamping/data',		['uses' => 'PendampingController@pendampingData', 'as'=>'travel-pendamping-data']);

	Route::get('/travel/paket',					['uses' => 'PaketController@paket', 'as'=>'travel-paket']);
	Route::get('/travel/paket/input',			['uses' => 'PaketController@paketInput', 'as'=>'travel-paket-input']);
	Route::get('/travel/paket/view',			['uses' => 'PaketController@paketView', 'as'=>'travel-paket-view']);

	Route::get('/travel/grup',					['uses' => 'GrupController@grup', 'as'=>'travel-grup']);
	Route::get('/travel/grup/input',			['uses' => 'GrupController@grupInput', 'as'=>'travel-grup-input']);
	Route::get('/travel/grup/edit',			    ['uses' => 'GrupController@grupEdit', 'as'=>'travel-grup-edit']);
	Route::get('/travel/grup/jamaahinput/{idpack}/{id}',	['uses' => 'GrupController@grupJamaahInput', 'as'=>'travel-grup-jamaahinput']);
	Route::get('/travel/grup/jamaahview/{idpack}/{id}',	['uses' => 'GrupController@grupJamaahView', 'as'=>'travel-grup-jamaah-view']);
	Route::get('/travel/grup/detail/{id}',      ['uses' => 'GrupController@grupDetail', 'as'=>'travel-grup-detail']);
	Route::post('/travel/grup/data',           	['uses' => 'GrupController@grupData', 'as'=>'travel-grup-data']);
	Route::post('/travel/grup/excel',    		['uses' => 'GrupController@grupExcel', 'as'=>'travel-grup-excel']);
	Route::post('/travel/grup/detailjamaah',    ['uses' => 'GrupController@detailjamaah', 'as'=>'travel-detail-jamaah']);
	Route::get('/travel/grup/getedit',            ['uses' => 'GrupController@groupgetedit', 'as'=>'travel-group-getedit']);

	Route::get('/travel/hotel',					['uses' => 'HotelController@hotel', 'as'=>'travel-hotel']);
	Route::get('/GetAllHotels',                 ['uses' => 'HotelController@AllHotels', 'as'=>'all-hotel']);
    Route::get('/SearchHotel',                  ['uses' => 'HotelController@SearchHotel', 'as'=>'search-hotel']);
	Route::get('/travel/hotel/input',			['uses' => 'HotelController@hotelInput', 'as'=>'travel-hotel-input']);
	Route::get('/travel/hotel/view',			['uses' => 'HotelController@hotelView', 'as'=>'travel-hotel-view']);
	Route::get('/travel/hotel/edit',            ['uses' => 'HotelController@editHotel', 'as'=>'travel-hotel-edit']);
    Route::post('/travel/hotel/postHotel',     	['uses' => 'HotelController@postHotel', 'as'=>'post-hotel']);
    Route::post('/travel/hotel/deleteHotel',    ['uses' => 'HotelController@deleteHotel', 'as'=>'delete-hotel']);

	Route::get('/ScanProvince',			        ['uses' => 'TravelController@Get001']);
	Route::get('/ScanKabupaten',			    ['uses' => 'TravelController@Get002']);
	Route::get('/ScanKota',			            ['uses' => 'TravelController@Get003']);
    Route::post('/UploadImage',			        ['uses' => 'TravelController@uploadfile', 'as'=>'cms-upload']);
    Route::post('/UploadImageTwo',			        ['uses' => 'TravelController@uploadfileTwo', 'as'=>'cms-uploadtwo']);
    Route::post('/update-profile',				['uses' => 'TravelController@updateprofile', 'as'=>'update-profile']);

	Route::get('/GetAllTravelsAgent',			['uses' => 'AgenController@Get004']);
	Route::get('/GetTravelSearch',			    ['uses' => 'AgenController@Get007']);
	Route::get('/GetDetailTravel',			    ['uses' => 'AgenController@Get005']);
	Route::get('/EditTravelAgent',			    ['uses' => 'AgenController@Get006']);
    Route::post('/InsertTravel',			    ['uses' => 'AgenController@Post001', 'as'=>'cms-inserttravel']);
    Route::post('/DeleteTravel',			    ['uses' => 'AgenController@Post002', 'as'=>'delete-travel']);


	Route::get('/GetAllPaketTravel',			['uses' => 'PaketController@Get007']);
	Route::get('/GetSearchPaketTravel',			['uses' => 'PaketController@Get0011']);
	Route::get('/GetDetailPaketTravel',			['uses' => 'PaketController@Get008']);
	Route::get('/GetDetailDayByDay',			['uses' => 'PaketController@Get009']);
	Route::get('/GetEditPaketTravel',			['uses' => 'PaketController@Get010']);
    Route::post('/InsertPaketTravel',			['uses' => 'PaketController@Post003', 'as'=>'save-pakettravel']);
    Route::post('/DeletePaketTravel',			['uses' => 'PaketController@Post004', 'as'=>'delete-paket']);

	Route::get('/GetDataPHS',			        ['uses' => 'GrupController@Get010']);
	Route::get('/GetKegiatanTime',			    ['uses' => 'GrupController@Get011']);
    Route::get('/GetLisKeberangkatan',			['uses' => 'GrupController@Get012']);
    Route::get('/GetRobonganSearch',			['uses' => 'GrupController@Get013']);
    



	// Route::get('/grup/jamaah', function () {
	//     return view("Travel::grup.jamaah");
	// });

    
});	
