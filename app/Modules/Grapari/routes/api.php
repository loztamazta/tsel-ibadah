<?php

Route::group(array('module' => 'Grapari', 'middleware' => ['api'], 'namespace' => 'App\Modules\Grapari\Controllers'), function() {

    Route::resource('Grapari', 'GrapariController');
    
});	
