<?php

Route::group(array('module' => 'Grapari', 'middleware' => ['web'], 'namespace' => 'App\Modules\Grapari\Controllers'), function() {

    //Route::resource('Grapari', 'GrapariController');

    Route::get('/index/grapari',			           ['uses' => 'GrapariController@index', 'as'=>'grapari']);
    Route::get('/GetAllGrapari',                       ['uses' => 'GrapariController@AllGraparis', 'as'=>'all-grapari']);
    Route::get('grapari/input',			               ['uses' => 'GrapariController@GrapariInput', 'as'=>'grapari-input']);
    Route::get('grapari/view',			               ['uses' => 'GrapariController@GrapariView', 'as'=>'grapari-view']);
    Route::get('grapari/edit',                         ['uses' => 'GrapariController@editGrapari', 'as'=>'grapari-edit']);
    Route::get('/searchGrapari',                       ['uses' => 'GrapariController@SearchGrapari', 'as'=>'grapari-search']);
    Route::get('/scanKotaGrapari',                     ['uses' => 'GrapariController@getKotaGrapari', 'as'=>'kota-grapari']);

    Route::post('grapari/postGrapari',                 ['uses' => 'GrapariController@postGrapari', 'as'=>'post-grapari']);
    Route::post('grapari/deleteGrapari',               ['uses' => 'GrapariController@deleteGrapari', 'as'=>'delete-grapari']);

});	
