@extends('Dts::layouts.dts')

@section('title','Grapari')

@section('content')
    <div class="row">
        <div class="col-md-6">

            <button class="btn btn-info rpt input-grapari">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
                Tambah Grapari
            </button>
        </div>
        <div class="col-md-3 pull-right">
            <div class="input-group">
                <div class="form-group label-floating">
                    <label class="control-label">
                        Search...
                    </label>
                    <input id="searchGrapari" name="searchGrapari" type="text" class="form-control">
                </div>
                <a href="javascript:void(0)" id="grapariSearch" class="input-group-addon">
                    <i class="material-icons">search</i>
                </a>
            </div>
        </div>
    </div>

    <div class="row" id="ListGrapari"></div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
    <script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('vendor/additional-methods.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKZrw7eKJ7OHhQ_bnpEBeCUV8MTdwV3LI"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function() {

            $("#grapariSearch").on('click', function () {
                var searchid = $("#searchGrapari").val();
                GetSearchGrapari(searchid)
                return false;
            });
            $('#searchGrapari').bind("enterKey",function(e){
                var searchid = $("#searchGrapari").val();
                GetSearchGrapari(searchid)

            });
            $('#searchGrapari').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
                if (!this.value) {
                    GetListGrapari()
                }
            });
        });

        GetListGrapari()

        String.prototype.trunc = String.prototype.trunc ||
            function(n){
                return (this.length > n) ? this.substr(0, n-1) + '&hellip;' : this;
            };

        function GetListGrapari()
        {
            console.log('Start')
            $.ajax({
                type: 'GET',
                url: '{{route('all-grapari')}}',
                global:false,
                success: function (data) {
                    var $AllGraparis= $('#ListGrapari');
                    $AllGraparis.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllGraparis.append('' +
                            '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">' +
                            '<div class="card card-product card-travel pointer">' +
                            '    <div class="card-image" data-header-animation="true">' +
                            '        <img class="img" src="'+(data[i].Img  ? 'storage/'+data[i].Img : '{{ asset('mdp/img/card-1.jpg') }}')+'">'+
                            '    </div>' +
                            '    <div class="card-content">' +
                            '        <div class="card-actions">' +
                            '            <button type="button" class="btn btn-danger btn-simple fix-broken-card">' +
                            '                <i class="material-icons">build</i> Fix Header!' +
                            '            </button>' +
                            '            <button type="button" class="btn btn-default btn-simple view-grapari" data-id="'+data[i].grapari_id+'" rel="tooltip" data-placement="bottom" title="Quick view">' +
                            '                <i class="material-icons">art_track</i>' +
                            '            </button>' +
                            '            <button type="button" class="btn btn-success btn-simple input-grapari" data-id="'+data[i].grapari_id+'" rel="tooltip" data-placement="bottom" title="Edit">' +
                            '                <i class="material-icons">edit</i>' +
                            '            </button>' +
                            '            <button type="button" class="btn btn-danger btn-simple delete-grapari" data-id="'+data[i].grapari_id+'" rel="tooltip" data-placement="bottom" title="Remove">' +
                            '                <i class="material-icons">close</i>' +
                            '            </button>' +
                            '    </div>' +
                            '        <h4 class="card-title text-left">' +
                            '            <a href="#TravelName">'+data[i].grapari_name.trunc(30)+'</a>' +
                            '        </h4>'+
                            '</div>'+
                            '</div>')
                    }
                }
            });
        }

        function GetSearchGrapari(searchid)
        {
            debugger
            $.ajax({
                type: 'GET',
                url: '{{url('/searchGrapari')}}',
                data: {TextId:searchid},
                global:false,
                success: function (data) {
                    var $AllGraparis = $('#ListGrapari');
                    $AllGraparis.empty();
                    if(data == ''){
                        $AllGraparis.append('<div class="col-md-12">' +'<h2>Data tidak ada</h2>'+'</div>');
                    }else{
                        for (var i = 0; i < data.length; i++) {
                            $AllGraparis.append('' +
                                '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">' +
                                '<div class="card card-product card-travel pointer">' +
                                '    <div class="card-image" data-header-animation="true">' +
                                '        <img class="img" src="'+(data[i].Img  ? 'storage/'+data[i].Img : '{{ asset('mdp/img/card-1.jpg') }}')+'">'+
                                '    </div>' +
                                '    <div class="card-content">' +
                                '        <div class="card-actions">' +
                                '            <button type="button" class="btn btn-danger btn-simple fix-broken-card">' +
                                '                <i class="material-icons">build</i> Fix Header!' +
                                '            </button>' +
                                '            <button type="button" class="btn btn-default btn-simple view-grapari" data-id="'+data[i].grapari_id+'" rel="tooltip" data-placement="bottom" title="Quick view">' +
                                '                <i class="material-icons">art_track</i>' +
                                '            </button>' +
                                '            <button type="button" class="btn btn-success btn-simple input-grapari" data-id="'+data[i].grapari_id+'" rel="tooltip" data-placement="bottom" title="Edit">' +
                                '                <i class="material-icons">edit</i>' +
                                '            </button>' +
                                '            <button type="button" class="btn btn-danger btn-simple delete-grapari" data-id="'+data[i].grapari_id+'" rel="tooltip" data-placement="bottom" title="Remove">' +
                                '                <i class="material-icons">close</i>' +
                                '            </button>' +
                                '    </div>' +
                                '        <h4 class="card-title text-left">' +
                                '            <a href="#TravelName">'+data[i].grapari_name.trunc(30)+'</a>' +
                                '        </h4>'+
                                '</div>'+
                                '</div>')
                        }
                    }
                }
            });
        }

        $('#rp').slideReveal({
            // trigger: $(".rpt"),
            position: "right",
            push: false,
            overlay: true,
            zIndex:1300,
        });
        $('#rp').delegate('.rp-close','click',function() {
            $('#rp').slideReveal("hide")
        })

        $(document).delegate('.input-grapari', 'click', function(e) {
            if($(this).attr('title') == 'Edit'){
                $('.loading').show()
                $.ajax({
                    type: 'GET',
                    url: '{{route('grapari-edit')}}',
                    data:{idGrapari:$(this).data('id')},
                    global:false,
                    success: function (data) {
                        $('#rp').empty().load("{{ route('grapari-input') }}", function(){
                            $('#rp').slideReveal("show")
                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()
//                            debugger;

                            var scr = 'storage/'+data['0'].Img;
                            $('.titleForm').empty().append('Ubah Grapari '+data['0'].grapari_name)
                            $("input[name='save_type']").val('EditSave')
                            $("input[name='IdGrapari']").val(data['0'].grapari_id)
                            $("input[name='grapari_name']").val(data['0'].grapari_name)
                            $("input[name='place_type']").val('6')
                            $("input[name='latitude']").val(data['0'].latitude)
                            $("input[name='longitude']").val(data['0'].longitude)
                            $("select[name='region_id']").val(data['0'].region_id).attr('selected', true).change();
                            $("textarea.address").val(data['0'].address);
                            $('#rp').slideReveal("show")
                            var marker;

                            function myMap() {
                                var mapProp= {
                                    center:new google.maps.LatLng(24.68773000,46.72185000),
                                    zoom:5,
                                    scrollwheel: false
                                };
                                var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

                                google.maps.event.addListener(map, 'click', function (event) {
                                    placeMarker(event.latLng);
                                    document.getElementById("latFld").value = event.latLng.lat();
                                    document.getElementById("lngFld").value = event.latLng.lng();
                                })
                                var lat = document.getElementById('latFld').value;
                                var lng = document.getElementById('lngFld').value;
                                var Location = new google.maps.LatLng(lat, lng);

                                placeMarker(Location)

                                function placeMarker(location) {



                                    if (marker == undefined){
                                        marker = new google.maps.Marker({
                                            position: location,
                                            map: map,
                                            animation: google.maps.Animation.DROP,
                                        });
                                    }
                                    else{
                                        marker.setPosition(location);
                                    }
                                    map.setCenter(location);

                                }
                            }
                            myMap()
                        })
                    }
                });

            }else{
                $('.loading').show()
                $('#rp').empty().load("{{ route('grapari-input') }}", function(){
                    $('.card-content-overflow').perfectScrollbar();
                    $('.loading').hide()
                    $('#rp').slideReveal("show")

                    var marker;

                    function myMap() {
                        var mapProp= {
                            center:new google.maps.LatLng(24.68773000,46.72185000),
                            zoom:5,
                            scrollwheel: false
                        };
                        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

                        google.maps.event.addListener(map, 'click', function (event) {
                            placeMarker(event.latLng);
                            document.getElementById("latFld").value = event.latLng.lat();
                            document.getElementById("lngFld").value = event.latLng.lng();
                        })
                        var lat = document.getElementById('latFld').value;
                        var lng = document.getElementById('lngFld').value;
                        var Location = new google.maps.LatLng(lat, lng);

                        placeMarker(Location)

                        function placeMarker(location) {



                            if (marker == undefined){
                                marker = new google.maps.Marker({
                                    position: location,
                                    map: map,
                                    animation: google.maps.Animation.DROP,
                                });
                            }
                            else{
                                marker.setPosition(location);
                            }
                            map.setCenter(location);

                        }
                    }
                    myMap()
                })
            }

        })
        $(document).delegate('.view-grapari', 'click', function(e) {
            // console.log('klik')
            $('.loading').show()
            $.ajax({
                type: 'GET',
                url: '{{route('grapari-edit')}}',
                data:{idGrapari:$(this).data('id')},
                global:false,
                success: function (data) {
                    $('#c-modal').empty().load("{{ route('grapari-view') }}", function(){
                        $('.loading').hide()

                        $("input[name='save_type']").val('EditSave')
                        $("input[name='IdGrapari']").val(data['0'].id)
                        $("input[name='grapari_name']").val(data['0'].grapari_name)
                        $("input[name='place_type']").val('6')
                        $("input[name='region_name']").val(data['0'].kota)
                        $("input[name='latitude']").val(data['0'].latitude)
                        $("input[name='longitude']").val(data['0'].longitude)
                        $("textarea.address").val(data['0'].address);
                        $('#modal').modal('show')
                        var marker;
                        var map;
                        function myMap() {
                            var lat = data['0'].latitude;
                            var lng = data['0'].longitude;
                            var Location = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                            var mapProp= {
                                center:Location,
                                zoom:5,
                                scrollwheel: false
                            };
                            map = new google.maps.Map(document.getElementById("googleMap"),mapProp);


                            console.log(Location);
                            placeMarker(Location)

                            function placeMarker(location) {



                                if (marker == undefined){
                                    marker = new google.maps.Marker({
                                        position: location,
                                        map: map,
                                        animation: google.maps.Animation.DROP,
                                    });
                                }
                                else{
                                    marker.setPosition(location);
                                }
                                map.setCenter(location);

                            }
                        }
                        myMap()
                        $('#modal').on('shown.bs.modal', function () {
                            google.maps.event.trigger(map, "resize");
                            var lat = data['0'].latitude;
                            var lng = data['0'].longitude;
                            var Location = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                            map.setCenter(Location);
                        })
                    })
                }
            });
        })
        $(document).delegate('.delete-grapari', 'click', function(e) {
            // console.log('klik')
            e.preventDefault();
            var IdTravel = $(this).data('id')
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: 'Iya!',
                cancelButtonText:'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                var data={
                    id:IdTravel,
                };
                $.post("{{route('delete-grapari') }}", data,function(r) {

                    swal(
                        'Berhasil!',
                        //'Data Grapari telah dihapus.',
                        '',
                        'success'
                    ).then(function() {
                        location.reload();
                    })
                }, 'json');

            })
        })

        function LoadKota(){
            $.ajax({
                type: 'GET',
                url: '{{route('kota-grapari')}}',
                global:false,
                success: function (data) {
                    var $lokasiHotel= $('#selectRegion');
                    $lokasiHotel.empty();
                    for (var i = 0; i < data.length; i++) {
                        $lokasiHotel.append('<option id="' + data[i].id + '" value="' + data[i].region_id + '">' + data[i].name + '</option>');
                    }
                    $('#selectRegion option:first-child').attr("selected", "selected");
                    $lokasiHotel.change();

                }
            });
        }

        $(document).delegate('#saveGrapariData', 'click', function(e) {
            e.preventDefault();
            var $validator = $('.card-content-overflow form').validate({
                rules: {
                    grapari_name: {
                        required: true,
                        contentSpecialCharacters: true,
                    },
                    latitude: {
                        required: true,
                        number: true,
                    },
                    longitude: {
                        required: true,
                        number: true,
                    },
                    region_id: {
                        required: true,
                    },

                },
                messages: {
                    grapari_name: "Wajib diisi",
                    latitude: "Wajib diisi / Karakter tidak diperbolehkan",
                    longitude: "Wajib diisi / Karakter tidak diperbolehkan",
                    region_id: "Wajib dipilih",
                },
                errorPlacement: function(error, element) {
                    $(element).parent('div').addClass('has-error');
                    $(element).parent('div').append(error);

                }

            });
            var $valid = $('.card-content-overflow form').valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            swal({
                title: 'Apakah Anda Yakin ?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()

                savedata()
            })
        });

        function savedata(){
                data = {
                    a: $("input[name='save_type").val(),
                    IdGrapari: $("input[name='IdGrapari").val(),
                    name: $("input[name='grapari_name").val(),
                    type_id: $("input[name='place_type").val(),
                    latitude: $("input[name='latitude").val(),
                    longitude: $("input[name='longitude").val(),
                    region_id: $('#selectRegion').find(":selected").val(),
                    address: $("textarea.address").val(),
                    IsFlag: $("input[name='IsFlag").val(),
                }
                $.post("{{route('post-grapari') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        $('#rp').empty().slideReveal("hide")
                        GetListGrapari()
                    })
                }, 'json');
            };


    </script>



@endsection