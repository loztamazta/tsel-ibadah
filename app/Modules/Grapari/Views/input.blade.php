<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
				<i class="material-icons">close</i>
			</span>
    <div class="card-content">

        <h4 class="card-title titleForm">Grapari</h4>
        <div class="card-content-overflow">

            {{--<div class="fileinput fileinput-new text-center" data-provides="fileinput">--}}
            {{--<div class="fileinput-new thumbnail">--}}
            {{--<img src="{{ asset('images/image_placeholder.jpg') }}" alt="...">--}}
            {{--</div>--}}
            {{--<div class="fileinput-preview fileinput-exists thumbnail"></div>--}}
            {{--<div>--}}
            {{--<span class="btn btn-rose btn-round btn-file">--}}
            {{--<span class="fileinput-new">Select image</span>--}}
            {{--<span class="fileinput-exists">Change</span>--}}
            {{--<input type="file" name="_______" />--}}
            {{--</span>--}}
            {{--<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            <form id="FormHotel">
                <div class="form-group label-floating">
                    <label class="control-label">Nama Grapari</label>
                    <input type="text" class="form-control" name="grapari_name">
                    <input type="hidden" name="IdGrapari">
                    <input type="hidden" value="6" name="place_type">
                    <input type="hidden" value="SaveGrapari" name="save_type">
                    <input type="hidden" value="0" name="IsFlag">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Lokasi di Peta</label>
                    {{--<input type="text" class="form-control" name="latitude">--}}
                    {{--</div>--}}
                    {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">Longitude</label>--}}
                    {{--<input type="text" class="form-control" name="longitude">--}}
                </div>
                <div id="googleMap" style="width:100%;height:400px;"></div>
                <div class="form-group label-floating">
                    <label class="control-label">Latitude</label>
                    <input type="text" id="latFld" name="latitude">

                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Longitude</label>
                    <input type="text" id="lngFld" name="longitude">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Lokasi Grapari</label>
                    <select class="selectpicker" data-style="select-with-transition" title="Pilih kota..." data-size="7" id="selectRegion" name="region_id">
                        <option disabled>Pilih kota</option>
                        <option value="1">Mekkah </option>
                        <option value="2">Madinnah</option>
                        <option value="3">Jeddah</option>
                    </select>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Alamat Grapari</label>
                    <textarea class="form-control address" placeholder="" rows="4" name="address"></textarea>
                </div>
            </form>

        </div>

    </div>

    <div class="card-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-fill btn-rose" id="saveGrapariData">Simpan</button>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>
