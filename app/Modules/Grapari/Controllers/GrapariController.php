<?php namespace App\Modules\Grapari\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Input;
use DB;
use Image;
use Storage;
use Excel;
class GrapariController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        if (Auth::user()->hasRole(['admin','sadmin'])) {
            return view("Grapari::index");
        }
	}

    public function AllGraparis()
    {
        $Graparis= DB::select('SELECT A.id as grapari_id, A.name as grapari_name, C.name as kota, latitude, longitude, address, IsFlag, ChangeBy
                               FROM db_ibadah.place AS A
                               JOIN db_ibadah.place_type AS B ON A.type_id = B.id
                               JOIN db_ibadah.place_region AS C ON A.region_id = C.id
                               WHERE A.type_id = 6 AND A.IsFlag = 0;');

//        $Graparis= DB::select('SELECT * FROM db_ibadah.place
//							  WHERE type_id = 6 AND IsFlag = 0');

        return $Graparis;
    }

    function SearchGrapari()
    {
        $keywordGrapari= $_GET['TextId'];
        $GetFilteredGrapari= DB::select('SELECT A.id as grapari_id, A.name as grapari_name, C.name as kota, latitude, longitude, address, IsFlag, ChangeBy
                                           FROM db_ibadah.place AS A
                                           JOIN db_ibadah.place_type AS B ON A.type_id = B.id
                                           JOIN db_ibadah.place_region AS C ON A.region_id = C.id
                                           WHERE A.type_id = 6 AND A.IsFlag = 0
                                           AND A.name LIKE "%'.$keywordGrapari.'%"');
        return $GetFilteredGrapari;
    }


    function GrapariInput()
    {
        return view("Grapari::input");
    }

    function postGrapari()
    {
        if (Input::get('a') == 'SaveGrapari') {
            DB::table('db_ibadah.place')->insert([
                'name' => Input::get('name'),
                'type_id' => Input::get('type_id'),
                'latitude' => Input::get('latitude'),
                'longitude' => Input::get('longitude'),
                'region_id' => Input::get('region_id'),
                'address' => Input::get('address'),
                'IsFlag' => Input::get('IsFlag'),
            ]);
            return json_encode(Input::all());
        } else {
            DB::table('db_ibadah.place')->where('id', Input::get('IdGrapari'))->update([
                'name' => Input::get('name'),
                'type_id' => Input::get('type_id'),
                'latitude' => Input::get('latitude'),
                'longitude' => Input::get('longitude'),
                'region_id' => Input::get('region_id'),
                'address' => Input::get('address'),
                'IsFlag' => Input::get('IsFlag'),

            ]);
            return json_encode(Input::all());
        };
    }

    function editGrapari()
    {
        $IdGrapari = $_GET['idGrapari'];
        $EditGrapari= DB::select('SELECT A.id as grapari_id, A.name as grapari_name, A.region_id, C.name as kota, latitude, longitude, address, IsFlag, ChangeBy
                                  FROM db_ibadah.place AS A
                                  JOIN db_ibadah.place_type AS B ON A.type_id = B.id
                                  JOIN db_ibadah.place_region AS C ON A.region_id = C.id
                                  WHERE A.IsFlag = 0 AND A.id =?;',[$IdGrapari]);
        return $EditGrapari;
    }

    function GrapariView()
    {
        return view('Grapari::view');

    }


    function deleteGrapari()
    {

        DB::table('db_ibadah.place')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
            'ChangeBy' => Auth::user()->id,
        ]);
        return json_encode(Input::all());

    }

}
