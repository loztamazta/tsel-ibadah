<?php

Route::group(array('module' => 'PackageProduct', 'middleware' => ['web'], 'namespace' => 'App\Modules\PackageProduct\Controllers'), function() {

//    Route::resource('PackageProduct', 'PackageProductController');

        Route::get('/indexProduct/',			                    ['uses' => 'PackageProductController@index', 'as'=>'products-all']);
//        Route::get('/index/resellerProduct',			            ['uses' => 'PackageProductController@indexReseller', 'as'=>'product-reseller']);

        Route::get('/GetAllProducts',                                ['uses' => 'PackageProductController@AllProducts', 'as'=>'all-products']);
        Route::Get('/searchProduct',                                 ['uses' => 'PackageProductController@SearchProducts',    'as' => 'search-products']);
//        Route::get('/GetAllResellerProduct',                                ['uses' => 'PackageProductController@AllReseller', 'as'=>'all-product-reseller']);
//        Route::get('/subname',                                      ['uses' => 'PackageProductController@SubName', 'as'=>'sub-name']);

        Route::get('PackageProduct/add',			                ['uses' => 'PackageProductController@ProductInput', 'as'=>'product-input']);
        Route::get('prodcut/view',			                        ['uses' => 'PackageProductController@ViewProduct', 'as'=>'product-view']);
        Route::get('product/edit',                                  ['uses' => 'PackageProductController@editProduct', 'as'=>'product-edit']);

        Route::post('product/insertProduct',                        ['uses' => 'PackageProductController@inputProduct', 'as'=>'input-product']);
        Route::post('product/publishProduct',                        ['uses' => 'PackageProductController@publishProduct', 'as'=>'publish-product']);
        Route::post('product/unpublishProduct',                        ['uses' => 'PackageProductController@unpublishProduct', 'as'=>'unpublish-product']);
        Route::post('product/deleteProduct',                        ['uses' => 'PackageProductController@deleteProduct', 'as'=>'delete-product']);


        Route::get('/index/inputPaket',			                    ['uses' => 'PackageProductController@indexAssignPaket', 'as'=>'assign-paket-home']);
        Route::get('/index/allAssignee',                            ['uses' => 'PackageProductController@allAssignee', 'as'=>'all-assigned-paket']);
        Route::get('/PackageProduct/assign',                        ['uses' => 'PackageProductController@assignProductView', 'as' =>'assign-paket']);
        Route::get('/PackageProduct/input',                        ['uses' => 'PackageProductController@assignProductView', 'as' =>'input-paket']);

        Route::post('/PackageProduct/post',                         ['uses' => 'PackageProductController@assignPaket', 'as' =>'post-assigned-paket']);
        Route::post('/PackageProduct/delete',                       ['uses' => 'PackageProductController@deleteAssignment', 'as' =>'delete-assignment']);


        Route::get('/PackageProduct/getAllTravel',                  ['uses' => 'PackageProductController@getAllTravel', 'as' => 'get-all-travel']);
        Route::get('/PackageProduct/getAllowedTravel',              ['uses' => 'PackageProductController@getAllowedTravel', 'as' => 'get-allowed-travel']);
        Route::get('/PackageProduct/getAllPaket',                   ['uses' => 'PackageProductController@getAllPaket', 'as' => 'get-all-paket']);
        Route::get('/PackageProduct/getAllPaket/edit',              ['uses' => 'PackageProductController@getAllPaketForEdit', 'as' => 'get-all-paket-edit']);

});	
