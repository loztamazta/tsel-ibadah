<?php

Route::group(array('module' => 'PackageProduct', 'middleware' => ['api'], 'namespace' => 'App\Modules\PackageProduct\Controllers'), function() {

    Route::resource('PackageProduct', 'PackageProductController');
    
});	
