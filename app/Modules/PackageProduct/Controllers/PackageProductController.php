<?php namespace App\Modules\PackageProduct\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Input;
use DB;
use Image;
use Storage;
use Excel;

class PackageProductController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("PackageProduct::index");
	}

//    public function indexReseller()
//    {
//        return view("PackageProduct::indexReseller");
//    }

    public function indexAssignPaket()
    {
        return view("PackageProduct::PackageAssigner.assignPaket");
    }

	public function AllProducts()
    {
        $products= DB::select('SELECT *, IF(db_ibadah.product.ProductType = 1, \'Reguler\',\'Reseller\') as tipe, IF(db_ibadah.product.is_enabled = 1, \'Aktif\',\'Tidak Aktif\') as status
                                FROM db_ibadah.product
                                WHERE IsFlag = 0');

        return $products;
    }

    function SearchProducts()
    {
        $keywordProduct= $_GET['TextId'];
        $GetFilteredProduct= DB::select('SELECT *, IF(db_ibadah.product.is_enabled = 1, \'Ya\',\'Tidak\') as status 
                                      FROM db_ibadah.product
									  WHERE IsFlag = 0 AND name LIKE "%'.$keywordProduct.'%"');
        return $GetFilteredProduct;
    }


    public function AllReseller()
    {
        $resellers = DB::select('SELECT * FROM db_ibadah.package_product
                                 WHERE seller_type = 2 AND IsFlag = 0');

        return $resellers;
    }

    public function ProductInput()
    {
        return view("PackageProduct::input");
    }

    public function ViewProduct()
    {
        return view("PackageProduct::view");
    }

    public function editProduct()
    {
        $idProduct= $_GET['idProduct'];
        $EditProduct= DB::select('SELECT *, IF(db_ibadah.product.ProductType = 1, \'Reguler\',\'Reseller\') as tipe
                                  FROM db_ibadah.product
                                  WHERE id = ?',[$idProduct]);

        return $EditProduct;

    }

    function publishProduct()
    {

        DB::table('db_ibadah.product')->where('id', Input::get('id'))->update([
            'is_enabled' => 1,
        ]);
        return json_encode(Input::all());

    }

    function unpublishProduct()
    {

        DB::table('db_ibadah.product')->where('id', Input::get('id'))->update([
            'is_enabled' => 0,
        ]);
        return json_encode(Input::all());

    }

    function deleteProduct()
    {

        DB::table('db_ibadah.product')->where('id', Input::get('id'))->update([
            'IsFlag' => 1,
        ]);
        return json_encode(Input::all());

    }

    public function inputProduct()
    {
        if(Input::get('a') == 'SaveProduct'){
            DB::table('db_ibadah.product')->insert([
                'name' => Input::get('product_name'),
                'Price' => Input::get('price'),
                'ProductType' => Input::get('seller_type'),
                'KeyWord' => Input::get('KeyWord'),
                'SubName' => Input::get('SubName'),
                'Duration' => Input::get('Duration'),
                'DataRoaming' => Input::get('DataRoaming'),
                'KeyValue' => Input::get('KeyValue'),
                'SMSRoaming' => Input::get('SMSRoaming'),
                'IndonesiaCallDuration' => Input::get('IndonesiaCallDuration'),
                'receiveCall' => Input::get('receiveCall'),
                'is_enabled' => 0,

            ]);
            return json_encode(Input::all());
        }else{
            DB::table('db_ibadah.product')->where('id', Input::get('id'))->update([
                'name' => Input::get('product_name'),
                'Price' => Input::get('price'),
                'ProductType' => Input::get('seller_type'),
                'KeyWord' => Input::get('KeyWord'),
                'SubName' => Input::get('SubName'),
                'Duration' => Input::get('Duration'),
                'DataRoaming' => Input::get('DataRoaming'),
                'KeyValue' => Input::get('KeyValue'),
                'SMSRoaming' => Input::get('SMSRoaming'),
                'IndonesiaCallDuration' => Input::get('IndonesiaCallDuration'),
                'receiveCall' => Input::get('receiveCall'),
                'is_enabled' => 0,

            ]);
            return json_encode(Input::all());
        }
    }

    // Package Assigner

    public function getAllTravel()
    {
        $travels = DB::select('SELECT * FROM db_ibadah.mt_kbih WHERE IsFlag = 0');

        return $travels;
    }

    public function getAllowedTravel()
    {
        $travels = DB::select('SELECT * FROM db_ibadah.mt_kbih AS A WHERE NOT EXISTS(SELECT id_travel FROM db_ibadah.paket_assigner AS B WHERE B.id_travel = A.IdKbih) AND IsFlag = 0');

        return $travels;
    }

    public function getAllPaket()
    {
        $pakets = DB::select('SELECT *, IF(db_ibadah.product.ProductType = 1, \'Reguler\',\'Reseller\') as tipe
                              FROM db_ibadah.product WHERE IsFlag = 0 AND is_enabled = 1');

        return $pakets;
    }

    public function getAllPaketForEdit()
    {
        $idTravel = $_GET['idTravel'];
        $pakets = DB::select('select 
                                *,
                                IF(a.ProductType = 1, \'Reguler\',\'Reseller\') as tipe,
                                a.id as paketId,
                                coalesce(c.IdKbih,0,1) checked
                                from 
                                    db_ibadah.product a
                                    left join (select * from db_ibadah.paket_assigner where id_travel= "'.$idTravel.'") b
                                        on a.id = b.id_paket
                                    left join  db_ibadah.mt_kbih c
                                        on b.id_travel = c.IdKbih
                                where 
                                    a.is_enabled=1 and a.isflag=0
                                    and a.ProductType = 2
                            ');

        return $pakets;

    }

    public function assignProductView()
    {
        return view("PackageProduct::PackageAssigner.input2");
    }

    public function allAssignee()
    {
        $Assignees = DB::select('SELECT A.id as assigner_id, B.id as paket_id, B.name as paket_name, C.IdKbih as id_travel, C.KbihName as travel_name, COUNT(A.id_travel) as jumlah_paket
                                    FROM db_ibadah.paket_assigner AS A
                                    JOIN db_ibadah.product AS B ON A.id_paket = B.id
                                    JOIN db_ibadah.mt_kbih AS C ON A.id_travel = C.IdKbih
                                    WHERE B.IsFlag = 0 AND C.IsFlag = 0
                                    GROUP BY A.id_travel');

        return $Assignees;
    }

    public function assignPaket()
    {
        if(Input::get('a') == 'SavePaket')
        {

            $pakets = Input::get('Paket');

            DB::beginTransaction();

            foreach ($pakets as $paketCin)
            {
                DB::table('db_ibadah.paket_assigner')->insert([
                    'id_travel' => Input::get('TravelId'),
                    'id_paket' => $paketCin,
                ]);
            }

            DB::commit();

            return json_encode($pakets);
        }
        else
        {
            DB::table('db_ibadah.paket_assigner')->where('id_travel',Input::get('TravelId'))->delete();

            $pakets = Input::get('Paket');

            DB::beginTransaction();

            foreach ($pakets as $paketCin)
            {
                DB::table('db_ibadah.paket_assigner')->insert([
                    'id_travel' => Input::get('TravelId'),
                    'id_paket' => $paketCin,
                ]);
            }

            DB::commit();

            return json_encode($pakets);
        }

    }

    public function unasignOnePaket()
    {

    }

    public function deleteAssignment()
    {
        $IdTravel = Input::get('id');

        DB::table('db_ibadah.paket_assigner')->where('id_travel',Input::get('id'))->delete();

        return json_encode(Input::all());
    }



}
