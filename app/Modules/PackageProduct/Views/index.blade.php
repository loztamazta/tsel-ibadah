@extends('Dts::layouts.dts')

@section('title','Produk')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-info rpt input-product">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
                Tambah Produk
            </button>
        </div>
        {{--<div class="col-md-3 pull-right">--}}
            {{--<div class="input-group">--}}
                {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">--}}
                        {{--Cari berdasarkan nama produk--}}
                    {{--</label>--}}
                    {{--<input id="searchProduct" name="firstname" type="text" class="form-control">--}}
                {{--</div>--}}
                {{--<a href="javascript:void(0)" id="productSearch" class="input-group-addon">--}}
                    {{--<i class="material-icons">search</i>--}}
                {{--</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title"></h4>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center">Nama</th>
                                        <th class="text-center">Tipe</th>
                                        {{--<th class="text-center">Keyword</th>--}}
                                        {{--<th class="text-center">Sub-Nama</th>--}}
                                        <th class="text-center">Durasi</th>
                                        <th class="text-center">Data Roaming</th>
                                        <th class="text-center">Harga</th>
                                        <th class="text-center">SMS Roaming</th>
                                        <th class="text-center">Durasi Telfon</th>
                                        <th class="text-center">Durasi Terima</th>
                                        <th class="text-center">Status Produk</th>
                                        <th class="disabled-sorting text-right">Tindakan</th>
                                    </tr>
                                    </thead>
                                    {{--<tfoot>--}}
                                    {{--<tr>--}}
                                    {{--<th>Name</th>--}}
                                    {{--<th>Position</th>--}}
                                    {{--<th>Office</th>--}}
                                    {{--<th>Age</th>--}}
                                    {{--<th>Start date</th>--}}
                                    {{--<th class="text-right">Actions</th>--}}
                                    {{--</tr>--}}
                                    {{--</tfoot>--}}
                                    <tbody id="ListProducts">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
    <script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.datatables.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('vendor/additional-methods.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(function() {

            $("#productSearch").on('click', function () {
                var searchid = $("#searchProduct").val();
                GetSearchProduct(searchid)
                return false;
            });
            $('#searchProduct').bind("enterKey",function(e){
                var searchid = $("#searchProduct").val();
                GetSearchProduct(searchid)

            });
            $('#searchProduct').keyup(function(e){
                if(e.keyCode == 13)
                {
                    $(this).trigger("enterKey");
                }
                if (!this.value) {
                    GetListProduct()
                }
            });
        });

        GetListProduct()

        function GetListProduct()
        {
            console.log('Start')
            $.ajax({
                type: 'GET',
                url: '{{route('all-products')}}',
                global:false,
                success: function (data) {
                    var $AllProducts= $('#ListProducts');
                    $AllProducts.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllProducts.append('' +
                            '<tr>'+
                            '<td align="center">'+data[i].name+'</td>'+
                            '<td align="center">'+data[i].tipe+'</td>'+
//                            '<td align="center">'+data[i].KeyWord+'</td>'+
//                            '<td align="center">'+data[i].SubName+'</td>'+
                            '<td align="center">'+data[i].Duration+'</td>'+
                            '<td align="center">'+data[i].DataRoaming+'</td>'+
                            '<td align="center">'+data[i].Price+'</td>'+
                            '<td align="center">'+data[i].SMSRoaming+'</td>'+
                            '<td align="center">'+data[i].IndonesiaCallDuration+'</td>'+
                            '<td align="center">'+data[i].receiveCall+'</td>'+
                            '<td align="center">'+data[i].status+'</td>'+
                            '<td class="text-right">'+
                            '<a href="#" class="btn btn-simple btn-info btn-icon like view-product" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                            '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-product" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                            '<a href="#" class="btn btn-simple btn-danger btn-icon remove delete-product" data-id="'+data[i].id+'" data-enabled="'+data[i].is_enabled+'" ><i class="material-icons">close</i></a>'+
                            '</td>'+
                            '</tr>')
                    }
                    $('#datatables').DataTable({
                        pageLength: 10,
                        bLengthChange: false,
                        responsive: true,
                        language: {
                            search: "_INPUT_",
                            searchPlaceholder: "Cari data",
                        },
                    })
                }
            });

        }

        function GetSearchProduct(searchid)
        {
            $.ajax({
                type: 'GET',
                url: '{{url('/searchProduct')}}',
                data: {TextId:searchid},
                global:false,
                success: function (data) {
                    var $AllProducts = $('#ListProducts');
                    $AllProducts.empty();
                    if(data == ''){
                        $AllProducts.append('<div class="col-md-12">' +'<h2>Data tidak ada</h2>'+'</div>');
                    }else{
                        for (var i = 0; i < data.length; i++) {
                            $AllProducts.append('' +
                                '<tr>'+
                                '<td align="center">'+data[i].name+'</td>'+
                                '<td align="center">'+data[i].tipe+'</td>'+
//                                '<td align="center">'+data[i].KeyWord+'</td>'+
//                                '<td align="center">'+data[i].SubName+'</td>'+
                                '<td align="center">'+data[i].Duration+'</td>'+
                                '<td align="center">'+data[i].DataRoaming+'</td>'+
                                '<td align="center">'+data[i].Price+'</td>'+
                                '<td align="center">'+data[i].SMSRoaming+'</td>'+
                                '<td align="center">'+data[i].IndonesiaCallDuration+'</td>'+
                                '<td align="center">'+data[i].receiveCall+'</td>'+
                                '<td align="center">'+data[i].status+'</td>'+
                                '<td class="text-right">'+
                                '<a href="#" class="btn btn-simple btn-info btn-icon like view-product" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                                '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-product" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                                '<a href="#" class="btn btn-simple btn-danger btn-icon remove delete-product" data-id="'+data[i].id+'" data-enabled="'+data[i].is_enabled+'" ><i class="material-icons">close</i></a>'+
                                '</td>'+
                                '</tr>')
                        }
                    }
                }
            });
        }

        $('#rp').slideReveal({
            // trigger: $(".rpt"),
            position: "right",
            push: false,
            overlay: true,
            zIndex:1300,
        });
        $('#rp').delegate('.rp-close','click',function() {
            $('#rp').slideReveal("hide")
        })

        $(document).delegate('.input-product', 'click', function(e) {
            if($(this).attr('title') == 'Edit'){
                $('.loading').show()
                $.ajax({
                    type: 'GET',
                    url: '{{route('product-edit')}}',
                    data:{idProduct:$(this).data('id')},
                    global:false,
                    success: function (data) {
                        $('#rp').empty().load("{{ route('product-input') }}", function(){
//                            CKEDITOR.replace('editor');
                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()

//                            $('#SelectKota').on('change', function() {
//                                $('#selectRegion option[value='+data['0'].regionId+']').attr('selected','selected');
//                            });
                            var scr = 'storage/'+data['0'].Img;
                            $('.titleForm').empty().append('Ubah Produk')
                            $("input[name='save_type']").val('EditSave')
                            $("input[name='id']").val(data['0'].id)
                            $("input[name='product_name']").val(data['0'].name)
                            $("input[name='price']").val(data['0'].Price)
                            $("select[name='ProductType']").val(data['0'].ProductType).attr('selected', true).change();
                            $("input[name='keyword']").val(data['0'].KeyWord)
                            $("input[name='subname']").val(data['0'].SubName)
                            $("input[name='duration']").val(data['0'].Duration)
                            $("input[name='dataRoaming']").val(data['0'].DataRoaming)
                            $("input[name='keyValue']").val(data['0'].KeyValue)
                            $("input[name='sms']").val(data['0'].SMSRoaming)
                            $("input[name='indoDuration']").val(data['0'].IndonesiaCallDuration)
                            $("input[name='receiveCall']").val(data['0'].receiveCall)
//                            $("textarea.ckeditor").val(data['0'].description);
                            $('#rp').slideReveal("show")
                        })
                    }
                });

            }else{
                $('.loading').show()
                $('#rp').empty().load("{{ route('product-input') }}", function(){
//                    CKEDITOR.replace('editor');
                    $('.card-content-overflow').perfectScrollbar();
                    $('.loading').hide()
                    $('#rp').slideReveal("show")
                })
            }

        })

        $(document).delegate('.view-product', 'click', function(e) {
            // console.log('klik')
            $('.loading').show()
            $.ajax({
                type: 'GET',
                url: '{{route('product-edit')}}',
                data:{idProduct:$(this).data('id')},
                global:false,
                success: function (data) {
                    console.log('jalan')
                    $('#c-modal').empty().load("{{ route('product-view') }}", function(){

                        $('.loading').hide()
//                        var scr = 'storage/'+data['0'].Img;
                        $("input[name='save_type']").val('EditSave')
                        $("input[name='id']").val(data['0'].id)
                        $("input[name='product_name']").val(data['0'].name)
                        $("input[name='price']").val(data['0'].Price)
                        $("input[name='tipe']").val(data['0'].tipe)
                        $("input[name='keyword']").val(data['0'].KeyWord)
                        $("input[name='subname']").val(data['0'].SubName)
                        $("input[name='duration']").val(data['0'].Duration)
                        $("input[name='dataRoaming']").val(data['0'].DataRoaming)
                        $("input[name='keyValue']").val(data['0'].KeyValue)
                        $("input[name='sms']").val(data['0'].SMSRoaming)
                        $("input[name='indoDuration']").val(data['0'].IndonesiaCallDuration)
                        $("input[name='receiveCall']").val(data['0'].receiveCall)


                        if($("input[name='is_enabled']").val() == 0)
                        {
                            $('.publish').css('display','block')
                            $('.unpublish').css('display','none')
                        }
                        else
                        {
                            $('.publish').css('display','none')
                            $('.unpublish').css('display','block')
                        }

                        if (data['0'].IndonesiaCallDuration==0) {
                            $("input[name='indoDuration']").parents('.row').hide()
                        }
                        if (data['0'].receiveCall==0) {
                            $("input[name='receiveCall']").parents('.row').hide()
                        }
                        if (data['0'].SMSRoaming==0) {
                            $("input[name='sms']").parents('.row').hide()
                        }

                        $('#modal').modal('show')

                        $('#modal').delegate('.modal-close','click',function() {
                            $('#modal').modal("hide")
                        })

                    })
                }
            });
        });

        $(document).delegate('.publish', 'click', function() {
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    id: $("input[name='id").val(),
                }
                $.post("{{route('publish-product') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            });
        })

        $(document).delegate('.unpublish', 'click', function() {
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    id: $("input[name='id").val(),
                }
                $.post("{{route('unpublish-product') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            });
        });

        $(document).delegate('.delete-product', 'click', function() {
            var ids = $(this).data('id')
            var published = $(this).data('enabled')

            if (published == 0) {
                swal({
                    title: 'Apakah Anda Yakin?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Batal'
                }).then(function () {
                    // debugger;
                    swal('Mohon Menunggu')
                    swal.showLoading()
                    data = {
                        id: ids
                    }
                    $.post("{{route('delete-product') }}", data, function (r) {
                        swal(
                            "Berhasil",
                            "",
                            "success"
                        ).then(function () {
                            location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                        })
                    }, 'json');
                });
            }
            else {
                swal({
                    title: 'Data masih digunakan',
                    type: 'error',
                    showCancelButton: false,
                    confirmButtonText: 'Ok',
                }).then(function () {
                    location.reload();
                })
            }

        });

        $(document).delegate('#saveProductData', 'click', function() {
            var $validator = $('.card-content-overflow form').validate({
                rules: {
                    product_name: {
                        required: true,
                        contentSpecialCharacters: true,

                    },
                    price: {
                        required: true,
                        contentSpecialCharacters: true,
                    },
                    sellerType: {
                        required: true,
                    },
                    subname: {
                        required: true,
                    },
                    duration: {
                        required: true,
                        contentSpecialCharacters: true,
                    },
                    indoDuration: {
                        required: true,
                        contentSpecialCharacters: true,
                    },
                    receiveCall: {
                        required: true,
                        contentSpecialCharacters: true,
                    },
                    sms: {
                        required: true,
                        contentSpecialCharacters: true,
                    },
                    dataRoaming: {
                        required: true,
                        contentSpecialCharacters: true,
                    },

                },

                errorPlacement: function(error, element) {
                    $(element).parent('div').addClass('has-error');
                }
            });
            var $valid = $('.card-content-overflow form').valid();
            if(!$valid) {
                $validator.focusInvalid();
//                $('.alert-danger').show();
//                setTimeout(function() {
//                    $(".alert-danger").slideUp(500);
//                }, 2000);
                return false;
            }
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    a: $("input[name='save_type").val(),
                    id: $("input[name='id").val(),
                    product_name: $("input[name='product_name").val(),
                    price: $("input[name='price").val(),
                    seller_type: $('#sellerType').find(":selected").val(),
                    KeyWord: $("input[name='keyword").val(),
                    SubName: $("input[name='subname").val(),
                    Duration: $("input[name='duration").val(),
                    DataRoaming: $("input[name='dataRoaming").val(),
                    KeyValue: $("input[name='keyValue").val(),
                    SMSRoaming: $("input[name='sms").val(),
                    IndonesiaCallDuration: $("input[name='indoDuration").val(),
                    receiveCall: $("input[name='receiveCall").val(),

//                description:CKEDITOR.instances['editor'].getData(),
                }
                $.post("{{route('input-product') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            })
        });



    </script>

@endsection