<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
				<i class="material-icons">close</i>
			</span>
    <div class="card-content">

        <h4 class="card-title titleForm">Produk Baru</h4>
        <div class="card-content-overflow">
            {{--<div class="fileinput fileinput-new text-center" data-provides="fileinput">--}}
            {{--<div class="fileinput-new thumbnail">--}}
            {{--<img src="{{ asset('images/image_placeholder.jpg') }}" alt="...">--}}
            {{--</div>--}}
            {{--<div class="fileinput-preview fileinput-exists thumbnail"></div>--}}
            {{--<div>--}}
            {{--<span class="btn btn-rose btn-round btn-file">--}}
            {{--<span class="fileinput-new">Select image</span>--}}
            {{--<span class="fileinput-exists">Change</span>--}}
            {{--<input type="file" name="_______" />--}}
            {{--</span>--}}
            {{--<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>--}}
            {{--</div>--}}
            {{--</div>--}}
            <form id="FormProduk">
                <div class="form-group label-floating">
                    <label class="control-label">Nama Produk <small>(wajib diisi)</small></label>
                    <input type="text" class="form-control" name="product_name" required>
                    <input type="hidden" value="SaveProduct" name="save_type">
                    <input type="hidden" name="id">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Harga <small>(wajib diisi)</small></label>
                    <input type="number" class="form-control" name="price" required>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Tipe Penjual <small>(wajib diisi)</small></label>
                        <select class="form-control" id="sellerType" required name="sellerType" required data-style="select-with-transition" title="Pilih Tipe..." data-size="7">
                            <option disabled>Pilih Tipe</option>
                            <option value="1">Reguler</option>
                            <option value="2">Reseller</option>
                </select>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Keyword</label>
                    <input type="text" class="form-control" name="keyword" required placeholder="Isi dengan tanda - bila tipe penjual adalah Reguler">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Sub-Nama <small>(wajib diisi)</small></label>
                    <input type="text" class="form-control" name="subname" required>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Masa Aktif <small>(wajib diisi)</small></label>
                    <input type="number" class="form-control" name="duration" required>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Internet Data Roaming <small>(wajib diisi)</small></label>
                    <input type="number" class="form-control" name="dataRoaming" placeholder="dalam MB, 1GB = 1000MB" required>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Kode Akses Langsung </label>
                    <input type="text" class="form-control" name="keyValue" placeholder="Contoh : *266*105#">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">SMS Roaming <small>(wajib diisi)</small></label>
                    <input type="number" class="form-control" name="sms" required>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Kuota Bicara (dalam menit) <small>(wajib diisi)</small></label>
                    <input type="number" class="form-control" name="indoDuration" required>
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Kuota Terima (dalam menit) <small>(wajib diisi)</small></label>
                    <input type="number" class="form-control" name="receiveCall" required>
                </div>
                {{--<div class="form-group label-floating">--}}
                    {{--<label class="control-label">Deskripsi Produk</label>--}}
                    {{--<textarea class="form-control ckeditor description" id="editor" placeholder="" rows="35" name="editor"></textarea>--}}
                {{--</div>--}}
            </form>

        </div>

    </div>

    <div class="card-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-fill btn-rose" id="saveProductData">Simpan</button>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>