<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="card ">
            <span class="btn card-header card-header-icon round pull-right modal-close" data-background-color="red">
				<i class="material-icons">close</i>
            </span>
            <div class="card-content">
                <h4 class="card-title text-left">
                    <input type="text" class="form-control" name="product_name" readonly>
                    <br>
                    <input type="hidden" name="id">
                    <input type="hidden" name="is_enabled">
                </h4>
                <div class="card-description">
                    <div class="row">
                        <div class="col-xs-2">
                            Tipe
                        </div>
                        <div class="col-xs-10  text-right" id="source" name="source">
                            <input type="text" class="form-control" name="tipe" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            Keyword
                        </div>
                        <div class="col-xs-10  text-right" id="source" name="source">
                            <input type="text" class="form-control" name="keyword" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            Sub-Nama
                        </div>
                        <div class="col-xs-10  text-right" id="source" name="source">
                            <input type="text" class="form-control" name="subname" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            Durasi
                        </div>
                        <div class="col-xs-10  text-right" id="source" name="source">
                            <input type="text" class="form-control" name="duration" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            Data Roaming
                        </div>
                        <div class="col-xs-10  text-right" id="source" name="source">
                            <input type="text" class="form-control" name="dataRoaming" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            Harga
                        </div>
                        <div class="col-xs-10  text-right" id="source" name="source">
                            <input type="text" class="form-control" name="price" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            SMS Roaming
                        </div>
                        <div class="col-xs-10  text-right" id="source" name="source">
                            <input type="text" class="form-control" name="sms" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            Durasi Telfon
                        </div>
                        <div class="col-xs-10  text-right" id="source" name="source">
                            <input type="text" class="form-control" name="indoDuration" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-2">
                            Durasi Terima
                        </div>
                        <div class="col-xs-10  text-right" id="source" name="source">
                            <input type="text" class="form-control" name="receiveCall" readonly>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="media-middle">
                            <button type="submit" class="btn btn-fill btn-rose publish">Aktifkan</button>
                        </div>
                        <div class="media-middle">
                            <button type="submit" class="btn btn-fill btn-rose unpublish">Non-Aktifkan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
