@extends('Dts::layouts.dts')

@section('title','Input Paket')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-info rpt assign-paket">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
                Masukkan Paket
            </button>
        </div>
        <div class="col-md-3 pull-right">
            <div class="input-group">
                <div class="form-group label-floating">
                    <label class="control-label">
                        Search...
                    </label>
                    <input name="firstname" type="text" class="form-control">
                </div>
                <span class="input-group-addon">
			        <i class="material-icons">search</i>
			    </span>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title"></h4>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Nama Travel</th>
                                        {{--<th>Sumber</th>--}}


                                        <th>Jumlah Paket</th>
                                        <th class="disabled-sorting text-right">Tindakan</th>
                                    </tr>
                                    </thead>
                                    {{--<tfoot>--}}
                                    {{--<tr>--}}
                                    {{--<th>Name</th>--}}
                                    {{--<th>Position</th>--}}
                                    {{--<th>Office</th>--}}
                                    {{--<th>Age</th>--}}
                                    {{--<th>Start date</th>--}}
                                    {{--<th class="text-right">Actions</th>--}}
                                    {{--</tr>--}}
                                    {{--</tfoot>--}}
                                    <tbody id="ListAssigned">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right slidePanel-large">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
    <link href="{{ asset('mdp/css/multi-select.css') }}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
    <script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
    <script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.multi-select.js') }}"></script>
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        GetListAssignedPackage()

        function GetListAssignedPackage()
        {
            console.log('Start')
            $.ajax({
                type: 'GET',
                url: '{{route('all-assigned-paket')}}',
                global:false,
                success: function (data) {
                    var $AssignedPackages= $('#ListAssigned');
                    $AssignedPackages.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AssignedPackages.append('' +
                            '<tr>'+
                            '<td>'+data[i].travel_name+'</td>'+

                            '<td>'+data[i].jumlah_paket+'</td>'+
                            '<td class="text-right">'+
//                            '<a href="#" class="btn btn-simple btn-info btn-icon like view-tips" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                            '<a href="#" class="btn btn-simple btn-warning btn-icon edit assign-paket"  title="Edit" data-id="'+data[i].id_travel+'" ><i class="material-icons">edit</i></a>'+
                            '<a href="#" class="btn btn-simple btn-danger btn-icon remove delete-paket" data-id="'+data[i].id_travel+'" ><i class="material-icons">close</i></a>'+
                            '</td>'+
                            '</tr>')
                    }
                }
            });
        }
        $('#rp').slideReveal({
            // trigger: $(".rpt"),
            position: "right",
            push: false,
            overlay: true,
            zIndex:1300,
        });
        $('#rp').delegate('.rp-close','click',function() {
            $('#rp').slideReveal("hide")
        })

        {{--$(document).delegate('.assign-paket', 'click', function(e) {--}}
            {{--if($(this).attr('title') == 'Edit'){--}}
                {{--$('.loading').show()--}}
                {{--$.ajax({--}}
                    {{--type: 'GET',--}}
                    {{--url: '{{route('product-edit')}}',--}}
                    {{--data:{IdAssigner:$(this).data('id')},--}}
                    {{--global:false,--}}
                    {{--success: function (data) {--}}
                        {{--$('#rp').empty().load("{{ route('assign-paket') }}", function(){--}}
                            {{--GetListTravel();--}}
                            {{--GetListPaket();--}}
                            {{--$('#callbacks').multiSelect({--}}
                                {{--afterSelect: function(values){--}}
                                    {{--alert("Select value: "+values);--}}
                                {{--},--}}
                                {{--afterDeselect: function(values){--}}
                                    {{--alert("Deselect value: "+values);--}}
                                {{--}--}}
                            {{--});--}}
                            {{--$('.card-content-overflow').perfectScrollbar();--}}
                            {{--$('.loading').hide()--}}

{{--//                            $('#SelectKota').on('change', function() {--}}
{{--//                                $('#selectRegion option[value='+data['0'].regionId+']').attr('selected','selected');--}}
{{--//                            });--}}
                            {{--var scr = 'storage/'+data['0'].Img;--}}
                            {{--$("input[name='save_type']").val('EditSave')--}}
                            {{--$("input[name='id']").val(data['0'].id)--}}
                            {{--$("input[name='product_name']").val(data['0'].product_name)--}}
                            {{--$("input[name='price']").val(data['0'].price)--}}
                            {{--$("select[name='sellerType']").val(data['0'].seller_type).attr('selected', true).change();--}}

                            {{--$('#rp').slideReveal("show")--}}
                        {{--})--}}
                    {{--}--}}
                {{--});--}}

            {{--}else{--}}
                {{--$('.loading').show()--}}
                {{--$('#rp').empty().load("{{ route('assign-paket') }}", function(){--}}
                    {{--GetListTravel();--}}
                    {{--GetListPaket();--}}
                    {{--$('.card-content-overflow').perfectScrollbar();--}}
                    {{--$('.loading').hide()--}}
                    {{--$('#rp').slideReveal("show")--}}
                {{--})--}}
            {{--}--}}

        {{--})--}}

        $(document).delegate('.assign-paket', 'click', function(e) {
            if($(this).attr('title') == 'Edit'){
                var x = $(this).data('id');
                $('.loading').show()
                $.ajax({
                    type: 'GET',
                    url: '{{route('get-all-paket-edit')}}',
                    data:{idTravel:$(this).data('id')},
                    global:false,
                    success: function (data) {
                        console.log(data)
                        $('#rp').empty().load("{{ route('assign-paket') }}", function () {

                            GetListTravel(x);
                            GetListPaketEdit(x);
                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()

//                            var scr = 'storage/'+data['0'].Img;
                            $("input[name='save_type']").val('EditSave')
                            // $("input[name='idTravel']").val(data['0'].id_travel)
//                            $("input[name='product_name']").val(data['0'].product_name)
//                            $("input[name='price']").val(data['0'].price)
                            // $("select[name='id_travel']").val(data['0'].id_travel).attr({selected:'selected', disabled:'disabled'}).change();


                            $('#rp').slideReveal("show")
                        })
                    }
                })

            }else{
                $.get("{{ route('get-allowed-travel') }}", function(tottrv){
                    console.log(tottrv)
                    if (tottrv.length!=0) {
                        $('.loading').show()
                        $('#rp').empty().load("{{ route('assign-paket') }}", function(){
                            GetListAllowedTravel();
                            GetListPaket();
                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()
                            $('#rp').slideReveal("show")
                        })
                    } else {
                        swal(
                            "",
                            "Tidak ada travel yang tersedia",
                            "error"
                        )
                    }
                })

            }

        });

        function GetListTravel(x)
        {

            $.ajax({
                type: 'GET',
                url: '{{route('get-all-travel')}}',
                global:false,
                success: function (data) {
                    var $AllTravels= $('#id_travel');
                    $AllTravels.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllTravels.append('' +
                            '<option value="'+data[i].IdKbih+'">'+data[i].KbihName+'</option>')
                    }
// debugger
                    if (x) {
                        $("select[name='id_travel']").val(x).attr({selected:'selected', disabled:'disabled'}).change();
                    }
                }
            });
        }

        function GetListAllowedTravel()
        {
            $.ajax({
                type: 'GET',
                url: '{{route('get-allowed-travel')}}',
                global:false,
                success: function (data) {
                    var $AllTravels= $('#id_travel');
                    $AllTravels.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllTravels.append('' +
                            '<option value="'+data[i].IdKbih+'">'+data[i].KbihName+'</option>')
                    }
                }
            });
        }

        function GetListPaket()
        {
            $.ajax({
                type: 'GET',
                url: '{{route('get-all-paket')}}',
                global:false,
                success: function (data) {
                    var $AllPaket= $('#ListAvailPaket');
                    $AllPaket.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllPaket.append('' +
                            '<tr>'+
                            '<td align="center"><input type="checkbox" name="productId[]" id="productId[]" '+(data[i].checked > 0 ? 'checked':'')+' data-paketid = "'+data[i].id+'" /></td>'+
                            '<td align="center">'+data[i].name+'</td>'+
                            '<td align="center">'+data[i].tipe+'</td>'+
                            '<td align="center">'+data[i].KeyWord+'</td>'+
                            '<td align="center">'+data[i].SubName+'</td>'+
                            '<td align="center">'+data[i].Duration+'</td>'+
                            '<td align="center">'+data[i].DataRoaming+'</td>'+
                            '<td align="center">'+data[i].Price+'</td>'+
                            '<td align="center">'+data[i].SMSRoaming+'</td>'+
                            '<td align="center">'+data[i].IndonesiaCallDuration+'</td>'+
                            '<td align="center">'+data[i].receiveCall+'</td>'+
                            '</tr>')
                    }
                    $('#id_paket').multiSelect({});
                }
            });
        }

        function GetListPaketEdit(x)
        {
            $.ajax({
                type: 'GET',
                url: '{{route('get-all-paket-edit')}}',
                data:{idTravel:x},
                global:false,
                success: function (data) {
                    var $AllPaket= $('#ListAvailPaket');
                    $AllPaket.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllPaket.append('' +
                            '<tr>'+
                            '<td align="center"><input class ="xyz" type="checkbox" name="productId[]" id="productId[]" data-paketid = "'+data[i].paketId+'" '+(data[i].checked > 0 ? 'checked':'')+' /></td>'+
                            '<td align="center">'+data[i].name+'</td>'+
                            '<td align="center">'+data[i].tipe+'</td>'+
                            '<td align="center">'+data[i].KeyWord+'</td>'+
                            '<td align="center">'+data[i].SubName+'</td>'+
                            '<td align="center">'+data[i].Duration+'</td>'+
                            '<td align="center">'+data[i].DataRoaming+'</td>'+
                            '<td align="center">'+data[i].Price+'</td>'+
                            '<td align="center">'+data[i].SMSRoaming+'</td>'+
                            '<td align="center">'+data[i].IndonesiaCallDuration+'</td>'+
                            '<td align="center">'+data[i].receiveCall+'</td>'+
                            '</tr>')
                    }
                }
            });
        }


        $(document).delegate(".AddPaket","click", function (e) {
            e.preventDefault();
            var IdAssigner= $(this).data('id')
            var iCntTxt = 1;
            var container = $(document.getElementById('assigner'));
            iCntTxt = iCntTxt + 1;
            $(container).append('<div class="form-group label-floating" data-id="'+iCntTxt+'">'+
                '                   <label class="control-label">Pilih Paket'+
                '                       <small>(wajib diisi)</small>'+
                '                   </label>'+
                '                   <select class="form-control idPaket Paket" id="id_paket'+iCntTxt+'" name="id_paket[]" data-style="select-with-transition" title="Single Select" data-size="7">'+
                '                   </select>'+
                '               </div>');
            GetListPaket();
        });

        $(document).delegate('.delete-paket', 'click', function() {

            var xx = $(this).data('id');
            swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function () {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()
                data = {
                    id: xx
                }
                $.post("{{route('delete-assignment') }}", data, function (r) {
                    swal(
                        "Berhasil",
                        "",
                        "success"
                    ).then(function () {
                        location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                    })
                }, 'json');
            });
        });

        $(document).delegate('#SaveTravelData', 'click', function(e) {
            e.preventDefault();
            var $validator = $('.card-content-overflow form').validate({
                rules: {
                    TravelName: {
                        required: true,
                        letterswithbasicpunc: true
                    },
                    Owner: {
                        required: true,
                        letterswithbasicpunc: true
                    },
                    NoPilot: {
                        required: true,
                        phonetsel: true
                    },
                    Address: {
                        required: true,
                        letterswithbasicpunc: true
                    },
                    NoTelp: {
                        required: true,
                        telpindo: true,
                    },
                    NoFax: {
                        required: true,
                        telpindo: true,
                    },
                    Email: {
                        required: true,
                        email2: true
                    },
                    Siup: {
                        required: true,
                        alphanumeric: true
                    },


                },
                messages: {
                    TravelName: "Wajib diisi",
                    Owner: "Karakter dan Angka tanpa Simbol/Spesial Karakter",
                    NoPilot: "Harus Nomor Telkomsel ",
                    Address: "Karakter dan Angka tanpa Simbol/Spesial Karakter",
                    NoTelp: "Harus Nomor Telepon Indonesia",
                    NoFax: "Harus Nomor Telepon Indonesia",
                    Siup: "Wajib diisi",
                    Email: "Wajib diisi dengan Email yang benar",

                },
                errorPlacement: function(error, element) {
                    $(element).parent('div').addClass('has-error');
                    $(element).parent('div').append(error);

                }

            });
            var $valid = $('.card-content-overflow form').valid();
            if(!$valid) {
                $validator.focusInvalid();
                return false;
            }
            swal({
                title: 'Anda Yakin ?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
            }).then(function() {
                // debugger;
                swal('Mohon Menunggu')
                swal.showLoading()

                savedata()
            })
        });

        $(document).delegate('#assignDataPaket', 'click', function(e) {
            e.preventDefault();

            if($(":checkbox:checked").length > 0){

                var val = [];
                $(':checkbox:checked').each(function(i){
                    val[i] = $(this).data('paketid');
                });

                swal({
                    title: 'Apakah anda yakin?',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Ya!',
                    cancelButtonText: 'Tidak!'
                }).then(function() {
                    var data={
                        a:$("input[name='save_type']").val(),
//                    TravelId:$("input[name='idTravel']").val(),
                        TravelId:$('#id_travel').find(":selected").val(),
                        Paket:val,
                    };
                    $.post("{{route('post-assigned-paket') }}", data,function(r) {
                        swal(
                            'Berhasil',
                            '',
                            'success'
                        ).then(function() {
                            $('#rp').empty().slideReveal("hide")
                            GetListAssignedPackage()
                        })
                    }, 'json');
                });

            }else {

                sweetAlert("Oops...", "Mohon memilih setidaknya 1 paket!", "error");

            }

        });



    </script>

@endsection