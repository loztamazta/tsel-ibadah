<div class="card">
    <div class="card-header card-header-icon" data-background-color="blue">
        <i class="material-icons">mode_edit</i>
    </div>
    <span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
        <i class="material-icons">close</i>
    </span>
    <div class="card-content">
        <h4 class="card-title">Assign Paket</h4>
        <div class="card-content-overflow">
            <form id="FormAssigner">
                <input type="hidden" name="idTravel">
                <input type="hidden" name="save_type" value="SavePaket">
                <div class="form-group label-floating">
                    <input type="hidden" name="id">
                    <input type="hidden" value="0" name="is_enabled">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Pilih Travel
                        <small>(wajib diisi)</small>
                    </label>
                    <select class="form-control" id="id_travel" name="id_travel" data-style="select-with-transition" title="Single Select" data-size="7">

                    </select>

                </div>

                {{--<div class="row btnadd">--}}
                {{--<div class="col-xs-2 pull-right">--}}
                {{--<button class="btn btn-danger btn-simple pl0 pr0 AddPaket" data-id="'+i+'" >--}}
                {{--<i class="material-icons">add</i>Tambah Paket--}}
                {{--<div class="ripple-container"></div>--}}
                {{--</button>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="form-group label-floating">--}}
                {{--<label class="control-label">Pilih Paket--}}
                {{--<small>(wajib diisi)</small>--}}
                {{--</label>--}}
                {{--<select class="form-control Paket idPaket" id="id_paket[]" name="id_paket[]" data-style="select-with-transition" title="Single Select" data-size="7">--}}

                {{--</select>--}}
                {{--</div>--}}
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-header card-header-icon" data-background-color="purple">
                                        <i class="material-icons">assignment</i>
                                    </div>
                                    <div class="card-content">
                                        <h4 class="card-title">Daftar Produk</h4>
                                        <div class="toolbar">
                                            <!--        Here you can write extra buttons/actions for the toolbar              -->
                                        </div>
                                        <div class="material-datatables">
                                            <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th></th>
                                                    <th class="text-center">Nama</th>
                                                    <th class="text-center">Tipe</th>
                                                    <th class="text-center">Keyword</th>
                                                    <th class="text-center">Sub-Nama</th>
                                                    <th class="text-center">Durasi</th>
                                                    <th class="text-center">Data Roaming</th>
                                                    <th class="text-center">Harga</th>
                                                    <th class="text-center">SMS Roaming</th>
                                                    <th class="text-center">Durasi Telfon</th>
                                                    <th class="text-center">Durasi Terima</th>
                                                </tr>
                                                </thead>
                                                {{--<tfoot>--}}
                                                {{--<tr>--}}
                                                {{--<th>Name</th>--}}
                                                {{--<th>Position</th>--}}
                                                {{--<th>Office</th>--}}
                                                {{--<th>Age</th>--}}
                                                {{--<th>Start date</th>--}}
                                                {{--<th class="text-right">Actions</th>--}}
                                                {{--</tr>--}}
                                                {{--</tfoot>--}}
                                                <tbody id="ListAvailPaket">
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- end content-->
                                </div>
                                <!--  end card  -->
                            </div>
                            <!-- end col-md-12 -->
                        </div>
                        <!-- end row -->
                    </div>
                </div>

                {{--<div class="fileinput fileinput-new text-center" data-provides="fileinput">--}}
                {{--<div class="fileinput-new thumbnail">--}}
                {{--<img class="img" id="IM" src="{{ asset('images/image_placeholder.jpg') }}" alt="...">--}}
                {{--</div>--}}
                {{--<div class="fileinput-preview fileinput-exists thumbnail"></div>--}}
                {{--<div>--}}
                {{--<span class="btn btn-rose btn-round btn-file">--}}
                {{--<span class="fileinput-new">Select image</span>--}}
                {{--<span class="fileinput-exists">Change</span>--}}
                {{--<input type="file" name="Image" id="Uploads" class="Uploads" />--}}
                {{--</span>--}}
                {{--<a href="#RmImage" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Remove</a>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div id="ImageDoa">--}}

                {{--</div>--}}
            </form>
        </div>
    </div>

    <div class="card-footer">
        <div class="pull-right">
            <button type="submit" class="btn btn-fill btn-rose" id="assignDataPaket">Simpan</button>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
</script>