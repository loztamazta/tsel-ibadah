@extends('Dts::layouts.dts')

@section('title','Reguler')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <button class="btn btn-info rpt input-product">
				<span class="btn-label">
					<i class="material-icons">add</i>
				</span>
                Tambah Product
            </button>
        </div>
        <div class="col-md-3 pull-right">
            <div class="input-group">
                <div class="form-group label-floating">
                    <label class="control-label">
                        Search...
                    </label>
                    <input name="firstname" type="text" class="form-control">
                </div>
                <span class="input-group-addon">
			        <i class="material-icons">search</i>
			    </span>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-icon" data-background-color="purple">
                            <i class="material-icons">assignment</i>
                        </div>
                        <div class="card-content">
                            <h4 class="card-title"></h4>
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="material-datatables">
                                <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Nama Produk</th>
                                        {{--<th>Sumber</th>--}}


                                        <th>Harga Produk</th>
                                        <th class="disabled-sorting text-right">Tindakan</th>
                                    </tr>
                                    </thead>
                                    {{--<tfoot>--}}
                                    {{--<tr>--}}
                                    {{--<th>Name</th>--}}
                                    {{--<th>Position</th>--}}
                                    {{--<th>Office</th>--}}
                                    {{--<th>Age</th>--}}
                                    {{--<th>Start date</th>--}}
                                    {{--<th class="text-right">Actions</th>--}}
                                    {{--</tr>--}}
                                    {{--</tfoot>--}}
                                    <tbody id="ListProducts">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end content-->
                    </div>
                    <!--  end card  -->
                </div>
                <!-- end col-md-12 -->
            </div>
            <!-- end row -->
        </div>
    </div>

@endsection

@section('after-content')
    <div id='rp' class="slidePanel slidePanel-right">
    </div>
@endsection

@section('css')
    <link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
    <script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
    <script src="{{ asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
    <script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        GetListProduct()

        function GetListProduct()
        {
            console.log('Start')
            $.ajax({
                type: 'GET',
                url: '{{route('all-product-reguler')}}',
                global:false,
                success: function (data) {
                    var $AllProducts= $('#ListProducts');
                    $AllProducts.empty();
                    for (var i = 0; i < data.length; i++) {
                        $AllProducts.append('' +
                            '<tr>'+
                            '<td>'+data[i].product_name+'</td>'+

                            '<td>'+data[i].price+'</td>'+
                            '<td class="text-right">'+
//                            '<a href="#" class="btn btn-simple btn-info btn-icon like view-tips" data-id="'+data[i].id+'" ><i class="material-icons">art_track</i></a>'+
                            '<a href="#" class="btn btn-simple btn-warning btn-icon edit input-product" data-id="'+data[i].id+'" title="Edit" ><i class="material-icons">edit</i></a>'+
                            '<a href="#" class="btn btn-simple btn-danger btn-icon remove delete-product" data-id="'+data[i].id+'" ><i class="material-icons">close</i></a>'+
                            '</td>'+
                            '</tr>')
                    }
                }
            });
        }
        $('#rp').slideReveal({
            // trigger: $(".rpt"),
            position: "right",
            push: false,
            overlay: true,
            zIndex:1300,
        });
        $('#rp').delegate('.rp-close','click',function() {
            $('#rp').slideReveal("hide")
        })

        $(document).delegate('.input-product', 'click', function(e) {
            if($(this).attr('title') == 'Edit'){
                $('.loading').show()
                $.ajax({
                    type: 'GET',
                    url: '{{route('product-edit')}}',
                    data:{idProduct:$(this).data('id')},
                    global:false,
                    success: function (data) {
                        $('#rp').empty().load("{{ route('product-input') }}", function(){
                            CKEDITOR.replace('editor');
                            $('.card-content-overflow').perfectScrollbar();
                            $('.loading').hide()

//                            $('#SelectKota').on('change', function() {
//                                $('#selectRegion option[value='+data['0'].regionId+']').attr('selected','selected');
//                            });
                            var scr = 'storage/'+data['0'].Img;
                            $("input[name='save_type']").val('EditSave')
                            $("input[name='id']").val(data['0'].id)
                            $("input[name='product_name']").val(data['0'].product_name)
                            $("input[name='price']").val(data['0'].price)
                            $("select[name='sellerType']").val(data['0'].seller_type).attr('selected', true).change();
                            $("textarea.ckeditor").val(data['0'].description);
                            $('#rp').slideReveal("show")
                        })
                    }
                });

            }else{
                $('.loading').show()
                $('#rp').empty().load("{{ route('product-input') }}", function(){
                    CKEDITOR.replace('editor');
                    $('.card-content-overflow').perfectScrollbar();
                    $('.loading').hide()
                    $('#rp').slideReveal("show")
                })
            }

        })

        $(document).delegate('.delete-product', 'click', function() {
            data={
                id:$(this).data('id'),
            }
            $.post("{{route('delete-product') }}", data, function(r) {
                swal(
                    "Success",
                    "Success",
                    "success"
                ).then(function(){
                    location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                })
            }, 'json');
        });

        $(document).delegate('#saveProductData', 'click', function() {

            data={
                a:$("input[name='save_type").val(),
                id:$("input[name='id").val(),
                product_name:$("input[name='product_name").val(),
                price:$("input[name='price").val(),
                seller_type: $('#sellerType').find(":selected").val(),
                description:CKEDITOR.instances['editor'].getData(),
            }
            $.post("{{route('input-product') }}", data, function(r) {
                swal(
                    "Success",
                    "Success",
                    "success"
                ).then(function(){
                    location.reload();
//                    $('.List').DataTable().ajax.reload(null, false);
//                    $("#reset").click();
//                    $('#FormNews').find("input,textarea,select").val('').end();
                })
            }, 'json');
        });



    </script>

@endsection