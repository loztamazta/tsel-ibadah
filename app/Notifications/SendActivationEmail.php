<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendActivationEmail extends Notification implements ShouldQueue
{
    use Queueable;

    protected $token;
    protected $p;

    /**
     * Create a new notification instance.
     *
     * SendActivationEmail constructor.
     * @param $token
     * @param $user
     */
    public function __construct($token, $p)
    {
        $this->token = $token;
        $this->p = $p;
        $this->onQueue('social');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        // dd($this->user);
        $message = new MailMessage;
        $message->subject(trans('emails.activationSubject'))
            ->greeting(trans('emails.activationGreeting'))
            ->line(trans('emails.activationMessage').' Kata sandi kamu adalah "'.$this->p.'". Kamu dapat mengubah kata sandi di dalam aplikasi.')
            ->action(trans('emails.activationButton'), route('authenticated.activate', ['token' => $this->token]))
            ->line(trans('emails.activationThanks'));

        return ($message);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
