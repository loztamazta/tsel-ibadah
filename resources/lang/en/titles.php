<?php

return [

	'app'			=> 'Laravel',
	'app2'			=> 'Laravel Auth 2.0',
	'home'			=> 'Home',
	'login'			=> 'Masuk',
	'logout'		=> 'Keluar',
	'register'		=> 'Register',
	'resetPword'	=> 'Memasang kembali kata sandi',
	'toggleNav'		=> 'Toggle Navigation',
	'profile'		=> 'Profil',
	'editProfile'	=> 'Edit Profile',
	'createProfile'	=> 'Create Profile',

	'activation'	=> 'Registration Started  | Activation Required',
	'exceeded'		=> 'Activation Error',

	'editProfile'	=> 'Edit Profile',
	'createProfile'	=> 'Create Profile',
	'adminUserList'	=> 'Users Administration',
	'adminEditUsers'=> 'Edit Users',
	'adminNewUser'	=> 'Create New User',

	'adminThemesList' => 'Themes',
	'adminThemesAdd'  => 'Add New Theme',

	'adminLogs'		=> 'Log Files',
	'adminPHP'		=> 'PHP Information',
	'adminRoutes'	=> 'Routing Details',

];
