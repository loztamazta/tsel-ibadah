<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Kata sandi minimal 6 Karakter dan harus sama dengan Ulangi Kata sandi.',
    'reset' => 'Kata sandi Anda sudah diganti !',
    'sent' => 'Kami telah mengirimkan Aktifasi Lupa Kata sandi ke Email Anda',
    'token' => 'Kode Aktifasi sudah tidak Valid',
    'user' => "Email Anda tidak terdaftar, Mohon diperiksa kembali !",

];
