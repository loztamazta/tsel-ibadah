@extends('Dts::layouts.dts')

@section('h-title','Manajemen Pengguna')

@section('title','Manajemen Pengguna')

@section('content')
		<div class="row">
			<div class="col-sm-12">

				<div class="card">
					<div class="card-header card-header-icon" data-background-color="rose">
						<i class="material-icons">people</i>
					</div>
					<span class="card-header card-header-icon round btn btn-info btn-xs pull-right user-reg" data-id='0' data-background-color="blue">
						<i class="material-icons">add</i>
					</span>                    
					<div class="card-content">
						<h4 class="card-title">Daftar Pengguna</h4>
						<div class="table-responsive users-table">
							<table class="table table-striped data-table">
								<thead>
									<tr>
										<th class="text-right">ID</th>
										<th>Nama Pengguna</th>
										<th class="hidden-xs">Email</th>
										<th class="hidden-xs">Nama Depan</th>
										<th class="hidden-xs">Nama Belakang</th>
										<th>Role / Peran</th>
										<th class="hidden-sm hidden-xs hidden-md">Dibuat Pada</th>
										<th class="hidden-sm hidden-xs hidden-md">Diubah Pada</th>
										<th class="">Status</th>
										<th class="text-right">Tindakan</th>
									</tr>
								</thead>
								<tbody>
									@foreach($users as $user)
										<tr>
											<td class="text-right">{{$user->id}}</td>
											<td>{{$user->name}}</td>
											<td class="hidden-xs"><a href="mailto:{{ $user->email }}" title="email {{ $user->email }}">{{ $user->email }}</a></td>
											<td class="hidden-xs">{{$user->first_name}}</td>
											<td class="hidden-xs">{{$user->last_name}}</td>
											<td>
												@foreach ($user->roles as $user_role)
													@if ($user_role->name == 'Super Admin')
														@php $labelClass = 'success' @endphp
													@elseif ($user_role->name == 'Admin')
														@php $labelClass = 'info' @endphp
													@elseif ($user_role->name == 'Travel')
														@php $labelClass = 'warning' @endphp
													@else
														@php $labelClass = 'default' @endphp
													@endif
													<span class="label label-{{$labelClass}}">{{ $user_role->name }}</span>
												@endforeach
											</td>
											<td class="hidden-sm hidden-xs hidden-md">{{$user->created_at}}</td>
											<td class="hidden-sm hidden-xs hidden-md">{{$user->updated_at}}</td>
											<td>
												@if ($user->activated==0)
													<span class="label label-default">Belum Terverifikasi</span>													{{-- expr --}}
												@else
													<span class="label label-info">Terverifikasi</span>													{{-- expr --}}
												@endif
											</td>
											<td>
												<div class="row">
													<div class="col-sm-4">
														<a class="btn btn-default btn-simple" href="{{ URL::to('users/' . $user->id) }}" data-toggle="tooltip" title="Quick view">
															<i class="material-icons">art_track</i>'
														</a>
													</div>
													<div class="col-sm-4">
														<a class="btn btn-success btn-simple user-reg" data-id='{{ $user->id }}' href="" data-toggle="tooltip" title="Edit">
															<i class="material-icons">edit</i>
														</a>
													</div>
													<div class="col-sm-4">
														{!! Form::open(array('url' => 'users/' . $user->id, 'class' => '', 'data-toggle' => 'tooltip', 'title' => 'Delete')) !!}
															{!! Form::hidden('_method', 'DELETE') !!}
															{!! Form::button('<i class="material-icons">close</i>', array('class' => 'btn btn-danger btn-simple','type' => 'button', 'style' =>'width: 100%;' ,'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Hapus User', 'data-message' => 'Apakah anda yakin mau menghapus user ?')) !!}
														{!! Form::close() !!}
													</div>
												</div>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
		</div>

	@include('modals.modal-delete')

@endsection

@section('after-content')
	<div id='rp' class="slidePanel slidePanel-right">
	</div>
@endsection

@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">
	<style type="text/css" media="screen">
		.users-table {
			border: 0;
		}
		.users-table tr td:first-child {
			padding-left: 15px;
		}
		.users-table tr td:last-child {
			padding-right: 15px;
		}
		.users-table.table-responsive,
		.users-table.table-responsive table {
			margin-bottom: 0;
		}
	</style>
	<link href="{{ asset('vendor/slidePanel/slidePanel.css') }}" rel="stylesheet" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
@endsection

@section('js')
	<script src="{{ asset('mdp/js/jasny-bootstrap.min.js') }}"></script>
	<script src="{{asset('mdp/js/jquery.select-bootstrap.js') }}"></script>
	<script src="{{ asset('mdp/js/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
	<script src="{{ asset('vendor/additional-methods.js') }}"></script>
	<script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
	</script>

	@if (count($users) > 10)
		@include('scripts.datatables')
	@endif
	@include('scripts.delete-modal-script')
	@include('scripts.save-modal-script')
	{{--
		@include('scripts.tooltips')
	--}}

	{{-- right panel --}}
	<script src="{{ asset('vendor/slidereveal/jquery.slidereveal.min.js') }}"></script>
	<script>
		$('#rp').slideReveal({
			position: "right",
			push: false,
			overlay: true,
			zIndex:1300,
		});
		$('#rp').delegate('.rp-close','click',function() {
			$('#rp').slideReveal("hide")
		})

		$('.user-reg').click(function(e) {
			e.preventDefault()
			$('.loading').show()

			uid=$(this).data('id')
		   
			$('#rp').empty().load("{{ route('user-reg') }}", 'uid='+uid, function(){


				$('.card-content-overflow').perfectScrollbar();
				$('#rp').slideReveal("show")
				$('.loading').hide()
		   	})
		})
		
	</script>
@endsection