<div class="card">
	<div class="card-header card-header-icon" data-background-color="blue">
		<i class="material-icons">mode_edit</i>
	</div>
	<span class="btn card-header card-header-icon round pull-right rp-close" data-background-color="red">
		<i class="material-icons">close</i>
	</span>
	<div class="card-content">
		<h4 class="card-title">{{ $user ? 'Ubah pengguna : '.$user->name : 'Tambah Pengguna Baru' }}</h4>
		<div class="card-content-overflow">
			<form id="freg">
				<input type="hidden" name="id" data-id="{{ $user ? $user->id: 0 }}">
				<div class="form-group label-floating">
					<label class="control-label">Nama Pengguna</label>
					<input type="text" class="form-control" name="name" value="{{ $user ? $user->name : '' }}" {{ $user?'readonly':'' }}>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Email</label>
					<input type="text" class="form-control" name="email" value="{{ $user ? $user->email : '' }}" {{ $user?'readonly':'' }}>
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nama Depan</label>
					<input type="text" class="form-control" name="first_name" value="{{ $user ? $user->first_name : '' }}">
				</div>
				<div class="form-group label-floating">
					<label class="control-label">Nama Belakang</label>
					<input type="text" class="form-control" name="last_name" value="{{ $user ? $user->last_name : '' }}">
				</div>
				<div class="form-group label-floating">
					<label class="control-label">User role / Peran Pengguna</label>
					<select class="selectpicker" data-style="select-with-transition" name="r" id="role">
						@foreach($roles as $role)
							<option value="{{ $role->id }}" {{ ($user ? $user->r : 0) == $role->id ? 'selected="selected"' : '' }}>{{ $role->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group label-floating" id="tra" hidden>
					<label class="control-label">Agen Travel</label>
					<select class="selectpicker" data-style="select-with-transition" name="t" id="travel">
						<option value="0" disabled> Pilih agen perjalanan</option>
						@foreach($travels as $travel)
							<option value="{{ $travel->i }}" {{ ($user ? $user->t : 0) == $travel->i ? 'selected="selected"' : '' }}>{{ $travel->n }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group label-floating" hidden>
					<label class="control-label">Kata Sandi</label>
					<input type="password" class="form-control" name="password" id="p1">
				</div>
				<div class="form-group label-floating" hidden>
					<label class="control-label">Konfirmasi Kata Sandi</label>
					<input type="password" class="form-control" name="password_confirmation" id="p2">
				</div>
			</form>
		</div>
	</div>
	<div class="card-footer">
		<div class="pull-right">
			<button id="sfreg" type="submit" class="btn btn-fill btn-rose">Simpan</button>
		</div>				
	</div>
</div>

<script>
	$('.selectpicker').selectpicker();

	$('#role').change(function(e) {
		e.preventDefault()
		// console.log($(this).val())
		// if role == travel
		if ($(this).val()==2) {
			$('#tra').slideDown()
		} else {
			$('#tra').slideUp()
		}
	})
	$('#role').change()

	$('#sfreg').click(function(e) {
		e.preventDefault()
        var $validator = $('.card-content-overflow form').validate({
            rules: {
                name: {
                    required: true,
                    contentSpecialCharacters: true
                },
                first_name: {
                    required: true,
                    contentSpecialCharacters: true
                },
                last_name: {
                    required: true,
                    contentSpecialCharacters: true
                },
                email: {
                    required: true,
                    email5: true
                },

            },
            messages: {
                name: "Wajib diisi / Spesial Karakter Tidak Diperbolehkan",

            },
            errorPlacement: function(error, element) {
                $(element).parent('div').addClass('has-error');
                $(element).parent('div').append(error);

            }

        });
        var $valid = $('.card-content-overflow form').valid();
        if(!$valid) {
            $validator.focusInvalid();
            return false;
        }
        swal({
            title: 'Apakah Anda Yakin?',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Batal'
        }).then(function () {
        swal('Mohon Menunggu')
        swal.showLoading()

        uploadnupdate()

        if ($('input[name=id]').data('id')==0) {
	        $('#p1').val(Math.random().toString(36).substring(5))
	        $('#p2').val($('#p1').val())
			data=$("#freg").serialize();
		    // console.log('data',data)
		    saveusr(data)
        } else {
			data=$("#freg").serialize();
		    editusr(data)
        }


	})

	function saveusr(data) {
		$.post("{{ url('register') }}", data, function(r) {
	        console.log(r.status)
	        if(r.status!='ok'){
	            swal(
	                'Terjadi Kesalahan',
	                'Mohon Dicek Kembali',
	                'error'
	            )
	        } else {
		        swal(
		            'Berhasil',
		            '',
		            'success'
		        ).then(function() {
		            $('#rp').empty().slideReveal("hide")
		            location.reload(true)
		        })
	        }
	    }, 'json').fail(function(response) {
	        // console.log(response)
	        swal(
	            'Terjadi kesalahan! ',
	            //response.responseText,
				'Mohon Dicek Kembali',
	            'error'
	        )
	    }); 
	}

	function editusr(data) {
		var url='{{ route('users.update', ":usr") }}'
		url = url.replace(':usr', '{{ $user?$user->id:0 }}')
		$.ajax({
		    url: url,
		    type: 'PUT',
		    data:data,
		    success: function(r) {
		        console.log(r)
		        if(r!='ok'){
		            swal(
		                'Terjadi kesalahan!',
		                'Mohon Dicek Kembali.',
		                'error'
		            )
		        } else {
			        swal(
			            'Berhasil',
			            '',
			            'success'
			        ).then(function() {
		            	$('#rp').empty().slideReveal("hide")
		           		location.reload(true)
			        })
		        }
		    },				    
		}).fail(function(response){
			console.log(response)
	        swal(
	            'Terjadi Kesalahan!',
	            //response.responseText,
				'Mohon Dicek Kembali',
	            'error'
	        )			    	
		})
	}

	function uploadnupdate() {
        swal('Mohon Menunggu')
        swal.showLoading()

        old1 = $("#img").data('old')
        var form_data = new FormData();
        form_data.append("file", $("#img").prop("files")[0])
        form_data.append("dir", 'avatar')
        form_data.append("w", 800)
        form_data.append("h", 800)

        $.ajax({
            url: "{{route('cms-upload') }}",
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (p) {
                if (!p.p1) {
                    if (old1 != p.p1) {
                        p.p1 = old1;
                    }
                }
                data = {
                    idp: $('input[name=id]').data('id'),
                    p1: p.p1
                }
                $.post("{{route('update-profile') }}", data, function (r) {
                }, 'json').fail(function (response) {
                    console.log(response)
                    swal(
                        'Terjadi kesalahan!',
                        //response.responseText,
						'Mohon Dicek Kembali.',
                        'error'
                    )
                });
            }
        })
    }
	})

</script>