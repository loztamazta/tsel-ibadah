@extends('Dts::layouts.dts')

@section('h-title')
  {{ $user->name }}
@endsection

@section('title')
  Tampilkan Pengguna {{ $user->name }}
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-profile">
					<div class="card-content">
						@if ($user->id == Auth::user()->id)
							<div class="fileinput fileinput-new text-center pull-right" data-provides="fileinput">
								<div class="fileinput-new thumbnail img-circle">
									<img src="{{ $user->profile ? asset('storage/'.$user->profile->avatar) : asset('images/placeholder.jpg') }}">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail img-circle"></div>
								<div>
									<span class="btn btn-rose btn-round btn-file">
									<span class="fileinput-new">Pilih gambar</span>
									<span class="fileinput-exists">Ganti</span>
										<input type="file" name="img" id="img" data-old="{{ $user->profile ? asset('storage/'.$user->profile->avatar) : '' }}" />
									</span>
									<a href="#pablo" class="btn btn-danger btn-round fileinput-exists" data-dismiss="fileinput"><i class="fa fa-times"></i> Hapus</a>
								</div>
							</div>
						@endif
						@if ($user->id != Auth::user()->id)
							<div class="fileinput fileinput-new text-center pull-right" data-provides="fileinput">
								<div class="fileinput-new thumbnail img-circle">
									<img src="{{ $user->profile ? asset('storage/'.$user->profile->avatar) : asset('images/placeholder.jpg') }}">
								</div>
							</div>
						@endif

						<a href="{{ url()->previous() }}" class="btn btn-sm btn-info">
							<i class="fa fa-chevron-left fa-fw" aria-hidden="true"></i>&nbsp;
							<span class="hidden-xs hidden-sm"> Kembali</span>
						</a>
						<h4 class="card-title">Informasi Pengguna</h4>
						<form id="f-pw">
							<div class="row">
								<div class="col-md-3">
									<div class="form-group label-floating">
										@php
											if ($user->activated == 1) {
												$status='Terverifikasi';
											}else{
												$status='Belum Terverifikasi';
											}
										@endphp
										<label class="control-label">Status</label>
										<input type="text" class="form-control" value="{{ $status }}" readonly>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group label-floating">
										<label class="control-label">User role / Peran Pengguna</label>
										<input type="text" class="form-control" value="{{ $user->roles[0]->name }}" readonly>
										<input type="text" name='role' value="{{ $user->roles[0]->id }}" hidden>
									</div>
								</div>
								@if ($user->roles[0]->id == 2)
									@php
									    $agen=DB::select('SELECT KbihName FROM db_ibadah.mt_kbih where IdKbih=?',[$user->t])[0];
									@endphp
									<div class="col-md-3">
										<div class="form-group label-floating">
											<label class="control-label">Agen Travel</label>
											<input type="text" class="form-control" value="{{ $agen->KbihName }}" readonly>
										</div>
									</div>
								@endif
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Nama Pengguna</label>
										<input type="text" class="form-control" name="name" value="{{ $user->name }}" readonly>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Email</label>
										<input type="text" class="form-control" name="email" value="{{ $user->email }}" readonly>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Nama Depan</label>
										<input type="text" class="form-control" value="{{ $user->first_name }}" readonly>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group label-floating">
										<label class="control-label">Nama Belakang</label>
										<input type="text" class="form-control" value="{{ $user->last_name }}" readonly>
									</div>
								</div>
							</div>

							<div class="clearfix"></div>

							@if ($user->id == Auth::user()->id)
								<div class="row">
									<div id="pw" hidden>
										<div class="col-md-3">
											<div class="form-group label-floating" >
												<label class="control-label">Kata Sandi</label>
												<input type="password" class="form-control" name="password" id="p1">
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group label-floating" >
												<label class="control-label">Konfirmasi Kata Sandi</label>
												<input type="password" class="form-control" name="password_confirmation" id="p2">
											</div>
										</div>
									</div>
								</div>

								<span class="btn btn-sm btn-warning" id="chg-pw">
									<i class="fa fa-fw fa-lock"></i> &nbsp;
									<span class="txt-ganti"> Ganti Kata Sandi</span>
								</span>
								<span class="btn btn-sm btn-info" id="save-pw" style="display: none;">
									<i class="fa fa-fw fa-check"></i> &nbsp;
									<span class=""> Simpan Kata Sandi</span>
								</span>
							@endif
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('js')
	<script src="{{ asset('mdp/js/sweetalert2.js') }}"></script>
	<script>
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$('#chg-pw').click(function(e){
			e.preventDefault()
		    $(this).find('.fa').toggleClass('fa-times');
		    $(this).find('.fa').toggleClass('fa-lock');
			$('#pw').slideToggle(function(){
				if($('#pw').is(':hidden')){
				  $('.txt-ganti').html('Ganti Kata Sandi')
				}else{
				  $('.txt-ganti').html('Batal')
				}

				$('#save-pw').slideToggle('fast')
			})
		})

		$('#save-pw').click(function(e){
			e.preventDefault()

	        swal({
                title: 'Apakah Anda Yakin?',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Batal'
	        }).then(function() {
                swal('Mohon Menunggu')
                swal.showLoading()

				var url='{{ route('users.update', ":usr") }}'
				url = url.replace(':usr', '{{ $user->id }}')
				var data= $('#f-pw').serialize()
				$.ajax({
				    url: url,
				    type: 'PUT',
				    data:data,
				    success: function(r) {
				        console.log(r)
				        if(r!='ok'){
				            swal(
				                'Terjadi kesalahan!',
				                'Mohon Dicek Kembali.',
				                'error'
				            )
				        } else {
					        swal(
					            'Berhasil',
					            '',
					            'success'
					        ).then(function() {
					        })
				        }
				    },				    
				}).fail(function(response){
					console.log(response)
					errtext=''
					for(i in response.responseJSON.password){
						errtext+=(response.responseJSON.password[i]=="The password confirmation does not match."?"Konfirmasi kata sandi tidak sama. ":"")
						errtext+=(response.responseJSON.password[i]=="The password must be at least 6 characters."?"Kata sandi minimal 6 karakter. ":"")
					}
			        swal(
			            'Terjadi kesalahan!',
			            errtext,
			            'error'
			        )			    	
				})
      		})


		})

		$('#img').change(function() {
	        swal('Mohon Menunggu')
	        swal.showLoading()

	        old1 = $("#img").data('old')
	        var form_data = new FormData();
	        form_data.append("file", $("#img").prop("files")[0])
	        form_data.append("dir", 'avatar')
	        form_data.append("w", 800)
	        form_data.append("h", 800)

	        $.ajax({
	            url: "{{route('cms-upload') }}",
	            cache: false,
	            contentType: false,
	            processData: false,
	            data: form_data,
	            type: 'post',
	            success: function (p) {
	                if (!p.p1) {
	                    if (old1 != p.p1) {
	                        p.p1 = old1;
	                    }
	                }
	                data = {
	                    idp: "{{ $user->id }}",
	                    p1: p.p1
	                }
	                $.post("{{route('update-profile') }}", data, function (r) {
				        swal(
				            'Berhasil',
				            '',
				            'success'
				        ).then(function() {
			            	// $('#rp').empty().slideReveal("hide")
			           		location.reload(true)
				        })
	                	
	                }, 'json').fail(function (response) {
	                    console.log(response)
	                    swal(
	                        'Terjadi kesalahan!',
	                        //response.responseText,
                            'Mohon Dicek Kembali.',
	                        'error'
	                    )
	                });
	            }
	        })
		})
		
	</script>

@endsection
