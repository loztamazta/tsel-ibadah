@extends('Dts::layouts.land')

@section('h-title')
	{{ Lang::get('titles.activation') }}
@endsection

@section('content')
    <div class="content">
        <div class="container">
            <div class="row">
			<div class="col-md-10 col-md-offset-1">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="card card-login card-hidden">
                            <div class="card-header text-center" data-background-color="rose">
                                <h4 class="card-title">{{ Lang::get('titles.activation') }}</h4>
                            </div>
                            <div class="card-content">
								<p>{{ Lang::get('auth.regThanks') }}</p>
								<p>{{ Lang::get('auth.anEmailWasSent',['email' => $email, 'date' => $date ] ) }}</p>
								<p>{{ Lang::get('auth.clickInEmail') }}</p>
                            </div>
                            <div class="footer text-center">
								<p><a href='/activation' class="btn btn-primary">{{ Lang::get('auth.clickHereResend') }}</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection

