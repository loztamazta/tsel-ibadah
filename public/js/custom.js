$(function () {
	var url = window.location;
	$('.sidebar-wrapper a[href="' + url + '"]').parent('li').addClass('current-page active');
	$('.sidebar-wrapper a').filter(function () {
		return this.href == url;
	}).parent('li').addClass('current-page active')
	.parents('div').addClass('collapse in')
	.parents('li').addClass('active');
	// }).parent('li').addClass('current-page active').parents('ul').parent().addClass('active').parent('ul').parent().addClass('active');

});
